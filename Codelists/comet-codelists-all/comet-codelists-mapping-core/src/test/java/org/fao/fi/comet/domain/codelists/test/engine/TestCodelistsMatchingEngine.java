/**
 * (c) 2013 FAO / UN (project: comet-codelists)
 */
package org.fao.fi.comet.domain.codelists.test.engine;

import org.fao.fi.comet.core.engine.process.handlers.impl.SilentMatchingProcessHandler;
import org.fao.fi.comet.core.model.engine.MatchingEngineProcessConfiguration;
import org.fao.fi.comet.core.model.engine.MatchingEngineProcessResult;
import org.fao.fi.comet.core.model.matchlets.MatchletConfiguration;
import org.fao.fi.comet.core.patterns.data.partitioners.impl.IdentityDataPartitioner;
import org.fao.fi.comet.domain.codelists.engine.CodelistsMatchingEngine;
import org.fao.fi.comet.domain.codelists.model.CodelistData;
import org.fao.fi.comet.domain.codelists.patterns.data.providers.impl.MaterializedVirtualRepoCodelistProvider;
import org.fao.fi.comet.domain.codelists.patterns.handlers.id.CodelistIDHandler;
import org.fao.fi.sh.utility.core.helpers.singletons.io.JAXBHelper;
import org.junit.Test;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 3 Jul 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 3 Jul 2013
 */
public class TestCodelistsMatchingEngine {
	@Test
	public void testExactMatching() throws Throwable {
		CodelistsMatchingEngine engine = new CodelistsMatchingEngine(1);
		
		MatchingEngineProcessConfiguration conf = new MatchingEngineProcessConfiguration();
		conf.setMaxCandidatesPerEntry(1);
		conf.setMinimumAllowedWeightedScore(.1);
		conf.setHaltAtFirstValidMatching(true);
		conf.setHandleErrors(true);
		
		MatchletConfiguration mConf = new MatchletConfiguration("AuthoritativeEqualityMatchlet").
										 with("weight", 100).
										 withFalse("isOptional").
										 with("sourceColumnName", "Name (en)").
										 with("targetColumnName", "Name (en)");
		
		conf.getMatchletsConfigurations().add(mConf);
		
		MatchingEngineProcessResult<CodelistData, CodelistData, MatchingEngineProcessConfiguration> result = null;
		
		result = engine.compareAll(conf, 
//								   new SimpleGUIMatchingProcessHandler<CodelistData, CodelistData>(), 
								   new SilentMatchingProcessHandler<CodelistData, CodelistData>(),
								   new MaterializedVirtualRepoCodelistProvider("rtms-13001-22"), 
								   new IdentityDataPartitioner<CodelistData, CodelistData>(),
								   new MaterializedVirtualRepoCodelistProvider("rtms-13001-21"),
								   new CodelistIDHandler("ISO_2_CODE"),
								   new CodelistIDHandler("ISO_3_CODE"));
		
		System.out.println(
			JAXBHelper.toXML(new Class[] {
				CodelistData.class
			}, result)
		);
	}

	@Test
	public void testFuzzyMatching() throws Throwable {
		CodelistsMatchingEngine engine = new CodelistsMatchingEngine(1);
		
		MatchingEngineProcessConfiguration conf = new MatchingEngineProcessConfiguration();
		conf.setMaxCandidatesPerEntry(3);
		conf.setMinimumAllowedWeightedScore(.89);
		conf.setHaltAtFirstValidMatching(false);
		
		MatchletConfiguration mConf = new MatchletConfiguration("FuzzyStringMatchlet").
																with("weight", 100).
																withFalse("isOptional").
																with("soundexWeight", 0).
																with("minimumScore", .5).
																with("sourceColumnName", "Name (fr)").
																with("targetColumnName", "Name (en)");
		
		conf.getMatchletsConfigurations().add(mConf);
		
		mConf = new MatchletConfiguration("FuzzyStringMatchlet").
										  with("weight", 50).
										  withFalse("isOptional").
										  with("soundexWeight", 0).
										  with("minimumScore", .7).
										  with("sourceColumnName", "Name (es)").
										  with("targetColumnName", "Name (en)");
		
		conf.getMatchletsConfigurations().add(mConf);
		
		MatchingEngineProcessResult<CodelistData, CodelistData, MatchingEngineProcessConfiguration> result = null;
		
		result = engine.compareAll(conf, 
//								   new SimpleGUIMatchingProcessHandler<CodelistData, CodelistData>(), 
								   new SilentMatchingProcessHandler<CodelistData, CodelistData>(),
								   new MaterializedVirtualRepoCodelistProvider("rtms-13001-22"), 
								   new IdentityDataPartitioner<CodelistData, CodelistData>(),
								   new MaterializedVirtualRepoCodelistProvider("rtms-13001-21"),
								   new CodelistIDHandler("ISO_2_CODE"),
								   new CodelistIDHandler("ISO_3_CODE"));
		
		System.out.println(
			JAXBHelper.toXML(new Class[] {
				CodelistData.class
			}, result)
		);
	}
}
