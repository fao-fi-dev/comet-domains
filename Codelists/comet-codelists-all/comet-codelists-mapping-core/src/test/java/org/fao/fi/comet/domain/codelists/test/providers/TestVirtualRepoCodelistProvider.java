/**
 * (c) 2013 FAO / UN (project: comet-codelists)
 */
package org.fao.fi.comet.domain.codelists.test.providers;

import org.fao.fi.comet.core.patterns.data.providers.DataProvider;
import org.fao.fi.comet.core.patterns.data.providers.ProvidedData;
import org.fao.fi.comet.domain.codelists.model.CodelistData;
import org.fao.fi.comet.domain.codelists.patterns.data.providers.impl.StreamingVirtualRepoCodelistProvider;
import org.fao.fi.sh.utility.core.helpers.singletons.io.JAXBHelper;
import org.junit.Test;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 3 Jul 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 3 Jul 2013
 */
public class TestVirtualRepoCodelistProvider {
	@Test
	public void testRetrieve() throws Throwable {
		DataProvider<CodelistData> provider = new StreamingVirtualRepoCodelistProvider("rtms-11002-23");

		for(ProvidedData<CodelistData> data : provider) {
			System.out.println(JAXBHelper.toXML(data.getData(), JAXBHelper.OMIT_XML_DECLARATION));
		}
	}
}
