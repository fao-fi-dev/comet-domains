/**
 * (c) 2013 FAO / UN (project: comet-core)
 */
package org.fao.fi.comet.domain.codelists.test.engine;

import java.util.Collection;

import org.fao.fi.comet.core.engine.process.MatchingEngineMetadataResolver;
import org.fao.fi.comet.core.model.matchlets.support.MatchletInfo;
import org.fao.fi.comet.domain.codelists.matchlets.FuzzyStringMatchlet;
import org.fao.fi.comet.domain.codelists.model.matchlets.CodelistDataScalarMatchlet;
import org.junit.Assert;
import org.junit.Test;


/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 4 Jul 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 4 Jul 2013
 */
public class TestMatchletsConfiguration {
	@Test
	public void testGetCodelistDataMatchersInfo() {
		Collection<MatchletInfo> matchletsInfo = new MatchingEngineMetadataResolver().getAvailableMatchletsInfo(CodelistDataScalarMatchlet.class);
		
		MatchletInfo stringMatchletInfo = null;
		
		for(MatchletInfo info : matchletsInfo) {
			if(info.getMatchletType().equals(FuzzyStringMatchlet.class.getName())) {
				stringMatchletInfo = info;
			}
		}
		
		Assert.assertNotNull("Unable to identify StringMatchlet info", stringMatchletInfo);
		Assert.assertEquals("java.lang.String", stringMatchletInfo.getExtractedSourceDataType());
		Assert.assertEquals("java.lang.String", stringMatchletInfo.getExtractedTargetDataType());
	}
}
