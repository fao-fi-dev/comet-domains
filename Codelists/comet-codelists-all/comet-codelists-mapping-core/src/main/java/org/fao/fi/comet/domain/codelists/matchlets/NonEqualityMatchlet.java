/**
 * (c) 2013 FAO / UN (project: comet-codelists)
 */
package org.fao.fi.comet.domain.codelists.matchlets;

import java.io.Serializable;

import org.fao.fi.comet.core.model.engine.DataIdentifier;
import org.fao.fi.comet.core.model.score.support.MatchingScore;
import org.fao.fi.comet.domain.codelists.model.CodelistData;


/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 3 Jul 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 3 Jul 2013
 */
public class NonEqualityMatchlet<SOURCE_DATA extends Serializable, TARGET_DATA extends Serializable> extends EqualityMatchlet<SOURCE_DATA, TARGET_DATA> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 3512360792908053689L;
	static public final String NAME = "NonEqualityMatchlet";
	
	/**
	 * Class constructor
	 *
	 */
	public NonEqualityMatchlet() {
		super();
		
		this._name = NAME;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.model.matchers.Matcher#getDescription()
	 */
	@Override
	public String getDescription() {
		return "Performs a non-authoritative non-equality match of the extracted data";
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.model.matchers.Matcher#computeScore(java.lang.Object, org.fao.fi.comet.core.model.engine.DataIdentifier, java.lang.Object, java.lang.Object, org.fao.fi.comet.core.model.engine.DataIdentifier, java.lang.Object)
	 */
	@Override
	public MatchingScore computeScore(CodelistData source, DataIdentifier sourceIdentifier, SOURCE_DATA sourceData, CodelistData target, DataIdentifier targetIdentifier, TARGET_DATA targetData) {
		return super.computeScore(source, sourceIdentifier, sourceData, target, targetIdentifier, targetData).reverse();
	}
}