/**
 * (c) 2013 FAO / UN (project: comet-codelists)
 */
package org.fao.fi.comet.domain.codelists.matchlets;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.HashMap;
import java.util.Map;

import org.fao.fi.comet.core.model.engine.DataIdentifier;
import org.fao.fi.comet.core.model.matchlets.annotations.MatchletParameter;
import org.fao.fi.comet.core.model.matchlets.annotations.parameters.DoubleRange;
import org.fao.fi.comet.core.model.score.support.MatchingScore;
import org.fao.fi.comet.core.model.score.support.MatchingType;
import org.fao.fi.comet.domain.codelists.model.CodelistData;
import org.fao.fi.comet.domain.codelists.model.matchlets.CodelistDataScalarMatchletSkeleton;
import org.fao.fi.sh.utility.common.helpers.singletons.text.StringDistanceHelper;
import org.fao.fi.sh.utility.lexical.processors.LexicalProcessorsQueue;
import org.fao.fi.sh.utility.lexical.processors.extras.queue.impl.ICUNameSimplifierProcessor;
import org.fao.fi.sh.utility.lexical.soundex.PhraseSoundexGenerator;
import org.fao.fi.sh.utility.lexical.soundex.impl.ExtendedPhraseSoundexGenerator;

import static org.fao.fi.sh.utility.core.helpers.singletons.lang.AssertionHelper.*;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 3 Jul 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 3 Jul 2013
 */
public class FuzzyStringMatchlet extends CodelistDataScalarMatchletSkeleton<String, String> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 3512360792908053689L;
	static public final String NAME = "FuzzyStringMatchlet";
	static final public String SOUNDEX_WEIGHT_PARAM = "soundexWeight";

	transient private LexicalProcessorsQueue _simplifier = new ICUNameSimplifierProcessor();
	transient private PhraseSoundexGenerator _soundexer = new ExtendedPhraseSoundexGenerator();

	@MatchletParameter(name=SOUNDEX_WEIGHT_PARAM,
					  description="Sets the weight of the soundex distance (with respect to the string distance) in the computation of the matching score. " +
					  			  "Valid values fall in the range [0.0, 1.0] with 0.0 meaning that the soundex distance will not be considered when computing the score " +
					  			  "computation and 1.0 meaning that only the soundex distance will be considered when computing the matching score.")
	@DoubleRange(from=0, to=1, includeFrom=true, includeTo=true)
	protected double _soundexWeight;

	private Map<String, String> SIMPLIFIED_STRING_MAP = new HashMap<String, String>();
	private Map<String, String> SOUNDEX_STRING_MAP = new HashMap<String, String>();
	private Map<String, String[]> SOUNDEX_PARTS_MAP = new HashMap<String, String[]>();

	/**
	 * Class constructor
	 *
	 */
	public FuzzyStringMatchlet() {
		super();

		this._name = NAME;
	}

	/**
	 * Class constructor
	 *
	 * @param soundexWeight
	 */
	public FuzzyStringMatchlet(double soundexWeight) {
		this();

		$gte(soundexWeight, MatchingScore.SCORE_NO_MATCH, "Soundex weight ({}) must be greater than or equal to {}", soundexWeight, MatchingScore.SCORE_NO_MATCH);
		$lte(soundexWeight, MatchingScore.SCORE_FULL_MATCH, "Soundex weight ({}) must be lower than or equal to {}", soundexWeight, MatchingScore.SCORE_FULL_MATCH);

		this._soundexWeight = soundexWeight;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.model.matchers.Matcher#getDescription()
	 */
	@Override
	public String getDescription() {
		return "Performs a fuzzy string match of the extracted data";
	}

	private String getSimplifiedData(String original) {
		if(original == null)
			return null;

		String simplified = this.SIMPLIFIED_STRING_MAP.get(original);

		if(simplified == null) {
			if(this.SIMPLIFIED_STRING_MAP.containsKey(original))
				return null;

			simplified = this._simplifier.process(original);

			this.SIMPLIFIED_STRING_MAP.put(original, simplified);
		}

		return simplified;
	}

	private String getSimplifiedDataSoundex(String simplified) {
		if(simplified == null || simplified.length() == 0)
			return null;

		String soundex = this.SOUNDEX_STRING_MAP.get(simplified);

		if(soundex == null) {
			if(this.SOUNDEX_STRING_MAP.containsKey(simplified))
				return null;

			soundex = this._soundexer.getFullPhraseSoundex(simplified);

			this.SOUNDEX_STRING_MAP.put(simplified, soundex);
		}

		return soundex;
	}

	private String[] getSimplifiedDataSoundexParts(String soundex) {
		if(soundex == null || soundex.length() == 0)
			return new String[0];

		String[] parts = this.SOUNDEX_PARTS_MAP.get(soundex);

		if(parts == null) {
			parts = soundex.split("\\s");

			this.SOUNDEX_PARTS_MAP.put(soundex, parts);
		}

		return parts;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.model.matchers.Matcher#computeScore(java.lang.Object, org.fao.fi.comet.core.model.engine.DataIdentifier, java.lang.Object, java.lang.Object, org.fao.fi.comet.core.model.engine.DataIdentifier, java.lang.Object)
	 */
	@Override
	public MatchingScore computeScore(CodelistData source, DataIdentifier sourceIdentifier, String sourceData, CodelistData target, DataIdentifier targetIdentifier, String targetData) {
		double nameWeight = MatchingScore.SCORE_FULL_MATCH - this._soundexWeight;
		boolean checkNames = Double.compare(nameWeight, MatchingScore.SCORE_NO_MATCH) > 0;
		boolean checkSoundex = Double.compare(this._soundexWeight, MatchingScore.SCORE_NO_MATCH) > 0;

		boolean onlySoundex = Double.compare(this._soundexWeight, MatchingScore.SCORE_FULL_MATCH) == 0;
		boolean onlyName = Double.compare(nameWeight, MatchingScore.SCORE_FULL_MATCH) == 0;

		//This should never happen...
		$true(!onlySoundex || !onlyName, "You cannot check only for soundex and string at the same time");

		double currentScore, nameScore, simplifiedNameScore, soundexScore;

		currentScore = nameScore = simplifiedNameScore = soundexScore = MatchingScore.SCORE_NO_MATCH; //MatchingScore.getNonAuthoritativeNoMatchTemplate();

		String simplifiedSourceData = this.getSimplifiedData(sourceData);
		String simplifiedTargetData = this.getSimplifiedData(targetData);

		if(checkNames) {
			if(sourceData.equalsIgnoreCase(targetData))
				return MatchingScore.getNonAuthoritativeFullMatchTemplate();

//			MatchingScore nameScore = new MatchingScore(StringDistanceHelper.computeRelativeSimilarity(sourceName, targetName), MatchingType.NON_AUTHORITATIVE);
			nameScore = StringDistanceHelper.computeRelativeSimilarity(sourceData, targetData);

			if(simplifiedSourceData.equals(simplifiedTargetData))
				return MatchingScore.getNonAuthoritativeFullMatchTemplate();

//			MatchingScore simplifiedNameScore = new MatchingScore(StringDistanceHelper.computeRelativeSimilarity(simplifiedSourceName, simplifiedTargetName), MatchingType.NON_AUTHORITATIVE);
			simplifiedNameScore = StringDistanceHelper.computeRelativeSimilarity(simplifiedSourceData, simplifiedTargetData);

			currentScore = Double.compare(nameScore, simplifiedNameScore) < 0 ? simplifiedNameScore : nameScore;
		}

		if(checkSoundex) {
			String sourceDataSoundex = this.getSimplifiedDataSoundex(simplifiedSourceData);
			String targetDataSoundex = this.getSimplifiedDataSoundex(simplifiedTargetData);

			String[] sourceDataSoundexParts = this.getSimplifiedDataSoundexParts(sourceDataSoundex);
			String[] targetDataSoundexParts = this.getSimplifiedDataSoundexParts(targetDataSoundex);

			int sourceDataSoundexPartsSize = sourceDataSoundexParts == null ? 0 : sourceDataSoundexParts.length;
			int targetDataSoundexPartsSize = targetDataSoundexParts == null ? 0 : targetDataSoundexParts.length;

			/* SLOWER, MORE ACCURATE VERSION */
			final boolean switchFirstAndSecond = sourceDataSoundexPartsSize < targetDataSoundexPartsSize;
			int partsNum = 0;

			double total, current, max;
			total = current = max = 0D;

			double factor;

			for(String fPart : switchFirstAndSecond ? targetDataSoundexParts : sourceDataSoundexParts) {
				for(String sPart : switchFirstAndSecond ? sourceDataSoundexParts : targetDataSoundexParts) {
					current = StringDistanceHelper.computeRelativeSimilarity(fPart, sPart);

					factor = ( 1.0 * ( switchFirstAndSecond ? fPart.length() : sPart.length() ) ) / Math.max(sPart.length(), fPart.length());

					$gte(factor, 0.0, "The soundex parts factor must be greater than (or equal to) 0.0 (currently: {})", factor);
					$lte(factor, 1.0, "The soundex parts factor must be lower than (or equal to) 1.0 (currently: {})", factor);

					current *= factor;

					if(current > max) {
						max = current;
					}
				}

				total += max;
				max = 0D;
			}

			partsNum = Math.max(sourceDataSoundexPartsSize, targetDataSoundexPartsSize);
			total /= partsNum == 0 ? 1D : partsNum;

			soundexScore = total;
		}

		if(onlyName)
			currentScore = simplifiedNameScore;
		else if(onlySoundex)
			currentScore = soundexScore;
		else
			currentScore = nameWeight * simplifiedNameScore + this._soundexWeight * soundexScore;

		return new MatchingScore(currentScore, MatchingType.NON_AUTHORITATIVE);
	}

	private void readObject(ObjectInputStream stream) throws IOException, ClassNotFoundException {
		stream.defaultReadObject();

		this._simplifier = new ICUNameSimplifierProcessor();
		this._soundexer = new ExtendedPhraseSoundexGenerator();
	}
}