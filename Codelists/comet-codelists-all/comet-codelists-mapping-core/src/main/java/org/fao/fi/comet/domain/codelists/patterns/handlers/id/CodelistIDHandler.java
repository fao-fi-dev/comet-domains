/**
 * (c) 2013 FAO / UN (project: comet-codelists)
 */
package org.fao.fi.comet.domain.codelists.patterns.handlers.id;

import org.fao.fi.comet.core.patterns.handlers.id.impl.IDHandlerSkeleton;
import org.fao.fi.comet.domain.codelists.model.CodelistData;
import org.fao.fi.sh.utility.core.helpers.singletons.text.StringsHelper;

import static org.fao.fi.sh.utility.core.helpers.singletons.lang.AssertionHelper.*;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 3 Jul 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 3 Jul 2013
 */
public class CodelistIDHandler extends IDHandlerSkeleton<CodelistData, String> {
	private String _idColumnName;

	/**
	 * Class constructor
	 *
	 * @param idColumnName
	 */
	public CodelistIDHandler(String idColumnName) {
		idColumnName = StringsHelper.trim(idColumnName);

		$nN(idColumnName, "The code column name cannot be null");

		this._idColumnName = idColumnName;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.patterns.handlers.id.impl.IDHandlerSkeleton#doGetId(java.lang.Object)
	 */
	@Override
	protected String doGetId(CodelistData data) {
		$true(data.hasColumn(this._idColumnName), "Unable to retrieve code column '{}' for data {}", this._idColumnName, data);
		$nN(data.getColumn(this._idColumnName), "The code column '{}' is NULL for data {}", this._idColumnName, data);

		return data.getColumn(this._idColumnName).toString();
	}
}
