/**
 * (c) 2013 FAO / UN (project: comet-codelists)
 */
package org.fao.fi.comet.domain.codelists.matchlets;

import org.fao.fi.comet.core.model.engine.DataIdentifier;
import org.fao.fi.comet.core.model.score.support.MatchingScore;
import org.fao.fi.comet.core.model.score.support.MatchingType;
import org.fao.fi.comet.domain.codelists.model.CodelistData;
import org.fao.fi.comet.domain.codelists.model.matchlets.CodelistDataScalarMatchletSkeleton;


/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 3 Jul 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 3 Jul 2013
 */
public class RelativeNumberMatchlet<SOURCE_DATA extends Number, TARGET_DATA extends Number> extends CodelistDataScalarMatchletSkeleton<SOURCE_DATA, TARGET_DATA> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 3512360792908053689L;
	static public final String NAME = "RelativeNumberMatchlet";
	
	/**
	 * Class constructor
	 *
	 */
	public RelativeNumberMatchlet() {
		super();
		
		this._name = NAME;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.model.matchers.Matcher#getDescription()
	 */
	@Override
	public String getDescription() {
		return "Performs a magnitude check between the extracted numbers, returning the relative value of the minimum number by the maximum number. If the numbers being compared have different signum, returns a 'non performed' match";
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.model.matchers.Matcher#computeScore(java.lang.Object, org.fao.fi.comet.core.model.engine.DataIdentifier, java.lang.Object, java.lang.Object, org.fao.fi.comet.core.model.engine.DataIdentifier, java.lang.Object)
	 */
	@Override
	public MatchingScore computeScore(CodelistData source, DataIdentifier sourceIdentifier, SOURCE_DATA sourceData, CodelistData target, DataIdentifier targetIdentifier, TARGET_DATA targetData) {
		double sourceValue = sourceData.doubleValue();
		double targetValue = targetData.doubleValue();
			
		boolean sameSign = Math.signum(sourceValue) == Math.signum(targetValue);
			
		if(!sameSign && sourceValue != 0 && targetValue != 0)
			return MatchingScore.getNonPerformedTemplate();
			
		double min = Math.min(sourceValue, targetValue);
		double max = Math.max(sourceValue, targetValue);
			
		if(Double.compare(min, max) == 0)
			return MatchingScore.getNonAuthoritativeFullMatchTemplate();
		
		if(min == 0)
			return MatchingScore.getNonAuthoritativeNoMatchTemplate();
			
		return new MatchingScore(Math.abs(min / max), MatchingType.NON_AUTHORITATIVE);
	}
}