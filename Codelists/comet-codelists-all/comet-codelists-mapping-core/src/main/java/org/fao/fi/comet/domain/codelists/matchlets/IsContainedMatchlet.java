/**
 * (c) 2013 FAO / UN (project: comet-codelists)
 */
package org.fao.fi.comet.domain.codelists.matchlets;

import org.fao.fi.comet.core.model.engine.DataIdentifier;
import org.fao.fi.comet.core.model.score.support.MatchingScore;
import org.fao.fi.comet.core.model.score.support.MatchingType;
import org.fao.fi.comet.domain.codelists.model.CodelistData;
import org.fao.fi.comet.domain.codelists.model.matchlets.CodelistDataScalarMatchletSkeleton;


/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 3 Jul 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 3 Jul 2013
 */
public class IsContainedMatchlet extends CodelistDataScalarMatchletSkeleton<String, String> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 3512360792908053689L;
	static public final String NAME = "IsContainedMatchlet";
	
	/**
	 * Class constructor
	 *
	 */
	public IsContainedMatchlet() {
		super();

		this._name = NAME;
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.model.matchers.Matcher#getDescription()
	 */
	@Override
	public String getDescription() {
		return "Performs a string containment match, returning a positive match if the source string is contained in the target string (the sooner the source string is encountered in the target string, the higher the score), otherwise NO match";
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.model.matchers.Matcher#computeScore(java.lang.Object, org.fao.fi.comet.core.model.engine.DataIdentifier, java.lang.Object, java.lang.Object, org.fao.fi.comet.core.model.engine.DataIdentifier, java.lang.Object)
	 */
	@Override
	public MatchingScore computeScore(CodelistData source, DataIdentifier sourceIdentifier, String sourceData, CodelistData target, DataIdentifier targetIdentifier, String targetData) {
		int at = targetData.indexOf(sourceData);
		
		double score = MatchingScore.SCORE_NO_MATCH;
		
		if(at >= 0)
			score = 1 - 1.0 * at / targetData.length();
				
		return new MatchingScore(score, MatchingType.NON_AUTHORITATIVE);
	}	
}