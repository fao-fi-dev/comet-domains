/**
 * (c) 2013 FAO / UN (project: comet-codelists)
 */
package org.fao.fi.comet.domain.codelists.matchlets;

import org.fao.fi.comet.core.model.engine.DataIdentifier;
import org.fao.fi.comet.core.model.score.support.MatchingScore;
import org.fao.fi.comet.domain.codelists.model.CodelistData;


/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 3 Jul 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 3 Jul 2013
 */
public class DoesNotContainMatchlet extends ContainsMatchlet {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 3512360792908053689L;
	static public final String NAME = "DoesNotContainMatchlet";
	
	/**
	 * Class constructor
	 *
	 */
	public DoesNotContainMatchlet() {
		super();

		this._name = NAME;
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.model.matchers.Matcher#getDescription()
	 */
	@Override
	public String getDescription() {
		return "Performs a string containment match, returning a score that is calculated just as the opposite of the 'Contains' matcher";
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.domain.codelists.matchlets.FuzzyStringMatchlet#computeScore(org.fao.fi.comet.domain.codelists.model.CodelistData, org.fao.fi.comet.core.model.engine.DataIdentifier, java.lang.String, org.fao.fi.comet.domain.codelists.model.CodelistData, org.fao.fi.comet.core.model.engine.DataIdentifier, java.lang.String)
	 */
	@Override
	public MatchingScore computeScore(CodelistData source, DataIdentifier sourceIdentifier, String sourceData, CodelistData target, DataIdentifier targetIdentifier, String targetData) {
		return super.computeScore(source, sourceIdentifier, sourceData, target, targetIdentifier, targetData).reverse();
	}
}