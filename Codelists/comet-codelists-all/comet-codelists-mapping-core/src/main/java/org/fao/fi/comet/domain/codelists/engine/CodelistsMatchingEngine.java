/**
 * (c) 2013 FAO / UN (project: comet-codelists)
 */
package org.fao.fi.comet.domain.codelists.engine;

import org.fao.fi.comet.core.engine.MatchingEngineCore;
import org.fao.fi.comet.core.model.engine.MatchingEngineProcessConfiguration;
import org.fao.fi.comet.core.model.matchlets.Matchlet;
import org.fao.fi.comet.domain.codelists.model.CodelistData;
import org.fao.fi.comet.domain.codelists.model.matchlets.CodelistDataScalarMatchlet;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 3 Jul 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 3 Jul 2013
 */
public class CodelistsMatchingEngine extends MatchingEngineCore<CodelistData, CodelistData, MatchingEngineProcessConfiguration> {
	public CodelistsMatchingEngine() {
		super();
	}
	
	public CodelistsMatchingEngine(int numThreads) {
		super(numThreads);
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.engine.AbstractMatchingEngineCore#getDiscoverableMatchletClass()
	 */
	@Override
	@SuppressWarnings("rawtypes")
	protected Class<? extends Matchlet> getDiscoverableMatchletClass() {
		return (Class<? extends Matchlet>)CodelistDataScalarMatchlet.class;
	}
}