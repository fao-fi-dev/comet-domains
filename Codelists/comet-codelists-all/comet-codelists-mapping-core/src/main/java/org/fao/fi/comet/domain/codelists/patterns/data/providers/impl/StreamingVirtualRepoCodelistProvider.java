/**
 * (c) 2013 FAO / UN (project: comet-codelists)
 */
package org.fao.fi.comet.domain.codelists.patterns.data.providers.impl;

import java.util.Collection;
import java.util.Iterator;

import org.fao.fi.comet.core.io.providers.impl.DataProviderSkeleton;
import org.fao.fi.comet.core.patterns.data.providers.ProvidedData;
import org.fao.fi.comet.core.patterns.data.providers.SizeAwareDataProvider;
import org.fao.fi.comet.core.patterns.data.providers.StreamingDataProvider;
import org.fao.fi.comet.domain.codelists.model.CodelistData;
import org.virtualrepository.VirtualRepository;
import org.virtualrepository.csv.CsvCodelist;
import org.virtualrepository.impl.Repository;
import org.virtualrepository.tabular.Column;
import org.virtualrepository.tabular.Row;
import org.virtualrepository.tabular.Table;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 3 Jul 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 3 Jul 2013
 */
public class StreamingVirtualRepoCodelistProvider extends DataProviderSkeleton<CodelistData> implements StreamingDataProvider<CodelistData>, SizeAwareDataProvider<CodelistData> {
	private VirtualRepository _repo;
	private Table _table;	
	private int _size;
	
	/**
	 * Class constructor
	 *
	 * @param providerID
	 */
	public StreamingVirtualRepoCodelistProvider(String providerID) {
		this(providerID, new Repository());
	}
	
	public StreamingVirtualRepoCodelistProvider(String providerID, VirtualRepository repo) {
		super(providerID);
		
		this._repo = repo;
		
		repo.discover(CsvCodelist.type);
		
		this.rewind();
		
		int counter = 0;
		
		Iterator<Row> rowIterator = this._table.iterator();
		
		while(rowIterator.hasNext()) {
			rowIterator.next();
			counter++;
		}
		
		this._size = counter;
		
		this.rewind();
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.patterns.data.providers.SizeAwareDataProvider#getAvailableDataSize()
	 */
	@Override
	public int getAvailableDataSize() {
		return this._size;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.patterns.data.providers.StreamingDataProvider#isRewindable()
	 */
	@Override
	public boolean isRewindable() {
		return true;
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.patterns.data.providers.impl.DataProviderSkeleton#doReleaseResources()
	 */
	@Override
	protected void doReleaseResources() throws Exception {
		//Do nothing
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.patterns.data.providers.StreamingDataProvider#rewind()
	 */
	@Override
	public void rewind() {
		this._table = this._repo.retrieve(this._repo.lookup(this.getProviderID()), Table.class);
	}

	/* (non-Javadoc)
	 * @see java.lang.Iterable#iterator()
	 */
	@Override
	public Iterator<ProvidedData<CodelistData>> iterator() {
		return new IteratorAdapter(this.getProviderID(), this._table);
	}
	
	private class IteratorAdapter implements Iterator<ProvidedData<CodelistData>> {
		private String _providerId;
		private Collection<Column> _columns;
		private Iterator<Row> _tableIterator;
		
		public IteratorAdapter(String providerId, Table table) {
			this._providerId = providerId;
			this._tableIterator = table.iterator();
			this._columns = table.columns();
		}
		
		@Override
		public boolean hasNext() {
			return this._tableIterator.hasNext();
		}
		
		@Override
		public ProvidedData<CodelistData> next() {
			return new ProvidedData<CodelistData>(this._providerId, new CodelistData(this._columns, this._tableIterator.next()));
		}

		@Override
		public void remove() {
			this._tableIterator.remove();
		}
	}
}