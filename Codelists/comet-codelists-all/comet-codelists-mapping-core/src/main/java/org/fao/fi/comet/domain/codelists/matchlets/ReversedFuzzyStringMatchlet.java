/**
 * (c) 2013 FAO / UN (project: comet-codelists)
 */
package org.fao.fi.comet.domain.codelists.matchlets;

import org.fao.fi.comet.core.model.engine.DataIdentifier;
import org.fao.fi.comet.core.model.score.support.MatchingScore;
import org.fao.fi.comet.domain.codelists.model.CodelistData;


/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 3 Jul 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 3 Jul 2013
 */
public class ReversedFuzzyStringMatchlet extends FuzzyStringMatchlet {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 3512360792908053689L;
	static public final String NAME = "ReversedFuzzyStringMatchlet";
		
	/**
	 * Class constructor
	 *
	 */
	public ReversedFuzzyStringMatchlet() {
		super();

		this._name = NAME;
	}

	/**
	 * Class constructor
	 *
	 * @param soundexWeight
	 */
	public ReversedFuzzyStringMatchlet(double soundexWeight) {
		super(soundexWeight);
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.model.matchers.Matcher#getDescription()
	 */
	@Override
	public String getDescription() {
		return "Performs a reversed fuzzy string match of the extracted data (i.e. the returned score will be higher when the two strings being compared do greatly differ)";
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.domain.codelists.matchlets.FuzzyStringMatchlet#computeScore(org.fao.fi.comet.domain.codelists.model.CodelistData, org.fao.fi.comet.core.model.engine.DataIdentifier, java.lang.String, org.fao.fi.comet.domain.codelists.model.CodelistData, org.fao.fi.comet.core.model.engine.DataIdentifier, java.lang.String)
	 */
	@Override
	public MatchingScore computeScore(CodelistData source, DataIdentifier sourceIdentifier, String sourceData, CodelistData target, DataIdentifier targetIdentifier, String targetData) {
		return super.computeScore(source, sourceIdentifier, sourceData, target, targetIdentifier, targetData).reverse();
	}
}