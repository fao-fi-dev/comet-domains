/**
 * (c) 2013 FAO / UN (project: comet-codelists)
 */
package org.fao.fi.comet.domain.codelists.matchlets.authoritative;

import java.io.Serializable;

import org.fao.fi.comet.core.model.engine.DataIdentifier;
import org.fao.fi.comet.core.model.matchlets.annotations.MatchletIsCutoffByDefault;
import org.fao.fi.comet.core.model.score.support.MatchingScore;
import org.fao.fi.comet.domain.codelists.model.CodelistData;
import org.fao.fi.comet.domain.codelists.model.matchlets.CodelistDataScalarMatchletSkeleton;


/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 3 Jul 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 3 Jul 2013
 */
@MatchletIsCutoffByDefault
public class AuthoritativeEqualityMatchlet<SOURCE_DATA extends Serializable, TARGET_DATA extends Serializable> extends CodelistDataScalarMatchletSkeleton<SOURCE_DATA, TARGET_DATA> {
	/** Field serialVersionUID */
	static private final long serialVersionUID = 3512360792908053689L;
	static public final String NAME = "AuthoritativeEqualityMatchlet";
	
	/**
	 * Class constructor
	 *
	 */
	public AuthoritativeEqualityMatchlet() {
		super();
		
		this._name = NAME;
		this._forceComparison = true;
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.model.matchlets.Matchlet#getDescription()
	 */
	@Override
	public String getDescription() {
		return "Performs an authoritative equality match of the extracted data. If the two data being compared are equal, returns a non-authoritative full match, otherwise an authoritative no match. Can handle NULLs (two NULLs being compared will return an non-authoritative full match)";
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.model.matchlets.Matchlet#computeScore(java.lang.Object, org.fao.fi.comet.core.model.engine.DataIdentifier, java.lang.Object, java.lang.Object, org.fao.fi.comet.core.model.engine.DataIdentifier, java.lang.Object)
	 */
	@Override
	public MatchingScore computeScore(CodelistData source, DataIdentifier sourceIdentifier, SOURCE_DATA sourceData, CodelistData target, DataIdentifier targetIdentifier, TARGET_DATA targetData) {
		return sourceData == null && targetData == null 
				? 
			   MatchingScore.getAuthoritativeFullMatchTemplate() 
			    : 
			   sourceData == null || targetData == null
			    ?
			   MatchingScore.getNonPerformedTemplate() 
			    :
			   sourceData.equals(targetData) ? MatchingScore.getNonAuthoritativeFullMatchTemplate() : MatchingScore.getAuthoritativeNoMatchTemplate();
	}
}