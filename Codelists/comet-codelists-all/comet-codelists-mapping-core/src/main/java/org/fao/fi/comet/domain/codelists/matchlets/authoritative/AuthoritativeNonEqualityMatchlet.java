/**
 * (c) 2013 FAO / UN (project: comet-codelists)
 */
package org.fao.fi.comet.domain.codelists.matchlets.authoritative;

import java.io.Serializable;

import org.fao.fi.comet.core.model.engine.DataIdentifier;
import org.fao.fi.comet.core.model.score.support.MatchingScore;
import org.fao.fi.comet.domain.codelists.model.CodelistData;


/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 3 Jul 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 3 Jul 2013
 */
public class AuthoritativeNonEqualityMatchlet<SOURCE_DATA extends Serializable, TARGET_DATA extends Serializable> extends AuthoritativeEqualityMatchlet<SOURCE_DATA, TARGET_DATA> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 3512360792908053689L;
	static public final String NAME = "AuthoritativeNonEqualityMatchlet";
	
	/**
	 * Class constructor
	 *
	 */
	public AuthoritativeNonEqualityMatchlet() {
		super();
		
		this._name = NAME;
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.model.matchers.Matchlet#getDescription()
	 */
	@Override
	public String getDescription() {
		return "Performs an authoritative non-equality match of the extracted data. If the two data being compared are equal, returns an authoritative no match template, otherwise a non-authoritative full match template. Can handle NULLs (two NULLs being compared will return an authoritative no match)";
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.model.matchers.Matchlet#computeScore(java.lang.Object, org.fao.fi.comet.core.model.engine.DataIdentifier, java.lang.Object, java.lang.Object, org.fao.fi.comet.core.model.engine.DataIdentifier, java.lang.Object)
	 */
	@Override
	public MatchingScore computeScore(CodelistData source, DataIdentifier sourceIdentifier, SOURCE_DATA sourceData, CodelistData target, DataIdentifier targetIdentifier, TARGET_DATA targetData) {
		return sourceData == null && targetData == null 
				? 
			   MatchingScore.getAuthoritativeNoMatchTemplate() 
			    : 
			   sourceData == null || targetData == null
			    ?
			   MatchingScore.getNonPerformedTemplate() 
			    :
			   sourceData.equals(targetData) ? MatchingScore.getAuthoritativeNoMatchTemplate() : MatchingScore.getNonAuthoritativeFullMatchTemplate();

	}
}