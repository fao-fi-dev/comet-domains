/**
 * (c) 2013 FAO / UN (project: comet-codelists-mapping)
 */
package org.fao.fi.comet.domain.codelists.model.meta;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 4 Jul 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 4 Jul 2013
 */
@XmlRootElement(name="CodelistColumnMetadata")
@XmlAccessorType(XmlAccessType.FIELD)
public class CodelistColumnMetadata implements Serializable {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -9196091024000527773L;

	@XmlElement(name="isCode")
	private boolean _isCode;

	@XmlElement(name="name")
	private String _name;

	@XmlElement(name="type")
	private String _type;

	/**
	 * Class constructor
	 *
	 * @param isCode
	 * @param name
	 * @param type
	 */
	public CodelistColumnMetadata(boolean isCode, String name, String type) {
		super();
		this._isCode = isCode;
		this._name = name;
		this._type = type;
	}

	/**
	 * @return the 'isCode' value
	 */
	public boolean getIsCode() {
		return this._isCode;
	}

	/**
	 * @param isCode the 'isCode' value to set
	 */
	public void setCode(boolean isCode) {
		this._isCode = isCode;
	}

	/**
	 * @return the 'name' value
	 */
	public String getName() {
		return this._name;
	}

	/**
	 * @param name the 'name' value to set
	 */
	public void setName(String name) {
		this._name = name;
	}

	/**
	 * @return the 'type' value
	 */
	public String getType() {
		return this._type;
	}

	/**
	 * @param type the 'type' value to set
	 */
	public void setType(String type) {
		this._type = type;
	}
}