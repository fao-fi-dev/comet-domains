@XmlSchema(namespace = "http://adapters.model.codelists.domain.comet.fi.fao.org", elementFormDefault = XmlNsForm.UNQUALIFIED)
package org.fao.fi.comet.domain.codelists.model.adapters;
import javax.xml.bind.annotation.XmlNsForm;
import javax.xml.bind.annotation.XmlSchema;
