/**
 * (c) 2013 FAO / UN (project: comet-codelists-model)
 */
package org.fao.fi.comet.domain.codelists.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import org.fao.fi.sh.utility.model.BasicPair;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 3 Jul 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 3 Jul 2013
 */
@XmlType(name="ColumnData")
@XmlAccessorType(XmlAccessType.FIELD)
public class ColumnData extends BasicPair<String, Object> implements Serializable {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 8335719301148321144L;

	/**
	 * Class constructor
	 *
	 */
	public ColumnData() {
		super();
	}

	/**
	 * Class constructor
	 *
	 * @param columnName
	 * @param columnValue
	 */
	public ColumnData(String columnName, Object columnValue) {
		super(columnName, columnValue);
	}
	
	@XmlElement(name="ColumnName")
	public String getColumnName() {
		return this.getLeftPart();
	}
	
	public void setColumnName(String columnName) {
		this.setLeftPart(columnName);
	}
	
	@XmlElement(name="ColumnValue")
	public Object getColumnValue() {
		return this.getRightPart();
	}
	
	public void setColumnValue(Object columnValue) {
		this.setRightPart(columnValue);
	}
}
