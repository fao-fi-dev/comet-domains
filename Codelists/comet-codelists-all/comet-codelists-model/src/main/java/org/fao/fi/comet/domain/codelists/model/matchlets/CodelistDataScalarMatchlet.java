/**
 * (c) 2013 FAO / UN (project: comet-codelists-model)
 */
package org.fao.fi.comet.domain.codelists.model.matchlets;

import java.io.Serializable;

import org.fao.fi.comet.core.model.matchlets.ScalarMatchlet;
import org.fao.fi.comet.domain.codelists.model.CodelistData;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 3 Jul 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 3 Jul 2013
 */
public interface CodelistDataScalarMatchlet<SOURCE_DATA extends Serializable, TARGET_DATA extends Serializable>
	   extends CodelistDataMatchlet<SOURCE_DATA, TARGET_DATA>,
			   ScalarMatchlet<CodelistData, SOURCE_DATA, CodelistData, TARGET_DATA> {
}