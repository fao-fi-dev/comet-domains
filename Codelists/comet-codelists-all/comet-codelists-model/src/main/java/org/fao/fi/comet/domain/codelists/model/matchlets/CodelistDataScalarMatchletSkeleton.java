/**
 * (c) 2013 FAO / UN (project: comet-codelists-model)
 */
package org.fao.fi.comet.domain.codelists.model.matchlets;

import java.io.Serializable;

import org.fao.fi.comet.core.matchlets.skeleton.ScalarMatchletSkeleton;
import org.fao.fi.comet.core.model.engine.DataIdentifier;
import org.fao.fi.comet.core.model.matchlets.annotations.MatchletParameter;
import org.fao.fi.comet.core.model.matchlets.annotations.parameters.NotNull;
import org.fao.fi.comet.domain.codelists.model.CodelistData;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 3 Jul 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 3 Jul 2013
 */
abstract public class CodelistDataScalarMatchletSkeleton<SOURCE_DATA extends Serializable, TARGET_DATA extends Serializable> 
				extends ScalarMatchletSkeleton<CodelistData, SOURCE_DATA, CodelistData, TARGET_DATA> 
				implements CodelistDataScalarMatchlet<SOURCE_DATA, TARGET_DATA> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -7104504514774331273L;
	
	@MatchletParameter(name="sourceColumnName", mandatory=true, description="The name of the source codelist column to extract data from")
	@NotNull
	private String _sourceColumnName;
	
	@MatchletParameter(name="targetColumnName", mandatory=true, description="The name of the target codelist column to extract data from")
	@NotNull
	private String _targetColumnName;

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.model.matchers.ScalarMatcher#extractSourceData(java.lang.Object, org.fao.fi.comet.core.model.engine.DataIdentifier)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public SOURCE_DATA extractSourceData(CodelistData source, DataIdentifier sourceIdentifier) {
		return (SOURCE_DATA)source.getColumn(this._sourceColumnName);
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.model.matchers.ScalarMatcher#extractTargetData(java.lang.Object, org.fao.fi.comet.core.model.engine.DataIdentifier)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public TARGET_DATA extractTargetData(CodelistData target, DataIdentifier targetIdentifier) {
		return (TARGET_DATA)target.getColumn(this._targetColumnName);
	}
}