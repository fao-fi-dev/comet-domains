/**
 * (c) 2013 FAO / UN (project: comet-codelists-model)
 */
package org.fao.fi.comet.domain.codelists.model;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.fao.fi.comet.domain.codelists.model.adapters.CodelistDataMapAdapter;
import org.virtualrepository.tabular.Column;
import org.virtualrepository.tabular.Row;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 3 Jul 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 3 Jul 2013
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name="CodelistData")
public class CodelistData implements Serializable {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -2514846093186049236L;

	@XmlJavaTypeAdapter(CodelistDataMapAdapter.class)
	@XmlElement(name="RowData")
	private Map<String, ColumnData> _rowData;
	/**
	 * Class constructor
	 */
	public CodelistData() {
		super();

		this._rowData = new HashMap<String, ColumnData>();
	}

	/**
	 * Class constructor
	 */
	public CodelistData(Collection<Column> columns, Row row) {
		this();

		String name, value;
		for(Column column : columns) {
			name = column.name().getLocalPart();
			value = row.get(column);

			this._rowData.put(name, new ColumnData(name, value));
		}
	}

	public Object getColumn(String QName) {
		return this.hasColumn(QName) ? this._rowData.get(QName).getColumnValue() : null;
	}

	public boolean hasColumn(String QName) {
		return this._rowData.containsKey(QName);
	}

	public String toString() {
		StringBuilder string = new StringBuilder();

		string.append("{ ");

		for(String in : this._rowData.keySet()) {
			string.append(in).append(": ").append(this._rowData.get(in).getColumnValue()).append(", ");
		}

		string.append(" }");

		return string.toString();
	}
}