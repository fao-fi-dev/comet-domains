/**
 * (c) 2013 FAO / UN (project: comet-codelists-mapping)
 */
package org.fao.fi.comet.domain.codelists.model.meta;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 5 Jul 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 5 Jul 2013
 */
@XmlRootElement(name="CodelistMappingMetadata")
@XmlAccessorType(XmlAccessType.FIELD)
public class CodelistMappingMetadata implements Serializable {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -1633145105058669410L;

	@XmlElement(name="Source")
	private CodelistMetadata _source;

	@XmlElement(name="Target")
	private CodelistMetadata _target;

	/**
	 * Class constructor
	 *
	 * @param source
	 * @param target
	 */
	public CodelistMappingMetadata(CodelistMetadata source, CodelistMetadata target) {
		super();
		this._source = source;
		this._target = target;
	}

	/**
	 * @return the 'source' value
	 */
	public CodelistMetadata getSource() {
		return this._source;
	}

	/**
	 * @param source the 'source' value to set
	 */
	public void setSource(CodelistMetadata source) {
		this._source = source;
	}

	/**
	 * @return the 'target' value
	 */
	public CodelistMetadata getTarget() {
		return this._target;
	}

	/**
	 * @param target the 'target' value to set
	 */
	public void setTarget(CodelistMetadata target) {
		this._target = target;
	}
}
