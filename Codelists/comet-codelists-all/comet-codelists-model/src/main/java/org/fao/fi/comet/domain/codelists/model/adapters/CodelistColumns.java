/**
 * (c) 2013 FAO / UN (project: comet-codelists-model)
 */
package org.fao.fi.comet.domain.codelists.model.adapters;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;

import org.fao.fi.comet.domain.codelists.model.ColumnData;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 3 Jul 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 3 Jul 2013
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class CodelistColumns implements Serializable {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -5889696563544047454L;
	
	@XmlElementWrapper(name="RowColumns")
	@XmlElement(name="RowColumn")
	private Collection<ColumnData> _columns;
	
	/**
	 * Class constructor
	 */
	public CodelistColumns() {
		super();

		this._columns = new ArrayList<ColumnData>();
	}

	/**
	 * Class constructor
	 *
	 * @param columns
	 */
	public CodelistColumns(Collection<ColumnData> columns) {
		super();

		this._columns = columns;
	}

	/**
	 * @return the 'columns' value
	 */
	public Collection<ColumnData> getColumns() {
		return this._columns;
	}

	/**
	 * @param columns the 'columns' value to set
	 */
	public void setColumns(Collection<ColumnData> columns) {
		this._columns = columns;
	}
}