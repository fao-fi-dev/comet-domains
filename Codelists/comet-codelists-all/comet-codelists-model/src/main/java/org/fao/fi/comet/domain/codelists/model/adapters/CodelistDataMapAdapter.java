/**
 * (c) 2013 FAO / UN (project: comet-codelists-model)
 */
package org.fao.fi.comet.domain.codelists.model.adapters;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import org.fao.fi.comet.domain.codelists.model.ColumnData;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 3 Jul 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 3 Jul 2013
 */
public class CodelistDataMapAdapter extends XmlAdapter<CodelistColumns, Map<String, ColumnData>> {

	/* (non-Javadoc)
	 * @see javax.xml.bind.annotation.adapters.XmlAdapter#unmarshal(java.lang.Object)
	 */
	@Override
	public Map<String, ColumnData> unmarshal(CodelistColumns collection) throws Exception {
		Map<String, ColumnData> map = new HashMap<String, ColumnData>();
		
		if(collection != null)
			for(ColumnData pair : collection.getColumns())
				map.put(pair.getColumnName(), pair);
		
		return map;
	}

	/* (non-Javadoc)
	 * @see javax.xml.bind.annotation.adapters.XmlAdapter#marshal(java.lang.Object)
	 */
	@Override
	public CodelistColumns marshal(Map<String, ColumnData> map) throws Exception {
		Collection<ColumnData> collection = new ArrayList<ColumnData>();
		
		if(map != null)
			for(String key : map.keySet())
				collection.add(map.get(key));
		
		return new CodelistColumns(collection);
	}
}