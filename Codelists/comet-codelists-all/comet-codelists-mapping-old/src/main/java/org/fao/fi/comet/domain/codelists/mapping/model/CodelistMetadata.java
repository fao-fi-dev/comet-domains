/**
 * (c) 2013 FAO / UN (project: comet-codelists-mapping)
 */
package org.fao.fi.comet.domain.codelists.mapping.model;

import java.io.Serializable;
import java.util.Collection;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 5 Jul 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 5 Jul 2013
 */
@XmlRootElement(name="CodelistMetadata")
@XmlAccessorType(XmlAccessType.FIELD)
public class CodelistMetadata implements Serializable {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -5752157667153625169L;

	@XmlElement(name="codelistId")
	private String _codelistId;

	@XmlElement(name="codelistName")
	private String _codelistName;

	@XmlElement(name="defaultCodeColumn")
	private String _defaultCodeColumn;

	@XmlElement(name="selectedCodeColumn")
	private String _selectedCodeColumn;

	@XmlElementWrapper(name="columnsMetadata")
	@XmlElement(name="columnsMetadata")
	private Collection<CodelistColumnMetadata> _columnsMetadata;

	/**
	 * Class constructor
	 *
	 * @param codelistId
	 * @param codelistName
	 * @param defaultCodeColumn
	 */
	public CodelistMetadata(String codelistId, String codelistName) {
		super();
		this._codelistId = codelistId;
		this._codelistName = codelistName;
	}

	/**
	 * @return the 'codelistId' value
	 */
	public String getCodelistId() {
		return this._codelistId;
	}

	/**
	 * @param codelistId the 'codelistId' value to set
	 */
	public void setCodelistId(String codelistId) {
		this._codelistId = codelistId;
	}

	/**
	 * @return the 'codelistName' value
	 */
	public String getCodelistName() {
		return this._codelistName;
	}

	/**
	 * @param codelistName the 'codelistName' value to set
	 */
	public void setCodelistName(String codelistName) {
		this._codelistName = codelistName;
	}

	/**
	 * @return the 'columnsMetadata' value
	 */
	public Collection<CodelistColumnMetadata> getColumnsMetadata() {
		return this._columnsMetadata;
	}

	/**
	 * @param columnsMetadata the 'columnsMetadata' value to set
	 */
	public void setColumnsMetadata(Collection<CodelistColumnMetadata> columnsMetadata) {
		this._columnsMetadata = columnsMetadata;
	}

	/**
	 * @return the 'defaultCodeColumn' value
	 */
	public String getDefaultCodeColumn() {
		return this._defaultCodeColumn;
	}

	/**
	 * @param defaultCodeColumn the 'defaultCodeColumn' value to set
	 */
	public void setDefaultCodeColumn(String defaultCodeColumn) {
		this._defaultCodeColumn = defaultCodeColumn;
	}

	/**
	 * @return the 'selectedCodeColumn' value
	 */
	public String getSelectedCodeColumn() {
		return this._selectedCodeColumn;
	}

	/**
	 * @param selectedCodeColumn the 'selectedCodeColumn' value to set
	 */
	public void setSelectedCodeColumn(String selectedCodeColumn) {
		this._selectedCodeColumn = selectedCodeColumn;
	}
}