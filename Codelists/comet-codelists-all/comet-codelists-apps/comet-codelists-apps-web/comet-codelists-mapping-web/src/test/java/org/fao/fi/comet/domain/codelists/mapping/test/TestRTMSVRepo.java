/**
 * (c) 2013 FAO / UN (project: comet-codelist-mapping-ws)
 */
package org.fao.fi.comet.domain.codelists.mapping.test;

import org.junit.Before;
import org.junit.Test;
import org.virtualrepository.Asset;
import org.virtualrepository.VirtualRepository;
import org.virtualrepository.impl.Repository;
import org.virtualrepository.sdmx.SdmxCodelist;
import org.virtualrepository.tabular.Column;
import org.virtualrepository.tabular.Table;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 27 Jun 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 27 Jun 2013
 */
public class TestRTMSVRepo {
	private VirtualRepository _repo;

	@Before
	public void foo() {
		this._repo = new Repository();
	}
	
	@Test
	public void testListRTMSAssets() {
//		this._repo.discover(CsvCodelist.type);
		this._repo.discover(SdmxCodelist.type);
		
		for(Asset asset : this._repo) {
			try {
				System.out.println(asset.properties());
				
				Table content = this._repo.retrieve(asset, Table.class);
				
				for(Column col : content.columns())
					System.out.println(col.name() + " / " + col.getKind() + " / " + col.type());
			} catch (Throwable t) {
				System.err.println(t.getMessage());
			}
		}
	}
}