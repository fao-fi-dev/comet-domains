/**
 * (c) 2013 FAO / UN (project: comet-codelist-mapping-ws)
 */
package org.fao.fi.comet.domain.codelists.mapping.ws.rest;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.BadRequestException;

import org.fao.fi.comet.domain.codelists.mapping.ws.rest.exceptions.AbstractManagedException;
import org.fao.fi.comet.domain.codelists.mapping.ws.rest.exceptions.ServerException;
import org.fao.fi.comet.domain.codelists.model.meta.CodelistColumnMetadata;
import org.fao.fi.comet.domain.codelists.model.meta.CodelistMetadata;
import org.fao.fi.sh.utility.core.AbstractLoggingAwareClient;
import org.virtualrepository.VirtualRepository;
import org.virtualrepository.csv.CsvAsset;
import org.virtualrepository.csv.CsvCodelist;
import org.virtualrepository.csv.CsvTable;
import org.virtualrepository.impl.Repository;
import org.virtualrepository.impl.ServiceInspector;
import org.virtualrepository.tabular.Column;
import org.virtualrepository.tabular.Row;
import org.virtualrepository.tabular.Table;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 4 Jul 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 4 Jul 2013
 */
public class CommonManagementService extends AbstractLoggingAwareClient {
	static protected VirtualRepository REPO;
	
	static {
		REPO = new Repository();
		
//		REPO.services().lookup(new QName("CSVFileRepo"));
//		REPO.services().lookup(new QName("MappingFileRepo"));
//		REPO.services().lookup(RepositoryPlugin.name);

		REPO.discover(CsvCodelist.type);
	}

	protected HttpSession getSession(HttpServletRequest request) {
		return this.getSession(request, true);
	}

	protected HttpSession getSession(HttpServletRequest request, boolean createNew) {
		if(request == null)
			throw new BadRequestException("No HTTP servlet request available");

		HttpSession session = request.getSession(createNew);

		if(!createNew && session == null)
			throw new BadRequestException("Current session is NULL");

		return session;
	}

	protected AbstractManagedException handle(Throwable t) {
		return this.handle(t, "<NOT SET>");
	}

	protected AbstractManagedException handle(Throwable t, String message) {
		try {
			if(t instanceof AbstractManagedException)
				return (AbstractManagedException)t;
	
			this._log.error("Unexpected {} caught: {} - application specific message is {}", t.getClass().getSimpleName(), t.getMessage(), message, t);
	
			return new ServerException(t);
		} finally {
			t.printStackTrace();
		}
	}

	final protected CodelistMetadata getMetadataForAsset(String assetId, CsvCodelist asset, Table asTable) {
		if(asset == null)
			return null;

		CodelistMetadata meta = new CodelistMetadata(assetId, asset.name());

		Collection<CodelistColumnMetadata> columns = new ArrayList<CodelistColumnMetadata>();

		String type;

		final int codeIndex = asset.codeColumn();

		int counter = 0;
		for(Column column : asTable.columns()) {
			type = column.type().getSimpleName();

			columns.add(new CodelistColumnMetadata(counter == codeIndex,
												   column.name().toString(),
												   type));

			if(counter == codeIndex)
				meta.setDefaultCodeColumn(column.name().toString());

			counter++;
		}

		meta.setColumnsMetadata(columns);

		return meta;
	}
	
	protected TableData doRetrieveAssetAsTable(CsvAsset asset) throws IOException {
		return this.doRetrieveAssetAsTable(asset, null);
	}
	
	protected TableData doRetrieveAssetAsTable(CsvAsset asset, Integer maxRows) throws IOException {
		Table table = null;
		
		ServiceInspector inspector = new ServiceInspector(asset.service());
		InputStream assetStream = null;

		try {
			if(inspector.takes(CsvAsset.type, InputStream.class)) {
				assetStream = REPO.retrieve(asset, InputStream.class);
			
				table = new CsvTable(asset, assetStream);
			} else {
				table = REPO.retrieve(asset, Table.class);
			}
	
			Collection<Map<String, String>> data = new ArrayList<Map<String, String>>();
	
			Map<String, String> rowData;
	
			int counter = 0;
	
			for(Row row : table) {
				counter++;
				
				rowData = new HashMap<String, String>();
	
				for(Column column : table.columns())
					rowData.put(column.name().toString(), (String)row.get(column));
	
				if(maxRows == null || counter <= maxRows)
					data.add(rowData);
			}
	
			return new TableData(data.size(), counter, data);
		} finally {
			if(assetStream != null)
				assetStream.close();
		}
	}
	
	protected TableData doRetrieveAssetAsTable(String assetID) throws IOException {
		return this.doRetrieveAssetAsTable(assetID, null);
	}
	
	protected TableData doRetrieveAssetAsTable(String assetID, Integer maxRows) throws IOException {
		return this.doRetrieveAssetAsTable((CsvAsset)REPO.lookup(assetID), maxRows);
	}
	
	protected class TableData {
		private Integer _currentRows;
		private Integer _maxRows;
		private Collection<Map<String, String>> _data;

		/**
		 * Class constructor
		 *
		 * @param currentRows
		 * @param maxRows
		 * @param data
		 */
		public TableData(Integer currentRows, Integer maxRows, Collection<Map<String, String>> data) {
			super();
			this._currentRows = currentRows;
			this._maxRows = maxRows;
			this._data = data;
		}

		/**
		 * @return the 'currentRows' value
		 */
		public Integer getCurrentRows() {
			return this._currentRows;
		}

		/**
		 * @param currentRows the 'currentRows' value to set
		 */
		public void setCurrentRows(Integer currentRows) {
			this._currentRows = currentRows;
		}

		/**
		 * @return the 'maxRows' value
		 */
		public Integer getMaxRows() {
			return this._maxRows;
		}

		/**
		 * @param maxRows the 'maxRows' value to set
		 */
		public void setMaxRows(Integer maxRows) {
			this._maxRows = maxRows;
		}

		/**
		 * @return the 'data' value
		 */
		public Collection<Map<String, String>> getData() {
			return this._data;
		}

		/**
		 * @param data the 'data' value to set
		 */
		public void setData(Collection<Map<String, String>> data) {
			this._data = data;
		}
	}

}
