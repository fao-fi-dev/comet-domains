/**
 * (c) 2013 FAO / UN (project: comet-codelists)
 */
package org.fao.fi.comet.domain.codelists.mapping.patterns.data.providers.impl;

import java.util.Collection;
import java.util.Iterator;

import org.fao.fi.comet.core.io.providers.impl.DataProviderSkeleton;
import org.fao.fi.comet.core.patterns.data.providers.ProvidedData;
import org.fao.fi.comet.core.patterns.data.providers.StreamingDataProvider;
import org.fao.fi.comet.domain.codelists.model.CodelistData;
import org.virtualrepository.tabular.Column;
import org.virtualrepository.tabular.Row;
import org.virtualrepository.tabular.Table;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 3 Jul 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 3 Jul 2013
 */
public class ExistingVirtualRepoCodelistProvider extends DataProviderSkeleton<CodelistData> implements StreamingDataProvider<CodelistData> {
	private Table _table;

	public ExistingVirtualRepoCodelistProvider(String providerID, Table source) {
		super(providerID);

		this._table = source;
	}

	/* (non-Javadoc)
	 * @see java.lang.Iterable#iterator()
	 */
	@Override
	public Iterator<ProvidedData<CodelistData>> iterator() {
		return new WrappingIterator(getProviderID(), _table);
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.patterns.data.providers.StreamingDataProvider#isRewindable()
	 */
	@Override
	public boolean isRewindable() {
		return true;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.patterns.data.providers.StreamingDataProvider#rewind()
	 */
	@Override
	public void rewind() {
		System.out.println("Rewinding " + this.getProviderID() + "...");
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.patterns.data.providers.impl.DataProviderSkeleton#doReleaseResources()
	 */
	@Override
	protected void doReleaseResources() throws Exception {
		//Do nothing
	}
	
	private class WrappingIterator implements Iterator<ProvidedData<CodelistData>> {
		private int counter = 0;
		
		private String _providerId;
		
		@SuppressWarnings("unused")
		private Table _source;
		private Collection<Column> _sourceColumns;
		private Iterator<Row> _sourceIterator;
		
		public WrappingIterator(String providerId, Table table) {
			_providerId = providerId;
			_source = table;
			_sourceColumns = table.columns();
			_sourceIterator = table.iterator();
		}
		
		/* (non-Javadoc)
		 * @see java.util.Iterator#hasNext()
		 */
		@Override
		public boolean hasNext() {
			return _sourceIterator.hasNext();
		}
		
		public ProvidedData<CodelistData> next() {
			System.out.println("Returning record #" + ( ++counter ) + " for " + _providerId);
			return new ProvidedData<CodelistData>(_providerId, new CodelistData(_sourceColumns, _sourceIterator.next()));
		}
	}
}