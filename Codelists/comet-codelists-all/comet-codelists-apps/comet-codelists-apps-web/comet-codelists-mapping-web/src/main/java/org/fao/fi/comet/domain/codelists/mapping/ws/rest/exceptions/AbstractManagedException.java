package org.fao.fi.comet.domain.codelists.mapping.ws.rest.exceptions;

import javax.ws.rs.core.Response;

abstract public class AbstractManagedException extends Exception {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 8783994040799784086L;

	private Response.Status _statusCode;
	private String _message;
	/**
	 * Class constructor
	 *
	 * @param statusCode
	 * @param message
	 */
	public AbstractManagedException(Response.Status statusCode, String message) {
		super();
		this._statusCode = statusCode;
		this._message = message;
	}

	public AbstractManagedException(Response.Status statusCode, Throwable cause) {
		this(statusCode, cause.getClass().getSimpleName() + ": " + cause.getMessage());
	}

	/**
	 * @return the 'statusCode' value
	 */
	public Response.Status getStatusCode() {
		return this._statusCode;
	}

	/**
	 * @param statusCode the 'statusCode' value to set
	 */
	public void setStatusCode(Response.Status statusCode) {
		this._statusCode = statusCode;
	}

	/**
	 * @return the 'message' value
	 */
	public String getMessage() {
		return this._message;
	}

	/**
	 * @param message the 'message' value to set
	 */
	public void setMessage(String message) {
		this._message = message;
	}
}
