package org.fao.fi.comet.domain.codelists.mapping.ws.rest.exceptions;

import javax.ws.rs.core.Response;

public class ServerException extends AbstractManagedException {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -8578141130781452661L;

	/**
	 * Class constructor
	 *
	 * @param statusCode
	 * @param message
	 */
	public ServerException(String message) {
		super(Response.Status.INTERNAL_SERVER_ERROR, message);
	}

	/**
	 * Class constructor
	 *
	 * @param statusCode
	 * @param cause
	 */
	public ServerException(Throwable cause) {
		super(Response.Status.INTERNAL_SERVER_ERROR, cause);
	}
}
