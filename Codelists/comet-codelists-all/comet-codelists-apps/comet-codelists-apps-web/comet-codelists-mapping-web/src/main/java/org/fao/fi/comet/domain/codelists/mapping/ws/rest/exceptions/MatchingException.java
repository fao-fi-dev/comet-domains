package org.fao.fi.comet.domain.codelists.mapping.ws.rest.exceptions;

import javax.ws.rs.core.Response;

public class MatchingException extends AbstractManagedException {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 4224454472956931484L;

	/**
	 * Class constructor
	 *
	 * @param statusCode
	 * @param message
	 */
	public MatchingException(Response.Status statusCode, String message) {
		super(statusCode, message);
	}

	/**
	 * Class constructor
	 *
	 * @param statusCode
	 * @param cause
	 */
	public MatchingException(Response.Status statusCode, Throwable cause) {
		super(statusCode, cause);
	}
}
