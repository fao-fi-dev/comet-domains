/**
 * (c) 2015 FAO / UN (project: comet-codelist-mapping-ws)
 */
package org.fao.fi.comet.domain.codelists.mapping;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;

import org.fao.fi.comet.domain.codelists.model.CodelistData;
import org.fao.fi.comet.mapping.model.MappingConfiguration;
import org.fao.fi.comet.mapping.model.MappingData;
import org.fao.fi.sh.utility.core.helpers.singletons.io.JAXBHelper;
import org.virtual.sr.RepositoryPlugin;
import org.virtualrepository.Property;
import org.virtualrepository.RepositoryService;
import org.virtualrepository.VirtualRepository;
import org.virtualrepository.comet.CometAsset;
import org.virtualrepository.impl.Repository;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 20 Jan 2015   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 20 Jan 2015
 */
public class MappingStager {
	/**
	 * @param args
	 */
	public static void main(String[] args) throws Throwable {
		VirtualRepository REPO = new Repository();
		RepositoryService serviceSR = REPO.services().lookup(RepositoryPlugin.name);

		FileInputStream fis = new FileInputStream(args[0]);
		byte[] buffer = new byte[8192];
		int len = -1;
		
		StringBuilder result = new StringBuilder();
		
		while((len = fis.read(buffer)) != -1) 
			result.append(new String(buffer, 0, len, "UTF-8"));
		
		fis.close();
		
		byte[] content = result.toString().getBytes("UTF-8");
		
		MappingData mappingData = JAXBHelper.fromXML(
			MappingData.class,
			new Class[] { CodelistData.class }, 
			result.toString()
		);
			
		String config = 
			JAXBHelper.toXML(
				new Class[] { MappingConfiguration.class },
				mappingData.getConfiguration()
			).replaceAll("\t", " ").replaceAll(">\\s+<", "><").replaceAll("\r|\n", "");
 
		ByteArrayInputStream bais = new ByteArrayInputStream(content);
		
		CometAsset asset = new CometAsset("ASFIS_TO_WORMS_PISCES_VERBOSE_verbose", serviceSR);
		
		asset.properties().add(
			new Property(
				"configuration", 
				config
			)
		);
		
		REPO.publish(asset, bais);
		
		bais.close();
	}

}
