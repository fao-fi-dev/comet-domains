package org.fao.fi.comet.domain.codelists.mapping.ws.rest;

import org.codehaus.jackson.jaxrs.JacksonJaxbJsonProvider;
import org.codehaus.jackson.map.ObjectMapper;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.server.ResourceConfig;


public class Application extends ResourceConfig {
	public Application() {
		super(JacksonFeature.class);

		ObjectMapper mapper = new ObjectMapper();

		JacksonJaxbJsonProvider provider = new JacksonJaxbJsonProvider();
		provider.setMapper(mapper);

		this.register(provider);
	}
}
