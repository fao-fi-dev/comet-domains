package org.fao.fi.comet.domain.codelists.mapping.ws.rest.exceptions.repo;

import javax.ws.rs.core.Response;

import org.fao.fi.comet.domain.codelists.mapping.ws.rest.exceptions.AbstractManagedException;

public class GenericRepoException extends AbstractManagedException {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 4224454472956931484L;

	/**
	 * Class constructor
	 *
	 * @param message
	 */
	public GenericRepoException(String message) {
		super(Response.Status.INTERNAL_SERVER_ERROR, message);
	}

	/**
	 * Class constructor
	 *
	 * @param statusCode
	 * @param cause
	 */
	public GenericRepoException(Throwable cause) {
		super(Response.Status.INTERNAL_SERVER_ERROR, cause);
	}
}
