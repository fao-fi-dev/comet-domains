package org.fao.fi.comet.domain.codelists.mapping.ws.rest;

public interface SessionConstants {
	String SOURCE_CODELIST_ID = "source.codelist.id";
	String TARGET_CODELIST_ID = "target.codelist.id";

	String SOURCE_CODELIST_ASSET = "source.codelist.asset";
	String TARGET_CODELIST_ASSET = "target.codelist.asset";

	String SOURCE_CODELIST_META = "source.codelist.meta";
	String TARGET_CODELIST_META = "target.codelist.meta";

	String CODELISTS_META = "codelists.meta";

	String MAPPING_TRACKER	 = "mapping.tracker";
	String MAPPING_CONFIG	 = "mapping.config";
	String MAPPING_RESULT	 = "mapping.result";
	String MAPPING_CONFIRMED = "mapping.result.confirmed";
}