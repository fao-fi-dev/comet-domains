/**
 * (c) 2013 FAO / UN (project: comet-codelists)
 */
package org.fao.fi.comet.domain.codelists.mapping.patterns.data.providers.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.fao.fi.comet.core.io.providers.impl.DataProviderSkeleton;
import org.fao.fi.comet.core.patterns.data.providers.ProvidedData;
import org.fao.fi.comet.core.patterns.data.providers.SizeAwareDataProvider;
import org.fao.fi.comet.domain.codelists.model.CodelistData;
import org.virtualrepository.tabular.Column;
import org.virtualrepository.tabular.Row;
import org.virtualrepository.tabular.Table;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 3 Jul 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 3 Jul 2013
 */
public class MaterializedExistingVirtualRepoCodelistProvider extends DataProviderSkeleton<CodelistData> implements SizeAwareDataProvider<CodelistData> {
	private List<ProvidedData<CodelistData>> _asList;

	public MaterializedExistingVirtualRepoCodelistProvider(String providerID, Table source) {
		super(providerID);

		this._asList = new ArrayList<ProvidedData<CodelistData>>();

		Collection<Column> columns = source.columns();

		for(Row row : source) {
			this._asList.add(new ProvidedData<CodelistData>(providerID, new CodelistData(columns, row)));
		}
	}

	/* (non-Javadoc)
	 * @see java.lang.Iterable#iterator()
	 */
	@Override
	public Iterator<ProvidedData<CodelistData>> iterator() {
		return this._asList.iterator();
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.patterns.data.providers.SizeAwareDataProvider#getAvailableDataSize()
	 */
	@Override
	public int getAvailableDataSize() {
		return this._asList.size();
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.patterns.data.providers.impl.DataProviderSkeleton#doReleaseResources()
	 */
	@Override
	protected void doReleaseResources() throws Exception {
		//Do nothing
	}
}