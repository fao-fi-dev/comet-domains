/**
 * (c) 2013 FAO / UN (project: comet-codelist-mapping-ws)
 */
package org.fao.fi.comet.domain.codelists.mapping.ws.rest;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.inject.Singleton;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.fao.fi.comet.core.engine.process.handlers.MatchingProcessHandler;
import org.fao.fi.comet.core.engine.process.handlers.impl.SilentMatchingProcessHandler;
import org.fao.fi.comet.core.model.engine.MatchingEngineProcessConfiguration;
import org.fao.fi.comet.core.model.engine.MatchingEngineProcessResult;
import org.fao.fi.comet.core.model.engine.MatchingEngineProcessStatus;
import org.fao.fi.comet.core.model.engine.MatchingEngineProcessorInfo;
import org.fao.fi.comet.core.model.matchlets.MatchletConfiguration;
import org.fao.fi.comet.core.model.matchlets.support.MatchletInfo;
import org.fao.fi.comet.core.patterns.data.partitioners.impl.IdentityDataPartitioner;
import org.fao.fi.comet.core.patterns.data.providers.DataProvider;
import org.fao.fi.comet.domain.codelists.engine.CodelistsMatchingEngine;
import org.fao.fi.comet.domain.codelists.mapping.patterns.data.providers.impl.MaterializedExistingVirtualRepoCodelistProvider;
import org.fao.fi.comet.domain.codelists.mapping.ws.rest.exceptions.AbstractManagedException;
import org.fao.fi.comet.domain.codelists.mapping.ws.rest.exceptions.MatchingException;
import org.fao.fi.comet.domain.codelists.mapping.ws.rest.exceptions.repo.CodelistMetadataException;
import org.fao.fi.comet.domain.codelists.model.CodelistData;
import org.fao.fi.comet.domain.codelists.model.meta.CodelistMappingMetadata;
import org.fao.fi.comet.domain.codelists.patterns.handlers.id.CodelistIDHandler;
import org.fao.fi.sh.utility.core.helpers.singletons.io.JAXBHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.virtualrepository.Asset;
import org.virtualrepository.tabular.Table;

import flexjson.JSONDeserializer;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 4 Jul 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 4 Jul 2013
 */
@Path("/match")
@Singleton
public class MatchingManagementService extends CommonMatchingService {
	final private CodelistsMatchingEngine REFERENCE_ENGINE;
	
	final private ExecutorService THREAD_POOL = Executors.newFixedThreadPool(8);

	private class MatchingTask implements Callable<MatchingEngineProcessResult<CodelistData, CodelistData, MatchingEngineProcessConfiguration>> {
		private Logger _log = LoggerFactory.getLogger(this.getClass());
		
		private String _requestID;
		private MatchingEngineProcessConfiguration _config;
		private MatchingProcessHandler<CodelistData, CodelistData> _tracker;
		private DataProvider<CodelistData> _sourceDataProvider;
		private DataProvider<CodelistData> _targetDataProvider;
		private CodelistIDHandler _sourceHandler;
		private CodelistIDHandler _targetHandler;
		
		/**
		 * Class constructor
		 */
		public MatchingTask(String requestID, 
							MatchingEngineProcessConfiguration config,
							MatchingProcessHandler<CodelistData, CodelistData> tracker, 
							DataProvider<CodelistData> sourceDataProvider, 
							DataProvider<CodelistData> targetDataProvider, 
							CodelistIDHandler sourceHandler,
							CodelistIDHandler targetHandler) {
			this._requestID = requestID;
			this._config = config;
			this._tracker = tracker;
			this._sourceDataProvider = sourceDataProvider;
			this._targetDataProvider = targetDataProvider;
			this._sourceHandler = sourceHandler;
			this._targetHandler = targetHandler;
		}
		
		/* (non-Javadoc)
		 * @see java.util.concurrent.Callable#call()
		 */
		@Override
		public MatchingEngineProcessResult<CodelistData, CodelistData, MatchingEngineProcessConfiguration> call() throws Exception {
			this._log.info("Match : BEGIN");

			CodelistsMatchingEngine engine = new CodelistsMatchingEngine();
	
			MatchingEngineProcessResult<CodelistData, CodelistData, MatchingEngineProcessConfiguration> result = null;
			
			this._tracker.getProcessStatus().setProcessId(this._requestID);

			this._log.info("Match : START");

			result = engine.compareAll(this._config,
									   this._tracker,
							           this._sourceDataProvider,
							           new IdentityDataPartitioner<CodelistData, CodelistData>(),
							           this._targetDataProvider,
							           this._sourceHandler,
							           this._targetHandler);
			
			this._log.info("Match : STOP");

			this._log.info("Match : END");

			CONFIGURATIONS_MAP.put(this._requestID, this._config);
			RESULTS_MAP.put(this._requestID, result);
			
			return result;
		}
	}
	
	/**
	 * Class constructor
	 */
	public MatchingManagementService() {
		super();

		this.REFERENCE_ENGINE = new CodelistsMatchingEngine();
	}

	@GET
	@Path("/describe/matchers")
	@Produces(MediaType.APPLICATION_JSON)
	public Collection<MatchletInfo> describeMatchlets() throws AbstractManagedException {
		try {
			return this.REFERENCE_ENGINE.getAllAvailableMatchletsInfo();
		} catch (Throwable t) {
			throw new MatchingException(Response.Status.INTERNAL_SERVER_ERROR, t);
		}
	}

	@GET
	@Path("/describe/matcher/{name}")
	@Produces(MediaType.APPLICATION_JSON)
	public MatchletInfo describeMatchlet(@PathParam(value="name") String matcherName) throws AbstractManagedException {
		try {
			MatchletInfo selected = null;
			for(MatchletInfo info : this.REFERENCE_ENGINE.getAllAvailableMatchletsInfo()) {
				if(info.getMatchletName().equals(matcherName)) {
					selected = info;
					break;
				}
			}

			if(selected == null)
				throw new MatchingException(Response.Status.BAD_REQUEST, "No matcher named '" + matcherName + "' is currently configured for this engine");

			return selected;
		} catch (Throwable t) {
			throw this.handle(t);
		}
	}

	@GET
	@Path("/configuration/codelists")
	@Produces(MediaType.APPLICATION_JSON)
	public CodelistMappingMetadata configuration(@Context HttpServletRequest request) throws AbstractManagedException {
		try {
			HttpSession session = this.getSession(request);

			CodelistMappingMetadata meta = (CodelistMappingMetadata)session.getAttribute(SessionConstants.CODELISTS_META);

			if(meta == null)
				throw new CodelistMetadataException(Response.Status.NOT_FOUND, "Codelists metadata are missing");

			return meta;
		} catch (Throwable t) {
			throw this.handle(t);
		}
	}

	@GET
	@Path("/status/{requestID}")
	@Produces(MediaType.APPLICATION_JSON)
	public MatchingEngineProcessStatus status(@Context HttpServletRequest request, @PathParam(value="requestID") String requestID) throws AbstractManagedException {
		try {
			MatchingProcessHandler<CodelistData, CodelistData> tracker = TRACKERS_MAP.get(requestID);

			if(tracker != null) {
				return tracker.getProcessStatus();
			}

			return null;
		} catch (Throwable t) {
			throw new MatchingException(Response.Status.INTERNAL_SERVER_ERROR, t);
		}
	}

	@POST
	@Path("/execute/{requestID}")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public void execute(@Context HttpServletRequest request, @PathParam(value="requestID") String requestID, @FormParam("configuration") String serializedConfiguration) throws AbstractManagedException {
		HttpSession session = this.getSession(request);

		session.setAttribute(SessionConstants.MAPPING_TRACKER, requestID);
		
		try {
			MatchingProcessHandler<CodelistData, CodelistData> tracker = TRACKERS_MAP.get(requestID);
			
			if(tracker != null) {
				tracker.halt();
				
				TRACKERS_MAP.remove(requestID);
			}
			
			tracker = new SilentMatchingProcessHandler<CodelistData, CodelistData>();
			
			TRACKERS_MAP.put(requestID, tracker);

			this._log.info("Handling GET /execute request : BEGIN");
			
			Map<String, Serializable> deserializedConfig = new JSONDeserializer<Map<String, Serializable>>().deserialize(serializedConfiguration);
			
			String sourceId = (String)session.getAttribute(SessionConstants.SOURCE_CODELIST_ID);
			String targetId = (String)session.getAttribute(SessionConstants.TARGET_CODELIST_ID);
			
			DataProvider<CodelistData> sourceDataProvider = new MaterializedExistingVirtualRepoCodelistProvider(sourceId, REPO.retrieve((Asset)session.getAttribute(SessionConstants.SOURCE_CODELIST_ASSET), Table.class));// StreamingVirtualRepoCodelistProvider(sourceId, REPO);
			DataProvider<CodelistData> targetDataProvider = new MaterializedExistingVirtualRepoCodelistProvider(targetId, REPO.retrieve((Asset)session.getAttribute(SessionConstants.TARGET_CODELIST_ASSET), Table.class)); //new StreamingVirtualRepoCodelistProvider(targetId, REPO); // new MaterializedExistingVirtualRepoCodelistProvider(targetId, REPO.retrieve((Asset)session.getAttribute(SessionConstants.TARGET_CODELIST_ASSET), Table.class));

			Map<String, Serializable> sourceDataConfiguration = this.getSourceDataConfiguration(this.getDataConfiguration(deserializedConfig));
			Map<String, Serializable> targetDataConfiguration = this.getTargetDataConfiguration(this.getDataConfiguration(deserializedConfig));
			
			String sourceCode = (String)sourceDataConfiguration.get("code");
			String targetCode = (String)targetDataConfiguration.get("code");

			MatchingEngineProcessConfiguration config = this.doBuildConfiguration(deserializedConfig);

			session.setAttribute(SessionConstants.MAPPING_CONFIG, config);
			
			THREAD_POOL.submit(
				new MatchingTask(
					requestID, 
					config, 
					tracker, 
					sourceDataProvider, 
					targetDataProvider, 
					new CodelistIDHandler(sourceCode),
					new CodelistIDHandler(targetCode)
				)
			);

			this._log.info("Handling GET /execute request : END");
		} catch (Throwable t) {
			MatchingProcessHandler<CodelistData, CodelistData> tracker = TRACKERS_MAP.get(requestID);
			
			if(tracker != null) {
				try {
					tracker.halt();
				} catch(Throwable tt) {
					//Do nothing, or the original error will be suffocated
				}
			}

			throw this.handle(t);
		}
	}
	
	private MatchingEngineProcessConfiguration doBuildConfiguration(Map<String, Serializable> deserializedConfig) {
		MatchingEngineProcessConfiguration config = new MatchingEngineProcessConfiguration();
					
		Double scoreThreshold = Double.valueOf(this.getOverallConfiguration(deserializedConfig).get("scoreThreshold").toString());
		Boolean haltAtFirstMatch = Boolean.valueOf(this.getOverallConfiguration(deserializedConfig).get("haltAtFirstMatch").toString());
		Long maxCandidates = haltAtFirstMatch ? null : Long.valueOf(this.getOverallConfiguration(deserializedConfig).get("maxCandidates").toString());

		if(scoreThreshold != null)
			config.setMinimumAllowedWeightedScore(scoreThreshold);

		if(haltAtFirstMatch != null)
			config.setHaltAtFirstValidMatching(haltAtFirstMatch);

		if(maxCandidates != null)
			config.setMaxCandidatesPerEntry(maxCandidates.intValue());

		config.setHandleErrors(false);

		MatchletConfiguration mConf = null;
		String paramName;
		Serializable paramValue;

		for(Map<String, Serializable> matchersConfiguration : this.getMatchletsConfiguration(deserializedConfig)) {
			mConf = new MatchletConfiguration(
				(String)matchersConfiguration.get("matchletId"),
				(String)matchersConfiguration.get("matchletName"),
				null
			);

			for(Map<String, Serializable> matcherParameter : this.getMatchletParametersConfiguration(matchersConfiguration)) {
				paramName = (String)matcherParameter.get("parameterName");
				paramValue = matcherParameter.get("parameterValue");

				if(paramValue != null)
					mConf.with(paramName, paramValue);
			}

			config.getMatchletsConfigurations().add(mConf);
		}
		
		return config;
	}

	@GET
	@Path("/results/{requestID}")
	@Produces(MediaType.APPLICATION_XML + "; charset=UTF-8")
	public Response results(@Context HttpServletRequest request, @PathParam(value="requestID") String requestID) throws AbstractManagedException {
		try {
			MatchingEngineProcessResult<CodelistData, CodelistData, MatchingEngineProcessConfiguration> result = RESULTS_MAP.get(requestID);
			MatchingEngineProcessConfiguration config = CONFIGURATIONS_MAP.get(requestID);
						
			if(result == null)
				throw new MatchingException(Response.Status.NOT_FOUND, "No matching result available for given requestID");

			result.setProcessorsInfo(new ArrayList<MatchingEngineProcessorInfo<MatchingEngineProcessConfiguration>>());
			
			MatchingEngineProcessorInfo<MatchingEngineProcessConfiguration> processorInfo = new MatchingEngineProcessorInfo<MatchingEngineProcessConfiguration>();
			processorInfo.setConfiguration(config);
			result.getProcessorsInfo().add(processorInfo);

			return Response.ok(
				JAXBHelper.toXML(
					new Class[] {
						CodelistData.class
					}, result
				), MediaType.APPLICATION_XML
			).build();
		} catch (Throwable t) {
			throw this.handle(t);
		}
	}

	@DELETE
	@Path("/halt/{requestId}")
	public void halt(@Context HttpServletRequest request, @PathParam("requestId") String requestID) throws AbstractManagedException {
		try {
			MatchingProcessHandler<CodelistData, CodelistData> tracker = TRACKERS_MAP.get(requestID);

			if(tracker == null) {
				throw new MatchingException(Response.Status.BAD_REQUEST, "Cannot halt the matching process right now");
			} 

			tracker.halt();
		} catch (Throwable t) {
			throw this.handle(t);
		}
	}

	@SuppressWarnings("unchecked")
	private Map<String, Serializable> getDataConfiguration(Map<String, Serializable> configuration) {
		return (Map<String, Serializable>)configuration.get("data");
	}

	@SuppressWarnings("unchecked")
	private Map<String, Serializable> getSourceDataConfiguration(Map<String, Serializable> configuration) {
		return (Map<String, Serializable>)configuration.get("source");
	}

	@SuppressWarnings("unchecked")
	private Map<String, Serializable> getTargetDataConfiguration(Map<String, Serializable> configuration) {
		return (Map<String, Serializable>)configuration.get("target");
	}

	@SuppressWarnings("unchecked")
	private Map<String, Serializable> getOverallConfiguration(Map<String, Serializable> configuration) {
		return (Map<String, Serializable>)configuration.get("overall");
	}

	@SuppressWarnings("unchecked")
	private Collection<Map<String, Serializable>> getMatchletsConfiguration(Map<String, Serializable> configuration) {
		return (Collection<Map<String, Serializable>>)configuration.get("matchlets");
	}

	@SuppressWarnings("unchecked")
	private Collection<Map<String, Serializable>> getMatchletParametersConfiguration(Map<String, Serializable> configuration) {
		return (Collection<Map<String, Serializable>>)configuration.get("matchletParameters");
	}
}
