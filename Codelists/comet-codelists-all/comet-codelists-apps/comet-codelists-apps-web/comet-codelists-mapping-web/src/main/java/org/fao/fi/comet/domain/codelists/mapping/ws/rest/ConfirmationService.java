/**
 * (c) 2013 FAO / UN (project: comet-codelist-mapping-ws)
 */
package org.fao.fi.comet.domain.codelists.mapping.ws.rest;

import static org.fao.fi.comet.bridge.mapping.dsl.DataProviderDSL.provider;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.inject.Singleton;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;
import javax.xml.bind.JAXBException;
import javax.xml.namespace.QName;

import org.fao.fi.comet.bridge.converter.CometFormatConverter;
import org.fao.fi.comet.bridge.mapping.model.CometDataProvider;
import org.fao.fi.comet.bridge.mapping.model.CometMappingConfiguration;
import org.fao.fi.comet.bridge.mapping.model.CometMappingData;
import org.fao.fi.comet.core.exceptions.MatchletConfigurationException;
import org.fao.fi.comet.core.matchlets.skeleton.ScalarMatchletSkeleton;
import org.fao.fi.comet.core.model.engine.DataIdentifier;
import org.fao.fi.comet.core.model.engine.Matching;
import org.fao.fi.comet.core.model.engine.MatchingDetails;
import org.fao.fi.comet.core.model.engine.MatchingEngineProcessConfiguration;
import org.fao.fi.comet.core.model.engine.MatchingEngineProcessResult;
import org.fao.fi.comet.core.model.engine.MatchingResult;
import org.fao.fi.comet.core.model.engine.MatchingResultData;
import org.fao.fi.comet.core.model.engine.MatchingsData;
import org.fao.fi.comet.core.model.matchlets.Matchlet;
import org.fao.fi.comet.core.model.matchlets.MatchletConfiguration;
import org.fao.fi.comet.core.model.matchlets.support.MatchingSerializationExclusionPolicy;
import org.fao.fi.comet.core.model.score.support.MatchingScore;
import org.fao.fi.comet.core.patterns.data.providers.ProvidedData;
import org.fao.fi.comet.domain.codelists.mapping.patterns.data.providers.impl.MaterializedExistingVirtualRepoCodelistProvider;
import org.fao.fi.comet.domain.codelists.mapping.ws.rest.exceptions.AbstractManagedException;
import org.fao.fi.comet.domain.codelists.mapping.ws.rest.exceptions.MatchingException;
import org.fao.fi.comet.domain.codelists.model.CodelistData;
import org.fao.fi.comet.domain.codelists.model.meta.CodelistMappingMetadata;
import org.fao.fi.sh.utility.core.helpers.singletons.io.JAXBHelper;
import org.virtual.sr.RepositoryPlugin;
import org.virtualrepository.Property;
import org.virtualrepository.RepositoryService;
import org.virtualrepository.comet.CometAsset;
import org.virtualrepository.csv.CsvAsset;
import org.virtualrepository.tabular.Table;

import flexjson.JSONDeserializer;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 27 Jun 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 27 Jun 2013
 */
@Path("/confirm")
@Singleton
public class ConfirmationService extends CommonMatchingService {
	final static private ManualMappingMatchlet DUMMY_MANUAL_MATCHLET = new ManualMappingMatchlet();
	final static private MatchingEngineProcessConfiguration DUMMY_MATCHING_ENGINE_CONFIGURATION;
	
	static {
		try {
			DUMMY_MATCHING_ENGINE_CONFIGURATION = ConfirmationService.getDummyMatchingEngineConfiguration();
		} catch(Throwable t) {
			throw new RuntimeException("Unable to configure the dummy matching engine on startup: " + t.getClass().getSimpleName() + " (" + t.getMessage() + ")", t);
		}
	}
		
	/**
	 * Class constructor
	 */
	public ConfirmationService() throws MatchletConfigurationException {
		super();
	}

	@POST
	@Path("/matchings")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public void confirmSynteticMatchings(@Context HttpServletRequest request, @FormParam("requestID") String requestID, @FormParam("mappings") String mappings) throws Throwable {
		HttpSession session = this.getSession(request);

		try {
			MatchingEngineProcessResult<CodelistData, CodelistData, MatchingEngineProcessConfiguration> results = RESULTS_MAP.get(requestID);
			
			if(results == null)
				throw new MatchingException(Response.Status.NOT_FOUND, "No matching result available for given requestID");
			
			this.doConfirmSynteticMappings(session, results, new JSONDeserializer<Map<String, ArrayList<String>>>().deserialize(mappings));
		} catch (Throwable t) {
			throw this.handle(t);
		}
	}

	private void doConfirmSynteticMappings(HttpSession session, MatchingEngineProcessResult<CodelistData, CodelistData, MatchingEngineProcessConfiguration> results, Map<String, ArrayList<String>> mappings) throws Throwable {
		CodelistMappingMetadata meta = (CodelistMappingMetadata)session.getAttribute(SessionConstants.CODELISTS_META);
		
		String sourceCodelistID = meta.getSource().getCodelistId();
		String targetCodelistID = meta.getTarget().getCodelistId();

		Map<String, Collection<String>> convertedMappings = this.doConvertMappings(sourceCodelistID, targetCodelistID, mappings);

		MatchingsData<CodelistData, CodelistData> matchings = results.getResults();

		@SuppressWarnings("unchecked")
		MatchingsData<CodelistData, CodelistData> confirmedMappings =
			JAXBHelper.fromXML(
				MatchingsData.class,
				new Class[] {
					CodelistData.class
				},
				JAXBHelper.toXML(
					new Class[] {
						CodelistData.class
					}, matchings
				)
			);

		Map<String, MatchingDetails<CodelistData, CodelistData>> detailsMap = confirmedMappings.asMap();
		Map<String, Matching<CodelistData, CodelistData>> matchingsMap;

		Set<String> detailsKeys = new HashSet<String>(detailsMap.keySet());
		Set<String> matchingsKeys;

		for(String key : detailsKeys) {
			if(!convertedMappings.containsKey(key)) {
				detailsMap.remove(key);
			} else {
				matchingsMap = detailsMap.get(key).asMap();

				matchingsKeys = new HashSet<String>(matchingsMap.keySet());

				for(String value : matchingsKeys) {
					if(!convertedMappings.get(key).contains(value))
						matchingsMap.remove(value);
				}
			}
		}

		session.setAttribute(SessionConstants.MAPPING_CONFIRMED, confirmedMappings);
	}
	
	@PUT
	@Path("/matchings")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public void confirmManualMatchings(@Context HttpServletRequest request, @FormParam("mappings") String mappings) throws Throwable {
		HttpSession session = this.getSession(request);

		try {
			this.doConfirmManualMappings(session, new JSONDeserializer<Map<String, ArrayList<String>>>().deserialize(mappings));
		} catch (Throwable t) {
			throw this.handle(t);
		}
	}

	private void doConfirmManualMappings(HttpSession session, Map<String, ArrayList<String>> mappings) throws Throwable {
		CodelistMappingMetadata meta = (CodelistMappingMetadata)session.getAttribute(SessionConstants.CODELISTS_META);
		
		String sourceCodelistID = meta.getSource().getCodelistId();
		String targetCodelistID = meta.getTarget().getCodelistId();
	
		String sourceCodelistCode = meta.getSource().getSelectedCodeColumn();
		String targetCodelistCode = meta.getTarget().getSelectedCodeColumn();

		Map<String, Collection<String>> manualMappings = this.doConvertMappings(sourceCodelistID, targetCodelistID, mappings);

		CsvAsset sourceAsset = (CsvAsset)REPO.lookup(sourceCodelistID);
		CsvAsset targetAsset = (CsvAsset)REPO.lookup(targetCodelistID);
		
		Map<String, CodelistData> sourceData = this.doGetAssetDataMap(sourceCodelistCode, new MaterializedExistingVirtualRepoCodelistProvider(sourceCodelistID, REPO.retrieve(sourceAsset, Table.class)));
		Map<String, CodelistData> targetData = this.doGetAssetDataMap(targetCodelistCode, new MaterializedExistingVirtualRepoCodelistProvider(targetCodelistID, REPO.retrieve(targetAsset, Table.class)));
		
		MatchingsData<CodelistData, CodelistData> confirmedMatchings = new MatchingsData<CodelistData, CodelistData>();
		MatchingDetails<CodelistData, CodelistData> matchingDetails;
		Matching<CodelistData, CodelistData> matching;
		
		Map<String, MatchingDetails<CodelistData, CodelistData>> overallMatchingsMap = confirmedMatchings.asMap();
		Map<String, Matching<CodelistData, CodelistData>> matchingsMap;
		
		Collection<MatchingResultData<CodelistData, CodelistData>> matchingData;

		CodelistData source, target;
		
		String sourceID, targetID;
		
		for(String sourceUID : manualMappings.keySet()) {
			matchingDetails = overallMatchingsMap.get(sourceUID);

			source = sourceData.get(sourceUID);
			sourceID = (String)source.getColumn(sourceCodelistCode);
			
			if(matchingDetails == null) {
				matchingDetails = new MatchingDetails<CodelistData, CodelistData>();
				matchingDetails.setSource(source);
				matchingDetails.setSourceProviderId(sourceCodelistID);
				matchingDetails.setSourceId(sourceID);
				matchingDetails.setSourceIdentifier(new DataIdentifier(sourceCodelistID, matchingDetails.getSourceId()));
				
				overallMatchingsMap.put(sourceUID, matchingDetails);
			} 
			
			matchingsMap = matchingDetails.asMap();
			
			for(String targetUID : manualMappings.get(sourceUID)) {
				matching = matchingsMap.get(targetUID);
				
				target = targetData.get(targetUID);
				targetID = (String)target.getColumn(targetCodelistCode);
				
				if(matching == null) {
					matching = new Matching<CodelistData, CodelistData>();
					matching.setSourceId(sourceID);
					matching.setTarget(target);
					matching.setTargetProviderId(targetCodelistID);
					matching.setTargetId(targetID);
					matching.setTargetIdentifier(new DataIdentifier(targetCodelistID, matching.getTargetId()));

					matchingsMap.put(targetUID, matching);
				}
				
				matching.setScore(MatchingScore.getAuthoritativeFullMatchTemplate());
				matching.setProcessID("@ManualMapping");
				
				matchingData = new ArrayList<MatchingResultData<CodelistData, CodelistData>>();
				matchingData.add(
					new MatchingResultData<CodelistData, CodelistData>(
						MatchingScore.getAuthoritativeFullMatchTemplate(),
						source,
						new DataIdentifier(sourceCodelistID, sourceUID),
						Arrays.asList(new CodelistData[] { target })
					)
				);
				
				matching.getMatchingResults().
					add(
						new MatchingResult<CodelistData, CodelistData>(
							DUMMY_MANUAL_MATCHLET, 
							MatchingScore.getAuthoritativeFullMatchTemplate(),
							matchingData
						)
					);
			}
		}

		session.setAttribute(SessionConstants.MAPPING_CONFIG, DUMMY_MATCHING_ENGINE_CONFIGURATION);
		session.setAttribute(SessionConstants.MAPPING_CONFIRMED, confirmedMatchings);
	}
	
	private Map<String, Collection<String>> doConvertMappings(String sourceCodelistID, String targetCodelistID, Map<String, ArrayList<String>> mappings) {
		Map<String, Collection<String>> convertedMappings = new HashMap<String, Collection<String>>();

		DataIdentifier sid, tid;

		Collection<String> targets;

		for(String source : mappings.keySet()) {
			sid = new DataIdentifier(sourceCodelistID, source);

			if(!convertedMappings.containsKey(sid.toUID())) {
				convertedMappings.put(sid.toUID(), new ArrayList<String>());
			}

			targets = convertedMappings.get(sid.toUID());

			for(String target : (ArrayList<String>)mappings.get(source)) {
				tid = new DataIdentifier(targetCodelistID, target);

				targets.add(tid.toUID());

				this._log.info("Confirmed mapping from {} to {}",
								new DataIdentifier(sourceCodelistID, source).toUID(),
								new DataIdentifier(targetCodelistID, target).toUID());
			}
		}

		return convertedMappings;
	}

	private Map<String, CodelistData> doGetAssetDataMap(String codeColumn, MaterializedExistingVirtualRepoCodelistProvider provider) {
		Map<String, CodelistData> data = new HashMap<String, CodelistData>();
			
		for(ProvidedData<CodelistData> in : provider) {
			data.put(new DataIdentifier(provider.getProviderID(), (String)in.getData().getColumn(codeColumn)).toUID(), in.getData());
		}
		
		return data;
	}
	
	@GET
	@Path("/results")
	@Produces(MediaType.APPLICATION_XML + "; charset=UTF-8")
	@SuppressWarnings("unchecked")
	public Response results(@Context HttpServletRequest request) throws AbstractManagedException {
		try {
			HttpSession session = this.getSession(request);

			MatchingEngineProcessResult<CodelistData, CodelistData, MatchingEngineProcessConfiguration> result = new MatchingEngineProcessResult<CodelistData, CodelistData, MatchingEngineProcessConfiguration>();

			result.setResults((MatchingsData<CodelistData, CodelistData>)session.getAttribute(SessionConstants.MAPPING_CONFIRMED));

			if(result.getResults() == null)
				throw new MatchingException(Response.Status.BAD_REQUEST, "No confirmed mappings available in current HTTP session");

			return Response.ok(
				JAXBHelper.toXML(
					new Class[] {
						CodelistData.class
					}, result
				), MediaType.APPLICATION_XML
			).build();
		} catch (Throwable t) {
			throw this.handle(t);
		}
	}

	@POST
	@Path("/persist/{mode}")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_XML + "; charset=UTF-8")
	public CometMappingData persistMappings(@Context HttpServletRequest request, @PathParam(value="mode") String mode, @FormParam("id") String id, @FormParam("name") String name, @FormParam("version") String version, @FormParam("author") String author, @FormParam("description") String description) throws AbstractManagedException {
		HttpSession session = this.getSession(request);

		boolean verbose = "verbose".equalsIgnoreCase(mode);
		
		mode = verbose ? "verbose" : "barebone";

		try {
			CodelistMappingMetadata codelists = (CodelistMappingMetadata)session.getAttribute(SessionConstants.CODELISTS_META);

			MatchingEngineProcessConfiguration config = (MatchingEngineProcessConfiguration)session.getAttribute(SessionConstants.MAPPING_CONFIG);
	
			CometDataProvider sourceDataProvider = provider("http://provider.comet.fi.fao.org/" + MaterializedExistingVirtualRepoCodelistProvider.class.getName()).of("http://data.comet.fi.fao.org/" + CodelistData.class.getName()).named("urn:" + codelists.getSource().getCodelistId());
			CometDataProvider targetDataProvider = provider("http://provider.comet.fi.fao.org/" + MaterializedExistingVirtualRepoCodelistProvider.class.getName()).of("http://data.comet.fi.fao.org/" + CodelistData.class.getName()).named("urn:" + codelists.getTarget().getCodelistId());
			
			@SuppressWarnings("unchecked")
			MatchingsData<CodelistData, CodelistData> mappings = (MatchingsData<CodelistData, CodelistData>)session.getAttribute(SessionConstants.MAPPING_CONFIRMED);

			CometFormatConverter<CodelistData, CodelistData, MatchingEngineProcessConfiguration> converter = new CometFormatConverter<CodelistData, CodelistData, MatchingEngineProcessConfiguration>();
			CometMappingData mappingData = converter.convert(config, sourceDataProvider, targetDataProvider, mappings, CodelistData.class, CodelistData.class, id, version, author, description, verbose);
			
			String finalName = name;
			
			if(finalName == null || finalName.trim().equals(""))
				finalName = id;
			
			return this.doPersist(mappingData, mode, finalName);
		} catch (Throwable t) {
			throw this.handle(t);
		}
	}
	
	@POST
	@Path("/download/{mode}")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	@Produces(MediaType.APPLICATION_XML + "; charset=UTF-8")
	public Response downloadMappings(@Context HttpServletRequest request, @PathParam(value="mode") String mode, @FormParam("id") String id, @FormParam("name") String name, @FormParam("version") String version, @FormParam("author") String author, @FormParam("description") String description) throws AbstractManagedException {
		HttpSession session = this.getSession(request);

		boolean verbose = "verbose".equalsIgnoreCase(mode);
		
		mode = verbose ? "verbose" : "barebone";

		try {
			CodelistMappingMetadata codelists = (CodelistMappingMetadata)session.getAttribute(SessionConstants.CODELISTS_META);

			MatchingEngineProcessConfiguration config = (MatchingEngineProcessConfiguration)session.getAttribute(SessionConstants.MAPPING_CONFIG);
	
			CometDataProvider sourceDataProvider = provider("http://provider.comet.fi.fao.org/" + MaterializedExistingVirtualRepoCodelistProvider.class.getName()).of("http://data.comet.fi.fao.org/" + CodelistData.class.getName()).named("urn:" + codelists.getSource().getCodelistId());
			CometDataProvider targetDataProvider = provider("http://provider.comet.fi.fao.org/" + MaterializedExistingVirtualRepoCodelistProvider.class.getName()).of("http://data.comet.fi.fao.org/" + CodelistData.class.getName()).named("urn:" + codelists.getTarget().getCodelistId());
			
			@SuppressWarnings("unchecked")
			MatchingsData<CodelistData, CodelistData> mappings = (MatchingsData<CodelistData, CodelistData>)session.getAttribute(SessionConstants.MAPPING_CONFIRMED);

			CometFormatConverter<CodelistData, CodelistData, MatchingEngineProcessConfiguration> converter = new CometFormatConverter<CodelistData, CodelistData, MatchingEngineProcessConfiguration>();
			CometMappingData mappingData = converter.convert(config, sourceDataProvider, targetDataProvider, mappings, CodelistData.class, CodelistData.class, id, version, author, description, verbose);
			
			String finalName = name;
			
			if(finalName == null || finalName.trim().equals(""))
				finalName = id;
			
			byte[] content = 
				JAXBHelper.toXML(
					new Class[] {
						CodelistData.class
					},
					mappingData
				).getBytes("UTF-8");
			
			try(ByteArrayInputStream bais = new ByteArrayInputStream(content)) {
				ResponseBuilder responseBuilder = Response.ok((Object)bais);
				responseBuilder.header("Content-Disposition", "attachment; filename=\"" + finalName + "_" + mode + ".xml\"");
				
				return responseBuilder.build();
			}
		} catch (Throwable t) {
			throw this.handle(t);
		}
	}
	
	private CometMappingData doPersist(CometMappingData mappingData, String mode, String name) throws IOException, JAXBException {
		RepositoryService serviceFS = REPO.services().lookup(new QName("MappingFileRepo"));
		@SuppressWarnings("unused")
		RepositoryService serviceSR = REPO.services().lookup(RepositoryPlugin.name);

		byte[] content = 
			JAXBHelper.toXML(
				new Class[] {
					CodelistData.class
				},
				mappingData
			).getBytes("UTF-8");
		
		String config = 
			JAXBHelper.toXML(
				new Class[] { CometMappingConfiguration.class },
				mappingData.getConfiguration()
			).replaceAll("\t", " ").replaceAll(">\\s+<", "><").replaceAll("\r|\n", "");
		
		ByteArrayInputStream bais = new ByteArrayInputStream(content);
		
		CometAsset asset = new CometAsset(name + "_" + mode, serviceFS);
		
		asset.properties().add(
			new Property(
				"configuration", 
				config
			)
		);
		
		REPO.publish(asset, bais);
		
		bais.close();

		//Temporarily removing the publication of COMET mappings on the semantic repo
		//due to the unavailability of the remote server

		/**
		bais = new ByteArrayInputStream(content);
		
		asset = new CometAsset(name + "_" + mode, serviceSR);
		
		asset.properties().add(
			new Property(
				"configuration", 
				config
			)
		);
		
		try {
			REPO.publish(asset, bais);
		
			bais.close();
		} catch(Throwable t) {
			_log.error("Unable to publish asset through the Semantic Repository: {}", t.getMessage(), t);
		}
		**/
		
		return mappingData;
	}
	
	static private class ManualMappingMatchlet extends ScalarMatchletSkeleton<CodelistData, CodelistData, CodelistData, CodelistData> implements Matchlet<CodelistData, CodelistData, CodelistData, CodelistData> {
		/** Field serialVersionUID */
		private static final long serialVersionUID = -9143129658517898461L;
		
		/**
		 * Class constructor
		 *
		 */
		public ManualMappingMatchlet() {
			super();
			
			this._id = "matchlet#1";
			this._name = this.getClass().getSimpleName();
			this._type = this._name;
			this._weight = 100.0;
			this._exclusionCases = new MatchingSerializationExclusionPolicy[] { MatchingSerializationExclusionPolicy.NEVER };
			this._isOptional = false;
			this._isCutoff = false;
			this._minimumScore = MatchingScore.SCORE_FULL_MATCH;
		}

		/* (non-Javadoc)
		 * @see org.fao.fi.comet.core.model.matchlets.ScalarMatchlet#extractSourceData(java.lang.Object, org.fao.fi.comet.core.model.engine.DataIdentifier)
		 */
		@Override
		public CodelistData extractSourceData(CodelistData source, DataIdentifier sourceIdentifier) {
			return source;
		}

		/* (non-Javadoc)
		 * @see org.fao.fi.comet.core.model.matchlets.ScalarMatchlet#extractTargetData(java.lang.Object, org.fao.fi.comet.core.model.engine.DataIdentifier)
		 */
		@Override
		public CodelistData extractTargetData(CodelistData target, DataIdentifier targetIdentifier) {
			return target;
		}

		/* (non-Javadoc)
		 * @see org.fao.fi.comet.core.model.matchlets.Matchlet#getDescription()
		 */
		@Override
		public String getDescription() {
			return "Dummy matchlet for manual mapping operations";
		}

		/* (non-Javadoc)
		 * @see org.fao.fi.comet.core.model.matchlets.Matchlet#computeScore(java.lang.Object, org.fao.fi.comet.core.model.engine.DataIdentifier, java.lang.Object, java.lang.Object, org.fao.fi.comet.core.model.engine.DataIdentifier, java.lang.Object)
		 */
		@Override
		public MatchingScore computeScore(CodelistData source, DataIdentifier sourceIdentifier, CodelistData sourceData, CodelistData target, DataIdentifier targetIdentifier, CodelistData targetData) {
			return MatchingScore.getAuthoritativeFullMatchTemplate();
		}
	}
	
	static private MatchingEngineProcessConfiguration getDummyMatchingEngineConfiguration() throws MatchletConfigurationException {
		MatchingEngineProcessConfiguration engineConfiguration = new MatchingEngineProcessConfiguration();
		
		engineConfiguration.setHaltAtFirstValidMatching(false);
		engineConfiguration.setHandleErrors(false);
		engineConfiguration.setMinimumAllowedWeightedScore(MatchingScore.SCORE_FULL_MATCH);
		engineConfiguration.setMaxCandidatesPerEntry(MatchingEngineProcessConfiguration.MAX_CANDIDATES_PER_ENTRY_UNBOUND);
		
		MatchletConfiguration matchletConfiguration = new MatchletConfiguration();
		matchletConfiguration.setMatchletId(DUMMY_MANUAL_MATCHLET.getId());
		matchletConfiguration.setMatchletName(DUMMY_MANUAL_MATCHLET.getName());
		matchletConfiguration.setMatchletType(DUMMY_MANUAL_MATCHLET.getType());
		matchletConfiguration.setMatchletParameters(DUMMY_MANUAL_MATCHLET.getConfiguration());
		
		engineConfiguration.setMatchletsConfiguration(Arrays.asList(new MatchletConfiguration[] { matchletConfiguration }));
		
		return engineConfiguration;
	}
}