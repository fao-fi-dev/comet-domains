/**
 * (c) 2015 FAO / UN (project: comet-codelist-mapping-ws)
 */
package org.fao.fi.comet.domain.codelists.mapping.ws.rest;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.fao.fi.comet.core.engine.process.handlers.MatchingProcessHandler;
import org.fao.fi.comet.core.model.engine.MatchingEngineProcessConfiguration;
import org.fao.fi.comet.core.model.engine.MatchingEngineProcessResult;
import org.fao.fi.comet.domain.codelists.model.CodelistData;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 13 Jan 2015   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 13 Jan 2015
 */
public abstract class CommonMatchingService extends CommonManagementService {
	static final protected Map<String, MatchingProcessHandler<CodelistData, CodelistData>> TRACKERS_MAP = Collections.synchronizedMap(new HashMap<String, MatchingProcessHandler<CodelistData, CodelistData>>());
	static final protected Map<String, MatchingEngineProcessResult<CodelistData, CodelistData, MatchingEngineProcessConfiguration>> RESULTS_MAP = Collections.synchronizedMap(new HashMap<String, MatchingEngineProcessResult<CodelistData, CodelistData, MatchingEngineProcessConfiguration>>());
	static final protected Map<String, MatchingEngineProcessConfiguration> CONFIGURATIONS_MAP = Collections.synchronizedMap(new HashMap<String, MatchingEngineProcessConfiguration>());

}
