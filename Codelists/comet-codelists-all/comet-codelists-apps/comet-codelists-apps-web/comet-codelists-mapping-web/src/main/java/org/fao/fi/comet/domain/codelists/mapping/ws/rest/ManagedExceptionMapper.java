package org.fao.fi.comet.domain.codelists.mapping.ws.rest;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

import org.fao.fi.comet.domain.codelists.mapping.ws.rest.exceptions.AbstractManagedException;

@Provider
public class ManagedExceptionMapper implements ExceptionMapper<AbstractManagedException> {
	/* (non-Javadoc)
	 * @see javax.ws.rs.ext.ExceptionMapper#toResponse(java.lang.Throwable)
	 */
	@Override
	public Response toResponse(AbstractManagedException exception) {
		return Response.status(exception.getStatusCode()).entity(exception.getMessage()).build();
	}
}
