package org.fao.fi.comet.domain.codelists.mapping.ws.rest.exceptions.repo;

import javax.ws.rs.core.Response;

import org.fao.fi.comet.domain.codelists.mapping.ws.rest.exceptions.AbstractManagedException;

public class CodelistMetadataException extends AbstractManagedException {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 4224454472956931484L;

	/**
	 * Class constructor
	 *
	 * @param statusCode
	 * @param message
	 */
	public CodelistMetadataException(Response.Status statusCode, String message) {
		super(statusCode, message);
	}

	/**
	 * Class constructor
	 *
	 * @param statusCode
	 * @param cause
	 */
	public CodelistMetadataException(Response.Status statusCode, Throwable cause) {
		super(statusCode, cause);
	}
}
