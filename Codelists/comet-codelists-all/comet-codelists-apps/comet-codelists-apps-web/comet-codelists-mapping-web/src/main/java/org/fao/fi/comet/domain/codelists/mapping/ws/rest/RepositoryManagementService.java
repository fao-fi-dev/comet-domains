/**
 * (c) 2013 FAO / UN (project: comet-codelist-mapping-ws)
 */
package org.fao.fi.comet.domain.codelists.mapping.ws.rest;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import javax.inject.Singleton;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.namespace.QName;

import org.fao.fi.comet.domain.codelists.mapping.ws.rest.exceptions.AbstractManagedException;
import org.fao.fi.comet.domain.codelists.mapping.ws.rest.exceptions.repo.CodelistDataException;
import org.fao.fi.comet.domain.codelists.mapping.ws.rest.exceptions.repo.CodelistMetadataException;
import org.fao.fi.comet.domain.codelists.mapping.ws.rest.exceptions.repo.GenericRepoException;
import org.fao.fi.comet.domain.codelists.model.meta.CodelistMappingMetadata;
import org.fao.fi.comet.domain.codelists.model.meta.CodelistMetadata;
import org.virtualrepository.Asset;
import org.virtualrepository.csv.CsvAsset;
import org.virtualrepository.csv.CsvCodelist;
import org.virtualrepository.tabular.Table;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 27 Jun 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 27 Jun 2013
 */
@Path("/repo")
@Singleton
public class RepositoryManagementService extends CommonManagementService {
	/**
	 * Class constructor
	 */
	public RepositoryManagementService() {
		super();
	}
	
	@XmlRootElement
	@XmlAccessorType(XmlAccessType.FIELD)
	public class Codelist implements Serializable {
		/** Field serialVersionUID */
		private static final long serialVersionUID = 5773295072079312965L;

		@XmlElement(name="Service")
		private String _service;
		
		@XmlElement(name="Id")
		private String _id;
		
		@XmlElement(name="Name")
		private String _name;

		/**
		 * Class constructor
		 *
		 */
		public Codelist() {
			super();
			// TODO Auto-generated constructor stub
		}

		/**
		 * Class constructor
		 *
		 * @param service
		 * @param id
		 * @param name
		 */
		public Codelist(String service, String id, String name) {
			super();
			this._service = service;
			this._id = id;
			this._name = name;
		}

		/**
		 * @return the 'service' value
		 */
		public String getService() {
			return this._service;
		}

		/**
		 * @param service the 'service' value to set
		 */
		public void setService(String service) {
			this._service = service;
		}

		/**
		 * @return the 'id' value
		 */
		public String getId() {
			return this._id;
		}

		/**
		 * @param id the 'id' value to set
		 */
		public void setId(String id) {
			this._id = id;
		}

		/**
		 * @return the 'name' value
		 */
		public String getName() {
			return this._name;
		}

		/**
		 * @param name the 'name' value to set
		 */
		public void setName(String name) {
			this._name = name;
		}

		/* (non-Javadoc)
		 * @see java.lang.Object#hashCode()
		 */
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + getOuterType().hashCode();
			result = prime * result + ((this._id == null) ? 0 : this._id.hashCode());
			result = prime * result + ((this._name == null) ? 0 : this._name.hashCode());
			result = prime * result + ((this._service == null) ? 0 : this._service.hashCode());
			return result;
		}

		/* (non-Javadoc)
		 * @see java.lang.Object#equals(java.lang.Object)
		 */
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Codelist other = (Codelist) obj;
			if (!getOuterType().equals(other.getOuterType()))
				return false;
			if (this._id == null) {
				if (other._id != null)
					return false;
			} else if (!this._id.equals(other._id))
				return false;
			if (this._name == null) {
				if (other._name != null)
					return false;
			} else if (!this._name.equals(other._name))
				return false;
			if (this._service == null) {
				if (other._service != null)
					return false;
			} else if (!this._service.equals(other._service))
				return false;
			return true;
		}

		private RepositoryManagementService getOuterType() {
			return RepositoryManagementService.this;
		}
	}

	private List<Codelist> doListCodelists(@Context HttpServletRequest request) {
		List<Codelist> codelists = new ArrayList<Codelist>();

		QName serviceName;
		for(Asset asset : REPO) {
			serviceName = asset.service() == null ? null : asset.service().name();
			
			codelists.add(new Codelist(serviceName == null ? null : serviceName.toString(), asset.id(), asset.name()));
		}

		Collections.sort(codelists, new Comparator<Codelist>() {
			@Override
			public int compare(Codelist o1, Codelist o2) {
				String s1 = o1.getService(), s2 = o2.getService();
				
				if(s1 == null && s2 != null)
					return -1;
				
				if(s1 != null && s2 == null)
					return 1;
				
				int result = s1 == null && s2 == null ? 0 : s1.compareTo(s2);
				
				if(result != 0)
					return result;
				
				result = o1.getName().compareTo(o2.getName());
				
				return result == 0 ? o1.getId().compareTo(o2.getId()) : result;
			}
		});

		return codelists;
	}

	@GET
	@Path("/list")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Codelist> listCodelists(@Context HttpServletRequest request) throws AbstractManagedException {
		try {
			return this.doListCodelists(request);
		} catch (Throwable t) {
			throw this.handle(new GenericRepoException(t));
		}
	}

	@GET
	@Path("/describe/{assetId}")
	@Produces(MediaType.APPLICATION_JSON)
	public CodelistMetadata describeCodelist(@Context HttpServletRequest request, @PathParam("assetId") String assetId) throws Throwable {
		try {
			CsvCodelist codelistAsset = null;

			Table codelistTable = null;

			try {
				codelistAsset = (CsvCodelist)REPO.lookup(assetId);
				codelistTable = REPO.retrieve(codelistAsset, Table.class);
			} catch(Throwable t) {
				this._log.error("Unable to describe codelist: {}", t.getMessage(), t);
				
				throw new CodelistMetadataException(Response.Status.BAD_REQUEST, t.getMessage());
			}

			CodelistMetadata meta = this.getMetadataForAsset(assetId, codelistAsset, codelistTable);

			if(meta == null)
				throw new CodelistMetadataException(Response.Status.NOT_FOUND, "Unable to fetch metadata for asset " + assetId);

			return meta;
		} catch (Throwable t) {
			throw this.handle(t);
		}
	}

	@GET
	@Path("/retrieve/{assetId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Collection<Map<String, String>> retrieve(@Context HttpServletRequest request, @Context HttpServletResponse response, @PathParam("assetId") String assetId) throws Throwable {
		return this.retrieveMaxRows(request, response, assetId, null);
	}

	@GET
	@Path("/retrieve/{assetId}/max/{rows}")
	@Produces(MediaType.APPLICATION_JSON)
	public Collection<Map<String, String>> retrieveMaxRows(@Context HttpServletRequest request, @Context HttpServletResponse response, @PathParam("assetId") String assetId, @PathParam("rows") Integer maxRows) throws Throwable {
		try {
			Asset asset = null;

			try {
				asset = REPO.lookup(assetId);
			} catch (Throwable t) {
				this._log.error("Unable to lookup asset {}: {} caught ({})", assetId, t.getClass().getName(), t.getMessage(), t);

				throw new CodelistMetadataException(Response.Status.BAD_REQUEST, t.getMessage());
			}

			if(asset == null)
				throw new CodelistDataException(Response.Status.NOT_FOUND, "Unable to fetch data for asset " + assetId);

			TableData data = null;

			try {
				data = this.doRetrieveAssetAsTable((CsvAsset)asset, maxRows);
			} catch (Throwable t) {
				this._log.error("Unable to retrieve asset data for {}: {} caught ({})", assetId, t.getClass().getName(), t.getMessage(), t);

				throw new CodelistMetadataException(Response.Status.BAD_REQUEST, t.getMessage());
			}

			response.setHeader("MAX_ROWS_COUNT", String.valueOf(data.getMaxRows()));
			response.setHeader("CURRENT_ROWS_COUNT", String.valueOf(data.getCurrentRows()));

			return data.getData();
		} catch (Throwable t) {
			throw this.handle(t);
		}
	}

	@GET
	@Path("/select/source/{sourceCodelistId}/id/{sourceCodelistIdColumn}/target/{targetCodelistId}/id/{targetCodelistIdColumn}")
	public void select(@Context HttpServletRequest request,
					   @PathParam(value="sourceCodelistId") String sourceCodelistId,
					   @PathParam(value="sourceCodelistIdColumn") String sourceCodelistIdColumn,
					   @PathParam(value="targetCodelistId") String targetCodelistId,
					   @PathParam(value="targetCodelistIdColumn") String targetCodelistIdColumn) throws AbstractManagedException {
		try {
			HttpSession session = this.getSession(request);

			CodelistMetadata sourceMeta = null;

			CsvCodelist source, target;
			Table sourceTable, targetTable;

			try {
				source = (CsvCodelist)REPO.lookup(sourceCodelistId);
				sourceTable = REPO.retrieve(source, Table.class);
				sourceMeta = this.getMetadataForAsset(sourceCodelistId, source, sourceTable);
			} catch(Throwable t) {
				this._log.error("Unable to describe codelist: {}", t.getMessage(), t);
				
				throw new CodelistMetadataException(Response.Status.BAD_REQUEST, t);
			}

			CodelistMetadata targetMeta = null;

			try {
				target = (CsvCodelist)REPO.lookup(targetCodelistId);
				targetTable = REPO.retrieve(target, Table.class);
				targetMeta = this.getMetadataForAsset(targetCodelistId, target, targetTable);
			} catch(Throwable t) {
				this._log.error("Unable to describe codelist: {}", t.getMessage(), t);
				
				throw new CodelistMetadataException(Response.Status.BAD_REQUEST, t);
			}

			if(sourceMeta == null || targetMeta == null)
				throw new CodelistMetadataException(Response.Status.NOT_FOUND,
													"Source codelist metadata are " + ( sourceMeta == null ? "" : "not " ) + "NULL. " +
													"Target codelist metadata are " + ( targetMeta == null ? "" : "not " ) + "NULL.");

			if(sourceMeta.getColumnsMetadata() == null)
				throw new CodelistMetadataException(Response.Status.BAD_REQUEST, "No columns defined for asset " + sourceCodelistId);

			if(targetMeta.getColumnsMetadata() == null)
				throw new CodelistMetadataException(Response.Status.BAD_REQUEST, "No columns defined for asset " + targetCodelistId);

			sourceMeta.setSelectedCodeColumn(sourceCodelistIdColumn);
			targetMeta.setSelectedCodeColumn(targetCodelistIdColumn);

			session.setAttribute(SessionConstants.SOURCE_CODELIST_ID, sourceCodelistId);
			session.setAttribute(SessionConstants.TARGET_CODELIST_ID, targetCodelistId);

			session.setAttribute(SessionConstants.SOURCE_CODELIST_ASSET, source);
			session.setAttribute(SessionConstants.TARGET_CODELIST_ASSET, target);

			session.setAttribute(SessionConstants.SOURCE_CODELIST_META, sourceMeta);
			session.setAttribute(SessionConstants.TARGET_CODELIST_META, targetMeta);

			session.setAttribute(SessionConstants.CODELISTS_META, new CodelistMappingMetadata(sourceMeta, targetMeta));
		} catch (Throwable t) {
			throw this.handle(t);
		}
	}
}