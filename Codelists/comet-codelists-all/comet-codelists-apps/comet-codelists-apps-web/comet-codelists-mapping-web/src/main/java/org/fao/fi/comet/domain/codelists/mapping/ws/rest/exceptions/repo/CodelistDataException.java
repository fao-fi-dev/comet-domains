package org.fao.fi.comet.domain.codelists.mapping.ws.rest.exceptions.repo;

import javax.ws.rs.core.Response;

import org.fao.fi.comet.domain.codelists.mapping.ws.rest.exceptions.AbstractManagedException;

public class CodelistDataException extends AbstractManagedException {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 4224454472956931484L;

	/**
	 * Class constructor
	 *
	 * @param statusCode
	 * @param message
	 */
	public CodelistDataException(Response.Status statusCode, String message) {
		super(statusCode, message);
	}

	/**
	 * Class constructor
	 *
	 * @param statusCode
	 * @param cause
	 */
	public CodelistDataException(Response.Status statusCode, Throwable cause) {
		super(statusCode, cause);
	}
}
