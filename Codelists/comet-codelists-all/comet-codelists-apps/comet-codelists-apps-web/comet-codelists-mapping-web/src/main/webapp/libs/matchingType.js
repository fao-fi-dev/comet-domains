function initializationCallback(oncompletion) {
	buildCodelistsInfo(oncompletion);
};

function route() {
	var selected = $("input[name='matchingType']:checked").val();
	
	if(selected === "synth")
		goTo("syntheticMatching.jsp");
	else
		goTo("manualMatching.jsp");
};