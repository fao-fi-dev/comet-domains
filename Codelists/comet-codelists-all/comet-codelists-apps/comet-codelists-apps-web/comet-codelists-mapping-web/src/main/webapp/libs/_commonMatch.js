function sortData(a, b) { return b.name < a.name ? 1 : b.name === a.name ? 0 : -1; };

function buildCodelistsInfo(callback) {
	$.ajax({
		url: _WS_MATCH_ENDPOINT + "/configuration/codelists",
		success: function(data) {
			var source = data.source;
			var target = data.target;

			if(!isSet(source)) {
				alert("No source codelist selected in current user session");

				return;
			}

			if(!isSet(target)) {
				alert("No target codelist selected in current user session");

				return;
			}

			$("#sourceCodelistId").text(source.codelistId);
			$("#sourceCodelistCodeColumn").text(source.selectedCodeColumn == null ? source.defaultCodeColumn : source.selectedCodeColumn);
			$("#sourceCodelistName").text(source.codelistName);
			$("#targetCodelistId").text(target.codelistId);
			$("#targetCodelistCodeColumn").text(target.selectedCodeColumn == null ? target.defaultCodeColumn : target.selectedCodeColumn);
			$("#targetCodelistName").text(target.codelistName);

			$("#selectedCodelists [title]").tooltip({ position: { my: 'left bottom', at: 'left top-5'}});

			SOURCE_CODELIST = source;
			TARGET_CODELIST = target;

			if(isSet(callback)) callback();
		}
	});
};

function buildMatchingsTree(data, enableSelection) {
	var tree = $("#matchingsTree");

	var matchingDetails = [];

	var $matchingDetails = $("MatchingDetails", data);

	$(".matchingResultsTotal").text($matchingDetails.size());

	$matchingDetails.each(function() {
		var detail = {};
		var $detail = $(this);
		var $source = $("Source", $detail);

		detail.sourceProviderId = $detail.attr("sourceProviderId");
		detail.sourceId = $detail.attr("sourceId");
		detail.sourceType = $source.attr("xsi:type");

		if(isSet(detail.sourceType))
			detail.sourceType = detail.sourceType.replace(/^.+\:/g, '');
		else
			detail.sourceType = "----";

		detail.sourceData = extractRowData($source);

		detail.totalCandidates = $detail.attr("totalCandidates");
		detail.matchings = [];

		$("Matching", $detail).each(function() {
			detail.matchings.push(extractMatching(data, $(this)));
		});

		matchingDetails.push(detail);
	});

	matchingDetails = matchingDetails.sort(function(a, b) { return b.totalCandidates - a.totalCandidates; });

	for(var m=0; m<matchingDetails.length; m++) {
		tree.append(buildMatchingDetail(matchingDetails[m], enableSelection));
	}
};

function buildMatchingDetail(matchingDetail, enableSelection) {
	var $matchingDetail = $("<li class=\"matchingDetail\"/>");
	$matchingDetail.data("data", matchingDetail);

	var $label = $("<span class=\"sourceLabel\"/>");
	$label.append("<span class=\"sourceType\" title=\"Source type\">" + matchingDetail.sourceType + "&#160;</span>");
	$label.append("<span class=\"sourceProviderId ui-state-highlight\" title=\"The source codelist ID\">" + matchingDetail.sourceProviderId + "</span>");
	$label.append("<span class=\"providerIdSeparator\">#</span>");
	$label.append("<span class=\"sourceId ui-state-highlight\" title=\"The source codelist's code attribute for this entry\">" + matchingDetail.sourceId + "</span>");

	var $report = $("<span class=\"targetsReport\" title=\"Number of identified targets for this source\"/>");

	if(enableSelection)
		$report.text(" [ matches with " + matchingDetail.totalCandidates + " target(s) : ");
	else
		$report.text(" [ mapped to " + matchingDetail.totalCandidates + " target(s) ]");

	$matchingDetail.append($label);
	$label.append($report);

	if(enableSelection) {
		var $tatus = $("<span class=\"targetsStatus\" title=\"Number of confirmed matching targets for this source\"/>");
		$tatus.append("<span><span class=\"confirmedTargets\">0</span> confirmed ]</span>");

		$label.append($tatus);
	}

	var $matchings = $("<ul class=\"targets\"/>");

	for(var m=0; m<matchingDetail.matchings.length; m++)
		$matchings.append(buildMatching(matchingDetail.sourceId, matchingDetail.matchings[m], enableSelection));

	$matchingDetail.append($matchings);

	$matchingDetail.attr("id", "source&" + matchingDetail.sourceId);

	$matchingDetail.tooltip({
		items: ".sourceLabel",
		position: { my: 'left top', at: 'right+5 top' },
		content: function() {
			var matchingDetail = $matchingDetail.data("data");
			var sourceData = matchingDetail.sourceData;

			var tooltip = "<ul class=\"entryDescription\">";

			sourceData = sourceData.sort(sortData);

			for(var c=0;c<sourceData.length; c++) {
				tooltip += "<li class=\"entryAttribute\">";
					tooltip += "<b>" + sourceData[c].name + "</b>";
					tooltip += " [ " + ( isSet(sourceData[c].type) ? sourceData[c].type : "----" ) + " ] : ";
					tooltip += "<code>" + sourceData[c].value + "</code>";
				tooltip += "</li>";
			};

			tooltip += "</ul>";

			return $(tooltip);
		}
	});

	return $matchingDetail;
};

function buildMatching(sourceId, matching, enableSelection) {
	var $matching = $("<li class=\"matching\"/>");
	$matching.data("score", matching.score);
	$matching.data("data", matching);

	var $label = $("<span class=\"targetLabel\"/>");
	$label.append("<span class=\"targetType\" title=\"Target type\">" + matching.targetType + "&#160;</span>");
	$label.append("<span class=\"targetProviderId ui-state-highlight\" title=\"The target codelist ID\">" + matching.targetProviderId + "</span>");
	$label.append("<span class=\"providerIdSeparator\">#</span>");
	$label.append("<span class=\"targetId ui-state-highlight\" title=\"The target codelist's code attribute for this entry\">" + matching.targetId + "</span>");

	var $report = $("<span class=\"overallMatchingScore\" title=\"Overall matching score for this source / target pair\"/>");
	$report.text(" [ Score: " + sprintf("%.2f", parseFloat(matching.score * 100.0)) + "% | " + matching.scoreType + " ]");

	$label.append($report);

	if(enableSelection) {
		var $button = $("<input type=\"button\" class=\"matchingSelectionButton\" value=\"Select >\"></input>");

		$label.append($button);

		$button.click(function(event) {
			var sourceId = $(this).parents(".matchingDetail").attr("id");
			var targetId = $(this).parents(".matching").attr("id");

			selectMatching(sourceId, targetId);

			event.preventDefault();
			return false;
		});

		$button.button();
	}

	$matching.append($label);
	$matching.attr("id", "source&" + sourceId + "&target&" + matching.targetId);

	$matching.tooltip({
		items: ".targetLabel",
		position: { my: 'left top', at: 'right+5 top' },
		content: function() {
			var matching = $matching.data("data");
			var targetData = matching.targetData;

			var tooltip = "<ul class=\"entryDescription\">";

			targetData = targetData.sort(sortData);

			for(var c=0;c<targetData.length; c++) {
				tooltip += "<li class=\"entryAttribute\">";
					tooltip += "<b>" + targetData[c].name + "</b>";
					tooltip += " [ " + ( isSet(targetData[c].type) ? targetData[c].type : "----" )  + " ] : ";
					tooltip += "<code>" + targetData[c].value + "</code>";
				tooltip += "</li>";
			};

			tooltip += "</ul>";

			return $(tooltip);
		}
	});

	var $matchers = $("<ul class=\"matchers ui-helper-clearfix\"/>");

	for(var r=0; r<matching.results.length; r++) {
		$matchers.append(buildMatcherDetail(matching.results[r]));
	}

	$matching.append($matchers);

	return $matching;
};

function buildMatcherDetail(matcher) {
	var $matcher = $("<li class=\"matcher\"/>");
	$matcher.data("data", matcher);

	var $label = $("<span class=\"matcherLabel\"/>");
	$label.append("<span class=\"matcherType\" title=\"Matcher type\"><b>" + matcher.matchletType + "</b></span>");
	$label.append("<span>&#160;[&#160;</span>");
	$label.append("<span class=\"matcherWeight\" title=\"Matcher weight\">Weight: " + sprintf("%.2f", parseFloat(matcher.matchletWeight)) + "&#160;-&#160;</span>");
	$label.append("<span class=\"score\" title=\"Matcher score\">Score: " + sprintf("%.2f", parseFloat(matcher.score * 100.0)) + "%</span>");
	$label.append("<span class=\"scoreType\" title=\"Matcher score type\"> | " + matcher.scoreType + "</span>");
	$label.append("<span>&#160;]</span>");

	$matcher.append($label);

	$matcher.tooltip({
		items: ".matcherLabel",
		position: { my: 'left top', at: 'right+5 top' },
		content: function() {
			var data = $matcher.data("data");
			var matchingData = data.matchingData;

			var tooltip = "<ul class=\"matcherDataDescription\">";

			for(var md=0;md<matchingData.length;md++) {
				var sourceData = matchingData[md].sourceData;
				var targetData = matchingData[md].targetData;
					tooltip += "<li class=\"matcherSourceDataDetail\">";
						tooltip += "<span class=\"matcherSourceDataLabel\">" +
										"<b>Source data #" + ( md + 1) + ":&#160;</b><code>" + sourceData.value + "</code> (" + sourceData.type + ")" +
									 "</span>";
						tooltip += "<span class=\"matcherTargetDataLabel\"><b>Target data:</b></span>";
						tooltip += "<ul class=\"matcherTargetData\">";

						for(var t=0;t<targetData.length; t++) {
							tooltip += "<li class=\"matcherTargetDataDetail\">";

							tooltip += "#" + ( t + 1 ) + ": <code>" + targetData[t].value + "</code> (" + targetData[t].type + ")";

							tooltip += "</li>";
						}

						tooltip += "</ul>";
					tooltip += "</li>";
			}

			tooltip += "</ul>";

			return $(tooltip);
		}
	});

	return $matcher;
};

function extractRowData(element) {
	var rowData = [];

	$("RowColumn", element).each(function() {
		var $column = $(this);
		var columnData = {};

		var type = $("ColumnValue", $column).attr("xsi:type");
		if(type != null)
			type = type.replace(/^.+\:/g, '');

		columnData.name = $("ColumnName", $column).text();
		columnData.type = type;
		columnData.value = $("ColumnValue", $column).text();

		rowData.push(columnData);
	});

	return rowData;
};

function extractMatching(result, element) {
	var matching = {};
	matching.score = element.attr("score");
	matching.scoreType = element.attr("type");

	matching.targetProviderId = element.attr("targetProviderId");
	matching.targetId = element.attr("targetId");

	var $target = $("Target", element);

	matching.targetType = $target.attr("xsi:type");

	if(isSet(matching.targetType))
		matching.targetType = matching.targetType.replace(/^.+\:/g, '');
	else
		matching.targetType = "----";

	matching.targetData = extractRowData($target);

	matching.results = [];

	$("MatchingResult", element).each(function() {
		var $matchingResult = $(this);

		matching.results.push(extractMatchingResult(result, $matchingResult));
	});

	return matching;
};

function extractMatchingResult(result, element) {
	var matchingResult = {};
	matchingResult.matchletType = element.attr("matchletName");
	matchingResult.matchletWeight = element.attr("matchletWeight");
	matchingResult.score = element.attr("score");
	matchingResult.scoreType = element.attr("type");

	matchingResult.matchingData = [];

	$("MatchingResultData", element).each(function() {
		var $matchingData = $(this);

		matchingResult.matchingData.push(extractMatchingResultData(result, $matchingData));
	});

	return matchingResult;
};

function extractMatchingResultData(result, element) {
	var matchingResultData = {};
	matchingResultData.score = element.attr("score");
	matchingResultData.scoreType = element.attr("type");

	matchingResultData.sourceData = extractData(result, $("SourceData", element));
	matchingResultData.targetData = [];

	$("TargetData", element).each(function() {
		var $targetData = $(this);

		matchingResultData.targetData.push(extractData(result, $targetData));
	});

	return matchingResultData;
};

function extractData(result, element) {
	var attr = element.attr("xsi:type");

	var data = {};
	data.type = typeof attr === 'undefined' || attr == null ? "NIL" : attr.replace(/^.+\:/g, "");
	data.value = element.text();

	return data;
};
