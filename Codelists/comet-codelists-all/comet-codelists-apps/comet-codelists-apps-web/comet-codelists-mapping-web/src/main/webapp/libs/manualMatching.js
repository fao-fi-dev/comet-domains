function initializationCallback(oncompletion) {
	buildCodelistsInfo(
		function() { 
			retrieveSourcesData(
				function() { 
					retrieveTargetsData(oncompletion); 
				}
			); 
		}
	);
};

function buildDataTooltip() {
	$(document).tooltip({
		items: ".sampleContent",
		position: { my: "left top", at: "left bottom+4" },
		content: function() {
			var sourceData = $(this).data("content");
	
			var tooltip = "<ul class=\"entryDescription\">";
	
			for(var key in sourceData) {
				tooltip += "<li class=\"entryAttribute\">";
					tooltip += "<b>" + key + ":&nbsp;</b>";
					tooltip += "<code>" + sourceData[key] + "</code>";
				tooltip += "</li>";
			};
	
			tooltip += "</ul>";
	
			return $(tooltip);
		}
	});
}

function retrieveSourcesData(callback) {
	var sourceCodelistId = $("#sourceCodelistId").text();
	
	startOperation("Retrieving source codelist data");
	
	$.ajax({
		url: _WS_REPO_ENDPOINT + "/retrieve/" + sourceCodelistId,
		success: function(response) {
			completeOperation();
			
			callback();
			
			buildSourcesData(response);
		},
		error: function() {
			completeOperation();
		},
		complete: function() {

		}
	});
};

function retrieveTargetsData(callback) {
	var sourceCodelistId = $("#targetCodelistId").text();
	
	startOperation("Retrieving target codelist data");
	
	$.ajax({
		url: _WS_REPO_ENDPOINT + "/retrieve/" + sourceCodelistId,
		success: function(response) {
			completeOperation();
			
			callback();
			
			buildTargetsData(response);
			
			buildDataTooltip();
			
			$("code[title]").tooltip({
				position: { 
					my: 'left-2 top',
					at: 'left bottom'
				}
			});
		},
		error: function() {
			completeOperation();
		},
		complete: function() {

		}
	});
};

function buildTargetsData(data) {
	var targetCodelistCode = $("#targetCodelistCodeColumn").text();
	
	var container = $(".targetData .panelWrapper");
	
	$(".targetData .numEntries").text(data.length);
	
	var entry;
	for(var r=0; r<data.length; r++) {
		entry = $("<div class='entry target noSelect'/>");
		entry.append("<code class='id ui-state-highlight' title='The target entry code: " + data[r][targetCodelistCode] + "'>T: " + data[r][targetCodelistCode] + "</code>");
		
		var sample = $("<span class='sampleContent'/>");
		
		var html = "";
		for(var column in data[r]) {
			if(column !== targetCodelistCode) {
				html += "<label class='enableSelect'>" + column + ": </label>";
				html += "<span class='enableSelect'>" + data[r][column] + ", </span>";
			}
		}
		
		sample.html(html.replace(/\, <\/span>$/g, '</span>'));
		
		sample.data("content", data[r]);
		container.append(entry.append(sample));
	}
	
	$(".entry", container).draggable({
		revert: 'invalid',
		handle: "code",
		helper: function() { return $("<code class='id draggable ui-state-highlight'/>").append($("code", $(this)).text()); },
		appendTo: "body",
		scroll: false
	});
}

function buildSourcesData(data) {
	var sourceCodelistCode = $("#sourceCodelistCodeColumn").text();
	
	var container = $(".sourceData .panelWrapper");

	var entry;
	
	$(".sourceData .numEntries").text(data.length);
	
	for(var r=0; r<data.length; r++) {
		entry = $("<div class='entry source noSelect'/>");
		entry.append("<code class='ui-state-highlight' title='The source entry code: " + data[r][sourceCodelistCode] + "'>S: " + data[r][sourceCodelistCode] + "</code>");
		
		var sample = $("<span class='sampleContent'/>");
		
		var html = "";
		for(var column in data[r]) {
			if(column !== sourceCodelistCode) {
				html += "<label class='enableSelect'>" + column + ": </label>";
				html += "<span class='enableSelect'>" + data[r][column] + ", </span>";
			}
		}
		
		sample.html(html.replace(/\, <\/span>$/g, '</span>'));
		
		sample.data("content", data[r]);
		container.append(entry.append(sample));
	}
	
	$(".entry", container).droppable({
		hoverClass: "drop",
		accept: function(dragged) {
			var draggedId = $("code", $(dragged)).text();
			
			var accept = true;
			
			var receiver = $(this);
			
			$(".target code", receiver).each(function() {
				var targetId = $(this).text();
				
				if(targetId == draggedId)
					accept = false;
			});
			
			dragged.data("accepted", accept);
			
			return accept;
		},
		drop: function(event, ui) {
			var cloned = ui.draggable.clone();
			cloned.addClass("ui-state-hover");
			
			$("button", cloned).remove();
			
			$(".sampleContent", cloned).data("content", $(".sampleContent", ui.draggable).data("content"));
			
			var button = $("<button/>").button({
				 icons: {
					 primary: "ui-icon-trash"
				 },
				 text: false
			});
			
			button.click(function() { $(this).parent(".entry").remove(); checkNextStepAbilitation(); });			
			
			$("code", cloned).after(button);
			$("code", cloned).tooltip({
				position: { 
					my: 'left-2 top',
					at: 'left bottom'
				}
			});
			
			$(this).append(cloned);
			
			if(ui.draggable.hasClass("ui-state-hover"))
				ui.draggable.remove();
			
			var selected = $(".sourceData .ui-state-hover").size();
			$("input[value='Next']").button(selected > 0 ? "enable" : "disable");
			
			cloned.draggable({
				revert: 'invalid',
				handle: "code",
				helper: function() { return $("<code class='id draggable ui-state-highlight mapped'/>").append($("code", $(this)).text()); },
				appendTo: "body",
				scroll: false,
				stop: function(foo) { 
					checkNextStepAbilitation();
				}
			});
		}
	});
};

function checkNextStepAbilitation() {
	var selected = $(".sourceData .ui-state-hover").size();
	$("input[value='Next']").button(selected > 0 ? "enable" : "disable");
}

function confirmMatchings() {
	var matchings = {};

	$(".entry.source", ".sourceData").each(function() {
		var $ourceEntry = $(this);
		
		var targets = $(".ui-state-hover", $ourceEntry);
				
		var $ourceId = $("code:eq(0)", $ourceEntry).text().replace(/^S\:\s/g, '');

		if(targets.size() > 0) {
			if(typeof matchings[$ourceId] === 'undefined')
				matchings[$ourceId] = new Array();
			
			targets.each(function() {
				var $target = $(this);
				var $targetId = $("code:eq(0)", $target).text().replace(/^T\:\s/g, '');
				
				matchings[$ourceId].push($targetId);
			});
		}
	});
	
	startOperation("Confirming selected mappings");

	$.ajax({
		timeout: 1000 * 1000,
		url: _WS_CONFIRM_ENDPOINT + "/matchings",
		cache: false,
		type: "PUT",
		dataType: "text",
		data: {
			mappings: JSON.stringify(matchings)
		},
		success: function() {
			goTo("confirmMappings.jsp");
		},
		error: function(jqXHR, textStatus, errorThrown) {
			alert("Unable to send matching confirmation: " + jqXHR.responseText);
		},
		complete: completeOperation
	});
}