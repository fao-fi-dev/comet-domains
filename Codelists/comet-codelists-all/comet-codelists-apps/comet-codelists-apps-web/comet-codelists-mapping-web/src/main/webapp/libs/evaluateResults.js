var SOURCE_CODELIST, TARGET_CODELIST;

function initializationCallback(oncompletion) {
	initializeInterface();

	buildCodelistsInfo(buildMatchingResultsInfo(oncompletion));
};

function initializeInterface() {
	$.blockUI.defaults.theme = true;
	$.blockUI.defaults.themedCSS.width = "auto";

	$("#matchingsTree").on("click", "li.matchingDetail", function(event) { $(this).tooltip("close"); $("ul.targets", $(this)).toggle(); event.preventDefault(); return false; } );
	$("#matchingsTree").on("click", "li.matching", function(event) { $(this).tooltip("close"); $("ul.matchers", $(this)).toggle(); event.preventDefault(); return false; });

	$(".controls input[type='button']").tooltip({ position: { my: 'center bottom', at: 'center top-5' } });
	$(".controls input[type='button']").click(function() { $(".controls input[type='button']").tooltip("close"); return true; });

	$(".controls input[value='Confirm']").click(function() { confirm(); return true; });
	$(".controls input[value='Discard']").click(function() { discard(); return true; });
	$(".controls input[value='Show']").click(function() { show(); return true; });
};

function buildMatchingResultsInfo(oncompletion) {
	$.ajax({
		type: "GET",
		url: _WS_MATCH_ENDPOINT + "/results/" + LAST_REQUEST_ID,
		dataType: "xml",
		success: function(data) {
			buildMatchingsTree(data, true);

			if(isSet(oncompletion)) {
				oncompletion();
			}
		},
		error: function(jqXHR, textStatus, errorThrown) {
			alert("Unexpected error " + textStatus + ": " + errorThrown);
		},
		complete: function() {
		}
	});
};

function extractID(id) {
	var parsed = id.replace(/(.+)&([^&]+)$/g, "$2");

	return parsed;
};

function selectMatching(sourceId, targetId) {
	$(".matching.selected").removeClass("selected");

	var xSourceId = extractID(sourceId);
	var xTargetId = extractID(targetId);

	$(".sourceCodelistId").text(SOURCE_CODELIST.codelistId);
	$(".targetCodelistId").text(TARGET_CODELIST.codelistId);

	$(".sourceDataId").text(xSourceId);
	$(".targetDataId").text(xTargetId);

	$("#sourceParticipant .data ul, #targetParticipant .data ul").empty();

	var source = $("[id='" + sourceId + "']").data("data");
	var target = $("[id='" + targetId + "']").data("data");

	var sourceData = source.sourceData;
	var targetData = target.targetData;

	showMatchingData(sourceData, $("#sourceParticipant .data ul"));
	showMatchingData(targetData, $("#targetParticipant .data ul"));

	$("#details .matchingScore").text(sprintf("%.2f", parseFloat(target.score) * 100));
	$("#details .matchingType").text(target.scoreType);

	var matchers = null;

	for(var m=0; m<source.matchings.length; m++) {
		if(source.matchings[m].targetId === xTargetId) {
			matchers = source.matchings[m].results;
			break;
		}
	}

	$("#details .triggeredMatchers").text(matchers.length);
	var matchersList = $(".participatingMatchers ul").empty();

	for(var m=0; m<matchers.length; m++) {
		matchersList.append("<li><b>" + matchers[m].matchletType + "</b><span> [ Weight: " + matchers[m].matchletWeight + " ] - Score: " + sprintf("%.2f", parseFloat(matchers[m].score) * 100.0) + "% | " + matchers[m].scoreType + "</span></li>");
	}


	getSelected().addClass("selected");

	updateConfirmationButtons();
};

function showMatchingData(data, container) {
	data = data.sort(sortData);

	for(var c=0;c<data.length; c++) {
		var html = "";
		html += "<li class=\"entryAttribute\">";
			html += "<b>" + data[c].name + "</b>";
			html += " [ " + ( isSet(data[c].type) ? data[c].type : "----" )  + " ] : ";
			html += "<code>" + data[c].value + "</code>";
		html += "</li>";

		container.append(html);
	};
};



function confirm() {
	var sourceId = $(".sourceDataId").text();
	var targetId = $(".targetDataId").text();

	$("[id='source&" + sourceId + "&target&" + targetId + "']").addClass("ui-state-hover");

	updateConfirmationButtons();
	updateConfirmationStatus();
	updateStatus(sourceId);
};

function discard() {
	var sourceId = $(".sourceDataId").text();
	var targetId = $(".targetDataId").text();

	$("[id='source&" + sourceId + "&target&" + targetId + "']").removeClass("ui-state-hover");

	updateConfirmationButtons();
	updateConfirmationStatus();
	updateStatus(sourceId);
};

function updateConfirmationStatus() {
	var matched = 0;

	$(".matchingDetail").each(function() {
		var $this = $(this);

		if($(".ui-state-hover", $this).size() > 0)
			matched++;
	});

	$(".matchingResultsMatched").text(matched);

	$("input[value='Next']").button(matched > 0 ? "enable" : "disable");
};

function getSelected() {
	var sourceId = $(".sourceDataId").text();
	var targetId = $(".targetDataId").text();

	return $("[id='source&" + sourceId + "&target&" + targetId + "']");
};

function isConfirmed() {
	var sourceId = $(".sourceDataId").text();
	var targetId = $(".targetDataId").text();

	return $("[id='source&" + sourceId + "&target&" + targetId + "']").hasClass("ui-state-hover");
};

function updateConfirmationButtons() {
	$("#details input[value='Show']").button("enable");

	if(isConfirmed()) {
		$("#details input[value='Confirm']").button("disable");
		$("#details input[value='Discard']").button("enable");
	} else {
		$("#details input[value='Confirm']").button("enable");
		$("#details input[value='Discard']").button("disable");
	}
};

function updateStatus(sourceId) {
	this.doUpdateSourceStatus($("[id='source&" + sourceId + "']"));
};

function doUpdateSourceStatus(source) {
	source.each(function() {
		var $this = $(this);
		
		$(".confirmedTargets", $this).text($(".ui-state-hover", $(".targets", $this)).size());
	});
}

function show() {
	var sourceId = $(".sourceDataId").text();

	var source = $("[id='source&" + sourceId + "']");

	$("ul.targets", source).show();

	$("#matchingResults").parent(".panel").scrollTo(source, 500);
};

function confirmAll() {
	confirmBatch($("li.matching"));
};

function confirmNone() {
	confirmBatch($([]));
};

function confirmBest() {
	var matched = new Array();
	
	$("li.matchingDetail").each(function() {
		matched.push($("li.matching:eq(0)", $(this)));
	});
	
	confirmBatch($(matched));
};

function confirmFullMatches() {
	var matched = new Array();
	
	$("li.matching").each(function() {
		var $this = $(this);
		
		window.console.log($this.data("score"));
		
		if($this.data("score") === "1.0")
			matched.push($this);
	});
	
	confirmBatch($(matched));
};

function confirmBatch(selector) {
	$("#matchingsTree .matching.ui-state-hover").removeClass("ui-state-hover");
	
	$(selector).each(function() {
		$(this).addClass("ui-state-hover");		
	});

	updateConfirmationStatus();
	doUpdateSourceStatus($(".matchingDetail"));
}

function confirmMatchings() {
	var matchings = {};

	$("#matchingsTree .matching.ui-state-hover").each(function() {
		var $this = $(this);

		var id = $this.attr("id").replace(/^source&/g, "").replace(/target&/g, "");
		var ids = id.split("&");

		var sourceId = ids[0];
		var targetId = ids[1];

		if(matchings[sourceId] == null) {
			matchings[sourceId] = new Array();
		}

		matchings[sourceId].push(targetId);
	});

	startOperation("Confirming selected mappings");

	$.ajax({
		timeout: 1000 * 1000,
		url: _WS_CONFIRM_ENDPOINT + "/matchings",
		cache: false,
		type: "POST",
		dataType: "text",
		data: {
			requestID: LAST_REQUEST_ID,
			mappings: JSON.stringify(matchings)
		},
		success: function() {
			goTo("confirmMappings.jsp");
		},
		error: function(jqXHR, textStatus, errorThrown) {
			alert("Unable to send matching confirmation: " + jqXHR.responseText);
		},
		complete: completeOperation
	});
};