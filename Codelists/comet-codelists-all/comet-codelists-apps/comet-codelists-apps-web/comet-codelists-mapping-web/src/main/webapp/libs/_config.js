var _WS_ENDPOINT = "ws/rest";
var _WS_REPO_ENDPOINT = _WS_ENDPOINT + "/repo";
var _WS_MATCH_ENDPOINT = _WS_ENDPOINT + "/match";
var _WS_CONFIRM_ENDPOINT = _WS_ENDPOINT + "/confirm"
;
$(function() {
	$.ajaxSetup({
		cache: false,
		timeout: 600000,
		type: 'GET',
		dataType: 'json',
		error: function(jqXHR, textStatus, errorThrown) {
			alert("Unexpected error caught: " + errorThrown + " [ status: " + textStatus + " ]");
		}
	});
});