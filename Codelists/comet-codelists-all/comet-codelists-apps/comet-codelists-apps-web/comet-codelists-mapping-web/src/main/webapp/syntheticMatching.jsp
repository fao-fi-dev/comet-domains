<%@page language="java" errorPage="false" contentType="text/html; charset=UTF-8" trimDirectiveWhitespaces="true"%>
<%session.setAttribute("matchingType", "synth");%>
<!DOCTYPE html>
<html>
	<head>
		<title>COMET codelists matching application : CONFIGURE SYNTHETIC MATCHING</title>
		<%@include file="_externalStyles.jspf"%>
		<link rel="stylesheet" type="text/css" href="styles/_common.css"></link>
		<link rel="stylesheet" type="text/css" href="styles/_commonMatch.css"></link>
		<link rel="stylesheet" type="text/css" href="styles/selectCodelists.css"></link>
		<link rel="stylesheet" type="text/css" href="styles/syntheticMatching.css"></link>
	</head>
	<body class="block">
		<%@include file="_header.jspf"%>

		<h2 class="navigation ui-widget-header ui-helper-clearfix">
			<span class="left breadcrumb">
				1. <a href="selectCodelists.jsp">Select codelists</a> - 
				2. <a href="matchingType.jsp">Choose matching type</a> -
				3. <span class="current">Configure matching</span>
			</span>
			<span class="right">
				<input type="button" class="asButton smaller" value="Prev" onclick="javascript:goTo('matchingType.jsp');"/>
				<input type="button" class="asButton smaller" value="Next" onclick="javascript:goTo('evaluateResults.jsp');" disabled="disabled"/>
			</span>
		</h2>

		<%@include file="_selectedCodelists.jspf"%>

		<div class="hidden">
			<div class="left half matchersList ui-helper-clearfix">
				<div class="shadowWrapper">
					<h2 class="ui-widget-header ui-helper-clearfix">Available matchers:</h2>
					<div class="panel panelWrapper">
						<div id="matchersInfo">
						</div>
					</div>
				</div>
			</div>
	
			<div class="right half matchingDefinition ui-helper-clearfix">
				<div class="shadowWrapper">
					<h2 class="ui-widget-header ui-helper-clearfix">Matching process definition:</h2>
					<div class="panel panelWrapper">
						<div id="matchingConfiguration">
							<div id="matchersConfiguration">
								<h3 class="ui-widget-header ui-helper-clearfix">
									Matchers configuration
								</h3>
								<div id="configuredMatchers" class="panel">
								</div>
							</div>
							<div id="overallMatchingConfiguration">
								<h3 class="ui-widget-header ui-helper-clearfix">
									Overall configuration
								</h3>
								<div class="panel">
									<ul>
										<li>
											<label class="parameter">Score threshold:</label>
											<span class="sliderMin" >--</span>
											<span id="scoreThreshold" class="slider" data-name="scoreThreshold"></span>
											<span class="sliderMax">--</span>
											[ <span class="sliderValue">--</span> ]
										</li>
	
										<li>
											<label class="parameter">Halt at first match:</label>
											<span class="radio" data-name="haltAtFirstMatch">
												<label for="haltAtFirstMatch">TRUE</label>
												<input id="haltAtFirstMatch" name="haltAtFirstMatch" type="radio" value="true"/>
												<label for="dontHaltAtFirstMatch">FALSE</label>
												<input id="dontHaltAtFirstMatch" name="haltAtFirstMatch" type="radio" value="false" checked="checked"/>
											</span>
										</li>
	
										<li id="maxCandidatesContainer">
											<label class="parameter">Max candidates:</label>
											<span class="sliderMin">--</span>
											<span id="maxCandidates" class="slider" data-name="maxCandidates"></span>
											<span class="sliderMax">--</span>
											[ <span class="sliderValue">--</span> ]
										</li>
									</ul>
								</div>
							</div>
							<div id="status" class="ui-helper-clearfix">
								<div class="controls ui-helper-clearfix">
									<div class="buttons left ui-helper-clearfix">
										<input type="button" class="asButton smaller" value="Execute" disabled="disabled" onclick="javascript:execute();"></input>
										<input type="button" class="asButton smaller" value="Halt" disabled="disabled" onclick="javascript:halt();"></input>
									</div>
									<div id="processProgress" class="left ui-helper-clearfix">
										<div class="progress-label">Idle...</div>
									</div>
								</div>
								<div class="ui-helper-clearfix">
									<b>ID:</b>&#160;<span id="processID">--</span>
									<b>Start date:</b>&#160;<span id="processStartDate">--</span>
									<b>Completion date:</b>&#160;<span id="processEndDate">--</span>
								</div>
								<div class="ui-helper-clearfix">
									<b>Matchings:</b>&#160;<span id="processMatchings">--</span>
									<b>Atomic comparisons:</b>&#160;<span id="atomicComparisons">--</span>
									<b>Elapsed:</b>&#160;<span id="processElapsed">--h:--m:--s</span>.
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>

	<%@include file="_externalLibs.jspf"%>
	<%@include file="_commonLibs.jspf"%>
	<script type="text/javascript" src="libs/_commonMatch.js"></script>
	<script type="text/javascript" src="libs/_matchConfiguration.js"></script>
	<script type="text/javascript" src="libs/syntheticMatching.js"></script>
</html>
