<%@page language="java" errorPage="false" contentType="text/html; charset=UTF-8" trimDirectiveWhitespaces="true"%>
<%
	String matchingType = (String)session.getAttribute("matchingType");
	boolean isSynthetic = "synth".equals(matchingType);
%>
<!DOCTYPE html>
<html>
	<head>
		<title>COMET codelists matching : CONFIRM MAPPINGS</title>
		<%@include file="_externalStyles.jspf"%>
    <link rel="stylesheet" type="text/css" href="styles/_common.css"></link>
    <link rel="stylesheet" type="text/css" href="styles/_commonMatch.css"></link>
    <link rel="stylesheet" type="text/css" href="styles/selectCodelists.css"></link>
    <link rel="stylesheet" type="text/css" href="styles/evaluateResults.css"></link>
    <link rel="stylesheet" type="text/css" href="styles/confirmMappings.css"></link>
	</head>
	<body class="block">
		<%@include file="_header.jspf"%>

		<h2 class="navigation ui-widget-header ui-helper-clearfix">
			<span class="left breadcrumb">
				1. <a href="selectCodelists.jsp">Select codelists</a> -
				2. <a href="matchingType.jsp">Choose matching type</a> -
				<%if(isSynthetic) {%>
				3. <a href="syntheticMatching.jsp">Configure matching</a> -
				4. <a href="evaluateResults.jsp">Evaluate results</a> -
				5. <span class="current">Confirm mappings</span>
				<%} else {%>
				3. <a href="manualMatching.jsp">Create results</a> -
				4. <span class="current">Confirm mappings</span>
				<%}%>
			</span>
			<span class="right">
				<%if(isSynthetic) {%>
					<input type="button" value="Prev" class="asButton smaller" onclick="javascript:goTo('evaluateResults.jsp');"></input>
				<%} else { %>
					<input type="button" value="Prev" class="asButton smaller" onclick="javascript:goTo('manualMatching.jsp');"></input>
				<%} %>
			</span>
		</h2>

	<%@include file="_selectedCodelists.jspf"%>

		<div class="hidden ui-helper-clearfix">
			<div class="left half matchingsList ui-helper-clearfix">
				<div class="shadowWrapper">
					<h2 class="ui-widget-header">Confirmed mappings:</h2>
					<div class="panel panelWrapper">
						<div id="matchingResults">
							<ul id="matchingsTree">
							</ul>
						</div>
					</div>
				</div>
			</div>

			<div class="right half selectedMatching ui-helper-clearfix">
				<div class="shadowWrapper">
					<h2 class="ui-widget-header">Mappings metadata:</h2>
					<div class="panel panelWrapper ">
						<div id="metadata">
							<div class="ui-helper-clearfix">
								<div class="mappingMetadataPanel shadowWrapper left half">
									<h3 class="ui-widget-header">Identifier URI (required):</h3>
									<div class="panel">
										<input id="mappingId" title="The mapping identifier (a valid URI)" class="default ui-state-highlight mandatory" type="text"></input>
									</div>
								</div>
	
								<div class="mappingMetadataPanel shadowWrapper right half">
									<h3 class="ui-widget-header">Name (optional):</h3>
									<div class="panel">
										<input id="mappingName" title="The mapping name (defaults to the mapping identifier when not provided)" class="default ui-state-highlight" type="text"></input>
									</div>
								</div>
							</div>

							<div class="mappingMetadataPanel shadowWrapper ui-helper-clearfix">
								<h3 class="ui-widget-header">Version (required):</h3>
								<div class="panel">
									<input id="mappingVersion" title="The mapping version" class="default ui-state-highlight mandatory" type="text"></input>
								</div>
							</div>

							<div class="mappingMetadataPanel shadowWrapper ui-helper-clearfix">
								<h3 class="ui-widget-header">Author (required):</h3>
								<div class="panel">
									<input id="mappingAuthor" title="The mapping author" class="default ui-state-highlight mandatory" type="text"></input>
								</div>
							</div>

							<div class="mappingMetadataPanel shadowWrapper ui-helper-clearfix">
								<h3 class="ui-widget-header">Description (optional):</h3>
								<div class="panel">
									<textarea id="mappingDescription" title="The mapping description (optional)" class="default ui-state-highlight"></textarea>
								</div>
							</div>

							<div class="operations">
								<b>Persist in</b>
								<input type="button" class="asButton default" title="Persist only the mappings' basic information, excluding source and target elements data among other things" value="Barebone" disabled="disabled" onclick="javascript:persist(false);"></input>
								<input type="button" class="asButton default" title="Persist all the mappings' information, including source and target elements data and matchlets' configuration" value="Verbose" disabled="disabled" onclick="javascript:persist(true);"></input>
								<b>mode</b>
							</div>
						</div>
				 	</div>
				</div>
			</div>
		</div>
	</body>

	<%@include file="_externalLibs.jspf"%>
	<%@include file="_commonLibs.jspf"%>
	<%@include file="_sessionToJS.jspf"%>
	<script type="text/javascript" src="libs/_commonMatch.js"></script>
	<script type="text/javascript" src="libs/confirmMappings.js"></script>
</html>
