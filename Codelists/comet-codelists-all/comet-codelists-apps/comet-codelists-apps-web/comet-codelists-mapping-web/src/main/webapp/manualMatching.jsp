<%@page language="java" errorPage="false" contentType="text/html; charset=UTF-8" trimDirectiveWhitespaces="true"%>
<%session.setAttribute("matchingType", "manual");%>
<!DOCTYPE html>
<html>
	<head>
		<title>COMET codelists matching application : CONFIGURE MANUAL MATCHING</title>
		<%@include file="_externalStyles.jspf"%>
		<link rel="stylesheet" type="text/css" href="styles/_common.css"></link>
		<link rel="stylesheet" type="text/css" href="styles/_commonMatch.css"></link>
		<link rel="stylesheet" type="text/css" href="styles/selectCodelists.css"></link>
		<link rel="stylesheet" type="text/css" href="styles/manualMatching.css"></link>
	</head>
	<body class="block">
		<%@include file="_header.jspf"%>

		<h2 class="navigation ui-widget-header ui-helper-clearfix">
			<span class="left breadcrumb">
				1. <a href="selectCodelists.jsp">Select codelists</a> - 
				2. <a href="matchingType.jsp">Choose matching type</a> -
				3. <span class="current">Create results</span>
			</span>
			<span class="right">
				<input type="button" class="asButton smaller" value="Prev" onclick="javascript:goTo('matchingType.jsp');"/>
				<input type="button" class="asButton smaller" value="Next" onclick="javascript:confirmMatchings();" disabled="disabled"/>
			</span>
		</h2>

		<%@include file="_selectedCodelists.jspf"%>

		<div class="hidden playground">
			<div class="left half targetData ui-helper-clearfix">
				<div class="shadowWrapper">
					<h2 class="ui-widget-header ui-helper-clearfix noSelect">
						Target data: [ <span class="numEntries">0</span> entries ]</h2>
					<div class="panel panelWrapper">
					</div>
				</div>
			</div>
			
			<div class="right half sourceData ui-helper-clearfix noSelect">
				<div class="shadowWrapper">
					<h2 class="ui-widget-header ui-helper-clearfix">
						Source data: [ <span class="numEntries">0</span> entries ]</h2>
					<div class="panel panelWrapper">
					</div>
				</div>
			</div>
 		</div>
	</body>

	<%@include file="_externalLibs.jspf"%>
	<%@include file="_commonLibs.jspf"%>
	<script type="text/javascript" src="libs/_commonMatch.js"></script>
	<script type="text/javascript" src="libs/_matchConfiguration.js"></script>
	<script type="text/javascript" src="libs/manualMatching.js"></script>
</html>
