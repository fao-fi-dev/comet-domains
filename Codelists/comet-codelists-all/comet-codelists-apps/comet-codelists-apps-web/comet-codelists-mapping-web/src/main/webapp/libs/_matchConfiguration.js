var SLIDER_DEFAULTS = {
	"*": { 
		animate: true,
//		min: 0.0,
//		max: 1.0,
//		step: .01,
//		value: .5,
		range: "min"
	},
	weight: {
		min: 1,
		max: 100,
		step: 1,
		value: 50,
		range: false
	},
	scoreThreshold: {
		min: 0.01,
		max: 1,
		step: .01,
		value: .5,
		range: "max"
	},
	minimumScore: {
//		min: 0.00,
		range: "max"
	},
	soundexWeight: {
//		min: 0,
//		max: 1,
		range: false
	},
	maxCandidates: {
		min: 1,
		max: 50,
		step: 1,
		value: 5
	}
}; 

var BUTTONSET_DEFAULTS = {
	"*": {
		change: function(buttonset) {
			var $this = $(buttonset);
			
			var selected = [];
			
			$(".ui-state-active", $this).each(function() {
				selected.push($(this).text());
			});
			
			return selected;
		},
		handleChange: function(buttonset, selected) { }
	},
	haltAtFirstMatch: {
		handleChange: function(buttonset, selected) {
			var value = isSet(selected) ? selected[0] : null;
			
			if("TRUE" === value)
				$("#maxCandidatesContainer").hide();
			else
				$("#maxCandidatesContainer").show();
		}
	}
};