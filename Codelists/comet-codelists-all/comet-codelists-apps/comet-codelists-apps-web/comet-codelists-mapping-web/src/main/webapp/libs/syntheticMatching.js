var EXECUTING = false;
var LAST_REQUEST_ID = null;

var POLL_FREQUENCY = 1000;
var POLLER;
var SOURCE_CODELIST, TARGET_CODELIST;

function startPoller(force) {
	POLLER = setInterval(function() { checkProcessStatus(force); }, POLL_FREQUENCY);
};

function stopPoller() {
	POLLER = clearInterval(POLLER);

	unblockInputs();
};

function initializationCallback(oncompletion) {
	initializeInterface();

	buildCodelistsInfo(function() { buildMatchersInfo(oncompletion); });
};

function initializeInterface() {
	$.blockUI.defaults.theme = true;
	$.blockUI.defaults.themedCSS.width = "auto";

	initializeButtonsets($(".radio"));
	initializeProcessSliders($(".slider"));

	$("#configuredMatchers").accordion({ animate: false });

	var progressBar = $("#processProgress");
	var progressLabel = $("#processProgress .progress-label");

	progressBar.progressbar({
		value: false,
		change : function() {
			progressLabel.text(progressBar.progressbar("value") + "%");
		},
		complete : function() {
			progressLabel.text("Process completed!");
		}
	});
};

function initializeButtonsets(radios) {
	radios.buttonset();

	radios.each(function() {
		var $this = $(this);

		var name = $this.data("name");
		var config = BUTTONSET_DEFAULTS["*"];

		if(isSet(name) && isSet(BUTTONSET_DEFAULTS[name])) {
			config = $.extend(true, {}, config, BUTTONSET_DEFAULTS[name]);
		}

		var change = config.change;
		var handle = config.handleChange;

		$this.click(function(event) { handle($this, change($this)); });
	});
};

function initializeProcessSliders(sliders, matcherData) {
	sliders.each(function() {
		var $lider = $(this);

		var name = $lider.data("name");
		var config = SLIDER_DEFAULTS["*"];

		if(isSet(name) && isSet(SLIDER_DEFAULTS[name])) {
			config = $.extend(true, {}, config, SLIDER_DEFAULTS[name]);
		}

		var updater = function(event, ui) {
			var min = $lider.prev(".sliderMin");
			var max = $lider.next(".sliderMax");
			var val = max.next(".sliderValue");

			val.text(ui.value);
			min.text(config.min);
			max.text(config.max);
		};

		config.slide = updater;

		$lider.slider(config);

		updater(null, { value: config.value });
	});
};

function initializeSlider(slider, matcherParameter) {
	var $lider = $(slider);

	var name = matcherParameter.parameterName;
	var type = matcherParameter.parameterType;;
	var config = SLIDER_DEFAULTS["*"];

	var decimal = type == "double" || type == "float";
	var integer = !decimal;

	var lowerBounded = matcherParameter.constraint.includeFrom;
	var upperBounded = matcherParameter.constraint.includeTo;

	var min = matcherParameter.constraint.from;
	var max = matcherParameter.constraint.to;

	if(!isSet(min))
		min = 0;

	if(!isSet(max))
		max = 100;

	var value = ( max - min ) / 2;

	if(integer)
		value = Math.round(value);

	if(!lowerBounded) {
		if(decimal)
			min += 0.01;
		else
			min += 1;
	}

	if(!upperBounded) {
		if(decimal)
			max -= 0.01;
		else
			max -= 1;
	}

	config.min = min;
	config.max = max;
	config.step = decimal ? 0.01 : 1;
	config.value = value;

	if(isSet(name) && isSet(SLIDER_DEFAULTS[name])) {
		config = $.extend(true, {}, config, SLIDER_DEFAULTS[name]);
	}

	var updater = function(event, ui) {
		var min = $lider.prev(".sliderMin");
		var max = $lider.next(".sliderMax");
		var val = max.next(".sliderValue");

		val.text(ui.value);
		min.text(config.min);
		max.text(config.max);
	};

	config.slide = updater;

	$lider.slider(config);

	updater(null, { value: config.value });
};

function buildMatchersInfo(callback) {
	$.ajax({
		url: _WS_MATCH_ENDPOINT + "/describe/matchers",
		success: function(data) {
			if(!isSet(data)) {
				alert("Unable to retrieve matchers info");

				return;
			}

			var matchersInfo = $("#matchersInfo");
			var heading;
			var label;
			var selectButton;

			for(var d=0; d<data.length; d++) {
				label = $("<span class=\"left matcherDetails\">" + data[d].matchletName + " (" +
							"<span class=\"sourceDataType\" title=\"Source data type\">" + sanitizeType(data[d].extractedSourceDataType) + "</span>, " +
							"<span class=\"targetDataType\" title=\"Target data type\">" + sanitizeType(data[d].extractedTargetDataType) + "</span>" +
						")</span>");
				selectButton = $("<span class=\"right asButton\" title=\"Add this matcher to the matching chain\">ADD</span>");
				selectButton.button();

				heading = $("<h3 class=\"ui-helper-clearfix matcherHeading\"/>");
				heading.attr("title", data[d].matchletDescription);

				heading.append(selectButton);
				heading.append(label);

				heading.data("matcher", data[d]);

				matchersInfo.append(heading);
				matchersInfo.append(buildMatcherDetails(data[d]));

				selectButton.click(function() {
					addMatcher($(this).parents("h3").data("matcher"));

					return false;
				});
			}

			$("#matchersInfo > [title]").tooltip({ position: { my: 'right top', at: 'right-16 bottom' }});

			registerCallback(function() {
				$("#matchersInfo").accordion({
					active: false,
					collapsible: true,
					heightStyle: "content",
					activate: function(event, ui) {
						$(".matcherParameters", ui.newPanel).accordion({
							active: false,
							collapsible: true,
							heightStyle: "content"
						});

						$(".matcherParameters", ui.newPanel).slideDown();
					}
				});
			});

			if(isSet(callback)) callback();
		}
	});
};

function buildMatcherDetails(matcher) {
	var wrapper = $("<div/>");

	var content = $("<p/>");
	content.append("<label>Description:</label>");
	content.append("<br/>");
	content.append($("<span class=\"matcherDescription\"></span>").text(matcher.matchletDescription));

	wrapper.append(content);

	content = $("<p/>");
	content.append("<label>Implemented by:</label>");
	content.append("<br/>");
	content.append("<code class=\"matcherImplementingClass\">" + matcher.matchletType + "</code>");

	wrapper.append(content);

	content = $("<p/>");

	content.append("<label>Source data type:</label>");
	content.append("<code class=\"matcherExtractedSourceDataType\">" + sanitizeType(matcher.extractedSourceDataType) + "</code>");

	wrapper.append(content);

	content = $("<p/>");

	content.append("<label>Target data type:</label>");
	content.append("<code class=\"matcherExtractedTargetDataType\">" + sanitizeType(matcher.extractedTargetDataType) + "</code>");

	wrapper.append(content);

	content = $("<p/>");

	content.append("<label>Configuration parameters:</p>");
	content.append(buildMatcherParameters(matcher));

	wrapper.append(content);

	return wrapper;
};

function sanitizeType(type) {
	if(type.indexOf(".") < 0)
		return type;

	var sanitized = type.substring(type.lastIndexOf(".") + 1);

	if(new RegExp(";$").test(sanitized))
		sanitized = sanitized.replace(/\;$/g, "[]");

	if("Serializable" === sanitized || "Object" === sanitized)
		sanitized = sanitized.replace(/Serializable|Object/g, "*");

	return sanitized;
};

function buildMatcherParameters(matcher) {
	var params = matcher.matchletParameters;

	var wrapper = $("<div class='matcherParameters'/>");

	for(var p=0; p<params.length; p++) {
		describeMatcherParameter(wrapper, params[p]);
	}

	return wrapper;
};

function describeMatcherParameter(wrapper, parameter) {
	var paramName = parameter.parameterName;

	if(!isHiddenParam(paramName)) {
		var isMandatory = parameter.parameterIsMandatory;

		var heading = $("<h3>" +
							parameter.parameterName +
							" (" + sanitizeType(parameter.parameterType) + ")" +
							( isMandatory ? " *" : "" ) +
						"</h3>");

		heading.attr("title", ( isMandatory ? " [ MANDATORY ] " : "" ) + parameter.parameterDescription);
		heading.tooltip({ position: { my: "right top", at: "right bottom" } });

		wrapper.append(heading);

		var container = $("<div/>");

		var content = $("<p/>");

		content.append("<label>Type:</label>");
		content.append("<span class=\"matcherParameterType\">" + sanitizeType(parameter.parameterType) + "</span>");

		container.append(content);

		content = $("<p/>");

		content.append("<label>Description:</label>");
		content.append("<span class=\"matcherParameterDescription\">" + parameter.parameterDescription + "</span>");

		container.append(content);

		content = $("<p/>");

		content.append("<label>Mandatory:</label>");
		content.append("<span class=\"matcherParameterMandatory\">" + ( parameter.parameterIsMandatory ? "Yes" : "No" ) + "</span>");

		container.append(content);

		wrapper.append(container);
	}

	return wrapper;
};

function isHiddenParam(name) {
	return name === "isCutoff" ||
		   name === "forceComparison";
};

function addMatcher(matcherData) {
	var panel = $("#matchersConfiguration > .panel");

	this.buildMatchersConfiguration(panel, matcherData);

	var configuredMatchers = $("#configuredMatchers");

	configuredMatchers.accordion("refresh");

	var toActivate = $("h3", configuredMatchers).size() - 1;

	if(toActivate > 0)
		configuredMatchers.accordion({ active: toActivate });

	enableMatching();
};

function getMatcherParameterType(matcherData) {
	return matcherData.parameterType;
};

function isRangeParameter(matcherData) {
	return isSet(matcherData.constraint) && isSet(matcherData.constraint.from);
};

function isBooleanParameter(matcherData) {
	return getMatcherParameterType(matcherData) == "boolean";
};

function isValueSetParameter(matcherData) {
	return isSet(matcherData.constraint) && isSet(matcherData.constraint.values);
};

function isMultipleValueSetParameter(matcherData) {
	return isValueSetParameter(matcherData) && isSet(matcherData.constraint.multiple) && matcherData.constraint.multiple;
};

function buildMatchersConfiguration(target, matcherData) {
	var header = $("<h3 class=\"ui-helper-clearfix\">" +
					"<span class=\"left\">" + matcherData.matchletName + "</span>" +
					"<span class=\"right asButton visible\">REMOVE</span>" +
				   "</h3>");

	header.data("matcher", matcherData);

	$(".asButton", header).each(function() {
		var $this = $(this);
		$this.button();
		$this.click(function() {
			var header = $this.parents("h3");
			var content = header.next("div");

			header.remove();
			content.remove();

			var configuredMatchers = $("#configuredMatchers");

			configuredMatchers.accordion("refresh");

			enableMatching();
		});
	});


	target.append(header);

	var panel = $("<div class=\"matcherConfiguration\"/>");
	var configuration = $("<ul/>");

	panel.append(configuration);

	buildCommonConfiguration(configuration, matcherData);

	target.append(panel);

	return target;
};

function buildCommonConfiguration(target, matcherData) {
	target.append(buildDataColumnConfiguration(true, matcherData));
	target.append(buildDataColumnConfiguration(false, matcherData));

	var matcherParameters = matcherData.matchletParameters;
	var parameter;
	for(var p=0; p<matcherParameters.length; p++) {
		parameter = matcherParameters[p];

		if(!isHiddenParam(parameter.parameterName)) {
			if(isBooleanParameter(parameter))
				target.append(buildBooleanParameter(parameter));
			else if(isRangeParameter(parameter))
				target.append(buildRangeParameter(parameter));
			else if(isMultipleValueSetParameter(parameter))
				target.append(buildMultipleValueSetParameter(parameter));
			else if(isValueSetParameter(parameter))
				target.append(buildSingleValueSetParameter(parameter));
		}
	}

	$("select", target).change(enableMatching);
};

function buildRangeParameter(matcherParameter) {
	var slider = "<li>";
	slider += "<label class=\"parameter\">" + matcherParameter.parameterName + ":</label>";
	slider += "<span class=\"sliderMin\" >--</span>";
	slider += "<span class=\"slider\" data-name=\"" + matcherParameter.parameterName + "\" data-control=\"slider\"></span>";
	slider += "<span class=\"sliderMax\">--</span>";
	slider += "[ <span class=\"sliderValue\">--</span> ]";
	slider += "</li>";

	slider = $(slider);

	initializeSlider($(".slider", slider), matcherParameter);

	return slider;
};

function buildBooleanParameter(matcherParameter) {
	var name = "radio" + new Date().getTime();
	var radio = "<li>";
	radio += "<label class=\"parameter\">" + matcherParameter.parameterName + ":</label>";
	radio += "<span class=\"radio\" data-name=\"" + matcherParameter.parameterName + "\" data-control=\"radio\">";
	radio += "<label for=\"" + name + "_TRUE\">TRUE</label>";
	radio += "<input id=\"" + name + "_TRUE\" name=\"" + name + "\" type=\"radio\" value=\"true\" checked=\"checked\"/>";
	radio += "<label for=\"" + name + "_FALSE\">FALSE</label>";
	radio += "<input id=\"" + name + "_FALSE\" name=\"" + name + "\" type=\"radio\" value=\"false\"/>";

	radio += "</li>";

	radio = $(radio);

	initializeButtonsets($(".radio", radio));

	return radio;
};

function buildSingleValueSetParameter(matcherParameter) {
	var name = "radio" + new Date().getTime();
	var radio = "<li>";
	radio += "<label class=\"parameter\">" + matcherParameter.parameterName + ":</label>";
	radio += "<span class=\"radio\" data-name=\"" + matcherParameter.parameterName + "\" data-control=\"radio\">";

	var value;
	for(var v=0; v<matcherParameter.constraint.values.length; v++) {
		value = matcherParameter.constraint.values[v];

		radio += "<label for=\"" + name + "_" + value + "\">" + value + "</label>";
		radio += "<input id=\"" + name + "_" + value + "\" name=\"" + name + "\" type=\"radio\" value=\"" + value + "\"" + ( v == 0 ? " checked=\"checked\"" : "" ) + "/>";
	}

	radio += "</li>";

	radio = $(radio);

	initializeButtonsets($(".radio", radio));

	return radio;
};

function buildMultipleValueSetParameter(matcherParameter) {
	var name = "checkbox" + new Date().getTime();
	var checkbox = "<li>";
	checkbox += "<label class=\"parameter\">" + matcherParameter.parameterName + ":</label>";
	checkbox += "<span class=\"checkbox\" data-name=\"" + matcherParameter.parameterName + "\" data-control=\"checkbox\">";

	var value;
	for(var v=0; v<matcherParameter.constraint.values.length; v++) {
		value = matcherParameter.constraint.values[v];

		checkbox += "<label for=\"" + name + "_" + value + "\">" + value + "</label>";
		checkbox += "<input id=\"" + name + "_" + value + "\" name=\"" + name + "\" type=\"checkbox\" value=\"" + value + "\"" + ( v == 0 ? " checked=\"checked\"" : "" ) + "/>";
	}

	checkbox += "</li>";

	checkbox = $(checkbox);

	initializeButtonsets($(".checkbox", checkbox));

	return checkbox;
};

function buildDataColumnConfiguration(isSource, matcherData) {
	var dataColumn = "<li>";
	var codes, other;

	codes = [];
	other = [];

	var codelist = isSource ? SOURCE_CODELIST : TARGET_CODELIST;
	var columns = codelist.columnsMetadata;

	for(var c=0; c<columns.length; c++) {
		column = columns[c];

		if(column.name == codelist.selectedCodeColumn)
			codes.push(column);
		else
			other.push(column);
	}

	dataColumn += "<label class=\"parameter\">" + ( isSource ? "Source" : "Target" ) + " data column:</label>";
	dataColumn += "<select class=\"" + ( isSource ? "source" : "target" ) + "DataColumn\" data-name=\"" + ( isSource ? "source" : "target" ) + "ColumnName\" data-control=\"select\"\">";
		dataColumn += "<option selected=\"selected\" value=\"\">Please select a " + ( isSource ? "source" : "target" ) + " column</option>";

		dataColumn += "<optgroup label=\"Codes\">";

		for(var c=0; c<codes.length; c++) {
			dataColumn += buildColumnOption(codelist.codelistName, codes[c], ( isSource ? matcherData.extractedSourceDataType : matcherData.extractedTargetDataType ));
		}

		dataColumn += "</optgroup>";

		dataColumn += "<optgroup label=\"Other\">";

		for(var c=0; c<other.length; c++) {
			dataColumn += buildColumnOption(codelist.codelistName, other[c], ( isSource ? matcherData.extractedSourceDataType : matcherData.extractedTargetDataType ));
		}

		dataColumn += "</optgroup>";

	dataColumn += "</select>";
	dataColumn += "</li>";

	return dataColumn;
};

function buildColumnOption(codelistName, column, requiredType) {
	var isValid = isColumnValidForType(column.type, requiredType);

	var option = "<option value=\"" + column.name + "\"" + ( isValid ? "" : " disabled=\"disabled\"" ) + ">";
	option += codelistName + " : " + column.name + " [ " + column.type + " ]";
	option += "</option>";

	return option;
};

function isColumnValidForType(columnType, requiredType) {
	requiredType = sanitizeType(requiredType);

	if("*" === requiredType)
		return true;

	return requiredType === columnType;
};


function shouldEnableMatching() {
	var matcherConfigurations = $("#configuredMatchers .matcherConfiguration");

	var allConfigured = matcherConfigurations.size() > 0;

	$("select", matcherConfigurations).each(function() {
		var value = $("option:selected", $(this)).val();

		allConfigured &= value != null && value != "";
	});

	return allConfigured;
};

function enableMatching() {
	var allConfigured = shouldEnableMatching();

	$("input[value='Execute']").button(allConfigured ? "enable" : "disable");
};

function getCodelistConfiguration(codelist) {
	return {
		id: codelist.codelistId,
		name: codelist.codelistName,
		code: codelist.selectedCodeColumn == null ? codelist.defaultCodeColumn : codelist.selectedCodeColumn
	};
};

function buildConfiguration() {
	var configuration = {};

	var data = {};
	var overall = {};
	var matchers = [];

	data.source = getCodelistConfiguration(SOURCE_CODELIST);
	data.target = getCodelistConfiguration(TARGET_CODELIST);

	configuration.data = data;

	overall.scoreThreshold = $("#scoreThreshold").slider("value");
	overall.haltAtFirstMatch = $("[name='haltAtFirstMatch']:checked").val() == "true";

	if(!overall.haltAtFirstMatch)
		overall.maxCandidates = $("#maxCandidates").slider("value");

	configuration.overall = overall;

	var matcherCounter = 1;
	$("#configuredMatchers h3").each(function() {
		var matcherConfiguration = getMatcherConfiguration(matcherCounter++, $(this));

		matchers.push(matcherConfiguration);
	});

	configuration.matchlets = matchers;

	return configuration;
};

function getMatcherConfiguration(matcherCounter, matcherConfiguration) {
	var matcherData = matcherConfiguration.data("matcher");

	var matcherConfig = {};

	matcherConfig.matchletId = "matcher#" + matcherCounter;
	matcherConfig.matchletName = matcherData.matchletName;
	matcherConfig.matchletParameters = [];

	var matcherParameters = matcherConfiguration.next("div");

	$("[data-control]", matcherParameters).each(function() {
		var parameterConfig = {};

		var matcherParameter = $(this);

		var isSlider = matcherParameter.is("[data-control='slider']");
		var isRadio = matcherParameter.is("[data-control='radio']");
		var isCheckbox = matcherParameter.is("[data-control='checkbox']");
		var isSelect = matcherParameter.is("[data-control='select']");

		if(isSlider || isRadio || isCheckbox || isSelect) {
			parameterConfig.parameterName = matcherParameter.data("name");

			if(isSlider)
				parameterConfig.parameterValue = matcherParameter.slider("value");
			else if(isSelect)
				parameterConfig.parameterValue = $("option:selected", matcherParameter).val();
			else if(isRadio)
				parameterConfig.parameterValue = $("input[type='radio']:checked", matcherParameter).val();
			else if(isCheckbox) {
				var values = [];

				$("input[type='checkbox']:checked", matcherParameter).each(function() {
					values.push($(this).val());
				});

				parameterConfig.parameterValue = values.join("|");
			}

			matcherConfig.matchletParameters.push(parameterConfig);
		};
	});

	return matcherConfig;
};

function blockInputs() {
	$("input[value='Execute']").button("disable");

	block($(".matchersList > div > .panel, " +
	  		"#matchingConfiguration > div > .panel"), "Please wait...", "A matching process is currently executing");
};

function unblockInputs() {
	$("input[value='Execute']").button(shouldEnableMatching() ? "enable" : "disable");

	$(".matchersList > div > .panel, " +
	  "#matchingConfiguration > div > .panel").unblock();
};

function halt() {
	$.ajax({
		timeout: 10000,
		cache: false,
		url: _WS_MATCH_ENDPOINT + "/halt/" + LAST_REQUEST_ID,
		type: "DELETE",
		dataType: "text",
		success: function() {
		},
		error: function(jqXHR, textStatus, errorThrown) {
			alert("Unable to halt matching process: " + jqXHR.responseText);
		},
		complete: function() {
			stopPoller();

			$("input[value='Halt']").button("disable");
			$("input[value='Execute']").button("enable");
			$("#evaluate").button("disable");
		}
	});
};

function execute() {
	LAST_REQUEST_ID = "CM_PROC_" + new Date().getTime();

	blockInputs();

	var progressBar = $("#processProgress");
	var progressLabel = $(".progress-label", progressBar);
	$(progressBar).progressbar({
		value: false
	});

	$("#processID").text(isSet(LAST_REQUEST_ID) ? LAST_REQUEST_ID : "--");

	progressLabel.text("Idle...");

	$.ajax({
		timeout: 60 * 1000,
		url: _WS_MATCH_ENDPOINT + "/execute/" + LAST_REQUEST_ID,
		cache: false,
		type: "POST",
		dataType: "text",
		beforeSend: function() {
			$("input[value='Halt']").button("enable");
			$("#evaluate").button("disable");
		},
		data: {
			configuration: JSON.stringify(buildConfiguration())
		},
		success: function() {
			startPoller();
		},
		error: function(jqXHR, textStatus, errorThrown) {
			stopPoller();

			$("input[value='Halt']").button("disable");
			$("#evaluate").button("enable");

			alert("Unable to complete matching: " + jqXHR.responseText);
		}
	});
};

function checkProcessStatus() {
	if(typeof LAST_REQUEST_ID != "undefined" && LAST_REQUEST_ID != null) {
		$.ajax({
			timeout: 500,
			url: _WS_MATCH_ENDPOINT + "/status/" + LAST_REQUEST_ID,
			cache: false,
			type: "GET",
			dataType: "json",
			success: function(data) {
				if(isSet(data)) {
					if(LAST_REQUEST_ID == data.processId) {
						if(isSet(data.processEndTimestamp) && data.processEndTimestamp > 0) {
							stopPoller();
	
							$("#matchingResults").text(data);
							
							$("input[value='Execute'], input[value='Next']").button("enable");
							$("input[value='Halt']").button("disable");
						} else {
							$("input[value='Execute'], input[value='Next']").button("disable");
							$("input[value='Halt']").button("enable");
						}
	
						displayProcessStatus(data);
					} else {
						console.log("WARN: got matching status data from another process...");
					}
				} else {
					console.log("WARN: waiting for the matching process to start");
				}
			},
			error: function(jqXHR, textStatus, errorThrown) {
				if("timeout" !== errorThrown) {
					stopPoller();

					console.log("ERROR: cannot check process status (" + textStatus + ", " + errorThrown + ")");
				}
			}
		});
	}
};

function displayProcessStatus(data) {
	var startDate = new Date(data.processStartTimestamp);
	var endDate = null;

	if(isSet(data.processEndTimestamp) && data.processEndTimestamp > 0)
		endDate = new Date(data.processEndTimestamp);

	$("#processProgress").progressbar({ value: Math.round(100.0 * data.currentNumberOfComparisonRoundsPerformed / data.numberOfComparisonRounds) });

	var matchings = data.numberOfMatches;
	var atomicComparisons = data.totalNumberOfAtomicComparisonsPerformed;

	var elapsed = data.elapsed;

	$("#processStartDate").text(formatDate(startDate));
	$("#processEndDate").text(endDate == null ? "--" : formatDate(endDate));
	
	elapsed = elapsed / 1000;
	
	var hh = Math.floor(elapsed / 3600);
	var mm = Math.floor((elapsed - (hh * 3600)) / 60);
	var ss = Math.floor(elapsed - mm * 60 - hh * 3600);
	
	$("#processElapsed").text(sprintf("%02dh:%02dm:%02ds", hh, mm, ss));
	
	$("#processMatchings").text(matchings);
	$("#atomicComparisons").text(atomicComparisons);
};

function formatDate(date) {
	if(date == null)
		return "--";

	var formatted = $.datepicker.formatDate('yy-mm-dd', date);

	var hours = date.getHours();
	var mins = date.getMinutes();
	var secs = date.getSeconds();

	formatted += " " +
				(( hours < 10 ? "0" : "" ) + hours ) + ":" +
				(( mins < 10 ? "0" : "" ) + mins ) + ":" +
				(( secs < 10 ? "0" : "" ) + secs );

	return formatted;
};

function evaluate() {
	$("#submitForEvaluation").submit();
};

$(function() {
	$(window).on("beforeunload", function() {
		if(isSet(EXECUTING) && EXECUTING)
			return "A matching process is in progress: are you sure you want to navigate away from this page?  If you confirm, the process will still be running in the background until its completion. You might check its status by navigating back to this page, though.";
	});
});