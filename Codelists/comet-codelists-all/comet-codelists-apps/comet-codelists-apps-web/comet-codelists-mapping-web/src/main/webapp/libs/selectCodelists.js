var _PLEASE_WAIT_MESSAGE = "Please wait...";
var _RETRIEVING_METADATA_MESSAGE = "Retrieving codelist metadata...";
var _RETRIEVING_DATA_MESSAGE = "Retrieving codelist data...";
var _SET_AS_CODELIST_CODE_TITLE = "Click to set this field as the codelist's CODE attribute";
var _IS_CODELIST_CODE_TITLE = "This is the currently selected codelist's CODE attribute";
var _DEFAULT_TOOLTIP_POSITION = { position: { my: 'left top+1', at: 'left bottom' } };

var _REPOS = {};
var _CL_STATUS = {};

$(function() {
	listCodelists();

	$(".codelistMeta").on("click", "tbody tr", function() {
		var $this = $(this);
		var table = $this.parents("table");

		setAsCode(table, $this);

		handleMatchingAbilitation();
	}).on("mouseenter", "tbody tr", function() { $(this).addClass("ui-state-hover"); }).
	on("mouseout", "tbody tr", function() { $(this).removeClass("ui-state-hover"); });
});

function mark(codelistTypeId, selectedCodelist, status) {
	var affected = unmark(codelistTypeId, selectedCodelist);
	
	if(status != "selected") {
		affected.addClass(status);
	
		_CL_STATUS[selectedCodelist] = status;
	}
};

function unmark(codelistTypeId, selectedCodelist) {
	return $("option[value='" + selectedCodelist + "']").removeClass();
//	return $("#" + codelistTypeId + " option[value='" + selectedCodelist + "']").removeClass();
};

function setAsCode(meta, row) {
	$(".code", meta).each(function() {
		var $row = $(this);

		$row.removeClass("code").removeClass("ui-state-highlight");

		if($row.is(":data(ui-tooltip)"))
			$row.tooltip("destroy");

		$row.attr("title", _SET_AS_CODELIST_CODE_TITLE);
		$row.tooltip(_DEFAULT_TOOLTIP_POSITION);
	});

	if(row.is(":data(ui-tooltip)"))
		row.tooltip("destroy");

	row.addClass("code");
	row.addClass("ui-state-highlight");
	row.attr("title", _IS_CODELIST_CODE_TITLE);

	row.tooltip(_DEFAULT_TOOLTIP_POSITION);
	row.tooltip("open");
};

function handleMatchingAbilitation() {
	var codelistsSelected = $(".codelistMeta .code").size() == 2 && $(".codelist .error").size() == 0;
	
	$("input.asButton[value='Next']").button(codelistsSelected ? "enable" : "disable");	
};

function listCodelists() {
	startOperation("Retrieving available codelists");

	$.ajax({
		cache: true,
		url: _WS_REPO_ENDPOINT + "/list",
		success: function(response) {
			for(var c=0; c<response.length; c++) {
				var codelist = response[c];
				
				var service = codelist.service == null ? "UNSPECIFIED" : codelist.service;

				if(typeof _REPOS[service] == "undefined") {
					_REPOS[service] = {};
					_REPOS[service].codelists = new Array();
					_REPOS[service].maxLen = 0;
				}
				
				_REPOS[service].codelists.push(codelist);
				
				_REPOS[service].maxLen = Math.max(_REPOS[service].maxLen, codelist.id.length);
			}
			
			$("#sourceCodelist, #targetCodelist").each(function() {
				var $elect = $(this);
				$elect.append("<option value=''>Please select a codelist</option>");
				$("option:first", $elect).remove();
			});
			
			$("#sourceRepo, #targetRepo").each(function() {
				var $elect = $(this);
				$elect.append("<option value=''>Please select a repository</option>");
				$("option:first", $elect).remove();
				
				var sorted = Object.keys(_REPOS);
				sorted.sort(function(a, b) { a = a.toUpperCase(); b = b.toUpperCase(); return a < b ? -1 : a > b ? 1 : 0 });
				
				for(var r=0; r<sorted.length; r++) {
					var repo = sorted[r];
					
					var option = $("<option/>");
					option.val(repo);
					option.html(repo.toUpperCase() + " (" + _REPOS[repo].codelists.length + " codelists)");
					
					$elect.append(option)
					
					$elect.change(function() {
						var $this = $(this);
						var value = $this.val();
						
						var $codelist = $this.next("select");
						
						if(value == '') {
							$codelist.val("");
							$codelist.attr("disabled", "disabled");
						} else {
							$codelist.empty();
							$codelist.append("<option value=''>Please select a codelist</option>");
							
							for(var c=0; c<_REPOS[value].codelists.length; c++) {
								codelist = _REPOS[value].codelists[c];
								
								option = $("<option/>");
								option.val(codelist.id);
								option.html("[ " + pad(codelist.id.toUpperCase(), _REPOS[value].maxLen).replace(/\s/g, '&#160;') + " ] : " + codelist.name);
								
								if(typeof _CL_STATUS[codelist.id] != "undefined")
									option.addClass(_CL_STATUS[codelist.id]);
								
								$codelist.append(option);
							}
							
							$codelist.removeAttr("disabled");
							
							$codelist.unbind("change").change(
								function() { 
									codelistChanged(this); 
								}
							);
						}
						
						$codelist.change();
					});
				}				
			});
			
			$("#sourceRepo, #targetRepo").removeAttr("disabled");
			
//			$("#sourceCodelist, #targetCodelist").each(function() {
//				var $elect = $(this);
//				$elect.append("<option value=''>Please select a codelist</option>");
//				$("option:first", $elect).remove();
//
//				var codelist;
//				var option;
//				
//				var service;
//				
//				var groups = { };
//
//				for(var c=0; c<response.length; c++) {
//					codelist = response[c];
//					
//					service = codelist.service == null ? "UNSPECIFIED" : codelist.service;
//
//					if(typeof groups[service] == "undefined") {
//						groups[service] = {};
//						groups[service].codelists = new Array();
//						groups[service].maxLen = 0;
//					}
//					
//					groups[service].codelists.push(codelist);
//					
//					groups[service].maxLen = Math.max(groups[service].maxLen, codelist.id.length);
//				}
//				
//				for(var group in groups) {
//					var optgroup = $("<optgroup label='" + group.toUpperCase() + " (" + groups[group].codelists.length + " codelists)'/>");
//					
//					for(var c=0; c<groups[group].codelists.length; c++) {
//						codelist = groups[group].codelists[c];
//						
//						option = $("<option/>");
//						option.val(codelist.id);
//						option.html("[ " + pad(codelist.id.toUpperCase(), groups[group].maxLen).replace(/\s/g, '&#160;') + " ] : " + codelist.name);
//						
//						optgroup.append(option);
//					}
//
//					$elect.append(optgroup);
//				}
//
//				if(response.length > 0)
//					$elect.removeAttr("disabled");
//
//				$elect.change(function() { codelistChanged(this); });
//			});
		},
		complete: completeOperation
	});
};

function codelistChanged(select) {
	var $elect = $(select);
	var $id = $elect.attr("id");

	var isSource = $id.indexOf("source") == 0;
	
	var other = $("#" + ( isSource ? "target" : "source" ) + "Codelist");
	var selected = $("option:selected", $elect).val();

	$("option", other).removeClass("selected");

	if(selected != null && selected != '') {
		mark($id, selected, "selected");
	}
	
	describeCodelist($elect);
};

function describeCodelist(codelist) {
	codelist = $(codelist);

	var selectedCodelist = codelist.val();

	var codelistTypeId = codelist.attr("id");

	var wrapper = $("." + codelistTypeId);
	var meta = $("#" + codelistTypeId + "Meta");
	var data = $("#" + codelistTypeId + "Data");

	var valued = $(".valued", meta);
	var empty =  $(".empty", meta);

	var dataValued = $(".valued", data);
	var dataEmpty = $(".empty", data);

	if(selectedCodelist != null && selectedCodelist != '') {
		block(meta, _PLEASE_WAIT_MESSAGE, _RETRIEVING_METADATA_MESSAGE);

		setTimeout(function() { doDescribeCodelist(codelistTypeId, selectedCodelist); }, 200);
	} else {
		$(".numRows, .maxRows", wrapper).text("----");

		valued.hide();
		empty.show();

		dataValued.hide();
		dataEmpty.show();
	}
};

function doDescribeCodelist(codelistTypeId, selectedCodelist) {
	var meta = $("#" + codelistTypeId + "Meta");

	var valued = $(".valued", meta);
	var empty =  $(".empty", meta);

	$.ajax({
		cache: true,
		url: _WS_REPO_ENDPOINT + "/describe/" + encodeURIComponent(selectedCodelist),
		success: function(response) {
//			unmark(codelistTypeId, selectedCodelist);

			displayMeta(codelistTypeId, selectedCodelist, response);
		},
		error: function(jqXHR, errorMessage, errorThrown) {
			mark(codelistTypeId, selectedCodelist, "unavailable");

			valued.empty().hide();
			empty.show();

			var data = $("#" + codelistTypeId + "Data");
			var dataValued = $(".valued", data);
			var dataEmpty = $(".empty", data);

			$("." + codelistTypeId + " .numRows").text("----");
			$("." + codelistTypeId + " .maxRows").text("----");

			dataValued.empty().hide();
			dataEmpty.show();

			mark(codelistTypeId, selectedCodelist, "error");

			handleMatchingAbilitation();

			alert("Unable to describe selected codelist: " + jqXHR.responseText);
		},
		complete: function() {
			meta.unblock();
		}
	});
};

function displayMeta(codelistTypeId, codelist, metadata) {
	var meta = $("#" + codelistTypeId + "Meta");

	var valued = $(".valued", meta);
	var empty =  $(".empty", meta);

	if(metadata != null &&
	   metadata.columnsMetadata != null &&
	   metadata.columnsMetadata.length > 0) {
		var table = "";
		table += "<table class='meta'>";
			table += "<thead class='ui-widget-header'><tr><th class='metaRowNum'>#</th><th class='metaRowName'>Name</th><th class='metaRowType'>Type</th></tr></thead>";
			table += "<tbody>";

		var isOriginalCode;

		for(var m=0; m<metadata.columnsMetadata.length; m++) {
			isOriginalCode = metadata.columnsMetadata[m].isCode;

			table += "<tr title=\"" + _SET_AS_CODELIST_CODE_TITLE + "\"" + (isOriginalCode ? " class='originalCode code'" : "") + ">";
				table += "<td class='metaRowNum'>" + ( isOriginalCode ? "[ CODE ] ": "" ) + ( m + 1 ) + "</td>";
				table += "<td class='metaRowName'>" + metadata.columnsMetadata[m].name + "</td>";
				table += "<td class='metaRowType'>" + metadata.columnsMetadata[m].type + "</td>";
		}

			table += "</tbody>";
		table += "</table>";

		valued.empty().append(table);

		table = $("table", valued);

		setAsCode(table, $("tr.originalCode", table));

		valued.show();

		$("[title]", valued).tooltip(_DEFAULT_TOOLTIP_POSITION);

		retrieveData(codelistTypeId, codelist, metadata);
	} else {
		valued.empty().append("<b>Unable to retrieve codelist's metadata</b>");

		var data = $("#" + codelistTypeId + "Data");
		$(".empty", data).hide();
		$(".valued", data).empty().show().append("<b>Unable to retrieve codelist's metadata</b>");
		
		mark(codelistTypeId, codelist, "unavailable");
	}

	empty.hide();
};

function retrieveData(codelistTypeId, selectedCodelist, metadata) {
	var data = $("#" + codelistTypeId + "Data");

	var valued = $(".valued", data);
	var empty =  $(".empty", data);

	if(selectedCodelist != null && selectedCodelist != '') {
		block(data, _PLEASE_WAIT_MESSAGE, _RETRIEVING_DATA_MESSAGE);

		setTimeout(function() { doRetrieveData(codelistTypeId, selectedCodelist, metadata); }, 200);
	} else {
		valued.hide();
		empty.show();
	}
};

function doRetrieveData(codelistTypeId, selectedCodelist, metadata) {
	var data = $("#" + codelistTypeId + "Data");

	var valued = $(".valued", data);
	var empty =  $(".empty", data);

	$.ajax({
		cache: true,
		url: _WS_REPO_ENDPOINT + "/retrieve/" + encodeURIComponent(selectedCodelist) + "/max/100",
		success: function(response, status, jqXHR) {
			var maxRows = jqXHR.getResponseHeader("MAX_ROWS_COUNT");

			$(data).scrollTo({ top: 0, left:0 });

			displayData(codelistTypeId, selectedCodelist, metadata, response, maxRows);

			$("." + codelistTypeId).removeClass("error");
		},
		error: function(jqXHR, errorMessage, errorThrown) {
			mark(codelistTypeId, selectedCodelist, "unavailable");
			
			$("." + codelistTypeId + " .numRows").text("----");
			$("." + codelistTypeId + " .maxRows").text("----");

			valued.empty().hide();
			empty.show();

			$("." + codelistTypeId).addClass("error");

			alert("Unable to retrieve selected codelist data: " + jqXHR.responseText);
		},
		complete: function() {
			data.unblock();

			handleMatchingAbilitation();
		}
	});
};

function displayData(codelistTypeId, selectedCodelist, metadata, codelistData, maxRows) {
	var data = $("#" + codelistTypeId + "Data");

	var valued = $(".valued", data);
	var empty =  $(".empty", data);

	if(metadata != null &&
	   metadata.columnsMetadata != null &&
	   metadata.columnsMetadata.length > 0 &&
	   codelistData != null &&
	   codelistData.length > 0) {
 		$("." + codelistTypeId + " .numRows").text(codelistData.length);
 		$("." + codelistTypeId + " .maxRows").text(maxRows);

		var table = "";
		table += "<table class='data'>";
			table += "<thead class='ui-widget-header'>";

				table +="<tr>";
					table += "<th class='metaRowNum'>#</th>";

			for(var m=0; m<metadata.columnsMetadata.length; m++) {
					table += "<th class='colName'>" + metadata.columnsMetadata[m].name + "</th>";
			}
				table +="</tr>";

			table += "</thead>";

			table += "<tbody>";

		var rowId, colDescription, value;
		for(var d=0; d<codelistData.length; d++) {
			rowId = d + 1;
				table += "<tr>";
					table += "<td class='metaRowNum' title='Row #" + rowId + "'>" + rowId + "</td>";
				for(var m=0; m<metadata.columnsMetadata.length; m++) {
					colDescription = metadata.columnsMetadata[m].name;
					value = codelistData[d][colDescription];
					table += "<td title='" + colDescription + "' " + ( value == null ? "class='null'" : "" ) + ">" + ( value == null ? "---" : value ) + "</td>";
				}
				table += "</tr>";
		}

			table += "</tbody>";
		table += "</table>";

		table = $(table);

		$("[title]", table).tooltip(_DEFAULT_TOOLTIP_POSITION);

		valued.empty().append(table).show();
	} else {
		$("." + codelistTypeId + " .numRows").text("----");
		$("." + codelistTypeId + " .maxRows").text("----");

		valued.empty();
		

		if(codelistData != null && codelistData.length > 0) {
			var msg = "Unable to retrieve metadata for selected codelist"; 
			valued.append("<b>" + msg + "</b>");
			error(msg);
			
			mark(codelistTypeId, selectedCodelist, "unavailable");
		} else {
			var msg = "No data available for selected codelist"; 
			valued.append("<b>" + msg + "</b>");
			warn(msg);
			
			mark(codelistTypeId, selectedCodelist, "empty");
		}
	}

	empty.hide();
};

function match() {
	var sourceCodelist = $("#sourceCodelist").val();
	var targetCodelist = $("#targetCodelist").val();
	var sourceCodelistID = $("#sourceCodelistMeta .code td.metaRowName").text();
	var targetCodelistID = $("#targetCodelistMeta .code td.metaRowName").text();

	var link = "";
	link += _WS_REPO_ENDPOINT + "/select/" +
				"source/" +
					encodeURIComponent(sourceCodelist) + "/id/" + encodeURIComponent(sourceCodelistID) + "/" +
				"target/" +
					encodeURIComponent(targetCodelist) + "/id/" + encodeURIComponent(targetCodelistID);

	startOperation("Transferring data");

	$.ajax({
		url: link,
		success: function() {
			goTo("matchingType.jsp");
		}
	});
};