<%@page import="org.fao.fi.comet.domain.codelists.mapping.ws.rest.SessionConstants"%>
<%@page language="java" errorPage="false" contentType="text/html; charset=UTF-8" trimDirectiveWhitespaces="true"%>
<!DOCTYPE html>
<html>
	<head>
		<title>COMET codelists matching application : EVALUATE RESULTS</title>
		<%@include file="_externalStyles.jspf"%>
		<link rel="stylesheet" type="text/css" href="styles/_common.css"></link>
		<link rel="stylesheet" type="text/css" href="styles/_commonMatch.css"></link>
		<link rel="stylesheet" type="text/css" href="styles/selectCodelists.css"></link>
		<link rel="stylesheet" type="text/css" href="styles/evaluateResults.css"></link>
	</head>
	<body class="block">
		<%@include file="_header.jspf"%>

		<h2 class="navigation ui-widget-header ui-helper-clearfix">
			<span class="left breadcrumb">
				1. <a href="selectCodelists.jsp">Select codelists</a> -
				2. <a href="matchingType.jsp">Choose matching type</a> -
				3. <a href="syntheticMatching.jsp">Configure matching</a> -
				4. <span class="current">Evaluate results</span>
			</span>
			<span class="right">
				<input type="button" value="Prev" class="asButton smaller" onclick="javascript:goTo('syntheticMatching.jsp');"></input>
				<input type="button" value="Next" class="asButton smaller" onclick="javascript:confirmMatchings();" disabled="disabled"></input>
			</span>
		</h2>

		<%@include file="_selectedCodelists.jspf"%>

		<div class="hidden ui-helper-clearfix">
			<div class="left half matchingsList ui-helper-clearfix">
				<div class="shadowWrapper">
					<h2 class="ui-widget-header">Matching results:
						<span class="matchingResultsReport">
							[ <span class="matchingResultsMatched">0</span>
							sources matched over
							<span class="matchingResultsTotal">----</span> ]
						</span>
					</h2>
					<div class="panel panelWrapper">
						<div id="matchingResults">
							<ul id="matchingsTree">
							</ul>
						</div>
						<div id="matchingFilters" class="ui-widget-header">
							<h3>
								Confirm:
								<input type="button" class="asButton smaller" value="All" onclick="javascript:confirmAll();"/>
								<input type="button" class="asButton smaller" value="None" onclick="javascript:confirmNone();"/>
								<input type="button" class="asButton smaller" value="Best" onclick="javascript:confirmBest();"/>
								<input type="button" class="asButton smaller" value="Full matches" onclick="javascript:confirmFullMatches();"/>
							</h3> 
						</div>
					</div>
				</div>
			</div>

			<div class="right half selectedMatching ui-helper-clearfix">
				<div class="shadowWrapper">
					<h2 class="ui-widget-header">Selected matching:</h2>
					<div class="panel panelWrapper ">
						<div id="matching">
							<div id="participants">
								<div class="ui-helper-clearfix">
									<div id="sourceParticipant" class="participantData ui-helper-clearfix left half shadowWrapper">
										<h3 class="ui-widget-header">Source:</h3>
										<div class="panel">
											<span class="participantCodelistInfo">
												<label>Codelist:</label> <span class="sourceCodelistId ui-state-highlight">---------</span>
												<label>Code:</label> <span class="sourceDataId ui-state-highlight">----</span>
											</span>
											<span class="participantCodelistInfo"><label>Data:</label></span>
											<span class="data">
												<ul/>
											</span>
										</div>
									</div>
									<div id="targetParticipant" class="participantData ui-helper-clearfix right half shadowWrapper">
										<h3 class="ui-widget-header">Target:</h3>
										<div class="panel">
											<span class="participantCodelistInfo">
												<label>Codelist:</label> <span class="targetCodelistId ui-state-highlight">---------</span>
												<label>Code:</label> <span class="targetDataId ui-state-highlight">----</span>
											</span>
											<span class="participantCodelistInfo"><label>Data:</label></span>
											<span class="data">
												<ul/>
											</span>
										</div>
									</div>
								</div>
							</div>
							<div id="details">
								<div class="shadowWrapper">
									<h3 class="ui-widget-header">Matching details:</h3>
									<div class="panel">
										<p><label>Score: </label><span class="matchingScore">----</span> %</p>
										<p><label>Type: </label><span class="matchingType">----</span></p>
										<p><label>Triggered matchers: </label><span class="triggeredMatchers">----</span></p>
										<div class="participatingMatchers">
											<ul/>
										</div>
										<div class="controls">
											<input type="button" value="Confirm" title="Confirm current matching as valid" class="asButton" disabled="disabled"/>
											<input type="button" value="Discard" title="Remove current matching from the list of valid ones" class="asButton" disabled="disabled"/>
											<input type="button" value="Show" title="Show current matching in the matchings tree" class="asButton" disabled="disabled"/>

										</div>
									</div>
								</div>
							</div>
						</div>
				 	</div>
				</div>
			</div>
		</div>
	</body>

	<%@include file="_externalLibs.jspf"%>
	<%@include file="_commonLibs.jspf"%>
	<%@include file="_sessionToJS.jspf"%>
	<script type="text/javascript" src="libs/_commonMatch.js"></script>
	<script type="text/javascript" src="libs/evaluateResults.js"></script>
</html>
