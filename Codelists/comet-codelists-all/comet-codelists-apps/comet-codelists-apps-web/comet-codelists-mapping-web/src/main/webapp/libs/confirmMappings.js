var SOURCE_CODELIST, TARGET_CODELIST;

function initializationCallback(oncompletion) {
	initializeInterface();

	buildCodelistsInfo(buildMatchingResultsInfo(oncompletion));
};

function initializeInterface() {
	$.blockUI.defaults.theme = true;
	$.blockUI.defaults.themedCSS.width = "auto";

	$("#matchingsTree").on("click", "li.matchingDetail", function(event) { $(this).tooltip("close"); $("ul.targets", $(this)).toggle(); event.preventDefault(); return false; } );
	$("#matchingsTree").on("click", "li.matching", function(event) { $(this).tooltip("close"); $("ul.matchers", $(this)).toggle(); event.preventDefault(); return false; });
};

function buildMatchingResultsInfo(oncompletion) {
	$.ajax({
		timeout: 5 * 60 * 1000,
		type: "GET",
		url: _WS_CONFIRM_ENDPOINT + "/results",
		dataType: "xml",
		success: function(data) {
			buildMatchingsTree(data, false);
			
			$(".targets, .matchers").show();

			if(isSet(oncompletion)) {
				oncompletion();
			}
		},
		error: function(jqXHR, textStatus, errorThrown) {
			alert("Unexpected error " + textStatus + ": " + errorThrown);
		},
		complete: function() {
			$("#mappingId").focus();
		}
	});
};

function persist(verbose) {
	startOperation("Persisting mapping in " + ( verbose ? "VERBOSE" : "BAREBONE" ) + " mode");
	
	$.ajax({
		timeout: 5 * 60 * 1000,
		url: _WS_CONFIRM_ENDPOINT + "/persist/" + ( verbose ? "verbose" : "barebone" ),
		cache: false,
		type: "POST",
		dataType: "xml",
		data: {
			id: $("input#mappingId").val(),
			name: $("input#mappingName").val(),
			version: $("input#mappingVersion").val(),
			author: $("input#mappingAuthor").val(),
			description: $("textarea#mappingDescription").val()
		},
		success: function(response) {
			info("Current mappings have been successfully persisted in " + ( verbose ? "VERBOSE" : "BAREBONE" ) + " mode.");

			var xml = new XMLSerializer().serializeToString(response);
			var form = $("<form method=\"POST\" action=\"" + _WS_CONFIRM_ENDPOINT + "/download/" + ( verbose ? "verbose" : "barebone" ) + "\"/>");
			
			var input = $("<input name=\"id\" type=\"text\"/>");
			input.attr("value", $("input#mappingId").val());
			
			form.append(input);
			
			input = $("<input name=\"name\" type=\"text\"/>");
			input.attr("value", $("input#mappingName").val());
			
			form.append(input);
			
			input = $("<input name=\"author\" type=\"text\"/>");
			input.attr("value", $("input#mappingAuthor").val());
			
			form.append(input);
			
			input = $("<input name=\"version\" type=\"text\"/>");
			input.attr("value", $("input#mappingId").val());
			
			form.append(input);
			
			input = $("<input name=\"description\" type=\"text\"/>");
			input.attr("value", $("textarea#mappingDescription").val());
			
			form.append(input);
			
			form.hide();
			
			$("body").append(form);
			
			form.submit();

			form.remove();

			completeOperation();
		},
		error: function(jqXHR, textStatus, errorThrown) {
			alert("Unable to persist mappings: " + jqXHR.responseText);
		}
	});
};

$(function() {
	$("input[type='text']").change(function() {
		var disable = false;

		$("input[type='text'].mandatory").each(function() {
			var $this = $(this);

			if($this.val().replace(/\s/g, '') === '') {
				disable = true;
			}
		});

		$("input[value='Barebone'], input[value='Verbose']").button(disable ? "disable" : "enable");
	});
});
