<%@page language="java" errorPage="false" contentType="text/html; charset=UTF-8" trimDirectiveWhitespaces="true"%>
<!DOCTYPE html>
<html>
	<head>
		<title>COMET codelists matching application : SELECT CODELISTS</title>
		<%@include file="_externalStyles.jspf"%>
		<link rel="stylesheet" type="text/css" href="styles/_common.css"></link>
		<link rel="stylesheet" type="text/css" href="styles/selectCodelists.css"></link>
	</head>
	<body class="hidden">
		<%@include file="_header.jspf"%>

		<h2 class="navigation ui-widget-header ui-helper-clearfix">
			<span class="left breadcrumb"><span class="current">1. Select codelists</span></span>
			<span class="right">
				<input type="button" class="asButton smaller" value="Next" disabled="disabled" onclick="javascript:match();"></input>
			</span>
		</h2>

		<div class="hidden container ui-helper-clearfix">
			<div class="sourceCodelist codelist left half">
				<div class="shadowWrapper">
					<h2 class="ui-widget-header">Source codelist:</h2>
					<p>
						<select id="sourceRepo" disabled="disabled">
							<option value="">Loading...</option>
						</select>
						<select id="sourceCodelist" disabled="disabled">
							<option value="">Loading...</option>
						</select>
					</p>
				</div>

				<div class="shadowWrapper">
					<h2 class="ui-widget-header">Metadata:</h2>
					<div id="sourceCodelistMeta" class="codelistMeta smaller">
						<p class="empty">
							Select a valid codelist first...
						</p>
						<p class="valued hidden"></p>
					</div>
				</div>
				<div class="shadowWrapper">
					<h2 class="ui-widget-header ui-helper-clearfix">
						<span class="left">Data:</span>
						<span class="right smaller">[ <span class="numRows">----</span> out of <span class="maxRows">----</span> rows ]</span>
					</h2>
					<div id="sourceCodelistData" class="codelistData smaller">
						<p class="empty">
							Select a valid codelist first...
						</p>
						<p class="valued hidden"></p>
				</div>
				</div>
			</div>
			<div class="targetCodelist codelist right half">
				<div class="shadowWrapper">
					<h2 class="ui-widget-header">Target codelist:</h2>
					<p>
						<select id="targetRepo" disabled="disabled">
							<option value="">Loading...</option>
						</select>
					
						<select id="targetCodelist" disabled="disabled">
							<option value="">Loading...</option>
						</select>
					</p>
				</div>

				<div class="shadowWrapper">
					<h2 class="ui-widget-header">Metadata:</h2>
					<div id="targetCodelistMeta" class="codelistMeta smaller">
						<p class="empty">
							Select a codelist first...
						</p>
						<p class="valued hidden"></p>
					</div>
				</div>

				<div class="shadowWrapper">
					<h2 class="ui-widget-header ui-helper-clearfix">
						<span class="left">Data:</span>
						<span class="right smaller">[ <span class="numRows">----</span> out of <span class="maxRows">----</span> rows ]</span>
					</h2>
					<div id="targetCodelistData" class="codelistData smaller">
						<p class="empty">
							Select a codelist first...
						</p>
						<p class="valued hidden"></p>
					</div>
				</div>
			</div>
			<div class="main left">
			</div>
		</div>
	</body>
	<%@include file="_externalLibs.jspf"%>
	<%@include file="_commonLibs.jspf"%>
	<script type="text/javascript" src="libs/selectCodelists.js"></script>
</html>