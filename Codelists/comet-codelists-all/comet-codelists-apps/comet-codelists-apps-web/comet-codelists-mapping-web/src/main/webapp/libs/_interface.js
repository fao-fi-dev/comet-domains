var _AFTER_INITIALIZATION_CALLBACKS = [];

function block(what, title, message) {
	$(":data('uiTooltip')").tooltip("close");

	$(what).block({
		theme: true,
		title: title == null ? _PLEASE_WAIT_MESSAGE : title,
		message: "<span class='smaller message'>" + ( message == null ? _PLEASE_WAIT_MESSAGE : message ) +
				 	"<img src='images/spinner.gif'/>" +
				 "</span>",
		themedCSS: {
			width: '80%',
			left: '7%'
		}
	});
};

function startOperation(operation) {
	block($("html"), "Please wait...", typeof operation === 'undefined' ? "Performing remote operation" : operation);
};

function completeOperation() {
	$("html").unblock();
};

function registerCallback(callback) {
	_AFTER_INITIALIZATION_CALLBACKS.push(callback);
};

function goTo(page) {
	var location = window.location.href;
	location = location.substring(0, location.lastIndexOf("/"));
	location += page.indexOf("/") == 0 ? page : "/" + page;

	window.location.href = location;

};

function reveal() {
	$(".hidden").fadeIn();

	var callback;
	for(var i=0; i<_AFTER_INITIALIZATION_CALLBACKS.length; i++) {
		callback = _AFTER_INITIALIZATION_CALLBACKS[i];

		try {
			callback();
		} catch (E) {
			;
		}
	}
		
	$(".asButton, .buttonSet").css("display", "inline-block");

	if($("body").is(".block"))
		$("html").unblock();
};

function message(level, title, text) {
	var dialog = $("<div class='dialog'><div class='dialogContent'/></div>");
	$(".dialogContent", dialog).html(text);
	
	if(level == 'error')
		dialog.addClass('ui-state-error');
	else if(level == 'warning')
		dialog.addClass('ui-state-highlight');
		
	dialog.dialog({
		title: title,
		modal: true,
		minWidth: 500,
		minHeight: 300,
		buttons: [
		{
			text : "Ok",
			click : function() {
				$(this).dialog("close");

				$(":data('uiTooltip')").tooltip("close");
			}
		} ]
	});
};

function err(text) {
	$("html").unblock();
	
	message('error', 'An error has occurred!', text);
};

function warn(text) {
	message('warning', 'Warning!', text);
};

function info(text) {
	message('info', 'Message', text);
};

window.alert = err;

$(function() {
	$(".asButton").button();
	$(".buttonSet").buttonset();

	$("[title].default").tooltip();

	try {
		if($("body").is(".block"))
			block($("html"), "Please wait...", "Initializing user interface");

		if(isFunction(initializationCallback))
			initializationCallback(reveal);
		else
			reveal();
	} catch (E) {
		reveal();
	}
});