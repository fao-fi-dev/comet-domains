function isSet(what) {
	return typeof what != 'undefined' && what != null; 
};

function notSet(what) {
	return !isSet(what); 
};

function is(who, what) {
	return isSet(who) && isSet(what) && typeof who === what;
};

function isUndefined(who) {
	return is(who, "undefined");
};

function isNumber(who) {
	return is(who, "number");
};

function isString(who) {
	return is(who, "string");
};

function isObject(who) {
	return is(who, "object");
};

function isFunction(who) {
	return is(who, "function");
};

function pad(string, length) {
	while(string.length < length)
		string += ' ';
	
	return string;
};

function pad(string, length) {
	while(string.length < length)
		string += ' ';
	
	return string;
};