<%@page language="java" errorPage="false" contentType="text/html; charset=UTF-8" trimDirectiveWhitespaces="true"%>
<!DOCTYPE html>
<html>
	<head>
		<title>COMET codelists matching : CHOOSE MATCHING TYPE</title>
		<%@include file="_externalStyles.jspf"%>
		<link rel="stylesheet" type="text/css" href="styles/_common.css"></link>
		<link rel="stylesheet" type="text/css" href="styles/_commonMatch.css"></link>
		<link rel="stylesheet" type="text/css" href="styles/selectCodelists.css"></link>
		<link rel="stylesheet" type="text/css" href="styles/matchingType.css"></link>
	</head>
	<body class="block">
		<%@include file="_header.jspf"%>

		<h2 class="navigation ui-widget-header ui-helper-clearfix">
			<span class="left breadcrumb">
				1. <a href="selectCodelists.jsp">Select codelists</a> - 
				<span class="current">2. Choose matching type</span></span>
			<span class="right">
				<input type="button" class="asButton smaller" value="Prev" onclick="javascript:goTo('selectCodelists.jsp');"/>
				<input type="button" class="asButton smaller" value="Next" onclick="javascript:route();"/>
			</span>
		</h2>

		<%@include file="_selectedCodelists.jspf"%>

		<div class="hidden">
			<div class="shadowWrapper">
				<h2 class="ui-widget-header">Available matching types:</h2>
				<div class="container panel panelWrapper">
					<div class="innerContainer">
						<div class="buttonSet ui-helper-clearfix">
							<label for="synthMatching">Synthetic matching</label>
							<input id="synthMatching" type="radio" name="matchingType" value="synth"/>
						</div>
						
						<div class="buttonSet">
							<label for="manualMatching">Manual matching</label>
							<input id="manualMatching" type="radio" name="matchingType" value="manual" checked="checked"/>
						</div>
					</div>
				</div>
			</div>
		</div>
	</body>

	<%@include file="_externalLibs.jspf"%>
	<%@include file="_commonLibs.jspf"%>
	<script type="text/javascript" src="libs/_commonMatch.js"></script>
	<script type="text/javascript" src="libs/matchingType.js"></script>
</html>
