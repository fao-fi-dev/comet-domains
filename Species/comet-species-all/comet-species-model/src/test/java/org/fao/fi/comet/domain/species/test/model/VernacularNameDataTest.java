/**
 * (c) 2013 FAO / UN (project: comet-species-model)
 */
package org.fao.fi.comet.domain.species.test.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.fao.fi.comet.domain.species.VernacularNamesFactory;
import org.fao.fi.comet.domain.species.model.VernacularNameData;
import org.junit.Assert;
import org.junit.Test;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 11 Oct 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 11 Oct 2013
 */
public class VernacularNameDataTest {
	static final private VernacularNameData VN1   = VernacularNamesFactory.newInstance("1", "English", "United Kingdom", "foo");
	static final private VernacularNameData VN1_1 = VernacularNamesFactory.newInstance("1_1", "English", "Scotland", "fooh");
	static final private VernacularNameData VN1_2 = VernacularNamesFactory.newInstance("1_2", "English", "Wales", "fooy");
	static final private VernacularNameData VN1_3 = VernacularNamesFactory.newInstance("1_3", "English", "United States of America", "foow");
	static final private VernacularNameData VN1_4 = VernacularNamesFactory.newInstance("1_4", "Dutch", "Zeeland", "voo");
	static final private VernacularNameData VN1_5 = VernacularNamesFactory.newInstance("1_5", "Spain", "Asturias", "foon");
	static final private VernacularNameData VN1_6 = VernacularNamesFactory.newInstance("1_5", "Spain", "Asturias", "foos");
	static final private VernacularNameData VN1_7 = VernacularNamesFactory.newInstance("1_7", "Spain", "Asturias", "foos");
	static final private VernacularNameData VN2   = VernacularNamesFactory.newInstance("2", null, "Sicily", "crastuna");
	static final private VernacularNameData VN3   = VernacularNamesFactory.newInstance("3", "French", null, "bouillebasse");
	static final private VernacularNameData VN3_1 = VernacularNamesFactory.newInstance("3_1", "French", "Biscay", null);
	static final private VernacularNameData VN3_2 = VernacularNamesFactory.newInstance(null, "French", "Biscay", null);

	@Test
	public void testSorting() {
		List<VernacularNameData> c1 = new ArrayList<VernacularNameData>();
		c1.add(VN1);
		c1.add(VN1_1);
		c1.add(VN1_2);
		c1.add(VN1_3);
		c1.add(VN1_4);
		c1.add(VN1_5);
		c1.add(VN1_7);
		c1.add(VN1_6);
		c1.add(VN2);
		c1.add(VN3);
		c1.add(VN3_1);
		c1.add(VN3_2);

		Collections.sort(c1);

		Collection<String> ids = new ArrayList<String>();

		for(VernacularNameData n : c1)
			ids.add(n.getParentId());

		String[] expectedIds = { "2", "1_4", "1_1", "1", "1_3", "1_2", "3", "3_1", null, "1_5", "1_5", "1_7" };

		Assert.assertArrayEquals(expectedIds, ids.toArray(new String[ids.size()]));
	}
}