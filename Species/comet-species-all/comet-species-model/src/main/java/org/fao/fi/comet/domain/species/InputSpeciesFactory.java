/**
 * (c) 2013 FAO / UN (project: comet-species)
 */
package org.fao.fi.comet.domain.species;

import org.fao.fi.comet.domain.species.model.InputSpeciesData;
import org.fao.fi.comet.domain.species.model.VernacularNameData;
import org.fao.fi.comet.extras.model.common.LinkedTypedComplexName;
import org.fao.fi.comet.extras.model.common.TypedComplexName;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 19/ott/2013   Fabio     Creation.
 *
 * @version 1.0
 * @since 19/ott/2013
 */
public class InputSpeciesFactory {
	static final public String DEFAULT_DATA_SOURCE_ID = "UserProvidedData";

	static final public InputSpeciesData newInstance(String original) {
		return InputSpeciesFactory.newInstance(DEFAULT_DATA_SOURCE_ID,
											   original,
											   original);
	}

	static final public InputSpeciesData newInstance(String dataSource, String id, String original) {
		return InputSpeciesFactory.newInstance(dataSource,
											   id,
											   original,
											   original,
											   original,
											   null,
											   original,
											   null);
	}

	static final public InputSpeciesData newInstance(String dataSource,
													 String id,
													 String original,
													 String preparsedOriginal,
													 String parsedScientificName,
													 String parsedAuthority,
													 String postParsedScientificName,
													 String postParsedAuthority) {
		return InputSpeciesFactory.newInstance(dataSource,
											   id,
											   original,
											   preparsedOriginal,
											   parsedScientificName,
											   parsedAuthority,
											   postParsedScientificName,
											   postParsedAuthority,
											   "NIL");
	}

	static final public InputSpeciesData newInstance(String dataSource,
													 String id,
													 String original,
													 String preparsedOriginal,
													 String parsedScientificName,
													 String parsedAuthority,
													 String postParsedScientificName,
													 String postParsedAuthority,
													 String parser) {
		return InputSpeciesFactory.newInstance(dataSource,
											   id,
											   original,
											   preparsedOriginal,
											   parsedScientificName,
											   parsedAuthority,
											   postParsedScientificName,
											   postParsedAuthority,
											   null,
											   parser);
	}

	static final public InputSpeciesData newInstance(String dataSource,
													 String id,
													 String original,
													 String preparsedOriginal,
													 String parsedScientificName,
													 String parsedAuthority,
													 String postParsedScientificName,
													 String postParsedAuthority,
													 TypedComplexName scientificCName) {
		return InputSpeciesFactory.newInstance(dataSource,
											   id,
											   original,
											   preparsedOriginal,
											   parsedScientificName,
											   parsedAuthority,
											   postParsedScientificName,
											   postParsedAuthority,
											   scientificCName,
											   "NIL");
	}

	static final public InputSpeciesData newInstance(String dataSource,
							 						 String id,
							 						 String original,
							 						 String preparsedOriginal,
													 String parsedScientificName,
													 String parsedAuthority,
													 String postParsedScientificName,
													 String postParsedAuthority,
													 TypedComplexName scientificCName,
							 						 String parser) {
		InputSpeciesData data = new InputSpeciesData();

		data.setDataSource(dataSource);
		data.setId(id);

		data.setOriginal(original);
		data.setPreparsedOriginal(preparsedOriginal);

		data.setParsedScientificName(parsedScientificName);
		data.setParsedAuthority(parsedAuthority);

		data.setPostParsedScientificName(postParsedScientificName);
		data.setPostParsedAuthority(postParsedAuthority);

		data.setScientificName(postParsedScientificName);
		data.setAuthor(postParsedAuthority);

		data.setScientificCName(scientificCName);

		data.setVernacularNames(new VernacularNameData[]{
			VernacularNamesFactory.newInstance(id, VernacularNamesFactory.UNKNOWN_LANGUAGE, null, original)
		});

		if(scientificCName != null)
			data.setVernacularCNames(new LinkedTypedComplexName[] {
				new LinkedTypedComplexName(id,
										   VernacularNamesFactory.VERNACULAR_CNAME_PREFIX + VernacularNamesFactory.UNKNOWN_LANGUAGE,
										   scientificCName.getName(),
										   scientificCName.getSimplifiedName(),
										   scientificCName.getSimplifiedNameNGrams(),
										   scientificCName.getSimplifiedNameSoundex()
				)
			});

		data.setParser(parser);

		return data;
	}
}