/**
 * (c) 2013 FAO / UN (project: comet-engine-species)
 */
package org.fao.fi.comet.domain.species.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlType;

import org.fao.fi.comet.core.model.common.annotations.Identifier;
import org.fao.fi.sh.model.core.spi.Identified;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 28 May 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 28 May 2013
 */
@XmlType(name="SpeciesData")
@XmlAccessorType(XmlAccessType.FIELD)
@NoArgsConstructor @AllArgsConstructor @EqualsAndHashCode @ToString
public class SpeciesData implements Serializable, Identified<String> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -9087328209898529981L;

	@XmlElement
	protected @Getter @Setter String dataSource;

	@Identifier
	@XmlElement
	protected @Getter @Setter String id;

	@XmlElement
	protected @Getter @Setter String kingdom;

	@XmlElement
	protected @Getter @Setter String phylum;

	@XmlElement(name="class")
	protected @Getter @Setter String clazz;

	@XmlElement
	protected @Getter @Setter String order;

	@XmlElement(name="family")
	protected @Getter @Setter String family;

	@XmlElement(name="genus")
	protected @Getter @Setter String genus;

	@XmlElement
	protected @Getter @Setter String species;

	@XmlElement
	protected @Getter @Setter String scientificName;

	@XmlElementWrapper(name="vernacularNames")
	@XmlElement(name="vernacularName")
	protected @Getter @Setter VernacularNameData[] vernacularNames;

	@XmlElement(name="author")
	protected @Getter @Setter String author;
}