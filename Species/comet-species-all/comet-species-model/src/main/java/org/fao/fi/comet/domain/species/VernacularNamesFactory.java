/**
 * (c) 2013 FAO / UN (project: comet-species-model)
 */
package org.fao.fi.comet.domain.species;

import java.util.List;

import org.fao.fi.comet.domain.species.model.VernacularNameData;
import org.fao.fi.comet.extras.model.common.LinkedTypedComplexName;
import org.fao.fi.sh.utility.core.helpers.singletons.text.StringNGramsHelper;
import org.fao.fi.sh.utility.lexical.processors.LexicalProcessor;
import org.fao.fi.sh.utility.lexical.soundex.PhraseSoundexGenerator;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 20/ott/2013   Fabio     Creation.
 *
 * @version 1.0
 * @since 20/ott/2013
 */
public class VernacularNamesFactory {
	static final public String VERNACULAR_CNAME_PREFIX = "vernacular_";
	static final public String UNKNOWN_LANGUAGE = "UNKNOWN";

	static public VernacularNameData newInstance(String parentId, String language, String locality, String name) {
		VernacularNameData data = new VernacularNameData();

		data.setParentId(parentId);
		data.setLanguage(language);
		data.setLocality(locality);
		data.setName(name);

		return data;
	}

	static public LinkedTypedComplexName toComplexName(VernacularNameData data, LexicalProcessor processor, PhraseSoundexGenerator soundexer) {
		LinkedTypedComplexName toReturn = new LinkedTypedComplexName();
		toReturn.setParentId(data.getParentId());
		toReturn.setName(data.getName());
		toReturn.setType(VERNACULAR_CNAME_PREFIX + data.getLanguage());

		toReturn.setSimplifiedName(processor.process(data.getName()));

		if(toReturn.getSimplifiedName() != null) {
			List<String> nGrams = StringNGramsHelper.extractPhraseNGrams(3, toReturn.getSimplifiedName(), true);

			toReturn.setSimplifiedNameNGrams(nGrams.toArray(new String[nGrams.size()]));
		}

		toReturn.setSimplifiedNameSoundex(soundexer.getFullPhraseSoundex(toReturn.getSimplifiedName(), PhraseSoundexGenerator.DONT_INCLUDE_DUPLICATE_PARTS));
		toReturn.setSimplifiedNameSoundexParts(toReturn.getSimplifiedNameSoundex() == null ? null : toReturn.getSimplifiedNameSoundex().split("\\s"));

		return toReturn;
	}
}