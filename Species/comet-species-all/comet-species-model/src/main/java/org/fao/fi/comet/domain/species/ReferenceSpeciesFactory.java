/**
 * (c) 2013 FAO / UN (project: comet-species)
 */
package org.fao.fi.comet.domain.species;

import org.fao.fi.comet.domain.species.model.ReferenceSpeciesData;
import org.fao.fi.comet.domain.species.model.SpeciesData;
import org.fao.fi.comet.domain.species.model.VernacularNameData;
import org.fao.fi.comet.extras.model.common.TypedComplexName;
import org.fao.fi.sh.utility.core.helpers.singletons.text.StringsHelper;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 19/ott/2013   Fabio     Creation.
 *
 * @version 1.0
 * @since 19/ott/2013
 */
public class ReferenceSpeciesFactory extends CommonReferenceSpeciesFactory {
	static final public ReferenceSpeciesData newInstance(String name) {
		return ReferenceSpeciesFactory.newInstance(name, null);
	}

	static final public ReferenceSpeciesData newInstance(String scientificName, String author) {
		ReferenceSpeciesData data = new ReferenceSpeciesData();
		data.setId(scientificName);
		data.setScientificName(scientificName);
		data.setAuthor(author);

		data.setGenericCName(new TypedComplexName(ReferenceSpeciesData.GENERIC_CNAME, scientificName, null, null, null));

		return data;
	}

	static final public ReferenceSpeciesData newInstance(String dataSource,
														 String id,
														 String kingdom,
														 String phylum,
														 String klass,
														 String order,
														 String family,
														 String genus,
														 String species,
														 String scientificName,
														 VernacularNameData[] vernacularNames,
														 String author) {
		ReferenceSpeciesData data = new ReferenceSpeciesData();

		data.setDataSource(dataSource);
		data.setId(id);
		data.setKingdom(kingdom);
		data.setPhylum(phylum);
		data.setClazz(klass);
		data.setOrder(order);
		data.setFamily(family);
		data.setGenus(genus);
		data.setSpecies(species);
		data.setScientificName(StringsHelper.trim(( genus == null ? "" : genus ) + ( species == null ? "" : " " + species )));
		data.setVernacularNames(vernacularNames);
		data.setAuthor(author);

		return data;
	}

	static final public ReferenceSpeciesData newInstance(SpeciesData speciesData) {
		return ReferenceSpeciesFactory.newInstance(speciesData.getDataSource(),
												   speciesData.getId(),
												   speciesData.getKingdom(),
												   speciesData.getPhylum(),
												   speciesData.getClazz(),
												   speciesData.getOrder(),
												   speciesData.getFamily(),
												   speciesData.getGenus(),
												   speciesData.getSpecies(),
												   speciesData.getScientificName(),
												   speciesData.getVernacularNames(),
												   speciesData.getAuthor());
	}
}
