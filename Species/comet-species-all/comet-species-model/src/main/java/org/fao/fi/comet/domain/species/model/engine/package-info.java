@XmlSchema(namespace = "http://engine.model.species.domain.comet.fi.fao.org", elementFormDefault = XmlNsForm.UNQUALIFIED)
package org.fao.fi.comet.domain.species.model.engine;
import javax.xml.bind.annotation.XmlNsForm;
import javax.xml.bind.annotation.XmlSchema;
