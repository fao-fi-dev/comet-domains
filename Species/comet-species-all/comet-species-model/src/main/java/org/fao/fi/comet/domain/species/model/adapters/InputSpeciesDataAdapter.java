/**
 * (c) 2013 FAO / UN (project: comet-engine-species)
 */
package org.fao.fi.comet.domain.species.model.adapters;

import javax.xml.bind.annotation.adapters.XmlAdapter;

import org.fao.fi.comet.domain.species.InputSpeciesFactory;
import org.fao.fi.comet.domain.species.model.InputSpeciesData;
import org.fao.fi.comet.extras.model.common.TypedComplexName;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 31 May 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 31 May 2013
 */
public class InputSpeciesDataAdapter extends XmlAdapter<Object[], InputSpeciesData> {
	@Override
	public InputSpeciesData unmarshal(Object[] value) {
		return InputSpeciesFactory.newInstance(
			(String)value[0],
			(String)value[1],
			(String)value[2],
			(String)value[3],
			(String)value[4],
			(String)value[5],
			(String)value[6],
			(String)value[7],
			(TypedComplexName)value[9],
			(String)value[10]
		);
	}

	@Override
	public Object[] marshal(InputSpeciesData boundType) {
		return new Object[] {
			boundType.getDataSource(),
			boundType.getId(),
			boundType.getOriginal(),
			boundType.getPreparsedOriginal(),
			boundType.getParsedScientificName(),
			boundType.getParsedAuthority(),
			boundType.getPostParsedScientificName(),
			boundType.getPostParsedAuthority(),
			boundType.getScientificCName(),
			boundType.getParser()
		};
	}
}
