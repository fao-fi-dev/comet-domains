/**
 * (c) 2013 FAO / UN (project: comet-species-model)
 */
package org.fao.fi.comet.domain.species;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.fao.fi.comet.domain.species.model.ReferenceSpeciesData;
import org.fao.fi.comet.domain.species.model.VernacularNameData;
import org.fao.fi.comet.extras.model.common.LinkedTypedComplexName;
import org.fao.fi.comet.extras.model.common.TypedComplexName;
import org.fao.fi.sh.utility.core.helpers.singletons.lang.CollectionsHelper;
import org.fao.fi.sh.utility.core.helpers.singletons.text.StringNGramsHelper;
import org.fao.fi.sh.utility.core.helpers.singletons.text.StringsHelper;
import org.fao.fi.sh.utility.lexical.processors.LexicalProcessor;
import org.fao.fi.sh.utility.lexical.soundex.PhraseSoundexGenerator;
import org.fao.fi.sh.utility.lexical.soundex.SoundexGenerator;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 01/nov/2013   Fabio     Creation.
 *
 * @version 1.0
 * @since 01/nov/2013
 */
public class CommonReferenceSpeciesFactory {
	static final protected Pattern AUTHORITY_YEAR_PATTERN = Pattern.compile(".*([0-9]{4}).*");

	static public <S extends ReferenceSpeciesData> S updateComplexNames(S data, LexicalProcessor speciesSimplifierProcessor, LexicalProcessor speciesNormalizerProcessor, LexicalProcessor authoritySimplifierProcessor, LexicalProcessor authorityNormalizerProcessor, PhraseSoundexGenerator phraseSoundexer, SoundexGenerator soundexer) {
		if(speciesSimplifierProcessor == null ||
		   speciesNormalizerProcessor == null ||
		   authoritySimplifierProcessor == null ||
		   authorityNormalizerProcessor == null ||
		   phraseSoundexer == null ||
		   soundexer == null)
			return data;

		String simplified, simplifiedSoundex, normalized, normalizedSoundex;
		List<String> nGrams;

		if(data.getScientificName() != null) {
			simplified = speciesSimplifierProcessor.process(data.getScientificName());
			simplifiedSoundex = simplified == null ? null : phraseSoundexer.getFullPhraseSoundex(simplified, PhraseSoundexGenerator.DONT_INCLUDE_DUPLICATE_PARTS);

			nGrams = simplified == null ? null : StringNGramsHelper.extractPhraseNGrams(3, simplified, true);

			String[] parts = data.getScientificName().split(" ");

			data.setScientificCName(new TypedComplexName(ReferenceSpeciesData.SCIENTIFIC_CNAME, data.getScientificName(), simplified, nGrams == null ? null : nGrams.toArray(new String[nGrams.size()]), simplifiedSoundex));

			if(data.getSpecies() == null && data.getGenus() == null) {
				if (parts != null && parts.length >= 1) {
					if (simplified != null && data.getGenus() == null) {
						data.setGenus(parts[0]);
	
						simplified = speciesSimplifierProcessor.process(data.getGenus());
						simplifiedSoundex = simplified == null ? null : phraseSoundexer.getFullPhraseSoundex(simplified, PhraseSoundexGenerator.DONT_INCLUDE_DUPLICATE_PARTS);
	
						if (parts.length >= 2 && data.getSpecies() == null) {
							String remainingParts = CollectionsHelper.join(CollectionsHelper.behead(parts), " ");
	
							data.setSpecies(remainingParts);
	
							simplified = speciesSimplifierProcessor.process(remainingParts);
							simplifiedSoundex = simplified == null ? null : phraseSoundexer.getFullPhraseSoundex(simplified, PhraseSoundexGenerator.DONT_INCLUDE_DUPLICATE_PARTS);
	
							nGrams = simplified == null ? null : StringNGramsHelper.extractPhraseNGrams(3, simplified, true);
	
							if (simplified != null) {
								data.setSpeciesCName(new TypedComplexName(ReferenceSpeciesData.SPECIES_CNAME, remainingParts, simplified, nGrams == null ? null : nGrams.toArray(new String[nGrams.size()]), simplifiedSoundex));
							}
						}
					}
				}
			}
		}

		if (data.getGenericCName() != null) {
			simplified = speciesSimplifierProcessor.process(data.getGenericCName().getName());

			if (simplified != null) {
				CommonReferenceSpeciesFactory.update(data.getGenericCName(), simplified, phraseSoundexer.getFullPhraseSoundex(simplified, PhraseSoundexGenerator.DONT_INCLUDE_DUPLICATE_PARTS));
			} else {
				data.setGenericCName(null);
			}
		}

		if (data.getKingdom() != null) {
			simplified = speciesSimplifierProcessor.process(data.getKingdom());

			if (simplified != null) {
				data.setKingdomCName(new TypedComplexName(ReferenceSpeciesData.KINGDOM_CNAME));

				CommonReferenceSpeciesFactory.update(data.getKingdomCName(), data.getKingdom(), simplified, phraseSoundexer.getFullPhraseSoundex(simplified, PhraseSoundexGenerator.DONT_INCLUDE_DUPLICATE_PARTS));
			} else {
				data.setKingdomCName(null);
			}
		}

		if (data.getPhylum() != null) {
			simplified = speciesSimplifierProcessor.process(data.getPhylum());

			if (simplified != null) {
				data.setPhylumCName(new TypedComplexName(ReferenceSpeciesData.PHYLUM_CNAME));

				CommonReferenceSpeciesFactory.update(data.getPhylumCName(), data.getPhylum(), simplified, phraseSoundexer.getFullPhraseSoundex(simplified, PhraseSoundexGenerator.DONT_INCLUDE_DUPLICATE_PARTS));
			} else {
				data.setPhylumCName(null);
			}
		}

		if (data.getClazz() != null) {
			simplified = speciesSimplifierProcessor.process(data.getClazz());

			if (simplified != null) {
				data.setClassCName(new TypedComplexName(ReferenceSpeciesData.CLASS_CNAME));

				CommonReferenceSpeciesFactory.update(data.getClassCName(), data.getClazz(), simplified, phraseSoundexer.getFullPhraseSoundex(simplified, PhraseSoundexGenerator.DONT_INCLUDE_DUPLICATE_PARTS));
			} else {
				data.setClassCName(null);
			}
		}

		if (data.getOrder() != null) {
			simplified = speciesSimplifierProcessor.process(data.getOrder());

			if (simplified != null) {
				data.setOrderCName(new TypedComplexName(ReferenceSpeciesData.ORDER_CNAME));

				CommonReferenceSpeciesFactory.update(data.getOrderCName(), data.getOrder(), simplified, phraseSoundexer.getFullPhraseSoundex(simplified, PhraseSoundexGenerator.DONT_INCLUDE_DUPLICATE_PARTS));
			} else {
				data.setOrderCName(null);
			}
		}

		if (data.getFamily() != null) {
			simplified = speciesSimplifierProcessor.process(data.getFamily());

			if (simplified != null) {
				data.setFamilyCName(new TypedComplexName(ReferenceSpeciesData.FAMILY_CNAME));

				CommonReferenceSpeciesFactory.update(data.getFamilyCName(), data.getFamily(), simplified, phraseSoundexer.getFullPhraseSoundex(simplified, PhraseSoundexGenerator.DONT_INCLUDE_DUPLICATE_PARTS));
			} else {
				data.setFamilyCName(null);
			}
		}

		if (data.getGenus() != null) {
			simplified = speciesSimplifierProcessor.process(data.getGenus());

			nGrams = simplified == null ? null : StringNGramsHelper.extractPhraseNGrams(3, simplified, true);

			if (simplified != null) {
				data.setGenusCName(new TypedComplexName(ReferenceSpeciesData.GENUS_CNAME, data.getGenus(), simplified, nGrams == null ? null : nGrams.toArray(new String[nGrams.size()]), phraseSoundexer.getFullPhraseSoundex(simplified, PhraseSoundexGenerator.DONT_INCLUDE_DUPLICATE_PARTS)));

				normalized = StringsHelper.trim(speciesNormalizerProcessor.process(simplified));

				nGrams = normalized == null ? null : StringNGramsHelper.extractPhraseNGrams(3, normalized, true);

				normalizedSoundex = normalized == null ? null : soundexer.computeSoundex(normalized);

				data.setNormalizedGenusCName(new TypedComplexName(ReferenceSpeciesData.NORM_SPECIES_CNAME, normalized, normalized, nGrams == null ? null : nGrams.toArray(new String[nGrams.size()]), normalizedSoundex));

			} else {
				data.setGenusCName(null);
				data.setNormalizedGenusCName(null);
			}
		}

		if (data.getSpecies() != null) {
			simplified = speciesSimplifierProcessor.process(data.getSpecies());

			nGrams = simplified == null ? null : StringNGramsHelper.extractPhraseNGrams(3, simplified, true);

			if (simplified != null) {
				data.setSpeciesCName(new TypedComplexName(ReferenceSpeciesData.SPECIES_CNAME, data.getSpecies(), simplified, nGrams == null ? null : nGrams.toArray(new String[nGrams.size()]), phraseSoundexer.getFullPhraseSoundex(simplified, PhraseSoundexGenerator.DONT_INCLUDE_DUPLICATE_PARTS)));

				normalized = StringsHelper.trim(speciesNormalizerProcessor.process(simplified));

				nGrams = normalized == null ? null : StringNGramsHelper.extractPhraseNGrams(3, normalized, true);

				normalizedSoundex = normalized == null ? null : soundexer.computeSoundex(normalized);

				data.setNormalizedSpeciesCName(new TypedComplexName(ReferenceSpeciesData.NORM_SPECIES_CNAME, normalized, normalized, nGrams == null ? null : nGrams.toArray(new String[nGrams.size()]), normalizedSoundex));
			} else {
				data.setSpeciesCName(null);
				data.setNormalizedSpeciesCName(null);
			}
		}

		if (data.getAuthor() != null) {
			String authorities = StringsHelper.trim(data.getAuthor());

			Set<String> authoritiesSet = new HashSet<String>();

			if (authorities != null) {
				Matcher yearMatcher = AUTHORITY_YEAR_PATTERN.matcher(authorities);

				if (yearMatcher.matches())
					data.setAuthorityYear(Integer.parseInt(yearMatcher.group(1)));

				authorities = authorities == null ? null : StringsHelper.trim(authorities.replaceAll("[\\d\\(\\)]", ""));
				authorities = authorities == null ? null : StringsHelper.trim(authorities.replaceAll(",$", ""));

				data.setAuthority(authorities);

				List<TypedComplexName> authors = new ArrayList<TypedComplexName>();

				String normalizedAuthorities = authorityNormalizerProcessor.process(authorities);
				String simplifiedAuthority;

				if (normalizedAuthorities != null)
					for (String authority : normalizedAuthorities.split("\\,", -1)) {
						authority = StringsHelper.trim(authority);

						if (!authoritiesSet.contains(authority)) {
							simplifiedAuthority = authoritySimplifierProcessor.process(authority);

							nGrams = simplifiedAuthority == null ? null : StringNGramsHelper.extractPhraseNGrams(3, simplifiedAuthority, true);

							if (simplifiedAuthority != null && simplifiedAuthority.length() > 1) {
								authors.add(new TypedComplexName(ReferenceSpeciesData.AUTHORITY_CNAME, authority, simplifiedAuthority, nGrams == null ? null : nGrams.toArray(new String[nGrams.size()]), phraseSoundexer.getFullPhraseSoundex(simplifiedAuthority, PhraseSoundexGenerator.DONT_INCLUDE_DUPLICATE_PARTS)));
							}

							authoritiesSet.add(authority);
						}
					}

				if (!authors.isEmpty())
					data.setAuthoritiesCName(authors.toArray(new TypedComplexName[authors.size()]));
			}
		}

		ReferenceSpeciesFactory.updateComplexVernacularNames(data, speciesSimplifierProcessor, speciesNormalizerProcessor, authoritySimplifierProcessor, authorityNormalizerProcessor, phraseSoundexer, soundexer);

		return data;
	}

	// static public ReferenceSpeciesData updateComplexVernacularNames(ReferenceSpeciesData data) {
	// return ReferenceSpeciesFactory.updateComplexVernacularNames(data, SPECIES_SIMPLIFIER_PROCESSOR, SPECIES_NORMALIZER_PROCESSOR, AUTHORITY_SIMPLIFIER_PROCESSOR, AUTHORITY_NORMALIZER_PROCESSOR, PHRASE_SOUNDEXER, BASIC_SOUNDEXER);
	// }

	static public ReferenceSpeciesData updateComplexVernacularNames(ReferenceSpeciesData data, LexicalProcessor speciesSimplifierProcessor, LexicalProcessor speciesNormalizerProcessor, LexicalProcessor authoritySimplifierProcessor, LexicalProcessor authorityNormalizerProcessor, PhraseSoundexGenerator phraseSoundexer, SoundexGenerator soundexer) {
		if (data.getVernacularNames() != null) {
			List<VernacularNameData> sorted = Arrays.asList(data.getVernacularNames());

			Collections.sort(sorted);

			data.setVernacularNames(sorted.toArray(new VernacularNameData[sorted.size()]));

			Collection<LinkedTypedComplexName> vernacularCNames = new ArrayList<LinkedTypedComplexName>();

			for (VernacularNameData vernacularName : data.getVernacularNames()) {
				if (vernacularName != null) {
					vernacularCNames.add(VernacularNamesFactory.toComplexName(vernacularName, speciesSimplifierProcessor, phraseSoundexer));
				}
			}

			if (!vernacularCNames.isEmpty())
				data.setVernacularCNames(vernacularCNames.toArray(new LinkedTypedComplexName[vernacularCNames.size()]));
		}

		return data;
	}

	static private TypedComplexName update(TypedComplexName cName, String simplified, String simplifiedSoundex) {
		cName.setSimplifiedName(simplified);

		if (simplified != null) {
			List<String> nGrams = StringNGramsHelper.extractPhraseNGrams(3, simplified, true);

			cName.setSimplifiedNameNGrams(nGrams.toArray(new String[nGrams.size()]));
		}

		cName.setSimplifiedNameSoundex(simplifiedSoundex);

		if (simplifiedSoundex != null)
			cName.setSimplifiedNameSoundexParts(simplifiedSoundex.split("\\s"));

		return cName;
	}

	static private TypedComplexName update(TypedComplexName cName, String name, String simplified, String simplifiedSoundex) {
		TypedComplexName updated = CommonReferenceSpeciesFactory.update(cName, simplified, simplifiedSoundex);
		updated.setName(name);

		return updated;
	}
}
