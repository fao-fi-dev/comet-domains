/**
 * (c) 2013 FAO / UN (project: comet-species-model)
 */
package org.fao.fi.comet.domain.species.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 1 Oct 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 1 Oct 2013
 */
@XmlType(name="VernacularNameData")
@XmlAccessorType(XmlAccessType.FIELD)
@NoArgsConstructor @AllArgsConstructor @EqualsAndHashCode @ToString
public class VernacularNameData implements Serializable, Comparable<VernacularNameData> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -4792168612822020983L;

	@XmlElement(name="parentId")
	private @Getter @Setter String parentId;

	@XmlElement(name="language")
	private @Getter @Setter String language;

	@XmlElement(name="locality")
	private @Getter @Setter String locality;

	@XmlElement(name="name")
	private @Getter @Setter String name;

	/* (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(VernacularNameData other) {
		if(other == null)
			return 1;

		if(other == this || other.equals(this))
			return 0;

		int result = this.language == null ?
						( other.language == null ? 0 : -1 )
					 :
						( other.language == null ? 1 : this.language.compareTo(other.language) );

		if(result != 0)
			return result;

		result = this.locality == null ?
					( other.locality == null ? 0 : -1 )
				 :
					( other.locality == null ? 1 : this.locality.compareTo(other.locality) );

		if(result != 0)
			return result;

		result = this.name == null ?
					( other.name == null ? 0 : -1 )
				 :
					( other.name == null ? 1 : this.name.compareTo(other.name) );

		if(result != 0)
			return result;

		return this.parentId == null ?
					( other.parentId == null ? 0 : 1 )
			   :
				    ( other.parentId == null ? -1 : this.parentId.compareTo(other.parentId) );
	}
}
