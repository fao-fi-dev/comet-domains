/**
 * (c) 2013 FAO / UN (project: comet-engine-species)
 */
package org.fao.fi.comet.domain.species.model;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlType;

import org.fao.fi.comet.extras.model.common.LinkedTypedComplexName;
import org.fao.fi.comet.extras.model.common.TypedComplexName;
import org.fao.fi.sh.utility.model.extensions.collections.ListSet;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 28 May 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 28 May 2013
 */
@XmlType(name="ReferenceSpeciesData")
@XmlAccessorType(XmlAccessType.FIELD)
@NoArgsConstructor @AllArgsConstructor @EqualsAndHashCode(callSuper=true) @ToString
public class ReferenceSpeciesData extends SpeciesData {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 5989571788918530470L;

	static final public String KINGDOM_CNAME = "kingdom";
	static final public String PHYLUM_CNAME = "phylum";
	static final public String CLASS_CNAME = "class";
	static final public String FAMILY_CNAME = "family";
	static final public String ORDER_CNAME = "order";

	static final public String GENUS_CNAME = "genus";
	static final public String NORM_GENUS_CNAME = "normalizedGenus";

	static final public String SPECIES_CNAME = "species";
	static final public String NORM_SPECIES_CNAME = "normalizedSpecies";

	static final public String GENERIC_CNAME = "generic";
	static final public String SCIENTIFIC_CNAME = "scientific";

	static final public String AUTHORITY_CNAME = "authority";

	@XmlTransient
	private @Getter @Setter String authority;

	@XmlTransient
	private @Getter @Setter Integer authorityYear;

	@XmlTransient
	private @Getter @Setter TypedComplexName kingdomCName;

	@XmlTransient
	private @Getter @Setter TypedComplexName phylumCName;

	@XmlTransient
	private @Getter @Setter TypedComplexName classCName;

	@XmlTransient
	private @Getter @Setter TypedComplexName orderCName;

	@XmlTransient
	private @Getter @Setter TypedComplexName familyCName;

	@XmlTransient
	private @Getter @Setter TypedComplexName genusCName;

	@XmlTransient
	private @Getter @Setter TypedComplexName normalizedGenusCName;

	@XmlTransient
	private @Getter @Setter TypedComplexName speciesCName;

	@XmlTransient
	private @Getter @Setter TypedComplexName normalizedSpeciesCName;

	@XmlTransient
	private @Getter @Setter TypedComplexName scientificCName;

	@XmlTransient
	private @Getter @Setter TypedComplexName genericCName;

	@XmlTransient
	private @Getter @Setter LinkedTypedComplexName[] vernacularCNames;

	@XmlTransient
	private @Getter @Setter TypedComplexName[] authoritiesCName;

	private TypedComplexName[] toArray(TypedComplexName toWrap) {
		if(toWrap == null)
			return null;

		return new TypedComplexName[] { toWrap };
	}

	public Map<String, TypedComplexName[]> getComplexNamesMap() {
		boolean isSimpleSpeciesData = this instanceof InputSpeciesData; //this.scientificCName.equalData(this.genericCName);

		Map<String, TypedComplexName[]> cNamesMap = new HashMap<String, TypedComplexName[]>();

		cNamesMap.put(SCIENTIFIC_CNAME, this.toArray(this.scientificCName));

		if(this.genericCName != null && !isSimpleSpeciesData) //!this.genericCName.equals(this.scientificCName))
			cNamesMap.put(GENERIC_CNAME, this.toArray(this.genericCName));

		if(this.kingdomCName != null && !isSimpleSpeciesData)
			cNamesMap.put(KINGDOM_CNAME, this.toArray(this.kingdomCName));

		if(this.phylumCName != null && !isSimpleSpeciesData) //!this.orderCName.equals(this.scientificCName))
			cNamesMap.put(PHYLUM_CNAME, this.toArray(this.phylumCName));

		if(this.classCName != null && !isSimpleSpeciesData) //!this.familyCName.equals(this.scientificCName))
			cNamesMap.put(CLASS_CNAME, this.toArray(this.classCName));

		if(this.orderCName != null && !isSimpleSpeciesData) //!this.orderCName.equals(this.scientificCName))
			cNamesMap.put(ORDER_CNAME, this.toArray(this.orderCName));

		if(this.familyCName != null && !isSimpleSpeciesData) //!this.familyCName.equals(this.scientificCName))
			cNamesMap.put(FAMILY_CNAME, this.toArray(this.familyCName));

		if(this.orderCName != null && !isSimpleSpeciesData) //!this.orderCName.equals(this.scientificCName))
			cNamesMap.put(ORDER_CNAME, this.toArray(this.orderCName));

		if(this.genusCName != null && !isSimpleSpeciesData) //!this.genusCName.equals(this.scientificCName))
			cNamesMap.put(GENUS_CNAME, this.toArray(this.genusCName));

		if(this.normalizedGenusCName != null && !isSimpleSpeciesData) //!this.genusCName.equals(this.scientificCName))
			cNamesMap.put(NORM_GENUS_CNAME, this.toArray(this.normalizedGenusCName));

		if(this.speciesCName != null && !isSimpleSpeciesData) //!this.speciesCName.equals(this.scientificCName))
			cNamesMap.put(SPECIES_CNAME, this.toArray(this.speciesCName));

		if(this.normalizedSpeciesCName != null && !isSimpleSpeciesData) //!this.genusCName.equals(this.scientificCName))
			cNamesMap.put(NORM_SPECIES_CNAME, this.toArray(this.normalizedSpeciesCName));

		if(this.vernacularCNames != null) {
			Map<String, Collection<TypedComplexName>> vernacularNamesMap = new HashMap<String, Collection<TypedComplexName>>();

			Collection<TypedComplexName> byLanguage;
			for(TypedComplexName vernacularCName : this.vernacularCNames) {
				byLanguage = vernacularNamesMap.get(vernacularCName.getType());

				if(byLanguage == null) {
					byLanguage = new ListSet<TypedComplexName>();

					vernacularNamesMap.put(vernacularCName.getType(), byLanguage);
				}

				byLanguage.add(vernacularCName);
			}

			for(String type : vernacularNamesMap.keySet()) {
				byLanguage = vernacularNamesMap.get(type);

				if(byLanguage != null && !byLanguage.isEmpty())
					cNamesMap.put(type, byLanguage.toArray(new TypedComplexName[byLanguage.size()]));
			}
		}

		//Also add 'authorities' cnames?

		return cNamesMap;
	}
}