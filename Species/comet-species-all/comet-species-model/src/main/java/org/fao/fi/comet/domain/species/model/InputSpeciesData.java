/**
 * (c) 2013 FAO / UN (project: comet-engine-species)
 */
package org.fao.fi.comet.domain.species.model;

import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

import org.fao.fi.comet.core.model.common.annotations.Identifier;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 28 May 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 28 May 2013
 */
@XmlType(name="InputSpeciesData")
@XmlAccessorType(XmlAccessType.FIELD)
@NoArgsConstructor @AllArgsConstructor @EqualsAndHashCode(callSuper=true) @ToString
final public class InputSpeciesData extends ReferenceSpeciesData {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 5989571788918530470L;

	@Identifier
	@XmlElement(name="originalData")
	private @Getter @Setter String original;

	@XmlElement(name="preparsedOriginalData")
	private @Getter @Setter String preparsedOriginal;

	@XmlElement(name="parsedScientificName")
	private @Getter @Setter String parsedScientificName;

	@XmlElement(name="parsedAuthority")
	private @Getter @Setter String parsedAuthority;

	@XmlElement(name="postParsedScientificName")
	private @Getter @Setter String postParsedScientificName;

	@XmlElement(name="postParsedAuthority")
	private @Getter @Setter String postParsedAuthority;

	@XmlElement(name="parser")
	private @Getter @Setter String parser;

	void afterUnmarshal(Unmarshaller u, Object parent) {
		this.setScientificName(this.parsedScientificName);
		this.setAuthor(this.parsedAuthority);
	}
}