/**
 * (c) 2013 FAO / UN (project: comet-species)
 */
package org.fao.fi.comet.domain.species.test.patterns.data.partitioners;

import org.fao.fi.comet.domain.species.ReferenceSpeciesFactory;
import org.fao.fi.comet.domain.species.model.ReferenceSpeciesData;
import org.fao.fi.comet.domain.species.patterns.data.partitioners.InitialsBasedSpeciesDataPartitioner;
import org.fao.fi.comet.domain.species.patterns.data.partitioners.LengthBasedSpeciesDataPartitioner;
import org.fao.fi.comet.domain.species.patterns.data.partitioners.SpeciesDataPartitioner;
import org.fao.fi.comet.domain.species.tools.lexical.processors.icu.queue.impl.AuthoritiesSimplifierProcessorQueue;
import org.fao.fi.comet.domain.species.tools.lexical.processors.icu.queue.impl.SpeciesNormalizerProcessorQueue;
import org.fao.fi.comet.domain.species.tools.lexical.processors.icu.queue.impl.SpeciesSimplifierProcessorQueue;
import org.fao.fi.comet.domain.species.tools.lexical.processors.impl.AuthoritiesNormalizerProcessor;
import org.fao.fi.sh.utility.lexical.processors.LexicalProcessor;
import org.fao.fi.sh.utility.lexical.soundex.PhraseSoundexGenerator;
import org.fao.fi.sh.utility.lexical.soundex.SoundexGenerator;
import org.fao.fi.sh.utility.lexical.soundex.impl.BasicPhraseSoundexGenerator;
import org.fao.fi.sh.utility.lexical.soundex.impl.BasicSoundexGenerator;
import org.junit.Assert;
import org.junit.Test;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 21/nov/2013   Fabio     Creation.
 *
 * @version 1.0
 * @since 21/nov/2013
 */
public class InitialsBasedSpeciesDataPartitionerTest {
	static final private String SOURCE_DATA_SOURCE = "SourceData";
	static final private String TARGET_DATA_SOURCE = "TargetData";

	static final private String GENUS = "genus";
	static final private String SPECIES = "species";

	static final private String SOURCE_GENUS = GENUS + "S";
	static final private String TARGET_GENUS = GENUS + "T";

	static final private String SOURCE_SPECIES = SPECIES + "s";
	static final private String TARGET_SPECIES = SPECIES + "t";

	static final private LexicalProcessor SPECIES_SIMPLIFIER = new SpeciesSimplifierProcessorQueue();
	static final private LexicalProcessor SPECIES_NORMALIZER = new SpeciesNormalizerProcessorQueue();
	static final private LexicalProcessor AUTHORITIES_SIMPLIFIER = new AuthoritiesSimplifierProcessorQueue();
	static final private LexicalProcessor AUTHORITIES_NORMALIZER = new AuthoritiesNormalizerProcessor();
	
	static final private PhraseSoundexGenerator PHRASE_SOUNDEXER = new BasicPhraseSoundexGenerator();
	static final private SoundexGenerator SOUNDEXER = new BasicSoundexGenerator();
	
	private final int THRESHOLD = 5;

	private final SpeciesDataPartitioner _partitioner 		= new InitialsBasedSpeciesDataPartitioner(THRESHOLD);
	private final SpeciesDataPartitioner _greedyPartitioner = new InitialsBasedSpeciesDataPartitioner(InitialsBasedSpeciesDataPartitioner.GREEDY);

	private final String overflow(int threshold, String what) {
		if(what == null)
			return null;

		int finalLength = what.length() + threshold + 1;

		while(what.length() < finalLength)
			what += "*";

		return what;
	}

	private final String overflow(String what) {
		return this.overflow(this.THRESHOLD * 2 + 1, what);
	}

	private ReferenceSpeciesData getReferenceSpeciesData(String source, String genus, String species) {
		String scientificName = genus == null ? species : ( genus + ( species == null ? "" : " " + species) );

		ReferenceSpeciesData data = ReferenceSpeciesFactory.newInstance(
			SOURCE_DATA_SOURCE,
			String.valueOf(scientificName.hashCode()),
			null,
			null,
			null,
			null,
			null,
			genus,
			species,
			scientificName,
			null,
			null
		);
		
		return ReferenceSpeciesFactory.updateComplexNames(
			data, 
			SPECIES_SIMPLIFIER, 
			SPECIES_NORMALIZER,
			AUTHORITIES_SIMPLIFIER,
			AUTHORITIES_NORMALIZER,
			PHRASE_SOUNDEXER,
			SOUNDEXER
		);
	}

	@Test
	public void testSSNull() {
		ReferenceSpeciesData source = this.getReferenceSpeciesData(SOURCE_DATA_SOURCE, SOURCE_GENUS, null);
		ReferenceSpeciesData target = this.getReferenceSpeciesData(TARGET_DATA_SOURCE, TARGET_GENUS, TARGET_SPECIES);

		Assert.assertTrue(this._partitioner.include(source, target, null));
	}

	@Test
	public void testTSNull() {
		ReferenceSpeciesData source = this.getReferenceSpeciesData(SOURCE_DATA_SOURCE, SOURCE_GENUS, "SSpecies");
		ReferenceSpeciesData target = this.getReferenceSpeciesData(TARGET_DATA_SOURCE, TARGET_GENUS, null);

		Assert.assertTrue(this._partitioner.include(source, target, null));
	}

	@Test
	public void testSSNullTSNull() {
		ReferenceSpeciesData source = this.getReferenceSpeciesData(SOURCE_DATA_SOURCE, SOURCE_GENUS, null);
		ReferenceSpeciesData target = this.getReferenceSpeciesData(TARGET_DATA_SOURCE, TARGET_GENUS, null);

		Assert.assertTrue(this._partitioner.include(source, target, null));
	}

	@Test
	public void testSGNull() {
		ReferenceSpeciesData source = this.getReferenceSpeciesData(SOURCE_DATA_SOURCE, null, SOURCE_SPECIES);
		ReferenceSpeciesData target = this.getReferenceSpeciesData(TARGET_DATA_SOURCE, TARGET_GENUS, TARGET_SPECIES);

		Assert.assertTrue(this._partitioner.include(source, target, null));
	}

	@Test
	public void testTGNull() {
		ReferenceSpeciesData source = this.getReferenceSpeciesData(SOURCE_DATA_SOURCE, SOURCE_GENUS, SOURCE_SPECIES);
		ReferenceSpeciesData target = this.getReferenceSpeciesData(TARGET_DATA_SOURCE, null, TARGET_SPECIES);

		Assert.assertTrue(this._partitioner.include(source, target, null));
	}

	@Test
	public void testSGNullTGNull() {
		ReferenceSpeciesData source = this.getReferenceSpeciesData(SOURCE_DATA_SOURCE, null, SOURCE_SPECIES);
		ReferenceSpeciesData target = this.getReferenceSpeciesData(TARGET_DATA_SOURCE, null, TARGET_SPECIES);

		Assert.assertTrue(this._partitioner.include(source, target, null));
	}

	@Test
	public void testSGOverflow() {
		ReferenceSpeciesData source = this.getReferenceSpeciesData(SOURCE_DATA_SOURCE, this.overflow(SOURCE_GENUS), SOURCE_SPECIES);
		ReferenceSpeciesData target = this.getReferenceSpeciesData(TARGET_DATA_SOURCE, TARGET_GENUS, TARGET_SPECIES);

		Assert.assertFalse(this._greedyPartitioner.include(source, target, null));
	}

	@Test
	public void testSSOverflow() {
		ReferenceSpeciesData source = this.getReferenceSpeciesData(SOURCE_DATA_SOURCE, SOURCE_GENUS, this.overflow(SOURCE_SPECIES));
		ReferenceSpeciesData target = this.getReferenceSpeciesData(TARGET_DATA_SOURCE, TARGET_GENUS, TARGET_SPECIES);

		Assert.assertFalse(this._greedyPartitioner.include(source, target, null));
	}

	@Test
	public void testTGNoMatch() {
		ReferenceSpeciesData source = this.getReferenceSpeciesData(SOURCE_DATA_SOURCE, SOURCE_GENUS, SOURCE_SPECIES);
		ReferenceSpeciesData target = this.getReferenceSpeciesData(TARGET_DATA_SOURCE, this.overflow(TARGET_GENUS), TARGET_SPECIES);

		Assert.assertFalse(this._greedyPartitioner.include(source, target, null));
	}

	@Test
	public void testTSNoMatch() {
		ReferenceSpeciesData source = this.getReferenceSpeciesData(SOURCE_DATA_SOURCE, SOURCE_GENUS, SOURCE_SPECIES);
		ReferenceSpeciesData target = this.getReferenceSpeciesData(TARGET_DATA_SOURCE, TARGET_GENUS, this.overflow(TARGET_SPECIES));

		Assert.assertFalse(this._greedyPartitioner.include(source, target, null));
	}

	@Test
	public void testSSTGOverflow() {
		ReferenceSpeciesData source = this.getReferenceSpeciesData(SOURCE_DATA_SOURCE, SOURCE_GENUS, this.overflow(SOURCE_SPECIES));
		ReferenceSpeciesData target = this.getReferenceSpeciesData(TARGET_DATA_SOURCE, this.overflow(TARGET_GENUS), TARGET_SPECIES);

		Assert.assertFalse(this._greedyPartitioner.include(source, target, null));
	}

	@Test
	public void testSGTSOverflow() {
		ReferenceSpeciesData source = this.getReferenceSpeciesData(SOURCE_DATA_SOURCE, this.overflow(SOURCE_GENUS), SOURCE_SPECIES);
		ReferenceSpeciesData target = this.getReferenceSpeciesData(TARGET_DATA_SOURCE, TARGET_GENUS, this.overflow(TARGET_SPECIES));

		Assert.assertFalse(this._greedyPartitioner.include(source, target, null));
	}

	@Test
	public void testSGNullTSOverflow() {
		ReferenceSpeciesData source = this.getReferenceSpeciesData(SOURCE_DATA_SOURCE, null, SOURCE_SPECIES);
		ReferenceSpeciesData target = this.getReferenceSpeciesData(TARGET_DATA_SOURCE, TARGET_GENUS, this.overflow(TARGET_SPECIES));

		Assert.assertFalse(this._greedyPartitioner.include(source, target, null));
	}

	@Test
	public void testSSNullTGOverflow() {
		ReferenceSpeciesData source = this.getReferenceSpeciesData(SOURCE_DATA_SOURCE, SOURCE_GENUS, null);
		ReferenceSpeciesData target = this.getReferenceSpeciesData(TARGET_DATA_SOURCE, this.overflow(TARGET_GENUS), TARGET_SPECIES);

		Assert.assertFalse(this._greedyPartitioner.include(source, target, null));
	}

	@Test
	public void testSSNullTSNullTGOverflow() {
		ReferenceSpeciesData source = this.getReferenceSpeciesData(SOURCE_DATA_SOURCE, SOURCE_GENUS, null);
		ReferenceSpeciesData target = this.getReferenceSpeciesData(TARGET_DATA_SOURCE, this.overflow(TARGET_GENUS), null);

		Assert.assertFalse(this._greedyPartitioner.include(source, target, null));
	}

	@Test
	public void testSGNullTGNullTSOverflow() {
		ReferenceSpeciesData source = this.getReferenceSpeciesData(SOURCE_DATA_SOURCE, null, SOURCE_SPECIES);
		ReferenceSpeciesData target = this.getReferenceSpeciesData(TARGET_DATA_SOURCE, null, this.overflow(TARGET_SPECIES));

		Assert.assertFalse(this._greedyPartitioner.include(source, target, null));
	}

	@Test
	public void testSGNullTSNull() {
		ReferenceSpeciesData source = this.getReferenceSpeciesData(SOURCE_DATA_SOURCE, null, SOURCE_SPECIES);
		ReferenceSpeciesData target = this.getReferenceSpeciesData(TARGET_DATA_SOURCE, TARGET_GENUS, null);

		Assert.assertTrue(this._partitioner.include(source, target, null));
	}

	@Test
	public void testSSNullTGNull() {
		ReferenceSpeciesData source = this.getReferenceSpeciesData(SOURCE_DATA_SOURCE, SOURCE_GENUS, null);
		ReferenceSpeciesData target = this.getReferenceSpeciesData(TARGET_DATA_SOURCE, null, TARGET_SPECIES);

		Assert.assertTrue(this._partitioner.include(source, target, null));
	}

	@Test
	public void testNoPartitioning() {
		ReferenceSpeciesData source = this.getReferenceSpeciesData(SOURCE_DATA_SOURCE, SOURCE_GENUS, this.overflow(SOURCE_SPECIES));
		ReferenceSpeciesData target = this.getReferenceSpeciesData(TARGET_DATA_SOURCE, this.overflow(TARGET_GENUS), TARGET_SPECIES);

		Assert.assertTrue(new LengthBasedSpeciesDataPartitioner().include(source, target, null));
	}
}
