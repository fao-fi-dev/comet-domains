/**
 * (c) 2013 FAO / UN (project: comet-species)
 */
package org.fao.fi.comet.domain.species.test.model;

import java.io.File;

import org.fao.fi.comet.core.model.engine.MatchingEngineProcessResult;
import org.fao.fi.sh.utility.core.helpers.singletons.io.JAXBHelper;
import org.junit.Test;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 02/nov/2013   Fabio     Creation.
 *
 * @version 1.0
 * @since 02/nov/2013
 */
public class MatchingEngineResultXSDTest {
	@Test
	public void testXSDGeneration() throws Throwable {
		File temp = File.createTempFile("matchingResults", ".xsd");
//		temp.deleteOnExit();

		System.out.println(temp.getAbsolutePath());

		JAXBHelper.generateSchema(null, temp.getAbsolutePath(), MatchingEngineProcessResult.class);
	}
}
