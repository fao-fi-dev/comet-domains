/**
 * (c) 2013 FAO / UN (project: comet-engine-species)
 */
package org.fao.fi.comet.domain.species.test.deserialization;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;

import org.fao.fi.comet.core.model.engine.DataIdentifier;
import org.fao.fi.comet.core.model.engine.Matching;
import org.fao.fi.comet.core.model.engine.MatchingDetails;
import org.fao.fi.comet.core.model.engine.MatchingEngineProcessConfiguration;
import org.fao.fi.comet.core.model.engine.MatchingEngineProcessResult;
import org.fao.fi.comet.core.model.engine.MatchingsData;
import org.fao.fi.comet.domain.species.model.InputSpeciesData;
import org.fao.fi.comet.domain.species.model.ReferenceSpeciesData;
import org.fao.fi.comet.extras.model.common.TypedComplexName;
import org.fao.fi.sh.utility.common.helpers.singletons.text.XMLHelper;
import org.fao.fi.sh.utility.core.helpers.singletons.io.JAXBHelper;
import org.junit.Assert;
import org.junit.Test;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 31 May 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 31 May 2013
 */
public class TestMatchingEngineProcessResultDeserialization {
	private String readResource(String classpathResource) throws Throwable {
		InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream(classpathResource);
		ByteArrayOutputStream baos = new ByteArrayOutputStream();

		byte[] buffer = new byte[2048];

		int len = -1;

		while((len = is.read(buffer)) != -1)
			baos.write(buffer, 0, len);

		baos.flush();
		baos.close();

		is.close();

		return new String(baos.toByteArray(), "UTF-8");
	}

	@SuppressWarnings("unchecked")
	private MatchingEngineProcessResult<ReferenceSpeciesData, ReferenceSpeciesData, MatchingEngineProcessConfiguration> deserialize() throws Throwable {
		return JAXBHelper.fromXML(
			MatchingEngineProcessResult.class,
			new Class<?>[] {
				InputSpeciesData.class,
				ReferenceSpeciesData.class,
				TypedComplexName.class
			},
			this.readResource("org/fao/fi/comet/domain/species/test/resources/serialized/serializedResults.xml")
		);
	}

	@Test
	public void testDeserializeSerialize() throws Throwable {
		System.out.println(XMLHelper.prettyPrint(JAXBHelper.toXML(new Class<?>[] {
			InputSpeciesData.class,
			ReferenceSpeciesData.class,
			TypedComplexName.class,
		}, this.deserialize())));
	}

	@Test
	public void testResultsDeserialization() throws Throwable {
		System.out.println(XMLHelper.prettyPrint(JAXBHelper.toXML(new Class[]{ InputSpeciesData.class, TypedComplexName.class }, this.deserialize())));
	}

	@Test
	public void testMatchingDetailsDeserialization() throws Throwable {
		MatchingsData<ReferenceSpeciesData, ReferenceSpeciesData> matchingsData = this.deserialize().getResults();

		Assert.assertNotNull(matchingsData);
		Assert.assertNotNull(matchingsData.getMatchingDetails());

		Assert.assertFalse(matchingsData.getMatchingDetails().isEmpty());
		Assert.assertTrue(matchingsData.getMatchingDetails().size() == 2);
	}

	protected MatchingDetails<ReferenceSpeciesData, ReferenceSpeciesData> testMatchingDetailsDeserialization(String sourceId) throws Throwable {
		MatchingsData<ReferenceSpeciesData, ReferenceSpeciesData> matchingsData = this.deserialize().getResults();

		MatchingDetails<ReferenceSpeciesData, ReferenceSpeciesData> details = matchingsData.findMatchingDetailsBySourceIdentifier(new DataIdentifier("UserProvidedData", sourceId));

		Assert.assertNotNull(details);
		Assert.assertNotNull(details.getMatchings());
		Assert.assertEquals(details.getTotalCandidates(), details.getMatchings().size());

		return details;
	}

	protected MatchingDetails<ReferenceSpeciesData, ReferenceSpeciesData> testMatchingDecreasingScore(MatchingDetails<ReferenceSpeciesData, ReferenceSpeciesData> details) throws Throwable {
		double currentScore = Double.POSITIVE_INFINITY;

		for(Matching<ReferenceSpeciesData, ReferenceSpeciesData> matching : details.getSortedUniqueMatchings()) {
			Assert.assertTrue(Double.compare(matching.getScoreValue(), currentScore) <= 0);
			currentScore = matching.getScoreValue();
		}

		return details;
	}

	protected MatchingDetails<ReferenceSpeciesData, ReferenceSpeciesData> testSourceData(String sourceId, MatchingDetails<ReferenceSpeciesData, ReferenceSpeciesData> details) throws Throwable {
		Assert.assertEquals(sourceId, ((InputSpeciesData)details.getSource()).getScientificName());

		return details;
	}

	protected MatchingDetails<ReferenceSpeciesData, ReferenceSpeciesData> fullTest(String userInput, String id) throws Throwable {
		MatchingDetails<ReferenceSpeciesData, ReferenceSpeciesData> details = this.testSourceData(userInput, this.testMatchingDecreasingScore(this.testMatchingDetailsDeserialization(id)));

		InputSpeciesData source = (InputSpeciesData)details.getSource();
//		Assert.assertTrue(source.getEnglishName() == null);
//		Assert.assertTrue(source.getEnglishCName() == null);

		Assert.assertTrue(source.getOriginal() != null);
		Assert.assertTrue(source.getScientificName() != null);
		Assert.assertTrue(source.getScientificName().equals(source.getPostParsedScientificName()));

//		Assert.assertTrue(source.getScientificCName() != null);
//		Assert.assertTrue(!source.getScientificCName().isEmpty());

		return details;
	}

	@Test
	public void testSecondMatchingDetailsDeserialization() throws Throwable {
		this.fullTest("Agnipenzer Perticus", "2");
	}

	@Test
	public void testFirstMatchingDetailsDeserialization() throws Throwable {
		this.fullTest("Tonno obeso", "1");
	}
}
