/**
 * (c) 2013 FAO / UN (project: comet-engine-species)
 */
package org.fao.fi.comet.domain.species.test.model;

import org.fao.fi.comet.core.patterns.handlers.id.impl.basic.AnnotationDrivenDataIDHandler;
import org.fao.fi.comet.domain.species.InputSpeciesFactory;
import org.fao.fi.comet.domain.species.model.InputSpeciesData;
import org.fao.fi.sh.utility.core.helpers.singletons.io.JAXBHelper;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 31 May 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 31 May 2013
 */
public class InputSpeciesDataTest {
	static final private String TEST_SPECIES_NAME = "Acipenser persicus";

	@Test
	public void testIDRetrievement() throws Throwable {
		Assert.assertEquals(TEST_SPECIES_NAME, new AnnotationDrivenDataIDHandler<InputSpeciesData>().getSerializedId(InputSpeciesFactory.newInstance(TEST_SPECIES_NAME)));
	}

	@Test
	@Ignore
	public void testSerialization() throws Throwable {
		System.out.println(JAXBHelper.toXML(new Class[] { InputSpeciesData.class }, InputSpeciesFactory.newInstance(TEST_SPECIES_NAME)));

		Assert.fail("To be properly implemented...");
	}
}