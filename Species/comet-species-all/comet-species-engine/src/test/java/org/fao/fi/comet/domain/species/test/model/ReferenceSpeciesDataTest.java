/**
 * (c) 2013 FAO / UN (project: comet-engine-species)
 */
package org.fao.fi.comet.domain.species.test.model;

import org.fao.fi.comet.domain.species.ReferenceSpeciesFactory;
import org.fao.fi.comet.domain.species.model.ReferenceSpeciesData;
import org.fao.fi.comet.domain.species.tools.lexical.processors.LexicalProcessorsUtils;
import org.fao.fi.comet.domain.species.tools.lexical.processors.impl.AuthoritiesNormalizerProcessor;
import org.fao.fi.comet.domain.species.tools.lexical.processors.queue.AuthoritiesSimplifier;
import org.fao.fi.comet.domain.species.tools.lexical.processors.queue.SpeciesNormalizer;
import org.fao.fi.comet.domain.species.tools.lexical.processors.queue.SpeciesSimplifier;
import org.fao.fi.sh.utility.lexical.soundex.impl.BasicSoundexGenerator;
import org.fao.fi.sh.utility.lexical.soundex.impl.ExtendedPhraseSoundexGenerator;
import org.junit.Assert;
import org.junit.Test;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 28 May 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 28 May 2013
 */
public class ReferenceSpeciesDataTest {
	private ReferenceSpeciesData update(ReferenceSpeciesData data) {
		return ReferenceSpeciesFactory.updateComplexNames(data,
														  LexicalProcessorsUtils.getProcessor(SpeciesSimplifier.class),
														  LexicalProcessorsUtils.getProcessor(SpeciesNormalizer.class),
														  LexicalProcessorsUtils.getProcessor(AuthoritiesSimplifier.class),
														  new AuthoritiesNormalizerProcessor(),
														  new ExtendedPhraseSoundexGenerator(),
														  new BasicSoundexGenerator());
	}

	@Test
	public void testReferenceSpeciesDataFromString() {
		ReferenceSpeciesData data = ReferenceSpeciesFactory.newInstance("Agipenz Erpers1cusz Walley");

		Assert.assertEquals("Agipenz Erpers1cusz Walley", data.getScientificName());
	}

	@Test
	public void testUpdatedReferenceSpeciesDataFromString() {
		ReferenceSpeciesData data = ReferenceSpeciesFactory.newInstance("Agipenz Erpers1cusz Walley");

		data = this.update(data);

		Assert.assertNotNull(data.getScientificCName());
		Assert.assertTrue(!data.getScientificCName().isEmpty());
		Assert.assertEquals(data.getScientificName(), data.getScientificCName().getName());

		Assert.assertEquals("Agipenz Erpers1cusz Walley", data.getScientificName());
	}
}
