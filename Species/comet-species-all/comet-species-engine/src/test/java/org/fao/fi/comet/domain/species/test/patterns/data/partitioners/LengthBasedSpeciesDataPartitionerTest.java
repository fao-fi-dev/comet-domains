/**
 * (c) 2013 FAO / UN (project: comet-species)
 */
package org.fao.fi.comet.domain.species.test.patterns.data.partitioners;

import org.fao.fi.comet.domain.species.ReferenceSpeciesFactory;
import org.fao.fi.comet.domain.species.model.ReferenceSpeciesData;
import org.fao.fi.comet.domain.species.patterns.data.partitioners.LengthBasedSpeciesDataPartitioner;
import org.fao.fi.comet.domain.species.patterns.data.partitioners.SpeciesDataPartitioner;
import org.junit.Assert;
import org.junit.Test;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 21/nov/2013   Fabio     Creation.
 *
 * @version 1.0
 * @since 21/nov/2013
 */
public class LengthBasedSpeciesDataPartitionerTest {
	static final private String SOURCE_DATA_SOURCE = "SourceData";
	static final private String TARGET_DATA_SOURCE = "TargetData";

	static final private String GENUS = "genus";
	static final private String SPECIES = "species";

	static final private String SOURCE_GENUS = "S" + GENUS;
	static final private String TARGET_GENUS = "T" + GENUS;

	static final private String SOURCE_SPECIES = "s" + SPECIES;
	static final private String TARGET_SPECIES = "t" + SPECIES;

	private final int THRESHOLD = 2;

	private final SpeciesDataPartitioner _partitioner = new LengthBasedSpeciesDataPartitioner(THRESHOLD);

	private final String overflow(int threshold, String what) {
		if(what == null)
			return null;

		int finalLength = what.length() + threshold + 1;

		while(what.length() < finalLength)
			what += "*";

		return what;
	}

	private final String overflow(String what) {
		return this.overflow(this.THRESHOLD * 2 + 1, what);
	}

	private ReferenceSpeciesData getReferenceSpeciesData(String source, String genus, String species) {
		String scientificName = genus == null ? species : ( genus + ( species == null ? "" : " " + species) );

		return ReferenceSpeciesFactory.newInstance(
			SOURCE_DATA_SOURCE,
			String.valueOf(scientificName.hashCode()),
			null,
			null,
			null,
			null,
			null,
			genus,
			species,
			scientificName,
			null,
			null
		);
	}

	@Test
	public void testSSNull() {
		ReferenceSpeciesData source = this.getReferenceSpeciesData(SOURCE_DATA_SOURCE, SOURCE_GENUS, null);
		ReferenceSpeciesData target = this.getReferenceSpeciesData(TARGET_DATA_SOURCE, TARGET_GENUS, TARGET_SPECIES);

		Assert.assertTrue(this._partitioner.include(source, target, null));
	}

	@Test
	public void testTSNull() {
		ReferenceSpeciesData source = this.getReferenceSpeciesData(SOURCE_DATA_SOURCE, SOURCE_GENUS, "SSpecies");
		ReferenceSpeciesData target = this.getReferenceSpeciesData(TARGET_DATA_SOURCE, TARGET_GENUS, null);

		Assert.assertTrue(this._partitioner.include(source, target, null));
	}

	@Test
	public void testSSNullTSNull() {
		ReferenceSpeciesData source = this.getReferenceSpeciesData(SOURCE_DATA_SOURCE, SOURCE_GENUS, null);
		ReferenceSpeciesData target = this.getReferenceSpeciesData(TARGET_DATA_SOURCE, TARGET_GENUS, null);

		Assert.assertTrue(this._partitioner.include(source, target, null));
	}

	@Test
	public void testSGNull() {
		ReferenceSpeciesData source = this.getReferenceSpeciesData(SOURCE_DATA_SOURCE, null, SOURCE_SPECIES);
		ReferenceSpeciesData target = this.getReferenceSpeciesData(TARGET_DATA_SOURCE, TARGET_GENUS, TARGET_SPECIES);

		Assert.assertTrue(this._partitioner.include(source, target, null));
	}

	@Test
	public void testTGNull() {
		ReferenceSpeciesData source = this.getReferenceSpeciesData(SOURCE_DATA_SOURCE, SOURCE_GENUS, SOURCE_SPECIES);
		ReferenceSpeciesData target = this.getReferenceSpeciesData(TARGET_DATA_SOURCE, null, TARGET_SPECIES);

		Assert.assertTrue(this._partitioner.include(source, target, null));
	}

	@Test
	public void testSGNullTGNull() {
		ReferenceSpeciesData source = this.getReferenceSpeciesData(SOURCE_DATA_SOURCE, null, SOURCE_SPECIES);
		ReferenceSpeciesData target = this.getReferenceSpeciesData(TARGET_DATA_SOURCE, null, TARGET_SPECIES);

		Assert.assertTrue(this._partitioner.include(source, target, null));
	}

	@Test
	public void testSGOverflow() {
		ReferenceSpeciesData source = this.getReferenceSpeciesData(SOURCE_DATA_SOURCE, this.overflow(SOURCE_GENUS), SOURCE_SPECIES);
		ReferenceSpeciesData target = this.getReferenceSpeciesData(TARGET_DATA_SOURCE, TARGET_GENUS, TARGET_SPECIES);

		Assert.assertFalse(this._partitioner.include(source, target, null));
	}

	@Test
	public void testSSOverflow() {
		ReferenceSpeciesData source = this.getReferenceSpeciesData(SOURCE_DATA_SOURCE, SOURCE_GENUS, this.overflow(SOURCE_SPECIES));
		ReferenceSpeciesData target = this.getReferenceSpeciesData(TARGET_DATA_SOURCE, TARGET_GENUS, TARGET_SPECIES);

		Assert.assertFalse(this._partitioner.include(source, target, null));
	}

	@Test
	public void testTGNoMatch() {
		ReferenceSpeciesData source = this.getReferenceSpeciesData(SOURCE_DATA_SOURCE, SOURCE_GENUS, SOURCE_SPECIES);
		ReferenceSpeciesData target = this.getReferenceSpeciesData(TARGET_DATA_SOURCE, this.overflow(TARGET_GENUS), TARGET_SPECIES);

		Assert.assertFalse(this._partitioner.include(source, target, null));
	}

	@Test
	public void testTSNoMatch() {
		ReferenceSpeciesData source = this.getReferenceSpeciesData(SOURCE_DATA_SOURCE, SOURCE_GENUS, SOURCE_SPECIES);
		ReferenceSpeciesData target = this.getReferenceSpeciesData(TARGET_DATA_SOURCE, TARGET_GENUS, this.overflow(TARGET_SPECIES));

		Assert.assertFalse(this._partitioner.include(source, target, null));
	}

	@Test
	public void testSSTGOverflow() {
		ReferenceSpeciesData source = this.getReferenceSpeciesData(SOURCE_DATA_SOURCE, SOURCE_GENUS, this.overflow(SOURCE_SPECIES));
		ReferenceSpeciesData target = this.getReferenceSpeciesData(TARGET_DATA_SOURCE, this.overflow(TARGET_GENUS), TARGET_SPECIES);

		Assert.assertFalse(this._partitioner.include(source, target, null));
	}

	@Test
	public void testSGTSOverflow() {
		ReferenceSpeciesData source = this.getReferenceSpeciesData(SOURCE_DATA_SOURCE, this.overflow(SOURCE_GENUS), SOURCE_SPECIES);
		ReferenceSpeciesData target = this.getReferenceSpeciesData(TARGET_DATA_SOURCE, TARGET_GENUS, this.overflow(TARGET_SPECIES));

		Assert.assertFalse(this._partitioner.include(source, target, null));
	}

	@Test
	public void testSGNullTSOverflow() {
		ReferenceSpeciesData source = this.getReferenceSpeciesData(SOURCE_DATA_SOURCE, null, SOURCE_SPECIES);
		ReferenceSpeciesData target = this.getReferenceSpeciesData(TARGET_DATA_SOURCE, TARGET_GENUS, this.overflow(TARGET_SPECIES));

		Assert.assertFalse(this._partitioner.include(source, target, null));
	}

	@Test
	public void testSSNullTGOverflow() {
		ReferenceSpeciesData source = this.getReferenceSpeciesData(SOURCE_DATA_SOURCE, SOURCE_GENUS, null);
		ReferenceSpeciesData target = this.getReferenceSpeciesData(TARGET_DATA_SOURCE, this.overflow(TARGET_GENUS), TARGET_SPECIES);

		Assert.assertFalse(this._partitioner.include(source, target, null));
	}

	@Test
	public void testSSNullTSNullTGOverflow() {
		ReferenceSpeciesData source = this.getReferenceSpeciesData(SOURCE_DATA_SOURCE, SOURCE_GENUS, null);
		ReferenceSpeciesData target = this.getReferenceSpeciesData(TARGET_DATA_SOURCE, this.overflow(TARGET_GENUS), null);

		Assert.assertFalse(this._partitioner.include(source, target, null));
	}

	@Test
	public void testSGNullTGNullTSOverflow() {
		ReferenceSpeciesData source = this.getReferenceSpeciesData(SOURCE_DATA_SOURCE, null, SOURCE_SPECIES);
		ReferenceSpeciesData target = this.getReferenceSpeciesData(TARGET_DATA_SOURCE, null, this.overflow(TARGET_SPECIES));

		Assert.assertFalse(this._partitioner.include(source, target, null));
	}

	@Test
	public void testSGNullTSNull() {
		ReferenceSpeciesData source = this.getReferenceSpeciesData(SOURCE_DATA_SOURCE, null, SOURCE_SPECIES);
		ReferenceSpeciesData target = this.getReferenceSpeciesData(TARGET_DATA_SOURCE, TARGET_GENUS, null);

		Assert.assertTrue(this._partitioner.include(source, target, null));
	}

	@Test
	public void testSSNullTGNull() {
		ReferenceSpeciesData source = this.getReferenceSpeciesData(SOURCE_DATA_SOURCE, SOURCE_GENUS, null);
		ReferenceSpeciesData target = this.getReferenceSpeciesData(TARGET_DATA_SOURCE, null, TARGET_SPECIES);

		Assert.assertTrue(this._partitioner.include(source, target, null));
	}

	@Test
	public void testNoPartitioning() {
		ReferenceSpeciesData source = this.getReferenceSpeciesData(SOURCE_DATA_SOURCE, SOURCE_GENUS, this.overflow(SOURCE_SPECIES));
		ReferenceSpeciesData target = this.getReferenceSpeciesData(TARGET_DATA_SOURCE, this.overflow(TARGET_GENUS), TARGET_SPECIES);

		Assert.assertTrue(new LengthBasedSpeciesDataPartitioner().include(source, target, null));
	}
}
