/**
 * (c) 2013 FAO / UN (project: comet-species)
 */
package org.fao.fi.comet.domain.species.matchlets.extended;

import org.fao.fi.comet.core.model.engine.DataIdentifier;
import org.fao.fi.comet.core.model.score.support.MatchingScore;
import org.fao.fi.comet.core.model.score.support.MatchingType;
import org.fao.fi.comet.core.uniform.matchlets.skeleton.UScalarMatchletSkeleton;
import org.fao.fi.comet.domain.species.model.ReferenceSpeciesData;
import org.fao.fi.comet.extras.model.common.TypedComplexName;
import org.fao.fi.sh.utility.common.helpers.singletons.text.StringDistanceHelper;
import org.fao.fi.sh.utility.core.helpers.singletons.text.StringsHelper;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 7 Oct 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 7 Oct 2013
 */
public class FuzzyTaxamatchMatchlet extends UScalarMatchletSkeleton<ReferenceSpeciesData, ReferenceSpeciesData> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = -5646686342308645734L;

	static public final String NAME = "FuzzyTaxamatchMatchlet";

	/**
	 * Class constructor
	 *
	 */
	public FuzzyTaxamatchMatchlet() {
		super();

		this._name = NAME;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.model.matchlets.Matchlet#getDescription()
	 */
	@Override
	public String getDescription() {
		return "Performs a fuzzy Taxamatch-like matching between two species";
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.uniform.matchlets.skeleton.UScalarMatchletSkeleton#doExtractData(java.lang.Object, org.fao.fi.comet.core.model.engine.DataIdentifier)
	 */
	@Override
	protected ReferenceSpeciesData doExtractData(ReferenceSpeciesData entity, DataIdentifier dataIdentifier) {
		return entity;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.model.matchlets.Matchlet#computeScore(java.lang.Object, org.fao.fi.comet.core.model.engine.DataIdentifier, java.lang.Object, java.lang.Object, org.fao.fi.comet.core.model.engine.DataIdentifier, java.lang.Object)
	 */
	@Override
	public MatchingScore computeScore(ReferenceSpeciesData source, DataIdentifier sourceIdentifier, ReferenceSpeciesData sourceData, ReferenceSpeciesData target, DataIdentifier targetIdentifier, ReferenceSpeciesData targetData) {
		TypedComplexName sourceNormalizedGenusCName = source.getNormalizedGenusCName();
		TypedComplexName targetNormalizedGenusCName = target.getNormalizedGenusCName();

		TypedComplexName sourceNormalizedSpeciesCName = source.getNormalizedSpeciesCName();
		TypedComplexName targetNormalizedSpeciesCName = target.getNormalizedSpeciesCName();

		boolean noneHasGenus = ( sourceNormalizedGenusCName == null || sourceNormalizedGenusCName.isEmpty() ) &&
							   ( targetNormalizedGenusCName == null || targetNormalizedGenusCName.isEmpty() );

		boolean bothHaveGenus = ( sourceNormalizedGenusCName != null && !sourceNormalizedGenusCName.isEmpty() ) &&
								( targetNormalizedGenusCName != null && !targetNormalizedGenusCName.isEmpty() );

		boolean noneHasSpecies = ( sourceNormalizedSpeciesCName == null || sourceNormalizedSpeciesCName.isEmpty() ) &&
								 ( targetNormalizedSpeciesCName == null || targetNormalizedSpeciesCName.isEmpty() );

		boolean bothHaveSpecies = ( sourceNormalizedSpeciesCName != null && !sourceNormalizedSpeciesCName.isEmpty() ) &&
								  ( targetNormalizedSpeciesCName != null && !targetNormalizedSpeciesCName.isEmpty() );

		if(noneHasGenus && noneHasSpecies)
			return MatchingScore.getNonPerformedTemplate();

		double genusPhoneticScore, speciesPhoneticScore;
		genusPhoneticScore = speciesPhoneticScore = 0D;

		double genusRelativeSimilarityScore, speciesRelativeSimilarityScore;
		genusRelativeSimilarityScore = speciesRelativeSimilarityScore = 0D;

		double genusDistanceScore, speciesDistanceScore;
		genusDistanceScore = speciesDistanceScore = 0;

		double genusScore, speciesScore;
		genusScore = speciesScore = 0D;

		if(bothHaveGenus) {
			genusPhoneticScore = this.soundexSimilarity(sourceNormalizedGenusCName.getSimplifiedNameSoundex(), targetNormalizedGenusCName.getSimplifiedNameSoundex());
			genusDistanceScore = this.distance(sourceNormalizedGenusCName.getSimplifiedName(), targetNormalizedGenusCName.getSimplifiedName());
			genusRelativeSimilarityScore = this.relativeSimilarity(sourceNormalizedGenusCName.getSimplifiedName(), targetNormalizedGenusCName.getSimplifiedName());

			genusScore = this.overallScore(genusPhoneticScore,
					  					   genusDistanceScore,
					  					   genusRelativeSimilarityScore);
		}

		if(bothHaveSpecies) {
			speciesPhoneticScore = this.soundexSimilarity(sourceNormalizedSpeciesCName.getSimplifiedNameSoundex(), targetNormalizedSpeciesCName.getSimplifiedNameSoundex());
			speciesDistanceScore = this.distance(sourceNormalizedSpeciesCName.getSimplifiedName(), targetNormalizedSpeciesCName.getSimplifiedName());
			speciesRelativeSimilarityScore = this.relativeSimilarity(sourceNormalizedSpeciesCName.getSimplifiedName(), targetNormalizedSpeciesCName.getSimplifiedName());

			speciesScore = this.overallScore(speciesPhoneticScore,
											 speciesDistanceScore,
											 speciesRelativeSimilarityScore);
		}

		if(bothHaveGenus && bothHaveSpecies) {
			return new MatchingScore(( genusScore + speciesScore ) / 2.0, MatchingType.NON_AUTHORITATIVE);
		} else {
			double score = this.overallScore(Math.max(genusPhoneticScore, speciesPhoneticScore),
	 				   						 Math.min(genusDistanceScore, speciesDistanceScore),
	 				   						 Math.max(genusRelativeSimilarityScore, speciesRelativeSimilarityScore));

			return new MatchingScore(score, MatchingType.NON_AUTHORITATIVE);
		}
	}

	private double overallScore(double phoneticScore, double distanceScore, double similarityScore) {
		double score = ( phoneticScore + distanceScore + similarityScore ) / 3.0;

		return score;
	}

	private double distance(String source, String target) {
		int distance = StringDistanceHelper.computeDistance(StringsHelper.trim(source), StringsHelper.trim(target));

		return distance > 4 ? 0 : 1D - ( distance / 5.0 );
	}

	private double relativeSimilarity(String source, String target) {
		return StringDistanceHelper.computeRelativeSimilarity(StringsHelper.trim(source), StringsHelper.trim(target));
	}

	private double soundexSimilarity(String source, String target) {
		if(source == null || target == null)
			return MatchingScore.SCORE_NO_MATCH;

		if(source.equals(target))
			return MatchingScore.SCORE_FULL_MATCH;

		double score = MatchingScore.SCORE_NO_MATCH;

		char sourceSoundexStart = source.charAt(0);
		char targetSoundexStart = target.charAt(0);

		if(sourceSoundexStart == targetSoundexStart)
			score = 0.4;

		int sourceSoundexInt = Integer.valueOf(source.substring(1));
		int targetSoundexInt = Integer.valueOf(target.substring(1));

		int min = Math.min(sourceSoundexInt, targetSoundexInt);
		int max = Math.max(sourceSoundexInt, targetSoundexInt);

		if(min == max)
			score += 0.6;
		else
			score += 0.6 * min / max;

		return score;
	}
}