/**
 * (c) 2013 FAO / UN (project: comet-engine-species)
 */
package org.fao.fi.comet.domain.species.matchlets.extended.support;

import java.io.Serializable;
import java.util.Arrays;

import org.fao.fi.comet.core.model.engine.DataIdentifier;
import org.fao.fi.comet.core.model.score.support.MatchingScore;
import org.fao.fi.comet.core.model.score.support.MatchingType;
import org.fao.fi.comet.domain.species.model.ReferenceSpeciesData;
import org.fao.fi.comet.extras.model.common.TypedComplexName;
import org.fao.fi.sh.utility.common.helpers.singletons.text.StringDistanceHelper;
import org.fao.fi.sh.utility.core.helpers.singletons.lang.ObjectsHelper;
import org.fao.fi.sh.utility.core.helpers.singletons.text.StringNGramsHelper;

import static org.fao.fi.sh.utility.core.helpers.singletons.lang.AssertionHelper.*;

/**
 * Support class to perform complex name score computation (weighting soundex and names result)
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 4 Jun 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 4 Jun 2013
 */
final public class CNamesScoreCalculator implements Serializable {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 3412964792752998737L;

	static final private String[] NO_TRIGRAMS = new String[0];

	public MatchingScore computeScore(double levenshteinWeight, double soundexWeight, double trigramWeight, ReferenceSpeciesData source, DataIdentifier sourceIdentifier, TypedComplexName sourceData, ReferenceSpeciesData target, DataIdentifier targetIdentifier, TypedComplexName targetData) {
		String sourceName, targetName;
		String simplifiedSourceName, simplifiedTargetName;

		boolean checkLevenshtein = Double.compare(levenshteinWeight, MatchingScore.SCORE_NO_MATCH) > 0;
		boolean checkSoundex = Double.compare(soundexWeight, MatchingScore.SCORE_NO_MATCH) > 0;
		boolean checkTrigrams = Double.compare(trigramWeight, MatchingScore.SCORE_NO_MATCH) > 0;

		boolean onlySoundex = checkSoundex && !checkLevenshtein && !checkTrigrams;
		boolean onlyLevenshtein = checkLevenshtein && !checkSoundex && !checkTrigrams;
		boolean onlyTrigrams = checkTrigrams && !checkLevenshtein && !checkSoundex;

		double currentScore, simplifiedLevenshteinScore, levenshteinScore, soundexScore, trigramScore;

		currentScore = simplifiedLevenshteinScore = levenshteinScore = soundexScore = trigramScore = MatchingScore.SCORE_NO_MATCH;

		sourceName = sourceData.getName();
		targetName = targetData.getName();

		if(sourceName.equalsIgnoreCase(targetName))
			return MatchingScore.getNonAuthoritativeFullMatchTemplate();

		simplifiedSourceName = sourceData.getSimplifiedName();
		simplifiedTargetName = targetData.getSimplifiedName();

		if(simplifiedSourceName.equalsIgnoreCase(simplifiedTargetName))
			return MatchingScore.getNonAuthoritativeFullMatchTemplate();

		if(checkLevenshtein) {
//			levenshteinScore = StringUtils.computeRelativeSimilarity(sourceName, targetName);
			simplifiedLevenshteinScore = StringDistanceHelper.computeRelativeSimilarity(simplifiedSourceName, simplifiedTargetName);

//			levenshteinScore = Double.compare(levenshteinScore, simplifiedLevenshteinScore) < 0 ? simplifiedLevenshteinScore : levenshteinScore;
			levenshteinScore = simplifiedLevenshteinScore;
		}

		if(checkSoundex) {
			String[] firstNameSoundexParts = sourceData.getSimplifiedNameSoundexParts();
			String[] secondNameSoundexParts = targetData.getSimplifiedNameSoundexParts();

			int firstNameSoundexPartsSize = firstNameSoundexParts == null ? 0 : firstNameSoundexParts.length;
			int secondNameSoundexPartsSize = secondNameSoundexParts == null ? 0 : secondNameSoundexParts.length;

			/* SLOWER, MORE ACCURATE VERSION */
			final boolean switchFirstAndSecond = firstNameSoundexPartsSize < secondNameSoundexPartsSize;
			int partsNum = 0;

			double total, current, max;
			total = current = max = 0D;

			double factor;

			if(firstNameSoundexParts != null && secondNameSoundexParts != null)
				for(String fPart : switchFirstAndSecond ? secondNameSoundexParts : firstNameSoundexParts) {
					for(String sPart : switchFirstAndSecond ? firstNameSoundexParts : secondNameSoundexParts) {
						current = StringDistanceHelper.computeRelativeSimilarity(fPart, sPart);

						factor = ( 1.0 * ( switchFirstAndSecond ? fPart.length() : sPart.length() ) ) / Math.max(sPart.length(), fPart.length());

						$gte(factor, 0.0, "The soundex parts factor must be greater than (or equal to) 0.0 (currently: {})", factor);
						$lte(factor, 1.0, "The soundex parts factor must be lower than (or equal to) 1.0 (currently: {})", factor);

						current *= factor;

						if(current > max) {
							max = current;
						}
					}

					total += max;
					max = 0D;
				}

			partsNum = Math.max(firstNameSoundexPartsSize, secondNameSoundexPartsSize);
			total /= partsNum == 0 ? 1D : partsNum;

			soundexScore = total;
		}

		if(checkTrigrams) {
			trigramScore = StringNGramsHelper.calculateNGramsSimilarityScore(
				Arrays.asList(ObjectsHelper.coalesce(sourceData.getSimplifiedNameNGrams(), NO_TRIGRAMS)),
				Arrays.asList(ObjectsHelper.coalesce(targetData.getSimplifiedNameNGrams(), NO_TRIGRAMS))
			);
		}

		if(onlyLevenshtein)
			currentScore = levenshteinScore;
		else if(onlySoundex)
			currentScore = soundexScore;
		else if(onlyTrigrams)
			currentScore = trigramScore;
		else
			currentScore = ( levenshteinWeight * levenshteinScore +
							 soundexWeight * soundexScore +
							 trigramWeight * trigramScore ) /
						   ( levenshteinWeight + soundexWeight + trigramWeight );

		return new MatchingScore(currentScore, MatchingType.NON_AUTHORITATIVE);
	}
}