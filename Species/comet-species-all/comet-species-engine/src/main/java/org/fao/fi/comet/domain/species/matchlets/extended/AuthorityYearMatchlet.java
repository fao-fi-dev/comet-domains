/**
 * (c) 2013 FAO / UN (project: comet-engine-species)
 */
package org.fao.fi.comet.domain.species.matchlets.extended;


import static org.fao.fi.sh.utility.core.helpers.singletons.lang.AssertionHelper.$true;

import org.fao.fi.comet.core.model.engine.DataIdentifier;
import org.fao.fi.comet.core.model.matchlets.annotations.MatchletDefaultSerializationExclusionPolicy;
import org.fao.fi.comet.core.model.matchlets.annotations.MatchletIsCutoffByDefault;
import org.fao.fi.comet.core.model.matchlets.annotations.MatchletIsOptionalByDefault;
import org.fao.fi.comet.core.model.matchlets.support.MatchingSerializationExclusionPolicy;
import org.fao.fi.comet.core.model.score.support.MatchingScore;
import org.fao.fi.comet.core.model.score.support.MatchingType;
import org.fao.fi.comet.core.uniform.matchlets.skeleton.UScalarMatchletSkeleton;
import org.fao.fi.comet.domain.species.model.ReferenceSpeciesData;
import org.fao.fi.sh.utility.common.helpers.singletons.text.StringDistanceHelper;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 28 May 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 28 May 2013
 */
@MatchletIsOptionalByDefault
@MatchletIsCutoffByDefault
@MatchletDefaultSerializationExclusionPolicy(MatchingSerializationExclusionPolicy.NON_PERFORMED)
public class AuthorityYearMatchlet extends UScalarMatchletSkeleton<ReferenceSpeciesData, Integer> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 2057284178844548705L;

	static public final String NAME = "AuthorityYearMatchlet";
	
	private int _maxDelta = 10;
	
	/**
	 * Class constructor
	 *
	 */
	public AuthorityYearMatchlet() {
		super();
		
		this._name = NAME;
	}
	
	/**
	 * Class constructor
	 *
	 * @param maxDelta
	 */
	public AuthorityYearMatchlet(int maxDelta) {
		this();
		
		$true(maxDelta >= 0, "Max year delta should be greater than (or equal to) zero (currently: {})", maxDelta);
		
		this._maxDelta = maxDelta;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.model.matchlets.Matchlet#getDescription()
	 */
	@Override
	public String getDescription() {
		return "Matches authority years as extracted from a pair of input / reference matched entities";
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.uniform.matchlets.skeleton.UScalarMatchletSkeleton#doExtractData(java.io.Serializable)
	 */
	@Override
	protected Integer doExtractData(ReferenceSpeciesData entity, DataIdentifier dataIdentifier) {
		return entity.getAuthorityYear();
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.model.matchlets.Matchlet#computeScore(java.lang.Object, org.fao.fi.comet.core.model.engine.DataIdentifier, java.lang.Object, java.lang.Object, org.fao.fi.comet.core.model.engine.DataIdentifier, java.lang.Object)
	 */
	@Override
	public MatchingScore computeScore(ReferenceSpeciesData source, DataIdentifier sourceIdentifier, Integer sourceData, ReferenceSpeciesData target, DataIdentifier targetIdentifier, Integer targetData) {
		int delta = Math.abs(sourceData - targetData);
		
		MatchingScore result = new MatchingScore(MatchingScore.SCORE_NO_MATCH, MatchingType.NON_AUTHORITATIVE);
		
		double scoreAsNumber = MatchingScore.SCORE_NO_MATCH;
		double scoreAsString = MatchingScore.SCORE_NO_MATCH;
		
		if(this._maxDelta == 0) {
			 if(delta == 0)
				 scoreAsNumber = MatchingScore.SCORE_FULL_MATCH;
		} else if(delta <= this._maxDelta)
			scoreAsNumber = (1.0 - delta * 1.0 / this._maxDelta);

		scoreAsString = StringDistanceHelper.computeRelativeSimilarity(String.valueOf(sourceData), String.valueOf(targetData));
		
		result.setValue(Math.max(scoreAsNumber, scoreAsString));
		
		return result;
	}
}