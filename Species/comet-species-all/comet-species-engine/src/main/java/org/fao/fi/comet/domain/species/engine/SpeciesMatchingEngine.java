/**
 * (c) 2013 FAO / UN (project: comet-engine-species)
 */
package org.fao.fi.comet.domain.species.engine;

import org.fao.fi.comet.core.model.engine.MatchingEngineProcessConfiguration;
import org.fao.fi.comet.core.uniform.engine.UMatchingEngineCore;
import org.fao.fi.comet.domain.species.model.ReferenceSpeciesData;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 07/giu/2013   Fabio     Creation.
 *
 * @version 1.0
 * @since 07/giu/2013
 */
public class SpeciesMatchingEngine extends UMatchingEngineCore<ReferenceSpeciesData, MatchingEngineProcessConfiguration> {
	/**
	 * Class constructor
	 */
	public SpeciesMatchingEngine() {
		super(1);
	}

	/**
	 * Class constructor
	 *
	 * @param parallelThreads
	 */
	public SpeciesMatchingEngine(int parallelThreads) {
		super(parallelThreads);
	}
}
