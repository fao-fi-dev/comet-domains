/**
 * (c) 2013 FAO / UN (project: comet-species)
 */
package org.fao.fi.comet.domain.species.patterns.data.partitioners;

import static org.fao.fi.sh.utility.core.helpers.singletons.lang.AssertionHelper.$true;

import java.util.Collection;

import org.fao.fi.comet.core.model.matchlets.Matchlet;
import org.fao.fi.comet.domain.species.model.ReferenceSpeciesData;
import org.fao.fi.comet.extras.model.common.TypedComplexName;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 21/nov/2013   Fabio     Creation.
 *
 * @version 1.0
 * @since 21/nov/2013
 */
public class InitialsBasedSpeciesDataPartitioner extends SpeciesDataPartitioner {
	static final public int GREEDY = 0;

	private int _lookupLength = GREEDY;
	
	public InitialsBasedSpeciesDataPartitioner() {
		super();
	}

	/**
	 * Class constructor
	 *
	 * @param maxLength
	 */
	public InitialsBasedSpeciesDataPartitioner(int lookupLength) {
		super();

		this.setLookupLength(lookupLength);
	}
	
	/**
	 * @return the 'lookupLength' value
	 */
	public final int getLookupLength() {
		return this._lookupLength;
	}

	/**
	 * @param lookupLength the 'lookupLength' value to set
	 */
	public final void setLookupLength(int lookupLength) {
		$true(lookupLength >= GREEDY, "The lookup length parameter must be greater than or equal to {} (actually: {})", GREEDY, lookupLength);

		this._lookupLength = lookupLength;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.patterns.data.partitioners.DataPartitioner#include(java.io.Serializable, java.io.Serializable, java.util.Collection)
	 */
	@Override
	public boolean include(ReferenceSpeciesData source, ReferenceSpeciesData target, Collection<? extends Matchlet<ReferenceSpeciesData, ?, ReferenceSpeciesData, ?>> matchlets) {
		TypedComplexName sourceGenus = source.getGenusCName();
		TypedComplexName targetGenus = target.getGenusCName();

		TypedComplexName sourceSpecies = source.getSpeciesCName();
		TypedComplexName targetSpecies = target.getSpeciesCName();
		
		boolean skipGenus = ( sourceGenus == null || sourceGenus.getSimplifiedName().length() == 0 ) ||
							( targetGenus == null || targetGenus.getSimplifiedName().length() == 0 );
		
		boolean skipSpecies = ( sourceSpecies == null || sourceSpecies.getSimplifiedName().length() == 0 ) ||
							  ( targetSpecies == null || targetSpecies.getSimplifiedName().length() == 0 );

		if(skipGenus && skipSpecies)
			return true;

		boolean bothGenus, bothSpecies, sameGenus, sameSpecies;
		
		sameGenus = sameSpecies = true;
		
		bothGenus = !skipGenus;
		bothSpecies = !skipSpecies;
		
		if(bothGenus) 
			sameGenus = this.sameInitials(sourceGenus.getSimplifiedName(), targetGenus.getSimplifiedName(), this._lookupLength);
		
		if(bothSpecies)
			sameSpecies = this.sameInitials(sourceSpecies.getSimplifiedName(), targetSpecies.getSimplifiedName(), this._lookupLength);
		
		return sameGenus && sameSpecies;
	}
	
	protected boolean sameInitials(String first, String second, int lookupLength) {
		int actualLength = Math.min(Math.min(lookupLength, first.length()), second.length());
		
		if(this._lookupLength == GREEDY)
			return first.contains(second) || second.contains(first);
		
		return first.substring(0, actualLength).equalsIgnoreCase(second.substring(0, actualLength));
	}
}
