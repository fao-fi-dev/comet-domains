/**
 * (c) 2013 FAO / UN (project: comet-species)
 */
package org.fao.fi.comet.domain.species.patterns.data.partitioners;

import java.util.Collection;

import org.fao.fi.comet.core.model.matchlets.Matchlet;
import org.fao.fi.comet.domain.species.model.ReferenceSpeciesData;

import static org.fao.fi.sh.utility.core.helpers.singletons.lang.AssertionHelper.*;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 21/nov/2013   Fabio     Creation.
 *
 * @version 1.0
 * @since 21/nov/2013
 */
public class LengthBasedSpeciesDataPartitioner extends SpeciesDataPartitioner {
	static final public int INCLUDE_EVERYTHING = -1;

	private int _maxLength = INCLUDE_EVERYTHING;

	public LengthBasedSpeciesDataPartitioner() {
		super();
	}

	/**
	 * Class constructor
	 *
	 * @param maxLength
	 */
	public LengthBasedSpeciesDataPartitioner(int maxLength) {
		super();

		this.setMaxLength(maxLength);
	}

	/**
	 * @return the 'maxLength' value
	 */
	public int getMaxLength() {
		return this._maxLength;
	}

	/**
	 * @param maxLength the 'maxLength' value to set
	 */
	public void setMaxLength(int maxLength) {
		$true(maxLength >= INCLUDE_EVERYTHING, "The maximum length parameter must be greater than or equal to {} (actually: {})", INCLUDE_EVERYTHING, maxLength);

		this._maxLength = maxLength;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.patterns.data.partitioners.DataPartitioner#include(java.io.Serializable, java.io.Serializable, java.util.Collection)
	 */
	@Override
	public boolean include(ReferenceSpeciesData source, ReferenceSpeciesData target, Collection<? extends Matchlet<ReferenceSpeciesData, ?, ReferenceSpeciesData, ?>> matchlets) {
		if(this._maxLength == INCLUDE_EVERYTHING)
			return true;

		String sourceGenus = source.getGenus();
		String targetGenus = target.getGenus();
		String sourceScientificName = source.getScientificName();

		String sourceSpecies = source.getSpecies();
		String targetSpecies = target.getSpecies();
		String targetScientificName = target.getScientificName();

		boolean bothGenuses = sourceGenus != null && targetGenus != null;
		boolean bothSpecies = sourceSpecies != null && targetSpecies != null;
		boolean bothScientifcNames = sourceScientificName != null && targetScientificName != null;
		
		boolean keepByGenuses = !bothGenuses || Math.abs( sourceGenus.length() - targetGenus.length() ) <= this._maxLength;
		boolean keepBySpecies = !bothSpecies || Math.abs( sourceSpecies.length() - targetSpecies.length() ) <= this._maxLength;
		boolean keepByScientificName = !bothSpecies && ( bothScientifcNames && Math.abs( sourceScientificName.length() - targetScientificName.length() ) <= ( this._maxLength * 2 + 1 ) );

		return ( keepByGenuses && keepBySpecies ) || keepByScientificName;
	}
}
