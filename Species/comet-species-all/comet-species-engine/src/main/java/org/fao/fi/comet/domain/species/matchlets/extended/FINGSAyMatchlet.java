/**
 * (c) 2013 FAO / UN (project: YASMEEN-custom-matchlets)
 */
package org.fao.fi.comet.domain.species.matchlets.extended;

import org.fao.fi.comet.core.model.engine.DataIdentifier;
import org.fao.fi.comet.core.model.score.support.MatchingScore;
import org.fao.fi.comet.core.model.score.support.MatchingType;
import org.fao.fi.comet.core.uniform.matchlets.skeleton.UScalarMatchletSkeleton;
import org.fao.fi.comet.domain.species.model.ReferenceSpeciesData;
import org.fao.fi.comet.extras.model.common.TypedComplexName;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 28/ott/2013   Fabio     Creation.
 *
 * @version 1.0
 * @since 28/ott/2013
 */
public class FINGSAyMatchlet extends UScalarMatchletSkeleton<ReferenceSpeciesData, ReferenceSpeciesData> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 5534829313877324698L;

	static public final String NAME = "GSAyMatchlet";
	
	public enum Steplist {GSAy, GSAY, GSrAy, GSrAY, GSA, GSrA, GSY, GSrY, GS, GSr, SAy, SrAy, SAY, SrAY, GAy, GAY}

	/**
	 * Class constructor
	 *
	 */
	public FINGSAyMatchlet() {
		super();

		this._name = NAME;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.model.matchlets.Matchlet#computeScore(java.lang.Object, org.fao.fi.comet.core.model.engine.DataIdentifier, java.lang.Object, java.lang.Object, org.fao.fi.comet.core.model.engine.DataIdentifier, java.lang.Object)
	 */
	@Override
	public MatchingScore computeScore(ReferenceSpeciesData source,
									  DataIdentifier sourceIdentifier,
									  ReferenceSpeciesData sourceData,
									  ReferenceSpeciesData target,
									  DataIdentifier targetIdentifier,
									  ReferenceSpeciesData targetData) {

		MatchingScore result = new MatchingScore(MatchingScore.SCORE_NO_MATCH, MatchingType.NON_AUTHORITATIVE);

		double scoreValue = 0;

		//Perform GSAy calculation using sourceData and targetData attributes...
		String sourceGenusName = sourceData.getGenus();
		String sourceSpeciesName = sourceData.getSpecies();
		String sourceAuthor = sourceData.getAuthor();
		String sourceAuthority = sourceData.getAuthority();
		String sourceYear = sourceData.getAuthorityYear() == null ? null : sourceData.getAuthorityYear().toString();
		
		String targetGenusName = targetData.getGenus();
		String targetSpeciesName = targetData.getSpecies();
		String targetAuthor = targetData.getAuthor();
		String targetAuthority = targetData.getAuthority();
		String targetYear = targetData.getAuthorityYear() == null ? null : targetData.getAuthorityYear().toString();

		TypedComplexName sourceNormalizedSpeciesCName = sourceData.getNormalizedSpeciesCName();
		TypedComplexName targetNormalizedSpeciesCName = targetData.getNormalizedSpeciesCName();
		
		boolean sameGenusName = (sourceGenusName != null && !sourceGenusName.isEmpty()) 
				&& (targetGenusName != null && !targetGenusName.isEmpty()) &&
				(sourceGenusName.equalsIgnoreCase(targetGenusName));
		boolean sameSpeciesName = (sourceSpeciesName != null && !sourceSpeciesName.isEmpty()) && 
				(targetSpeciesName != null && !targetSpeciesName.isEmpty()) &&
				(sourceSpeciesName.equalsIgnoreCase(targetSpeciesName));
		boolean sameAuthor = (sourceAuthor != null && !sourceAuthor.isEmpty()) && 
				(targetAuthor != null && !targetAuthor.isEmpty()) &&(sourceAuthor.equalsIgnoreCase(targetAuthor));
		
		boolean sameNormalizedSpeciesCName = (sourceNormalizedSpeciesCName != null && !sourceNormalizedSpeciesCName.isEmpty()) && 
				(sourceNormalizedSpeciesCName.equals(targetNormalizedSpeciesCName));
		
		boolean hasAuthor = sourceAuthority != null && targetAuthority != null && (sourceAuthority.contains(targetAuthority) || targetAuthority.contains(sourceAuthority));
		boolean hasYear = sourceYear != null && targetYear != null && (sourceYear.contains(targetYear) || targetYear.contains(sourceYear));
		
		//GSAy, GSAY, GSrAy, GSrAY, GSA, GSrA, GSY, GSrY, GS, GSr, SAy, SrAy, SAY, SrAY, GAy, GAY
		
		if(sameGenusName) {
			
			if(sameSpeciesName) {
				if(sameAuthor) 
					scoreValue=100;			//GSAy
				else if(hasAuthor && hasYear) {  
					
					scoreValue=97;			//GSAY
				}else if(hasAuthor)
					scoreValue=88;			//GSA
				else if(hasYear)			
					scoreValue=82;			//GSY
				else
					scoreValue=76;			//GS
			}else if(sameNormalizedSpeciesCName) {
				if(sameAuthor) 
					scoreValue=94;			//GSrAy
				else if(hasAuthor && hasYear)  
					scoreValue=91;			//GSrAY
				else if(hasAuthor)
					scoreValue=85;			//GSrA
				else if(hasYear)			
					scoreValue=79;			//GSrY
				else
					scoreValue=73;			//GSr
			}else if(sameAuthor) {
				scoreValue=35;				//GAy
			}else if(hasAuthor && hasYear) {
				scoreValue=32;				//GAY
			}
		}else if(sameSpeciesName){
			if(sameAuthor) 
				scoreValue=70;			//SAy
			else if(hasAuthor && hasYear)  
				scoreValue=64;			//SAY
		}else if(sameNormalizedSpeciesCName) {
			if(sameAuthor) 
				scoreValue=67;			//SrAy
			else if(hasAuthor && hasYear)  
				scoreValue=61;			//SrAY
		}
		
		scoreValue = scoreValue/100;
		result.setValue(scoreValue);

		return result;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.model.matchlets.Matchlet#getDescription()
	 */
	@Override
	public String getDescription() {
		return "Performs GSAy matching between two reference species data";
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.uniform.matchlets.skeleton.UScalarMatchletSkeleton#doExtractData(java.lang.Object, org.fao.fi.comet.core.model.engine.DataIdentifier)
	 */
	@Override
	protected ReferenceSpeciesData doExtractData(ReferenceSpeciesData entity, DataIdentifier entityIdentifier) {
		return entity;
	}
}