/**
 * (c) 2013 FAO / UN (project: comet-engine-species)
 */
package org.fao.fi.comet.domain.species.matchlets.extended;

import org.fao.fi.comet.core.model.engine.DataIdentifier;
import org.fao.fi.comet.core.model.matchlets.annotations.MatchletIsOptionalByDefault;
import org.fao.fi.comet.domain.species.model.ReferenceSpeciesData;
import org.fao.fi.comet.extras.model.common.TypedComplexName;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 28 May 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 28 May 2013
 */
@MatchletIsOptionalByDefault
public class GenusCNameMatchlet extends AbstractCNameScalarMatchlet {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 2057284178844548705L;
	static public final String NAME = "GenusNameMatchlet";

	/**
	 * Class constructor
	 *
	 */
	public GenusCNameMatchlet() {
		super();

		this._name = NAME;
	}

	/**
	 * Class constructor
	 *
	 * @param soundexWeight
	 */
	public GenusCNameMatchlet(double levenshteinWeight, double soundexWeight, double trigramWeight) {
		super(levenshteinWeight, soundexWeight, trigramWeight);

		this._name = NAME;
	}

	@Override
	public String getDescription() {
		return "Matches genus names as extracted from a pair of input / reference matched entities";
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.uniform.matchlets.skeleton.UScalarMatchletSkeleton#doExtractData(java.io.Serializable)
	 */
	@Override
	protected TypedComplexName doExtractData(ReferenceSpeciesData entity, DataIdentifier dataIdentifier) {
		return entity.getGenusCName();
	}
}