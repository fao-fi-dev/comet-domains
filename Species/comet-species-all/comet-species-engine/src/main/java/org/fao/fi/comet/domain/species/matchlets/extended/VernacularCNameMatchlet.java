/**
 * (c) 2013 FAO / UN (project: comet-engine-species)
 */
package org.fao.fi.comet.domain.species.matchlets.extended;

import java.util.Collection;
import java.util.Map;

import org.fao.fi.comet.core.exceptions.MatchletConfigurationException;
import org.fao.fi.comet.core.model.engine.DataIdentifier;
import org.fao.fi.comet.core.model.matchlets.annotations.MatchletIsOptionalByDefault;
import org.fao.fi.comet.core.model.matchlets.annotations.MatchletParameter;
import org.fao.fi.comet.domain.species.VernacularNamesFactory;
import org.fao.fi.comet.domain.species.model.InputSpeciesData;
import org.fao.fi.comet.domain.species.model.ReferenceSpeciesData;
import org.fao.fi.comet.extras.model.common.LinkedTypedComplexName;
import org.fao.fi.comet.extras.model.common.TypedComplexName;
import org.fao.fi.sh.utility.core.helpers.singletons.text.StringsHelper;
import org.fao.fi.sh.utility.model.extensions.collections.ListSet;

import static org.fao.fi.sh.utility.core.helpers.singletons.lang.AssertionHelper.*;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 28 May 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 28 May 2013
 */
@MatchletIsOptionalByDefault
public class VernacularCNameMatchlet extends AbstractCNameVectorialMatchlet {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 2057284178844548705L;

	static final public String SUGGESTED_LANGS_PARAM = "suggestedLanguages";
	static final public String INCLUDE_UNKNOWN_LANGUAGE_PARAM = "includeUnknownLanguage";

	static public final String NAME = "VernacularNameMatchlet";

	@MatchletParameter(name=SUGGESTED_LANGS_PARAM,
					   description="Sets one (or many) of the available language names as the source of data being compared. " +
								   "It can be set to a comma-separated sequence of values.")
	private String _suggestedLanguages;

	@MatchletParameter(name=INCLUDE_UNKNOWN_LANGUAGE_PARAM,
			  		  description="When enabled, the matchlet will treat vernacular name in unknown languages as always valid during comparison")
	private Boolean _includeUnknownLanguage;

	/**
	 * Class constructor
	 */
	public VernacularCNameMatchlet() {
		super();

		this.setCombinationCriteria(COMBINATION_CRITERIA_OR);

		this._suggestedLanguages = null;
		this._includeUnknownLanguage = Boolean.TRUE;
		this._name = NAME;
	}

	public VernacularCNameMatchlet(double levenshteinWeight, double soundexWeight, double trigramWeight) {
		super(levenshteinWeight, soundexWeight, trigramWeight);

		this._suggestedLanguages = null;
		this._includeUnknownLanguage = Boolean.TRUE;
		this._name = NAME;
	}

	public VernacularCNameMatchlet(double levenshteinWeight, double soundexWeight, double trigramWeight, String suggestedLanguages, Boolean includeUnknownLanguage) {
		this(levenshteinWeight, soundexWeight, trigramWeight);

		if(suggestedLanguages != null) {
			this._suggestedLanguages = suggestedLanguages;
		} else
			this._suggestedLanguages = null;

		this._includeUnknownLanguage = Boolean.TRUE.equals(includeUnknownLanguage);
	}

	@Override
	public String getDescription() {
		return "Matches multiple language-specific names as extracted from a pair of input / reference matched entities";
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.domain.species.matchlets.extended.AbstractCNameVectorialMatchlet#doValidateConfiguration()
	 */
	@Override
	protected void doValidateConfiguration() throws MatchletConfigurationException {
		super.doValidateConfiguration();

		if(this._suggestedLanguages != null) {
			for(String language : this._suggestedLanguages.split("\\,")) {
				language = StringsHelper.trim(language);

				$nN(language, "{} cannot be configured with a NULL-equivalent suggested language", this.getClass().getSimpleName());
			}
		}
	}

	/**
	 * @return the 'suggestedLanguages' value
	 */
	public String getSuggestedLanguages() {
		return this._suggestedLanguages;
	}

	/**
	 * @param suggestedLanguages the 'suggestedLanguages' value to set
	 */
	public void setSuggestedLanguages(String suggestedLanguages) {
		this._suggestedLanguages = suggestedLanguages;
	}

	/**
	 * @return the 'includeUnknownLanguage' value
	 */
	public Boolean getIncludeUnknownLanguage() {
		return this._includeUnknownLanguage;
	}

	/**
	 * @param includeUnknownLanguage the 'includeUnknownLanguage' value to set
	 */
	public void setIncludeUnknownLanguage(Boolean includeUnknownLanguage) {
		this._includeUnknownLanguage = includeUnknownLanguage;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.uniform.matchlets.skeleton.UScalarMatchletSkeleton#doExtractData(java.io.Serializable)
	 */
	@Override
	protected Collection<TypedComplexName> doExtractData(ReferenceSpeciesData entity, DataIdentifier dataIdentifier) {
		if(entity.getVernacularCNames() == null || entity.getVernacularCNames().length == 0)
			return null;

		Collection<TypedComplexName> data = new ListSet<TypedComplexName>();

		Map<String, TypedComplexName[]> complexNamesMap = entity.getComplexNamesMap();

		boolean hasNameInSuggestedLanguage = false;

		Collection<String> suggestedLanguages = new ListSet<String>();

		if(Boolean.TRUE.equals(this._includeUnknownLanguage))
			suggestedLanguages.add(VernacularNamesFactory.VERNACULAR_CNAME_PREFIX + VernacularNamesFactory.UNKNOWN_LANGUAGE);

		if(this._suggestedLanguages != null)
			for(String suggestedLanguage : this._suggestedLanguages.split("\\,")) {
				suggestedLanguage = StringsHelper.trim(suggestedLanguage);

				if(suggestedLanguage != null)
					suggestedLanguages.add(VernacularNamesFactory.VERNACULAR_CNAME_PREFIX + suggestedLanguage);
			}

		if(!suggestedLanguages.isEmpty()) {
			for(String suggestedLanguage : suggestedLanguages) {
				suggestedLanguage = StringsHelper.trim(suggestedLanguage);
				if(this._includeUnknownLanguage || complexNamesMap.containsKey(suggestedLanguage)) {
					hasNameInSuggestedLanguage = true;

					break;
				}
			}
		}

		boolean isSimpleSpecies = entity instanceof InputSpeciesData;

		if(this._suggestedLanguages != null) {
			if(!hasNameInSuggestedLanguage) {
				//Target data (as extended species) should return no data in this case...
				if(!isSimpleSpecies)
					return null;
			}
		} else {
			for(LinkedTypedComplexName vernacular : entity.getVernacularCNames()) {
				data.add(vernacular);
			}
		}

		if(!suggestedLanguages.isEmpty()) {
			String key;
			for(String suggestedLanguage : suggestedLanguages) {
				key = suggestedLanguage;

				if(complexNamesMap.containsKey(key))
					for(TypedComplexName vernacular : complexNamesMap.get(key))
						data.add(vernacular);
			}
		}

		return data;
	}
}