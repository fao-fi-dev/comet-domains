/**
 * (c) 2013 FAO / UN (project: comet-engine-species)
 */
package org.fao.fi.comet.domain.species.matchlets.extended;

import org.fao.fi.comet.core.exceptions.MatchletConfigurationException;
import org.fao.fi.comet.core.model.engine.DataIdentifier;
import org.fao.fi.comet.core.model.matchlets.annotations.MatchletDefaultSerializationExclusionPolicy;
import org.fao.fi.comet.core.model.matchlets.annotations.MatchletParameter;
import org.fao.fi.comet.core.model.matchlets.annotations.parameters.DoubleRange;
import org.fao.fi.comet.core.model.matchlets.support.MatchingSerializationExclusionPolicy;
import org.fao.fi.comet.core.model.score.support.MatchingScore;
import org.fao.fi.comet.core.uniform.matchlets.skeleton.UScalarMatchletSkeleton;
import org.fao.fi.comet.domain.species.matchlets.extended.support.CNamesScoreCalculator;
import org.fao.fi.comet.domain.species.model.ReferenceSpeciesData;
import org.fao.fi.comet.extras.model.common.TypedComplexName;

import static org.fao.fi.sh.utility.core.helpers.singletons.lang.AssertionHelper.*;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 28 May 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 28 May 2013
 */
@MatchletDefaultSerializationExclusionPolicy(MatchingSerializationExclusionPolicy.NON_PERFORMED)
abstract public class AbstractCNameScalarMatchlet extends UScalarMatchletSkeleton<ReferenceSpeciesData, TypedComplexName> {
	static final public String LEVENSHTEIN_WEIGHT_PARAM = "levenshteinWeight";
	static final public String SOUNDEX_WEIGHT_PARAM = "soundexWeight";
	static final public String TRIGRAM_WEIGHT_PARAM = "trigramWeight";

	static final private double MIN_WEIGHT = 0D;
	static final private double MAX_WEIGHT = 100D;

	/** Field serialVersionUID */
	private static final long serialVersionUID = 1653730901184509134L;

	@MatchletParameter(name=LEVENSHTEIN_WEIGHT_PARAM,
					  description="Sets the weight of the levenshtein distance contribution in the computation of the final matching score.")
	@DoubleRange(from=MIN_WEIGHT, to=MAX_WEIGHT, includeFrom=true, includeTo=true)
	protected double _levenshteinWeight;

	@MatchletParameter(name=SOUNDEX_WEIGHT_PARAM,
					  description="Sets the weight of the soundex contribution in the computation of the final matching score.")
	@DoubleRange(from=MIN_WEIGHT, to=MAX_WEIGHT, includeFrom=true, includeTo=true)
	protected double _soundexWeight;

	@MatchletParameter(name=TRIGRAM_WEIGHT_PARAM,
			  description="Sets the weight of the trigram contribution in the computation of the final matching score.")
	@DoubleRange(from=MIN_WEIGHT, to=MAX_WEIGHT, includeFrom=true, includeTo=true)
	protected double _trigramWeight;

	private final CNamesScoreCalculator _scoreCalculator;

	/**
	 * Class constructor
	 */
	public AbstractCNameScalarMatchlet() {
		this(70, 30, 0);
	}

	/**
	 * Class constructor
	 */
	public AbstractCNameScalarMatchlet(double levenshteinWeight, double soundexWeight, double trigramWeight) {
		$gte(levenshteinWeight, MIN_WEIGHT, "Levenshtein weight ({}) must be greater than or equal to {}", levenshteinWeight, MIN_WEIGHT);
		$lte(levenshteinWeight, MAX_WEIGHT, "Levenshtein weight ({}) must be lower than or equal to {}", levenshteinWeight, MAX_WEIGHT);

		$gte(soundexWeight, MIN_WEIGHT, "Soundex weight ({}) must be greater than or equal to {}", soundexWeight, MIN_WEIGHT);
		$lte(soundexWeight, MAX_WEIGHT, "Soundex weight ({}) must be lower than or equal to {}", soundexWeight, MAX_WEIGHT);

		$gte(trigramWeight, MIN_WEIGHT, "Trigram weight ({}) must be greater than or equal to {}", trigramWeight, MIN_WEIGHT);
		$lte(trigramWeight, MAX_WEIGHT, "Trigram weight ({}) must be lower than or equal to {}", trigramWeight, MAX_WEIGHT);

		this._soundexWeight = soundexWeight;
		this._levenshteinWeight = levenshteinWeight;
		this._trigramWeight = trigramWeight;

		this._scoreCalculator = new CNamesScoreCalculator();
	}


	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.matchlets.skeleton.ScalarMatchletSkeleton#doValidateConfiguration()
	 */
	@Override
	protected void doValidateConfiguration() throws MatchletConfigurationException {
		super.doValidateConfiguration();

		$gte(_levenshteinWeight, MIN_WEIGHT, "{} Levenshtein weight must be greater than (or equal to) {} (currently: {})",
			this.getClass().getSimpleName(),
			MIN_WEIGHT,
			_levenshteinWeight
		);

		$lte(_levenshteinWeight, MAX_WEIGHT, "{} Levenshtein weight must be lower than (or equal to) {} (currently: {})",
			this.getClass().getSimpleName(),
			MAX_WEIGHT,
			_levenshteinWeight
		);

		$gte(_soundexWeight, MIN_WEIGHT, "{} soundex weight must be greater than (or equal to) {} (currently: {})",
			this.getClass().getSimpleName(),
			MIN_WEIGHT,
			_soundexWeight
		);

		$lte(_soundexWeight, MAX_WEIGHT, "{} soundex weight must be lower than (or equal to) {} (currently: {})",
			this.getClass().getSimpleName(),
			MAX_WEIGHT,
			_soundexWeight
		);

		$gte(_trigramWeight, MIN_WEIGHT, "{} trigram weight must be greater than (or equal to) {} (currently: {})",
			this.getClass().getSimpleName(),
			MIN_WEIGHT,
			_trigramWeight
		);

		$lte(_trigramWeight, MAX_WEIGHT, "{} trigram weight must be lower than (or equal to) {} (currently: {})",
			this.getClass().getSimpleName(),
			MAX_WEIGHT,
			_trigramWeight
		);
	}

	/**
	 * @return the 'levenshteinWeight' value
	 */
	public double getLevenshteinWeight() {
		return this._levenshteinWeight;
	}

	/**
	 * @param levenshteinWeight the 'levenshteinWeight' value to set
	 */
	public void setLevenshteinWeight(double levenshteinWeight) {
		this._levenshteinWeight = levenshteinWeight;
	}

	/**
	 * @return the 'soundexWeight' value
	 */
	public double getSoundexWeight() {
		return this._soundexWeight;
	}

	/**
	 * @param soundexWeight the 'soundexWeight' value to set
	 */

	public void setSoundexWeight(double soundexWeight) {
		this._soundexWeight = soundexWeight;
	}

	/**
	 * @return the 'trigramWeight' value
	 */
	public double getTrigramWeight() {
		return this._trigramWeight;
	}

	/**
	 * @param trigramWeight the 'trigramWeight' value to set
	 */
	public void setTrigramWeight(double trigramWeight) {
		this._trigramWeight = trigramWeight;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.matchlets.Matchlet#computeScore(java.io.Serializable, java.io.Serializable)
	 */
	@Override
	public MatchingScore computeScore(ReferenceSpeciesData source, DataIdentifier sourceIdentifier, TypedComplexName sourceData, ReferenceSpeciesData target, DataIdentifier targetIdentifier, TypedComplexName targetData) {
		boolean hasSourceData, hasTargetData;

		hasSourceData = sourceData != null && sourceData.getName() != null;
		hasTargetData = targetData != null && targetData.getName() != null;

		if(!hasSourceData || !hasTargetData) {
			if(this._isOptional)
				return MatchingScore.getNonPerformedTemplate();

			return MatchingScore.getNonAuthoritativeNoMatchTemplate();
		}

		return this._scoreCalculator.computeScore(this._levenshteinWeight, this._soundexWeight, this._trigramWeight, source, sourceIdentifier, sourceData, target, targetIdentifier, targetData);
	}
}