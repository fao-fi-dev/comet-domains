/**
 * (c) 2013 FAO / UN (project: comet-species)
 */
package org.fao.fi.comet.domain.species.patterns.data.partitioners;

import org.fao.fi.comet.core.patterns.data.partitioners.DataPartitioner;
import org.fao.fi.comet.domain.species.model.ReferenceSpeciesData;
/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 21/nov/2013   Fabio     Creation.
 *
 * @version 1.0
 * @since 21/nov/2013
 */
abstract public class SpeciesDataPartitioner implements DataPartitioner<ReferenceSpeciesData, ReferenceSpeciesData> {
}
