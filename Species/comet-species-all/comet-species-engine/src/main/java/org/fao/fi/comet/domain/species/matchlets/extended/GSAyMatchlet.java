/**
 * (c) 2013 FAO / UN (project: YASMEEN-custom-matchlets)
 */
package org.fao.fi.comet.domain.species.matchlets.extended;

import java.util.HashSet;
import java.util.Set;

import org.fao.fi.comet.core.model.engine.DataIdentifier;
import org.fao.fi.comet.core.model.score.support.MatchingScore;
import org.fao.fi.comet.core.model.score.support.MatchingType;
import org.fao.fi.comet.core.uniform.matchlets.skeleton.UScalarMatchletSkeleton;
import org.fao.fi.comet.domain.species.model.ReferenceSpeciesData;
import org.fao.fi.comet.extras.model.common.TypedComplexName;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 28/ott/2013   Fabio     Creation.
 *
 * @version 1.0
 * @since 28/ott/2013
 */
public class GSAyMatchlet extends UScalarMatchletSkeleton<ReferenceSpeciesData, ReferenceSpeciesData> {
	/** Field serialVersionUID */
	private static final long serialVersionUID = 5534829313877324698L;

	static public final String NAME = "GSAyMatchlet";

	final static private int SWITCH_G  = 1 << 1;
	final static private int SWITCH_S  = 1 << 2;
	final static private int SWITCH_Sr = 1 << 3;
	final static private int SWITCH_A  = 1 << 4;
	final static private int SWITCH_y  = 1 << 5;
	final static private int SWITCH_Y  = 1 << 6;
	
	final static private int SWITCH_Ay  = 1 << 7;
	final static private int SWITCH_AY  = 1 << 8;
	
	final static private int SWITCH_GSAy = SWITCH_G | SWITCH_S | SWITCH_Ay;
	final static private int SWITCH_GSAY = SWITCH_G | SWITCH_S | SWITCH_AY;
	final static private int SWITCH_GSrAy = SWITCH_G | SWITCH_Sr | SWITCH_Ay;
	final static private int SWITCH_GSrAY = SWITCH_G | SWITCH_Sr | SWITCH_AY;
	final static private int SWITCH_GSA = SWITCH_G | SWITCH_S | SWITCH_A;
	final static private int SWITCH_GSrA = SWITCH_G | SWITCH_Sr | SWITCH_A;
	final static private int SWITCH_GSY = SWITCH_G | SWITCH_S | SWITCH_Y;
	final static private int SWITCH_GSy = SWITCH_G | SWITCH_S | SWITCH_y;
	final static private int SWITCH_GSrY = SWITCH_G | SWITCH_Sr | SWITCH_Y;
	final static private int SWITCH_GSry = SWITCH_G | SWITCH_Sr | SWITCH_y;
	final static private int SWITCH_GS = SWITCH_G | SWITCH_S;
	final static private int SWITCH_GSr = SWITCH_G | SWITCH_Sr;
	final static private int SWITCH_SAy = SWITCH_S | SWITCH_Ay;
	final static private int SWITCH_SrAy = SWITCH_Sr | SWITCH_Ay;
	final static private int SWITCH_SAY = SWITCH_S | SWITCH_AY;
	final static private int SWITCH_SrAY = SWITCH_Sr | SWITCH_AY;
	final static private int SWITCH_GAy = SWITCH_G | SWITCH_Ay;
	final static private int SWITCH_GAY = SWITCH_G | SWITCH_AY;
	
	
	
	/**
	 * Class constructor
	 *
	 */
	public GSAyMatchlet() {
		super();

		this._name = NAME;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.model.matchlets.Matchlet#computeScore(java.lang.Object, org.fao.fi.comet.core.model.engine.DataIdentifier, java.lang.Object, java.lang.Object, org.fao.fi.comet.core.model.engine.DataIdentifier, java.lang.Object)
	 */
	@Override
	public MatchingScore computeScore(ReferenceSpeciesData source,
									  DataIdentifier sourceIdentifier,
									  ReferenceSpeciesData sourceData,
									  ReferenceSpeciesData target,
									  DataIdentifier targetIdentifier,
									  ReferenceSpeciesData targetData) {

		MatchingScore result = new MatchingScore(MatchingScore.SCORE_NO_MATCH, MatchingType.NON_AUTHORITATIVE);

		int triggered = 0;
		
		triggered = triggered | ( this.test_G(sourceData, targetData)  ? SWITCH_G  : 0 );
		triggered = triggered | ( this.test_S(sourceData, targetData)  ? SWITCH_S  : 0 );
		
		if((triggered & SWITCH_S) != SWITCH_S)
			triggered = triggered | ( this.test_Sr(sourceData, targetData) ? SWITCH_Sr : 0 );
		
		triggered = triggered | ( this.test_Ay(sourceData, targetData) ? SWITCH_Ay: 0 );
		
		if((triggered & SWITCH_Ay) != SWITCH_Ay)
			triggered = triggered | ( this.test_AY(sourceData, targetData) ? SWITCH_AY: 0 );
		
		if((triggered & SWITCH_Ay) != SWITCH_Ay && (triggered & SWITCH_AY) != SWITCH_AY )
			triggered = triggered | ( this.test_A(sourceData, targetData)  ? SWITCH_A : 0 );
		
		if((triggered & SWITCH_Ay) != SWITCH_Ay && (triggered & SWITCH_AY) != SWITCH_AY )
			triggered = triggered | ( this.test_Y(sourceData, targetData)  ? SWITCH_Y  : 0 );

		result.setValue(this.score(triggered) * 0.01);

		return result;
	}
	
	private int score(int triggered) {
		switch(triggered) {
			case SWITCH_GSAy:
				return 100;
			case SWITCH_GSAY:
				return 97;
			case SWITCH_GSrAy:
				return 94;
			case SWITCH_GSrAY:
				return 91;
			case SWITCH_GSA:
				return 88;
			case SWITCH_GSrA:
				return 85;
			case SWITCH_GSY:
			case SWITCH_GSy:
				return 82;
			case SWITCH_GSrY:
			case SWITCH_GSry:
				return 79;
			case SWITCH_GS:
				return 76;
			case SWITCH_GSr:
				return 73;
			case SWITCH_SAy:
				return 70;
			case SWITCH_SrAy:
				return 67;
			case SWITCH_SAY:
				return 64;
			case SWITCH_SrAY:
				return 61;
			case SWITCH_GAy:
				return 35;
			case SWITCH_GAY:
				return 32;
			default:
				return 0;
		}

	}

	private boolean hasParenthesis(String what) {
		return what != null && what.matches("^\\(.+\\)$");
	}
	
	private boolean areEquals(Object source, Object target) {
		return source == null && target == null 
				? true :
					source == null || target == null 
					? false :
						source.equals(target);
	}
	
	private boolean test_G(ReferenceSpeciesData sourceData, ReferenceSpeciesData targetData) {
		return this.areEquals(sourceData.getGenus(), targetData.getGenus());
	}

	private boolean test_S(ReferenceSpeciesData sourceData, ReferenceSpeciesData targetData) {
		return sourceData.getSpecies() != null && targetData.getSpecies() != null && this.areEquals(sourceData.getSpecies().toUpperCase(), targetData.getSpecies().toUpperCase());
	}
	
	private boolean test_Sr(ReferenceSpeciesData sourceData, ReferenceSpeciesData targetData) {
		return sourceData.getSpecies() != null && targetData.getSpecies() != null && 
			   this.areEquals(sourceData.getNormalizedSpeciesCName(), targetData.getNormalizedSpeciesCName());
	}

	private boolean test_Ay(ReferenceSpeciesData sourceData, ReferenceSpeciesData targetData) {
		Set<String> sourceAuthorities = new HashSet<String>();
		Set<String> targetAuthorities = new HashSet<String>();
		
		if(sourceData.getAuthoritiesCName() != null)
			for(TypedComplexName authority : sourceData.getAuthoritiesCName())
				if(authority.getSimplifiedName() != null && authority.getSimplifiedName().length() > 1)
					sourceAuthorities.add(authority.getSimplifiedName());

		if(targetData.getAuthoritiesCName() != null)
			for(TypedComplexName authority : targetData.getAuthoritiesCName())
				if(authority.getSimplifiedName() != null && authority.getSimplifiedName().length() > 1)
					targetAuthorities.add(authority.getSimplifiedName());
		
		
		boolean sameAuthors = !sourceAuthorities.isEmpty() && 
							  !targetAuthorities.isEmpty() && 
							  sourceAuthorities.equals(targetAuthorities);
		
		boolean sameParenthesis = this.hasParenthesis(sourceData.getAuthor()) == this.hasParenthesis(targetData.getAuthor());
		boolean sameYear =  sourceData.getAuthorityYear() != null && 
							targetData.getAuthorityYear() != null && 
							this.areEquals(sourceData.getAuthorityYear(), targetData.getAuthorityYear());
		
		return sameAuthors && sameParenthesis && sameYear;
	}
	
	private boolean test_AY(ReferenceSpeciesData sourceData, ReferenceSpeciesData targetData) {
		Set<String> sourceAuthorities = new HashSet<String>();
		Set<String> targetAuthorities = new HashSet<String>();
		
		if(sourceData.getAuthoritiesCName() != null)
			for(TypedComplexName authority : sourceData.getAuthoritiesCName())
				if(authority.getSimplifiedName() != null && authority.getSimplifiedName().length() > 1)
					sourceAuthorities.add(authority.getSimplifiedName());

		if(targetData.getAuthoritiesCName() != null)
			for(TypedComplexName authority : targetData.getAuthoritiesCName())
				if(authority.getSimplifiedName() != null && authority.getSimplifiedName().length() > 1)
					targetAuthorities.add(authority.getSimplifiedName());
		
		boolean sameAuthors = !sourceAuthorities.isEmpty() && 
							  !targetAuthorities.isEmpty() && 
							( sourceAuthorities.containsAll(targetAuthorities) || targetAuthorities.containsAll(sourceAuthorities) );

		boolean sameYear =  sourceData.getAuthorityYear() != null && 
							targetData.getAuthorityYear() != null && 
							this.areEquals(sourceData.getAuthorityYear(), targetData.getAuthorityYear());
		
		return sameAuthors && sameYear;
	}
	
	private boolean test_A(ReferenceSpeciesData sourceData, ReferenceSpeciesData targetData) {
		Set<String> sourceAuthorities = new HashSet<String>();
		Set<String> targetAuthorities = new HashSet<String>();
		
		if(sourceData.getAuthoritiesCName() != null)
			for(TypedComplexName authority : sourceData.getAuthoritiesCName())
				if(authority.getSimplifiedName() != null && authority.getSimplifiedName().length() > 1)
					sourceAuthorities.add(authority.getSimplifiedName());

		if(targetData.getAuthoritiesCName() != null)
			for(TypedComplexName authority : targetData.getAuthoritiesCName())
				if(authority.getSimplifiedName() != null && authority.getSimplifiedName().length() > 1)
					targetAuthorities.add(authority.getSimplifiedName());
		
		return !sourceAuthorities.isEmpty() && 
			   !targetAuthorities.isEmpty() && 
			   sourceAuthorities.equals(targetAuthorities);
	}
	
	@SuppressWarnings("unused")
	private boolean test_y(ReferenceSpeciesData sourceData, ReferenceSpeciesData targetData) {
		boolean sourceHasParenthesis = this.hasParenthesis(sourceData.getAuthor());
		boolean targetHasParenthesis = this.hasParenthesis(targetData.getAuthor());
		
		return sourceHasParenthesis == targetHasParenthesis && 
			   sourceData.getAuthorityYear() != null && 
			   targetData.getAuthorityYear() != null && 
			   this.areEquals(sourceData.getAuthorityYear(), targetData.getAuthorityYear());
	}
	
	private boolean test_Y(ReferenceSpeciesData sourceData, ReferenceSpeciesData targetData) {
		return sourceData.getAuthorityYear() != null && 
			   targetData.getAuthorityYear() != null && 
			   this.areEquals(sourceData.getAuthorityYear(), targetData.getAuthorityYear());
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.model.matchlets.Matchlet#getDescription()
	 */
	@Override
	public String getDescription() {
		return "Performs GSAy matching between two reference species data";
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.uniform.matchlets.skeleton.UScalarMatchletSkeleton#doExtractData(java.lang.Object, org.fao.fi.comet.core.model.engine.DataIdentifier)
	 */
	@Override
	protected ReferenceSpeciesData doExtractData(ReferenceSpeciesData entity, DataIdentifier entityIdentifier) {
		return entity;
	}
}