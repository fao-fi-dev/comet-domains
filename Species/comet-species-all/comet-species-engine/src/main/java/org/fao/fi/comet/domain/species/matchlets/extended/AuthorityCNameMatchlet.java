/**
 * (c) 2013 FAO / UN (project: comet-species)
 */
package org.fao.fi.comet.domain.species.matchlets.extended;

import java.util.Arrays;
import java.util.Collection;

import org.fao.fi.comet.core.model.engine.DataIdentifier;
import org.fao.fi.comet.core.model.matchlets.annotations.MatchletDefaultSerializationExclusionPolicy;
import org.fao.fi.comet.core.model.matchlets.annotations.MatchletIsOptionalByDefault;
import org.fao.fi.comet.core.model.matchlets.support.MatchingSerializationExclusionPolicy;
import org.fao.fi.comet.domain.species.model.ReferenceSpeciesData;
import org.fao.fi.comet.extras.model.common.TypedComplexName;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 5 Aug 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 5 Aug 2013
 */
@MatchletIsOptionalByDefault
@MatchletDefaultSerializationExclusionPolicy(MatchingSerializationExclusionPolicy.NON_PERFORMED)
public class AuthorityCNameMatchlet extends AbstractCNameVectorialMatchlet {
	static public final String NAME = "AuthorityNameMatchlet";

	/** Field serialVersionUID */
	private static final long serialVersionUID = 2575788466351826783L;

	/**
	 * Class constructor
	 *
	 */
	public AuthorityCNameMatchlet() {
		super();

		this._name = NAME;
	}

	/**
	 * Class constructor
	 *
	 * @param soundexWeight
	 */
	public AuthorityCNameMatchlet(double levenshteinWeight, double soundexWeight, double trigramWeight) {
		super(levenshteinWeight, soundexWeight, trigramWeight);

		this._name = NAME;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.model.matchlets.Matchlet#getDescription()
	 */
	@Override
	public String getDescription() {
		return "Matches authorities (author names) as extracted from a pair of input / reference matched entities";
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.uniform.matchlets.skeleton.UVectorialMatchletSkeleton#doExtractData(java.lang.Object, org.fao.fi.comet.core.model.engine.DataIdentifier)
	 */
	@Override
	protected Collection<TypedComplexName> doExtractData(ReferenceSpeciesData entity, DataIdentifier dataIdentifier) {
		return entity.getAuthoritiesCName() == null ? null : Arrays.asList(entity.getAuthoritiesCName());
	}
}