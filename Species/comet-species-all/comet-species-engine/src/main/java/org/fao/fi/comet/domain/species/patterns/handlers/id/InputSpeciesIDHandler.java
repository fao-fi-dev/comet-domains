/**
 * (c) 2013 FAO / UN (project: comet-species)
 */
package org.fao.fi.comet.domain.species.patterns.handlers.id;

import org.fao.fi.comet.core.patterns.handlers.id.IDHandler;
import org.fao.fi.comet.domain.species.model.InputSpeciesData;
import org.fao.fi.comet.domain.species.model.ReferenceSpeciesData;
import org.fao.fi.sh.utility.core.helpers.singletons.lang.ObjectsHelper;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 17 Jun 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 17 Jun 2013
 */
final public class InputSpeciesIDHandler implements IDHandler<ReferenceSpeciesData, String> {
	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.patterns.handlers.id.IDHandler#getId(java.lang.Object)
	 */
	@Override
	public String getId(final ReferenceSpeciesData data) {
		InputSpeciesData simple = (InputSpeciesData)data;
		
		return simple == null ? null : ObjectsHelper.coalesce(simple.getId(), simple.getOriginal());
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.patterns.handlers.id.IDHandler#serializeId(java.io.Serializable)
	 */
	@Override
	public String serializeId(final String id) {
		return id == null ? null : id;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.core.patterns.handlers.id.IDHandler#getSerializedId(java.lang.Object)
	 */
	@Override
	public String getSerializedId(final ReferenceSpeciesData data) {
		return this.getId(data);
	}
}
