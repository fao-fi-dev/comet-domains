/**
 * (c) 2013 FAO / UN (project: comet-species)
 */
package org.fao.fi.comet.domain.species.tools.lexical.processors.icu.queue.test;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.TreeSet;

import org.fao.fi.comet.domain.species.tools.lexical.processors.icu.queue.impl.SpeciesNormalizerProcessorQueue;
import org.fao.fi.sh.utility.core.helpers.singletons.io.ResourcesHelper;
import org.fao.fi.sh.utility.lexical.processors.LexicalProcessorsQueue;
import org.junit.Assert;
import org.junit.Test;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 8 Oct 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 8 Oct 2013
 */
public class SpeciesNormalizerProcessorQueueTest {
	static final Properties CFG_PROPERTIES;
	
	static {
		CFG_PROPERTIES = new Properties();
		
		try {
			CFG_PROPERTIES.load(ResourcesHelper.getClasspathResourceAsStream("FinDB.properties"));
		} catch(IOException IOe) {
			throw new RuntimeException("Unable to load DB properties from classpath resource", IOe);
		}
	}
	final protected Connection connect() throws Throwable {
		System.out.println(System.currentTimeMillis() + " Acquiring connection...");

		try {
			Class.forName(CFG_PROPERTIES.getProperty("fin.db.driver"));

			return DriverManager.getConnection(CFG_PROPERTIES.getProperty("fin.db.url"), CFG_PROPERTIES.getProperty("fin.db.user"), CFG_PROPERTIES.getProperty("fin.db.password"));
		} finally {
			System.out.println(System.currentTimeMillis() + " Connection acquired!");
		}
	}

	final protected ResultSet select(Connection connection) throws Throwable {
		try {
			System.out.println(System.currentTimeMillis() + " Executing query...");

			return connection.createStatement().executeQuery("SELECT * FROM taxamatch");
		} finally {
			System.out.println(System.currentTimeMillis() + " Query executed!");
		}
	}

	@Test
	public void testFINDBNormalization() throws Throwable {
		LexicalProcessorsQueue normalizerQueue = new SpeciesNormalizerProcessorQueue();
		Connection conn = this.connect();
		ResultSet rs = this.select(conn);

		Map<NameValue, String> genusNOK = new HashMap<NameValue, String>();
		Map<NameValue, String> lastEpithetNOK = new HashMap<NameValue, String>();

		try {
			if(rs != null) {
				String genus = null;
				String lastEpithet = null;
				String normGenus = null;
				String normLastEpithet = null;

				String processedNormGenus = null;
				String processedNormLastEpithet = null;

				while(rs.next()) {
					genus = rs.getString("syngenus");
					lastEpithet = rs.getString("lastepithet");
					normGenus = rs.getString("normalizegenus");
					normLastEpithet = rs.getString("normalizelastepithet");

					processedNormGenus = normalizerQueue.process(genus);
					processedNormLastEpithet = normalizerQueue.process(lastEpithet);

					if(processedNormGenus != null && !processedNormGenus.equals(normGenus))
						genusNOK.put(new NameValue(genus, normGenus), processedNormGenus);

					if(processedNormLastEpithet != null && !processedNormLastEpithet.equals(normLastEpithet))
						lastEpithetNOK.put(new NameValue(lastEpithet, normLastEpithet), processedNormLastEpithet);
				}

				if(!genusNOK.isEmpty()) {
					System.out.println(genusNOK.size() + " genuses are not equal");

					for(NameValue key : new TreeSet<NameValue>(genusNOK.keySet()))
						System.out.println(key + " -> " + genusNOK.get(key));
				}

				Assert.assertEquals(9, genusNOK.size());

				if(!lastEpithetNOK.isEmpty()) {
					System.out.println(lastEpithetNOK.size() + " last epithets are not equal");

					for(NameValue key : new TreeSet<NameValue>(lastEpithetNOK.keySet()))
						System.out.println(key + " -> " + lastEpithetNOK.get(key));
				}

				Assert.assertEquals(76, lastEpithetNOK.size());
			} else {
				throw new RuntimeException("No resultset (or empty resultset) returned");
			}
		} finally {
			rs.close();
			conn.close();
		}
	}

	protected class NameValue implements Comparable<NameValue> {
		private String _name;
		private String _value;
		/**
		 * Class constructor
		 *
		 */
		public NameValue() {
			super();
		}

		/**
		 * Class constructor
		 *
		 * @param name
		 * @param value
		 */
		public NameValue(String name, String value) {
			super();
			this._name = name;
			this._value = value;
		}

		/**
		 * @return the 'name' value
		 */
		public final String getName() {
			return this._name;
		}

		/**
		 * @param name the 'name' value to set
		 */
		public final void setName(String name) {
			this._name = name;
		}

		/**
		 * @return the 'value' value
		 */
		public final String getValue() {
			return this._value;
		}

		/**
		 * @param value the 'value' value to set
		 */
		public final void setValue(String value) {
			this._value = value;
		}

		@Override
		public String toString() {
			return "{ " + this._name + ", " + this._value + " }";
		}

		/* (non-Javadoc)
		 * @see java.lang.Comparable#compareTo(java.lang.Object)
		 */
		@Override
		public int compareTo(NameValue another) {
			int nameComparison = this._name.compareTo(another._name);

			return nameComparison != 0 ? nameComparison : this._value.compareTo(another._value);
		}
	}
}
