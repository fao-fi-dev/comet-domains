/**
 * (c) 2013 FAO / UN (project: comet-species)
 */
package org.fao.fi.comet.domain.species.tools.lexical.processors.icu.queue.impl;

import java.util.Arrays;
import java.util.Collection;

import org.fao.fi.comet.domain.species.tools.lexical.processors.queue.SpeciesSimplifier;
import org.fao.fi.sh.utility.lexical.processors.LexicalProcessor;
import org.fao.fi.sh.utility.lexical.processors.base.AbstractLexicalProcessorsQueue;
import org.fao.fi.sh.utility.lexical.processors.extras.impl.ICUTransliteratorProcessor;
import org.fao.fi.sh.utility.lexical.processors.impl.MultipleSpacesRemoverProcessor;
import org.fao.fi.sh.utility.lexical.processors.impl.SymbolsAndDigitsRemoverProcessor;
import org.fao.fi.sh.utility.lexical.processors.impl.SymbolsRemoverProcessor;
import org.fao.fi.sh.utility.lexical.processors.impl.UppercaseProcessor;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 8 Oct 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 8 Oct 2013
 */
public class SpeciesSimplifierProcessorQueue extends AbstractLexicalProcessorsQueue implements SpeciesSimplifier {
	/* (non-Javadoc)
	 * @see org.fao.vrmf.core.tools.lexical.processors.AbstractLexicalProcessorsQueue#doGetActualProcessors()
	 */
	@Override
	protected Collection<LexicalProcessor> doGetActualProcessors() {
		return Arrays.asList(new LexicalProcessor[] {
			new ICUTransliteratorProcessor(),
			new SymbolsAndDigitsRemoverProcessor(SymbolsRemoverProcessor.STRATEGY_REPLACE_WITH_BLANKS),
			new MultipleSpacesRemoverProcessor(2),
			new UppercaseProcessor()
		});
	}
}