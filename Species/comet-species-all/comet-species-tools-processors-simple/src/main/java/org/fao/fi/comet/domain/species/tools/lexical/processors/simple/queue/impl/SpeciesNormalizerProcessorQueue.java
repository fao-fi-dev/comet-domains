/**
 * (c) 2013 FAO / UN (project: comet-species)
 */
package org.fao.fi.comet.domain.species.tools.lexical.processors.simple.queue.impl;

import java.util.ArrayList;
import java.util.Collection;

import org.fao.fi.comet.domain.species.tools.lexical.processors.impl.SpeciesNormalizerProcessor;
import org.fao.fi.comet.domain.species.tools.lexical.processors.queue.SpeciesNormalizer;
import org.fao.fi.sh.utility.lexical.processors.LexicalProcessor;
import org.fao.fi.sh.utility.lexical.processors.base.AbstractReplacementStrategyAwareLexicalProcessor;
import org.fao.fi.sh.utility.lexical.processors.impl.MultipleEqualCharsRemoverProcessor;
import org.fao.fi.sh.utility.lexical.processors.impl.MultipleSpacesRemoverProcessor;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 8 Oct 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 8 Oct 2013
 */
public class SpeciesNormalizerProcessorQueue extends SpeciesSimplifierProcessorQueue implements SpeciesNormalizer {
	/* (non-Javadoc)
	 * @see org.fao.fi.comet.domain.species.processors.queue.SpeciesSimplifierProcessorQueue#doGetActualProcessors()
	 */
	@Override
	protected Collection<LexicalProcessor> doGetActualProcessors() {
		Collection<LexicalProcessor> superProcessors = new ArrayList<LexicalProcessor>(super.doGetActualProcessors());

		superProcessors.add(new MultipleSpacesRemoverProcessor(AbstractReplacementStrategyAwareLexicalProcessor.STRATEGY_REMOVE, 1));
		superProcessors.add(new MultipleEqualCharsRemoverProcessor());
		superProcessors.add(new SpeciesNormalizerProcessor());

		return superProcessors;
	}
}