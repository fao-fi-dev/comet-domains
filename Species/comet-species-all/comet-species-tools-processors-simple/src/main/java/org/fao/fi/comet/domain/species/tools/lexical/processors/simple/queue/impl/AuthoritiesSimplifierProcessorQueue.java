/**
 * (c) 2013 FAO / UN (project: comet-species)
 */
package org.fao.fi.comet.domain.species.tools.lexical.processors.simple.queue.impl;

import java.util.Arrays;
import java.util.Collection;

import org.fao.fi.comet.domain.species.tools.lexical.processors.queue.AuthoritiesSimplifier;
import org.fao.fi.sh.utility.lexical.processors.LexicalProcessor;
import org.fao.fi.sh.utility.lexical.processors.base.AbstractLexicalProcessorsQueue;
import org.fao.fi.sh.utility.lexical.processors.impl.DiacriticsProcessor;
import org.fao.fi.sh.utility.lexical.processors.impl.MultipleSpacesReplacerProcessor;
import org.fao.fi.sh.utility.lexical.processors.impl.SymbolsAndDigitsRemoverProcessor;
import org.fao.fi.sh.utility.lexical.processors.impl.UppercaseProcessor;
import org.fao.fi.sh.utility.lexical.processors.queue.impl.LanguageTransliteratorProcessor;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 8 Oct 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 8 Oct 2013
 */
public class AuthoritiesSimplifierProcessorQueue extends AbstractLexicalProcessorsQueue implements AuthoritiesSimplifier {
	/* (non-Javadoc)
	 * @see org.fao.vrmf.core.tools.lexical.processors.AbstractLexicalProcessorsQueue#doGetActualProcessors()
	 */
	@Override
	protected Collection<LexicalProcessor> doGetActualProcessors() {
		return Arrays.asList(new LexicalProcessor[] {
			new DiacriticsProcessor(),
			new LanguageTransliteratorProcessor(),
			new SymbolsAndDigitsRemoverProcessor(SymbolsAndDigitsRemoverProcessor.STRATEGY_REPLACE_WITH_BLANKS),
			new MultipleSpacesReplacerProcessor(),
			new UppercaseProcessor()
		});
	}
}