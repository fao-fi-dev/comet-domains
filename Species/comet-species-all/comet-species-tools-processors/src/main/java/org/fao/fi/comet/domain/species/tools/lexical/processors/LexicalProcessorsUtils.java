/**
 * (c) 2013 FAO / UN (project: comet-species-tools-processors)
 */
package org.fao.fi.comet.domain.species.tools.lexical.processors;

import java.util.Iterator;
import java.util.ServiceLoader;

import org.fao.fi.sh.utility.lexical.processors.LexicalProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 08/dic/2013   Fabio     Creation.
 *
 * @version 1.0
 * @since 08/dic/2013
 */
public class LexicalProcessorsUtils {
	static final Logger LOG = LoggerFactory.getLogger(LexicalProcessorsUtils.class);

	final static public <L extends LexicalProcessor> L getProcessor(Class<L> processorType) {
		ServiceLoader<L> loader = ServiceLoader.load(processorType);
		Iterator<L> iterator = loader.iterator();

		if(iterator.hasNext()) {
			L processor = iterator.next();

			LOG.info("[ SYSTEM ] : Using {} as actual implementation of service {}", processor.getClass().getName(), processorType.getName());

			return processor;
		}

		throw new IllegalArgumentException("No service of type '" + processorType.getName() + "' can be loaded from the classpath");
	}
}
