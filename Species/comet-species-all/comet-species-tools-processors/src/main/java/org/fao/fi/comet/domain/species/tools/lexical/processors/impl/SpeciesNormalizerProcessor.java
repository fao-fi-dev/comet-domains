/**
 * (c) 2013 FAO / UN (project: comet-species)
 */
package org.fao.fi.comet.domain.species.tools.lexical.processors.impl;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.fao.fi.sh.utility.core.helpers.singletons.text.StringsHelper;
import org.fao.fi.sh.utility.lexical.processors.base.AbstractLexicalProcessor;

/**
 * Place your class / interface description here.
 *
 * @version 1.0
 * @since 8 Oct 2013
 */
public class SpeciesNormalizerProcessor extends AbstractLexicalProcessor {
	final static private Pattern IG_PATTERN = Pattern.compile("(.*)(IG)(ER|RA|ROS|RUM|RUS)$", Pattern.CASE_INSENSITIVE);
	final static private Pattern SUFFIX_2_PATTERN = Pattern.compile("(.*)(AE|"
																	   + "AK|"
																	   + "AM|"
																	   + "AR|"
																	   + "AS|"
																	   + "AX|"
																	   + "EA|"
																	   + "ES|"
																	   + "EX|"
																	   + "II|"
																	   + "IS|"
																	   + "IX|"
																	   + "NS|"
																	   + "OK|"
																	   + "ON|"
																	   + "OR|"
																	   + "OS|"
																	   + "OX|"
																	   + "UM|"
																	   + "US|"
//																	   + "UA|"
																	   + "YS|"
																	   + "YX)$", Pattern.CASE_INSENSITIVE);

	final static private Pattern SUFFIX_1_PATTERN = Pattern.compile("(.*)(A|E|I|O|U|Y)$", Pattern.CASE_INSENSITIVE);

	/* (non-Javadoc)
	 * @see org.fao.vrmf.core.tools.lexical.processors.AbstractLexicalProcessor#doProcess(java.lang.String)
	 */
	@Override
	protected String doProcess(String toProcess) {
		toProcess = StringsHelper.trim(toProcess);

		if(toProcess == null)
			return null;

		Matcher IG_MATCHER = IG_PATTERN.matcher(toProcess);

		if(IG_MATCHER.matches()) {
			return IG_MATCHER.group(1) + IG_MATCHER.group(2);
		}

		Matcher SUFFIX_2_MATCHER = SUFFIX_2_PATTERN.matcher(toProcess);

		if(SUFFIX_2_MATCHER.matches()) {
			return SUFFIX_2_MATCHER.group(1);
		}

		Matcher SUFFIX_1_MATCHER = SUFFIX_1_PATTERN.matcher(toProcess);

		if(SUFFIX_1_MATCHER.matches()) {
			return SUFFIX_1_MATCHER.group(1);
		}

		return toProcess;
	}
}