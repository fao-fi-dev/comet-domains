/**
 * (c) 2013 FAO / UN (project: comet-species-tools-processors)
 */
package org.fao.fi.comet.domain.species.tools.lexical.processors.queue;

import org.fao.fi.sh.utility.lexical.processors.LexicalProcessorsQueue;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 08/dic/2013   Fabio     Creation.
 *
 * @version 1.0
 * @since 08/dic/2013
 */
public interface AuthoritiesSimplifier extends LexicalProcessorsQueue {
}
