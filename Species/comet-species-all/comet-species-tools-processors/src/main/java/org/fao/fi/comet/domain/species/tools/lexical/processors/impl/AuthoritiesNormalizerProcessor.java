/**
 * (c) 2013 FAO / UN (project: comet-species)
 */
package org.fao.fi.comet.domain.species.tools.lexical.processors.impl;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.fao.fi.sh.utility.core.helpers.singletons.text.StringsHelper;
import org.fao.fi.sh.utility.lexical.processors.base.AbstractLexicalProcessor;

/**
 * Place your class / interface description here.
 * 
 * @version 1.0
 * @since 8 Oct 2013
 */
public class AuthoritiesNormalizerProcessor extends AbstractLexicalProcessor {
	
	/* (non-Javadoc)
	 * @see org.fao.vrmf.core.tools.lexical.processors.AbstractLexicalProcessor#doProcess(java.lang.String)
	 */
	@Override
	protected String doProcess(String toProcess) {
		toProcess = StringsHelper.trim(toProcess);
		
		if(toProcess == null)
			return null;

		toProcess = toProcess.replaceAll("(?i)\\s(&|in|and)\\s", " , ");
		toProcess = toProcess.replaceAll("(?i)\\sin\\:\\s?", " , ");
		
		toProcess = toProcess.replaceAll("(?i)[^\\p{L}0-9\\s,-]", "");

		toProcess = toProcess.replaceAll(" , ", ", ");

		toProcess = toProcess.replaceAll("(?i)\\s\\p{L}\\,", ",");

		Pattern dangling = Pattern.compile("(?i)(^|\\s)\\p{L}(\\s+|\\.\\s*)");
		
		Matcher matcher = dangling.matcher(toProcess);
		
		while(matcher.find()) {
			toProcess = toProcess.replaceAll(dangling.pattern(), "");
		
			matcher = dangling.matcher(toProcess);
		}
		
		toProcess = toProcess.replaceAll("(?i)\\s\\p{L}\\.?$", "");
				
		return StringsHelper.trim(toProcess);
	}
}