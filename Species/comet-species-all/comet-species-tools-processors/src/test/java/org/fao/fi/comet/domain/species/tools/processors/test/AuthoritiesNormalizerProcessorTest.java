/**
 * (c) 2013 FAO / UN (project: comet-species-model)
 */
package org.fao.fi.comet.domain.species.tools.processors.test;

import org.fao.fi.comet.domain.species.tools.lexical.processors.impl.AuthoritiesNormalizerProcessor;
import org.fao.fi.sh.utility.lexical.processors.LexicalProcessor;
import org.junit.Assert;
import org.junit.Test;


/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 8 Oct 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 8 Oct 2013
 */
public class AuthoritiesNormalizerProcessorTest {
	private final LexicalProcessor _processor = new AuthoritiesNormalizerProcessor();

	private final String[][] COMMON_TESTS = new String[][] {
		{ "Fleurety, Ragnarose, 1975", " ( Fleurety in Ragnarose, 1975)   " },
		{ "Fleurety, Ragnarose, 1975", " ( Fleurety in: Ragnarose, 1975)   " },
		{ "Beddome, 1867", "(Beddome, 1867)" },
		{ "Theobald, 1876", "Theobald, 1876" },
		{ "Rafinesque", "Rafinesque" },
		{ "Günther, 1862", "[ Günther, 1862 ]" },
		{ "Duméril, Bibron, 1854", "Duméril & Bibron, 1854" },
		{ "Roux-estève, 1980", "Roux-estève, 1980" },
		{ "Iorellato", "F. Iorellato" },
		{ "Iorellato", "F Iorellato" },
		{ "Iorellato", "Iorellato F." },
		{ "Iorellato", "Iorellato F" },
		{ "Iorellato, Paiano", "Iorellato F. and Paiano P." },
		{ "Iorellato, Paiano", "Iorellato F and Paiano P" },
		{ "Iorellato, Paiano", "Iorellato F. in: Paiano P." },
		{ "Iorellato, Paiano", "Iorellato F in: Paiano P" },
		{ "Wood-Mason, Wood-Mason, Alcock, 1891a", "Wood-Mason in Wood-Mason & Alcock, 1891a" },
		{ "Normal", "A. B. Normal" },
		{ "Normal", "A B.  C. Normal" },
		{ null, "[  )" },
	};

	@Test
	public void testCommon() {
		String processed;
		for(String[] test : COMMON_TESTS) {
			processed = this._processor.process(test[1]);
			Assert.assertEquals("Raw: " + test[1] + ", Expected: " + test[0] + ", Actual: " + processed, test[0], processed);
		}
	}
}