/**
 * (c) 2013 FAO / UN (project: comet-species-model)
 */
package org.fao.fi.comet.domain.species.tools.processors.test;

import org.fao.fi.comet.domain.species.tools.lexical.processors.impl.SpeciesNormalizerProcessor;
import org.fao.fi.sh.utility.lexical.processors.LexicalProcessor;
import org.junit.Assert;
import org.junit.Test;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 8 Oct 2013   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 8 Oct 2013
 */
public class SpeciesNormalizerProcessorTest {
	private final LexicalProcessor _processor = new SpeciesNormalizerProcessor();

	@Test
	public void testIGRemover() {
		Assert.assertEquals("NIG", this._processor.process("NIGER"));
		Assert.assertEquals("NIG", this._processor.process("NIGRA"));
		Assert.assertEquals("NIG", this._processor.process("NIGROS"));
		Assert.assertEquals("NIG", this._processor.process("NIGRUM"));
		Assert.assertEquals("NIG", this._processor.process("NIGRUS"));
		Assert.assertEquals("NIGRIG", this._processor.process("NIGRIG"));
	}

	@Test
	public void testSuffix2Remover() {
		Assert.assertEquals("NIGR", this._processor.process("NIGRAE"));
		Assert.assertEquals("BAR", this._processor.process("BARAK"));
		Assert.assertEquals("NIGR", this._processor.process("NIGRAM"));
		Assert.assertEquals("APG", this._processor.process("APGAR"));
		Assert.assertEquals("NIGR", this._processor.process("NIGRAS"));
		Assert.assertEquals("RAP", this._processor.process("RAPAX"));
		Assert.assertEquals("NIGR", this._processor.process("NIGREA"));
		Assert.assertEquals("NIGR", this._processor.process("NIGRES"));
		Assert.assertEquals("T-R", this._processor.process("T-REX"));
		Assert.assertEquals("NIGR", this._processor.process("NIGRII"));
		Assert.assertEquals("NIGR", this._processor.process("NIGRIS"));
		Assert.assertEquals("MATR", this._processor.process("MATRIX"));
		Assert.assertEquals("NIGRIGA", this._processor.process("NIGRIGANS"));
		Assert.assertEquals("RAGNAR", this._processor.process("RAGNAROK"));
		Assert.assertEquals("SAUR", this._processor.process("SAURON"));
		Assert.assertEquals("NIGR", this._processor.process("NIGROR"));
		Assert.assertEquals("NIG", this._processor.process("NIGROS"));
		Assert.assertEquals("MAAL", this._processor.process("MAALOX"));
		Assert.assertEquals("NIG", this._processor.process("NIGRUM"));
		Assert.assertEquals("NIG", this._processor.process("NIGRUS"));
		Assert.assertEquals("FLEUR DE L", this._processor.process("FLEUR DE LYS"));
		Assert.assertEquals("STR", this._processor.process("STRYX"));
	}

	@Test
	public void testSuffix1Remover() {
		Assert.assertEquals("NIGER", this._processor.process("NIGERA"));
		Assert.assertEquals("NIGR", this._processor.process("NIGRE"));
		Assert.assertEquals("NIGR", this._processor.process("NIGRI"));
		Assert.assertEquals("NIGR", this._processor.process("NIGRO"));
		Assert.assertEquals("NIGR", this._processor.process("NIGRU"));
		Assert.assertEquals("NIGR", this._processor.process("NIGRY"));
	}
}