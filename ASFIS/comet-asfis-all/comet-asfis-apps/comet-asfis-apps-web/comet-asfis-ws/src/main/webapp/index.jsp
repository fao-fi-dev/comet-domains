<!doctype html>
<html>
	<head>
		<title>COMET - ASFIS : search by scientific and vernacular names</title>
		<link rel="stylesheet" href="styles.css"></link>
	</head>
	<body class="scientific">
		<h1>COMET - ASFIS</h1>
		<h2>Search by:</h2>
		<h2><a href="./scientificName.html">scientific name</a></h2>
		<h2><a href="./vernacularName.html">vernacular name</a></h2>
	</body>
</html>