function getEndpoint() { 
	return "vn";
};

function getCustomParameters() {
	return {
		language: isSet($("#language").val()) ? $("#language").val() : null
	};
};

function customCompletion() {
	var selected = $("#language").val();
	
	if(selected)
		$("#results").addClass(selected);
	else
		$("#results").addClass("Unknown");
}

$(function() {
	$("#language").change(function() {
		$("#results").removeClass("Unknown English French Spanish");
	});
});
