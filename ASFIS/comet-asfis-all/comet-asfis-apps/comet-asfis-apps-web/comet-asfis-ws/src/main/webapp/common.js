var LAST_REQUEST_ID = -1;

$(function() {
	$("select").change(search);
	$("input").keyup(function() {
		var text = $(this).val();
		
		if(text != null && text.length > 2)
			search();
		
		return true;
	});
});

function getCommonParameters() { 
	LAST_REQUEST_ID = new Date().getTime();
	var params = {
		requestId: LAST_REQUEST_ID,
		term: $("#term").val(),
		maxCandidates: $("#maxCandidates").val(),
		minScore: $("#minScore").val()
	};
	
	return params;
};

function displayResults(data) {
	if(data.requestId == LAST_REQUEST_ID) {
		var results = $("#results");
		results.empty();

		var matchingResults = data.MatchingResult;
		
		if(data.MatchingResult == null || data.MatchingResult.length == 0) {
			results.append("<h3>No results found</h3>");
		} else {
			var matchings = matchingResults[0].Matching;
			
			results.append("<h3>Elapsed: " + data.elapsed + " mSec.</h3>");
			results.append("<h3>Search type and term: " + data.searchType + " ~= <i>" + matchingResults[0].searchTerm + "</i>" + ( isSet(matchingResults[0].parsedTerm) ? ( " [ Parsed: " + matchingResults[0].parsedTerm + " ]" ) : "" ) + "</h3>");
			results.append("<h3>Number of matchings: " + matchings.length + "</h3>");
			
			var meanScore = 0;
			
			for(var r = 0; r<matchings.length; r++) {
				meanScore += matchings[r].score;
			}
			
			meanScore = meanScore / ( matchings.length == 0 ? 1 : matchings.length );
			
			results.append("<h3>Matchings mean score: " + meanScore + "</h3>");
			results.append("<h3>Matchings:</h3>");
			
			var ul = $("<ul class='results'/>");
			for(var r = 0; r<matchings.length; r++) {
				ul.append(displayResult(matchings[r]));
			}
			
			results.append(ul);
		}
	} else {
		try {
			window.console.log("Skipping previous request with ID " + data.requestId + " (last request ID is " + LAST_REQUEST_ID + ")");
		} catch (E) {
			
		}
	}
};

function isSet(what) {
	if(typeof what === "undefined" || what == null || "" == what)
		return false;
	
	return true;
}

function sanitize(what) {
	if(!isSet(what))
		return "[ NOT SET ]";
	
	return what;
}

function displayResult(entry) {
	var li = $("<li class='resultEntry'/>");
	var ul = $("<ul/>");
	
	li.append(ul);
	
	var lili = $("<li/>");
	
	lili.append("<li><label>Score:</label>&nbsp;<code>" + sanitize(entry.score) + "</code></li>");
	lili.append("<li><label>FIGIS ID:</label>&nbsp;<code>" + sanitize(entry.figisId) + "</code>" + ( isSet(entry.figisId) ? ( "&nbsp;-&nbsp;<a target='_FS_" + entry.FigisID + "' href='http://www.fao.org/fishery/species/" + entry.figisId + "/en'>Factsheet</a>" ) : "" ) + "</li>");
	lili.append("<li><label>3-alpha-code:</label>&nbsp;<span class='a3c'>" + sanitize(entry.a3c) + "</span></li>");
	lili.append("<li><label>Scientific name:</label>&nbsp;<span class='scientificName'>" + sanitize(entry.ScientificName) + "</span></li>");
	lili.append("<li><label>Authority:</label>&nbsp;<span class='authority'>" + sanitize(entry.Authority) + "</span></li>");
	
	if(entry.VernacularName) {
		var vernacular = $("<li><label>Vernacular names:</label></li>");
		var names = $("<ul class='vernacularNames'/>");
		
		vernacular.append(names);
		
		var name;
		for(var v=0; v<entry.VernacularName.length; v++) {
			name = entry.VernacularName[v];
			
			names.append("<li><label>" + name.language + ":</label>&nbsp;<span class='vernacularName " + name.language + "'>" + name.name + "</span></li>");
		}
		
		lili.append(vernacular);
	}
	
	ul.append(lili);
	
	return li;
}

function completed() {
	$("#results").css("opacity", 1.0);
};

function search() {
	var params = getCommonParameters();
	
	params = $.extend({}, params, getCustomParameters());
	
	$.ajax({
		type: "POST",
		contentType: "application/x-www-form-urlencoded; charset=UTF-8",
		dataType: "json",
		data: params,
		url: "search/" + getEndpoint(),
		beforeSend: function() {
			$("#results").css("opacity", .5);
		},
		success: displayResults,
		complete: function() {
			completed();
			
			customCompletion();
		},
		error: function(jqXHR, textStatus, errorThrown) {
			alert("Error! " + testStatus + "\n\n" + errorThrown);
		}
	});
};