/**
 * (c) 2014 FAO / UN (project: comet-asfis-ws)
 */
package org.fao.fi.comet.asfis.support;

import java.io.IOException;

import org.fao.fi.comet.core.patterns.data.providers.DataProvider;
import org.fao.fi.comet.domain.species.model.ReferenceSpeciesData;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 17 Sep 2014   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 17 Sep 2014
 */
public interface ReferenceDataInitializer {
	void initializeData() throws IOException;
	
	DataProvider<ReferenceSpeciesData> getReferenceData() throws IOException;
}
