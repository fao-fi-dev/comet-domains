/**
 * (c) 2014 FAO / UN (project: comet-asfis-ws)
 */
package org.fao.fi.comet.asfis.support;

import java.net.URL;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import javax.inject.Named;
import javax.inject.Singleton;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import org.fao.vrmf.core.helpers.singletons.text.xml.ns.FINamespaceContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 19 Sep 2014   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 19 Sep 2014
 */
@Singleton @Named("figis.id.to.3ac.mapper")
public class FIGISIdTo3ACMapper {
	final static private Logger LOG = LoggerFactory.getLogger(FIGISIdTo3ACMapper.class);
	
	final static private String SPECIES_FACTSHEETS_WS_URL = "http://www.fao.org/figis/ws/factsheets/domain/species/";
	final static private String SPECIES_3A_WS_URL = "http://www.fao.org/figis/ws/factsheets/domain/species/factsheet/{figisID}/language/en/xpathdescendant/FAO3AlphaCode"; 
	
	//Moniker URLs for the SPECIES_3A_WS:
	//"http://figisapps.fao.org/figis/moniker/figisdoc/species/{figisID}"; 
	
	protected final DocumentBuilder _documentBuilder;
	protected final XPathFactory _xPathFactory;
	
	private Map<String, Integer> _map = new HashMap<String, Integer>();
	
	static final protected FINamespaceContext FI_CUSTOM_NS_CONTEXT = new FINamespaceContext();
	
	protected XPath getXPath() {
		XPath xp = this._xPathFactory.newXPath();
		xp.setNamespaceContext(FI_CUSTOM_NS_CONTEXT);

		return xp;
	}
	
	/**
	 * Class constructor
	 *
	 */
	public FIGISIdTo3ACMapper() throws Throwable {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		factory.setNamespaceAware(true);
		
		this._documentBuilder = factory.newDocumentBuilder();

		this._xPathFactory = XPathFactory.newInstance();
		
		this.initialize();
	}
	
	private void initialize() throws Throwable {
		String _3AC = null;
		
		for(Integer ID : this.getAllIDs()) {
			_3AC = this.get3ACByFigisID(ID);
			
			if(_3AC != null)
				this._map.put(_3AC, ID);
		}
	}
	
	public Integer getFigisIDBy3AC(String _3AC) {
		return this._map.get(_3AC);
	}
	
	private Collection<Integer> getAllIDs() throws Throwable {
		LOG.info("Retrieving all available species factsheets details from {}...", SPECIES_FACTSHEETS_WS_URL);
		
		Document allFS = this._documentBuilder.parse(new URL(SPECIES_FACTSHEETS_WS_URL).openStream());
		
		LOG.info("Done: all available species factsheets details have been retrieved from {}", SPECIES_FACTSHEETS_WS_URL);
		
		this.getXPath().setNamespaceContext(FI_CUSTOM_NS_CONTEXT);

		XPathExpression xe = this.getXPath().compile("//fiws:FactsheetDiscriminator");
		
		NodeList nodes = (NodeList)xe.evaluate(allFS, XPathConstants.NODESET);

		Collection<Integer> IDs = new HashSet<Integer>();
		
		LOG.info("Extracting species factsheets IDs...");
		
		String figisID = null;
		for(int n=0; n<nodes.getLength(); n++) {
			try {
				figisID = nodes.item(n).getAttributes().getNamedItem("factsheet").getNodeValue();
				
				if(figisID != null && !"".equals(figisID.trim()))
					IDs.add(Integer.parseInt(figisID));
			} catch(NumberFormatException NFe) {
				LOG.error("Unable to parse {} as FIGIS ID", figisID);
			}
		}
		
		LOG.info("Done: {} unique species factsheets IDs have been extracted", IDs.size());
		
		return IDs;
	}
	
	private String get3ACByFigisID(Integer figisId) throws Throwable {
		LOG.info("Retrieving 3AC by FIGIS ID {}...", figisId);

		String URL = SPECIES_3A_WS_URL.replace("{figisID}", String.valueOf(figisId));
		
		LOG.info("Retrieving species factsheet by FIGIS ID {} from {}...", figisId, URL);
		
		Document FS = this._documentBuilder.parse(new URL(URL).openStream());
		
		LOG.info("Done: species factsheet by FIGIS ID {} has been retrieved from {}", figisId, URL);

		XPathExpression xe = this.getXPath().compile("//fi:FAO3AlphaCode");
		
		NodeList nodes = (NodeList)xe.evaluate(FS, XPathConstants.NODESET);
		
		String text;
		for(int n=0; n<nodes.getLength(); n++) {
			text = nodes.item(n).getTextContent();
			
			text = text == null ? null : text.trim();
			
			if("".equals(text))
				text = null;
			
			if(text != null) {
				LOG.info("Returning {} as 3-alpha-code for FIGIS ID {}...", text, figisId);
				return text;
			}
		}
		
		LOG.warn("Unable to identify 3-alpha-code for FIGIS ID {} from factsheet {}...", figisId, URL);
				
		return null;
	}
}
