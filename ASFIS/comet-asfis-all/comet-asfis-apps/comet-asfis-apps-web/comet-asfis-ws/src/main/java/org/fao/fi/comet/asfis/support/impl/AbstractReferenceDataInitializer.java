/**
 * (c) 2014 FAO / UN (project: comet-asfis-ws)
 */
package org.fao.fi.comet.asfis.support.impl;

import java.io.IOException;

import org.fao.fi.comet.asfis.support.ReferenceDataInitializer;
import org.fao.fi.comet.core.patterns.data.providers.DataProvider;
import org.fao.fi.comet.core.patterns.data.providers.impl.basic.CollectionBackedDataProvider;
import org.fao.fi.comet.domain.species.model.ReferenceSpeciesData;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 17 Sep 2014   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 17 Sep 2014
 */
public abstract class AbstractReferenceDataInitializer implements ReferenceDataInitializer {
	protected CollectionBackedDataProvider<ReferenceSpeciesData> _materializedDataProvider;

	/**
	 * Class constructor
	 *
	 */
	public AbstractReferenceDataInitializer() throws IOException {
		this.initializeData();
	}
	
	abstract protected DataProvider<ReferenceSpeciesData> getActualReferenceDataProvider() throws IOException;
	
	/* (non-Javadoc)
	 * @see org.fao.fi.comet.asfis.support.ReferenceDataInitializer#initializeData()
	 */
	final public void initializeData() throws IOException {
		this._materializedDataProvider = new CollectionBackedDataProvider<ReferenceSpeciesData>("ASFIS_MATERIALIZED", this.getActualReferenceDataProvider());
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.asfis.support.ReferenceDataInitializer#getMaterializedReferenceData()
	 */
	final public DataProvider<ReferenceSpeciesData> getReferenceData() {
		return this._materializedDataProvider;
	}
}