/**
 * (c) 2014 FAO / UN (project: comet-asfis-ws)
 */
package org.fao.fi.comet.asfis.support.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import org.fao.fi.comet.core.model.engine.Matching;
import org.fao.fi.comet.core.model.engine.MatchingDetails;
import org.fao.fi.comet.domain.species.model.ReferenceSpeciesData;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 18 Sep 2014   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 18 Sep 2014
 */
@XmlRootElement(name="SearchResponse")
@XmlAccessorType(XmlAccessType.FIELD)
public class ScientificNameSearchResponse extends CommonSearchResponse<ScientificNameMatchingResults> {
	/**
	 * Class constructor
	 *
	 */
	public ScientificNameSearchResponse() {
		super();
	}

	
	/**
	 * Class constructor
	 *
	 * @param requestId
	 * @param searchType
	 * @param minScore
	 * @param maxCandidates
	 * @param searchTerm
	 * @param elapsed
	 * @param data
	 */
	public ScientificNameSearchResponse(String requestId, String searchType, double minScore, int maxCandidates, long elapsed, Collection<MatchingDetails<ReferenceSpeciesData, ReferenceSpeciesData>> data) {
		super(requestId, searchType, minScore, maxCandidates, elapsed, data);
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.comet.asfis.support.model.CommonSearchResponse#convert(org.fao.fi.comet.core.model.engine.MatchingDetails, java.util.Collection)
	 */
	@Override
	protected ScientificNameMatchingResults convert(MatchingDetails<ReferenceSpeciesData, ReferenceSpeciesData> source, Collection<Matching<ReferenceSpeciesData, ReferenceSpeciesData>> targets) {
		ScientificNameMatchingResults results = new ScientificNameMatchingResults();

		ReferenceSpeciesData sourceData = (ReferenceSpeciesData)source.getSource();
		
		results.setSearchTerm(sourceData.getId());
		results.setParsedTerm(sourceData.getScientificName() + ( sourceData.getAuthor() == null ? "" : " (" + sourceData.getAuthor() + ")"));
		
		List<MatchingReferenceSpeciesData> matchings = new ArrayList<MatchingReferenceSpeciesData>();
		
		for(Matching<ReferenceSpeciesData, ReferenceSpeciesData> in : targets)
			matchings.add(
				new MatchingReferenceSpeciesData(
					(ReferenceSpeciesData)in.getTarget(),
					null, //The FIGIS ID must be set later...
				    in.getScoreValue()
				)
			);
		
		results.setMatchings(matchings);
		
		return results;
	}
}
