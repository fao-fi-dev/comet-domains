/**
 * (c) 2014 FAO / UN (project: comet-asfis-ws)
 */
package org.fao.fi.comet.asfis.support.model;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlType;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 22 Sep 2014   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 22 Sep 2014
 */
@XmlType(name="MatchingResults")
@XmlAccessorType(XmlAccessType.FIELD)
public class MatchingResults {
	@XmlAttribute(name="searchTerm")
	private String _searchTerm;
	
	@XmlElementWrapper(name="Matchings")
	@XmlElement(name="Matching")
	private List<MatchingReferenceSpeciesData> _matchings;
	
	/**
	 * Class constructor
	 *
	 */
	public MatchingResults() {
		super();
	}

	/**
	 * Class constructor
	 *
	 * @param searchTerm
	 * @param matchings
	 */
	public MatchingResults(String searchTerm, List<MatchingReferenceSpeciesData> matchings) {
		super();
		this._searchTerm = searchTerm;
		this._matchings = matchings;
		
		this.sortMatchings();
	}
	
	private void sortMatchings() {
		if(this._matchings != null) {
			Collections.sort(this._matchings, new Comparator<MatchingReferenceSpeciesData>() {
				/* (non-Javadoc)
				 * @see java.util.Comparator#compare(java.lang.Object, java.lang.Object)
				 */
				@Override
				public int compare(MatchingReferenceSpeciesData o1, MatchingReferenceSpeciesData o2) {
					return o2.getScore().compareTo(o1.getScore());
				}
			});
		}
	}

	/**
	 * @return the 'searchTerm' value
	 */
	public final String getSearchTerm() {
		return this._searchTerm;
	}

	/**
	 * @param searchTerm the 'searchTerm' value to set
	 */
	public final void setSearchTerm(String searchTerm) {
		this._searchTerm = searchTerm;
	}

	/**
	 * @return the 'matchings' value
	 */
	public final List<MatchingReferenceSpeciesData> getMatchings() {
		return this._matchings;
	}

	/**
	 * @param matchings the 'matchings' value to set
	 */
	public final void setMatchings(List<MatchingReferenceSpeciesData> matchings) {
		this._matchings = matchings;
		
		this.sortMatchings();
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((this._matchings == null) ? 0 : this._matchings.hashCode());
		result = prime * result + ((this._searchTerm == null) ? 0 : this._searchTerm.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MatchingResults other = (MatchingResults) obj;
		if (this._matchings == null) {
			if (other._matchings != null)
				return false;
		} else if (!this._matchings.equals(other._matchings))
			return false;
		if (this._searchTerm == null) {
			if (other._searchTerm != null)
				return false;
		} else if (!this._searchTerm.equals(other._searchTerm))
			return false;
		return true;
	}
}