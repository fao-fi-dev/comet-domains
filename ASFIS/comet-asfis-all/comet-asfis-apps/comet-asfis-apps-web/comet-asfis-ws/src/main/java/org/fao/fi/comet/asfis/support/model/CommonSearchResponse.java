/**
 * (c) 2014 FAO / UN (project: comet-asfis-ws)
 */
package org.fao.fi.comet.asfis.support.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;

import org.fao.fi.comet.core.model.engine.Matching;
import org.fao.fi.comet.core.model.engine.MatchingDetails;
import org.fao.fi.comet.domain.species.model.ReferenceSpeciesData;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 18 Sep 2014   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 18 Sep 2014
 */
@XmlAccessorType(XmlAccessType.FIELD)
abstract public class CommonSearchResponse<R extends MatchingResults> {
	@XmlAttribute(name="requestId")
	private String _requestId;
	
	@XmlAttribute(name="elapsed")
	private long _elapsed;
	
	@XmlAttribute(name="minScore")
	private double _minScore;

	@XmlAttribute(name="maxCandidates")
	private int _maxCandidates;
	
	@XmlAttribute(name="searchType")
	private String _searchType;
	
	@XmlElementWrapper(name="MatchingResults")
	@XmlElement(name="MatchingResult")
	private List<R> _matchingResults;
	
	/**
	 * Class constructor
	 *
	 */
	public CommonSearchResponse() {
	}

	/**
	 * Class constructor
	 *
	 * @param data
	 */
	public CommonSearchResponse(String requestId, String searchType, double minScore, int maxCandidates, long elapsed, Collection<MatchingDetails<ReferenceSpeciesData, ReferenceSpeciesData>> data) {
		super();
		
		this._requestId = requestId;
		this._searchType = searchType;
		this._minScore = minScore;
		this._maxCandidates = maxCandidates;
		this._elapsed = elapsed;
		
		this._matchingResults = new ArrayList<R>();

		for(MatchingDetails<ReferenceSpeciesData, ReferenceSpeciesData> in : data) {
			this._matchingResults.add(this.convert(in, in.getMatchings()));
		}
	}
	
	abstract protected R convert(MatchingDetails<ReferenceSpeciesData, ReferenceSpeciesData> source, 
								 Collection<Matching<ReferenceSpeciesData, ReferenceSpeciesData>> targets);
	
	/**
	 * @return the 'requestId' value
	 */
	public final String getRequestId() {
		return this._requestId;
	}

	/**
	 * @param requestId the 'requestId' value to set
	 */
	public final void setRequestId(String requestId) {
		this._requestId = requestId;
	}

	/**
	 * @return the 'data' value
	 */
	public final Collection<R> getData() {
		return this._matchingResults;
	}

	/**
	 * @param data the 'data' value to set
	 */
	public final void setData(List<R> data) {
		this._matchingResults = data;
	}

	/**
	 * @return the 'searchType' value
	 */
	public final String getSearchType() {
		return this._searchType;
	}

	/**
	 * @param searchType the 'searchType' value to set
	 */
	public final void setSearchType(String searchType) {
		this._searchType = searchType;
	}

	/**
	 * @return the 'elapsed' value
	 */
	public final long getElapsed() {
		return this._elapsed;
	}

	/**
	 * @param elapsed the 'elapsed' value to set
	 */
	public final void setElapsed(long elapsed) {
		this._elapsed = elapsed;
	}

	/**
	 * @return the 'minScore' value
	 */
	public final double getMinScore() {
		return this._minScore;
	}

	/**
	 * @param minScore the 'minScore' value to set
	 */
	public final void setMinScore(double minScore) {
		this._minScore = minScore;
	}

	/**
	 * @return the 'maxCandidates' value
	 */
	public final int getMaxCandidates() {
		return this._maxCandidates;
	}

	/**
	 * @param maxCandidates the 'maxCandidates' value to set
	 */
	public final void setMaxCandidates(int maxCandidates) {
		this._maxCandidates = maxCandidates;
	}

	/**
	 * @return the 'matchingResults' value
	 */
	public final List<R> getMatchingResults() {
		return this._matchingResults;
	}

	/**
	 * @param matchingResults the 'matchingResults' value to set
	 */
	public final void setMatchingResults(List<R> matchingResults) {
		this._matchingResults = matchingResults;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (this._elapsed ^ (this._elapsed >>> 32));
		result = prime * result + ((this._matchingResults == null) ? 0 : this._matchingResults.hashCode());
		result = prime * result + this._maxCandidates;
		long temp;
		temp = Double.doubleToLongBits(this._minScore);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((this._requestId == null) ? 0 : this._requestId.hashCode());
		result = prime * result + ((this._searchType == null) ? 0 : this._searchType.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		@SuppressWarnings("rawtypes")
		CommonSearchResponse other = (CommonSearchResponse) obj;
		if (this._elapsed != other._elapsed)
			return false;
		if (this._matchingResults == null) {
			if (other._matchingResults != null)
				return false;
		} else if (!this._matchingResults.equals(other._matchingResults))
			return false;
		if (this._maxCandidates != other._maxCandidates)
			return false;
		if (Double.doubleToLongBits(this._minScore) != Double.doubleToLongBits(other._minScore))
			return false;
		if (this._requestId == null) {
			if (other._requestId != null)
				return false;
		} else if (!this._requestId.equals(other._requestId))
			return false;
		if (this._searchType == null) {
			if (other._searchType != null)
				return false;
		} else if (!this._searchType.equals(other._searchType))
			return false;
		return true;
	}
}
