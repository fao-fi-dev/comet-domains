/**
 * (c) 2014 FAO / UN (project: comet-asfis-ws)
 */
package org.fao.fi.comet.asfis.support.impl;

import java.util.ArrayList;
import java.util.Collection;

import org.fao.fi.comet.asfis.support.AbstractASFISMatchingEngine;
import org.fao.fi.comet.core.model.matchlets.Matchlet;
import org.fao.fi.comet.core.model.matchlets.MatchletConfiguration;
import org.fao.fi.comet.core.patterns.data.partitioners.DataPartitioner;
import org.fao.fi.comet.domain.species.model.ReferenceSpeciesData;
import org.fao.fi.comet.domain.species.patterns.data.partitioners.LengthBasedSpeciesDataPartitioner;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 18 Sep 2014   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 18 Sep 2014
 */
public class ScientificNameASFISMatchingEngine extends AbstractASFISMatchingEngine {
	/**
	 * Class constructor
	 *
	 * @param minScore
	 * @param maxCandidates
	 */
	public ScientificNameASFISMatchingEngine(Double minScore, Integer maxCandidates) {
		super(minScore, maxCandidates);
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.asfis.support.AbstractASFISMatchingEngine#getPartitioner()
	 */
	@Override
	protected DataPartitioner<ReferenceSpeciesData, ReferenceSpeciesData> getPartitioner() {
		return new LengthBasedSpeciesDataPartitioner(5);
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.comet.asfis.support.AbstractASFISMatchingEngine#getConfiguration()
	 */
	@Override
	protected Collection<MatchletConfiguration> getConfiguration() {
		Collection<MatchletConfiguration> matchlets = new ArrayList<MatchletConfiguration>();

		matchlets.add(new MatchletConfiguration("ScientificNameMatchlet").
						  with("levenshteinWeight", "100").
						  with("soundexWeight", "0").
						  with("trigramWeight", "25").
						  with(Matchlet.WEIGHT_PARAM, "60").
						  with(Matchlet.MIN_SCORE_PARAM, "0"));
		
		matchlets.add(new MatchletConfiguration("AuthorityNameMatchlet").
						  with("levenshteinWeight", "100").
						  with("soundexWeight", "0").
						  with("trigramWeight", "25").
						  with(Matchlet.WEIGHT_PARAM, "30").
						  with(Matchlet.MIN_SCORE_PARAM, "0").
						  withTrue("isOptional"));

		matchlets.add(new MatchletConfiguration("AuthorityYearMatchlet").
						  with(Matchlet.WEIGHT_PARAM, "15").
						  with(Matchlet.MIN_SCORE_PARAM, "0").
						  withTrue("isOptional"));
		
		return matchlets;
	}
}
