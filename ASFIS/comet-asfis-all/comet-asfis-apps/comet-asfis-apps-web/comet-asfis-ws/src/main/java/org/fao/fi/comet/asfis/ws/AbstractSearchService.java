/**
 * (c) 2014 FAO / UN (project: comet-asfis-ws)
 */
package org.fao.fi.comet.asfis.ws;

import java.io.IOException;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Singleton;

import org.fao.fi.comet.asfis.support.FIGISIdTo3ACMapper;
import org.fao.fi.comet.asfis.support.SourceDataInitializer;
import org.fao.fi.comet.asfis.support.impl.ClasspathReferenceDataInitializer;
import org.fao.fi.comet.asfis.support.model.CommonSearchResponse;
import org.fao.fi.comet.asfis.support.model.MatchingReferenceSpeciesData;
import org.fao.fi.comet.asfis.support.model.MatchingResults;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 19 Sep 2014   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 19 Sep 2014
 */
abstract public class AbstractSearchService<M extends MatchingResults> {
	final protected Logger LOG = LoggerFactory.getLogger(this.getClass());
	
	protected ClasspathReferenceDataInitializer _referenceData;
	protected SourceDataInitializer _sourceData;
	
	@PostConstruct
	protected void initialize() throws IOException {
		this._referenceData = new ClasspathReferenceDataInitializer();
		this._sourceData = new SourceDataInitializer();
	}
	
	@Inject protected @Singleton FIGISIdTo3ACMapper _mapper;
	
	protected <R extends CommonSearchResponse<M>> R updateResponse(R response) {
		for(MatchingResults entry : response.getMatchingResults())
			for(MatchingReferenceSpeciesData in : entry.getMatchings()) {
				if(in.get3ac() != null)
					in.setFigisID(this._mapper.getFigisIDBy3AC(in.get3ac()));
			}
		
		return response;
	}
}
