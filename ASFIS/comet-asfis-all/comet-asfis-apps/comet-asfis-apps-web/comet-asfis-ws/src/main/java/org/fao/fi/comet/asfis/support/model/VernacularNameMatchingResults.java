/**
 * (c) 2014 FAO / UN (project: comet-asfis-ws)
 */
package org.fao.fi.comet.asfis.support.model;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 22 Sep 2014   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 22 Sep 2014
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class VernacularNameMatchingResults extends MatchingResults {
	/**
	 * Class constructor
	 *
	 */
	public VernacularNameMatchingResults() {
		super();
	}

	/**
	 * Class constructor
	 *
	 * @param searchTerm
	 * @param matchings
	 */
	public VernacularNameMatchingResults(String searchTerm, List<MatchingReferenceSpeciesData> matchings) {
		super(searchTerm, matchings);
	}

}