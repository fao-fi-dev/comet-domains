@XmlSchema(namespace = "http://www.fao.org/fi/comet/asfis", elementFormDefault = XmlNsForm.QUALIFIED)
package org.fao.fi.comet.asfis.support.model;
import javax.xml.bind.annotation.XmlNsForm;
import javax.xml.bind.annotation.XmlSchema;
