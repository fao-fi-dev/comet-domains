/**
 * (c) 2014 FAO / UN (project: comet-asfis-ws)
 */
package org.fao.fi.comet.asfis.support.impl;

import java.io.IOException;

import javax.enterprise.inject.Alternative;
import javax.inject.Singleton;

import org.fao.fi.comet.core.patterns.data.providers.DataProvider;
import org.fao.fi.comet.core.patterns.data.providers.impl.basic.CollectionBackedDataProvider;
import org.fao.fi.comet.domain.species.model.ReferenceSpeciesData;
import org.fao.fi.comet.domain.species.tools.io.providers.streaming.taf.protocols.classpath.ClasspathTAFStreamingSpeciesReferenceDataProvider;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 17 Sep 2014   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 17 Sep 2014
 */
@Alternative @Singleton
public class ClasspathReferenceDataInitializer extends AbstractReferenceDataInitializer {
	static final private String ASFIS_CLASSPATH_PREFIX = "org/fao/fi/comet/asfis/taf";
	
	/**
	 * Class constructor
	 *
	 */
	public ClasspathReferenceDataInitializer() throws IOException {
		super();
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.comet.asfis.support.impl.AbstractReferenceDataInitializer#getActualReferenceDataProvider()
	 */
	final public DataProvider<ReferenceSpeciesData> getActualReferenceDataProvider() throws IOException {
		ClasspathTAFStreamingSpeciesReferenceDataProvider provider = new ClasspathTAFStreamingSpeciesReferenceDataProvider();
		provider.setProviderID("ASFIS");
		provider.setURIs("classpath:///" + ASFIS_CLASSPATH_PREFIX + "/ASFIS_taxa.taf.gz",
				 		 "classpath:///" + ASFIS_CLASSPATH_PREFIX + "/ASFIS_vernacular.taf.gz");
		
		return new CollectionBackedDataProvider<ReferenceSpeciesData>("ASFIS", provider);
	}
}