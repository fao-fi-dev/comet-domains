/**
 * (c) 2014 FAO / UN (project: comet-asfis-ws)
 */
package org.fao.fi.comet.asfis.support.impl;

import java.util.ArrayList;
import java.util.Collection;

import org.fao.fi.comet.asfis.support.AbstractASFISMatchingEngine;
import org.fao.fi.comet.core.model.matchlets.Matchlet;
import org.fao.fi.comet.core.model.matchlets.MatchletConfiguration;
import org.fao.fi.comet.core.patterns.data.partitioners.DataPartitioner;
import org.fao.fi.comet.core.patterns.data.partitioners.impl.IdentityDataPartitioner;
import org.fao.fi.comet.domain.species.matchlets.extended.VernacularCNameMatchlet;
import org.fao.fi.comet.domain.species.model.ReferenceSpeciesData;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 18 Sep 2014   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 18 Sep 2014
 */
public class VernacularNameASFISMatchingEngine extends AbstractASFISMatchingEngine {
	private String _language;
	
	/**
	 * Class constructor
	 *
	 * @param minScore
	 * @param maxCandidates
	 * @param language
	 */
	public VernacularNameASFISMatchingEngine(Double minScore, Integer maxCandidates, String language) {
		super(minScore, maxCandidates);
		
		this._language = language;
	}

	/* (non-Javadoc)
	 * @see org.fao.fi.comet.asfis.support.AbstractASFISMatchingEngine#getPartitioner()
	 */
	@Override
	protected DataPartitioner<ReferenceSpeciesData, ReferenceSpeciesData> getPartitioner() {
		return new IdentityDataPartitioner<ReferenceSpeciesData, ReferenceSpeciesData>();
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.comet.asfis.support.AbstractASFISMatchingEngine#getConfiguration()
	 */
	@Override
	protected Collection<MatchletConfiguration> getConfiguration() {
		Collection<MatchletConfiguration> matchlets = new ArrayList<MatchletConfiguration>();
		
		MatchletConfiguration config = 
			new MatchletConfiguration("VernacularNameMatchlet").
			    with("levenshteinWeight", "100").
			    with("soundexWeight", "0").
			    with("trigramWeight", "0").
//			    with(Matchlet.WEIGHT_PARAM, "100").
			    with(Matchlet.MIN_SCORE_PARAM, "0.2");
		
		if(this._language == null || "".equals(this._language.trim()))
			config.withTrue(VernacularCNameMatchlet.INCLUDE_UNKNOWN_LANGUAGE_PARAM);
		else
			config.with(VernacularCNameMatchlet.SUGGESTED_LANGS_PARAM, this._language);
		
		matchlets.add(config);
		
		return matchlets;
	}
}
