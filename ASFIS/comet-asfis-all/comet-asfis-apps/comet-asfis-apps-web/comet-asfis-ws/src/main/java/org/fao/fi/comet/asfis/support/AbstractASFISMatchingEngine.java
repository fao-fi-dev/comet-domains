/**
 * (c) 2014 FAO / UN (project: comet-asfis-ws)
 */
package org.fao.fi.comet.asfis.support;

import java.util.Collection;

import org.fao.fi.comet.core.engine.process.handlers.impl.SilentMatchingProcessHandler;
import org.fao.fi.comet.core.model.engine.MatchingEngineProcessConfiguration;
import org.fao.fi.comet.core.model.engine.MatchingEngineProcessResult;
import org.fao.fi.comet.core.model.matchlets.MatchletConfiguration;
import org.fao.fi.comet.core.patterns.data.partitioners.DataPartitioner;
import org.fao.fi.comet.core.patterns.data.providers.DataProvider;
import org.fao.fi.comet.domain.species.engine.SpeciesMatchingEngine;
import org.fao.fi.comet.domain.species.model.ReferenceSpeciesData;
import org.fao.fi.comet.domain.species.patterns.handlers.id.ReferenceSpeciesDataIDHandler;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 17 Sep 2014   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 17 Sep 2014
 */
abstract public class AbstractASFISMatchingEngine {
	private Double _minScore;
	private Integer _maxCandidates;
	
	private SpeciesMatchingEngine _engine;
	
	/**
	 * Class constructor
	 *
	 * @param minScore
	 * @param maxCandidates
	 */
	public AbstractASFISMatchingEngine(Double minScore, Integer maxCandidates) {
		super();
		
		this._minScore = minScore;
		this._maxCandidates = maxCandidates;
	}

	abstract protected Collection<MatchletConfiguration> getConfiguration();
	abstract protected DataPartitioner<ReferenceSpeciesData, ReferenceSpeciesData> getPartitioner();
	
	public MatchingEngineProcessResult<ReferenceSpeciesData, ReferenceSpeciesData, MatchingEngineProcessConfiguration> match(DataProvider<ReferenceSpeciesData> sources, DataProvider<ReferenceSpeciesData> targets) throws Exception {
		MatchingEngineProcessConfiguration configuration = new MatchingEngineProcessConfiguration();
		
		configuration.setMaxCandidatesPerEntry(this._maxCandidates == null || this._maxCandidates == 0 ? 10 : this._maxCandidates);
		configuration.setMinimumAllowedWeightedScore(this._minScore == null || Double.compare(this._minScore, 0D) <= 0 || Double.compare(this._minScore, 1D) > 0 ? .5 : this._minScore);
		
		configuration.setHandleErrors(true);
			
		configuration.setMatchletsConfiguration(this.getConfiguration());
		
		this._engine = new SpeciesMatchingEngine();
		
		return
			this._engine.compareAll(configuration, 
									new SilentMatchingProcessHandler<ReferenceSpeciesData>(), 
									sources, 
									this.getPartitioner(),
									targets,
									new ReferenceSpeciesDataIDHandler(),
									new ReferenceSpeciesDataIDHandler());
	}
}