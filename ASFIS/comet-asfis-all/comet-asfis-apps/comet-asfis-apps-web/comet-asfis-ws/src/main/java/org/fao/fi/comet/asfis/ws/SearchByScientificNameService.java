/**
 * (c) 2014 FAO / UN (project: comet-asfis-ws)
 */
package org.fao.fi.comet.asfis.ws;

import java.util.Collection;
import java.util.List;

import javax.inject.Named;
import javax.inject.Singleton;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.fao.fi.comet.asfis.support.AbstractASFISMatchingEngine;
import org.fao.fi.comet.asfis.support.impl.ScientificNameASFISMatchingEngine;
import org.fao.fi.comet.asfis.support.model.ScientificNameMatchingResults;
import org.fao.fi.comet.asfis.support.model.ScientificNameSearchResponse;
import org.fao.fi.comet.core.model.engine.MatchingDetails;
import org.fao.fi.comet.core.model.engine.MatchingEngineProcessConfiguration;
import org.fao.fi.comet.core.model.engine.MatchingEngineProcessResult;
import org.fao.fi.comet.core.patterns.data.providers.DataProvider;
import org.fao.fi.comet.domain.species.model.ReferenceSpeciesData;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 18 Sep 2014   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 18 Sep 2014
 */
@Singleton @Named("asfis.ws.search.scientific.name")
@Path("/sn")
public class SearchByScientificNameService extends AbstractSearchService<ScientificNameMatchingResults> {
	protected MatchingEngineProcessResult<ReferenceSpeciesData, ReferenceSpeciesData, MatchingEngineProcessConfiguration> match(Double minScore, Integer maxCandidates, List<String> input) throws Exception {
		DataProvider<ReferenceSpeciesData> sources = this._sourceData.convert(input.toArray(new String[input.size()]));
		DataProvider<ReferenceSpeciesData> targets = this._referenceData.getReferenceData();
		
		AbstractASFISMatchingEngine engine = new ScientificNameASFISMatchingEngine(minScore, maxCandidates);
		
		return engine.match(sources, targets);
	}
	
	@POST
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public ScientificNameSearchResponse searchPOST(@FormParam("requestId") String requestId, @FormParam("minScore") Double minScore, @FormParam("maxCandidates") Integer maxCandidates, @FormParam("term") List<String> term) throws Exception {
		return this.doSearch(requestId, minScore, maxCandidates, term);
	}
	
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("json")
	public ScientificNameSearchResponse searchJSON(@QueryParam("requestId") String requestId, @QueryParam("minScore") Double minScore, @QueryParam("maxCandidates") Integer maxCandidates, @QueryParam("term") List<String> term) throws Exception {
		return this.doSearch(requestId, minScore, maxCandidates, term);
	}
	
	@GET
	@Produces({ MediaType.APPLICATION_XML })
	@Path("xml")
	public ScientificNameSearchResponse searchXML(@QueryParam("requestId") String requestId, @QueryParam("minScore") Double minScore, @QueryParam("maxCandidates") Integer maxCandidates, @QueryParam("term") List<String> term) throws Exception {
		return this.doSearch(requestId, minScore, maxCandidates, term);
	}
	
	protected ScientificNameSearchResponse doSearch(String requestId, Double minScore, Integer maxCandidates, List<String> term) throws Exception {
		long elapsed, start = System.currentTimeMillis();

		if(requestId == null || "".equals(requestId.trim()))
			requestId = String.valueOf(start);
		
		LOG.info("Searching for species with scientific name ~= {} [ parameters: requestId={}, minScore={}, maxCandidates={} ]", term, requestId, minScore, maxCandidates);
		
		if(maxCandidates == null || maxCandidates <= 0)
			maxCandidates = 10;
		
		if(minScore == null || 
		   Double.compare(minScore, 0D) <= 0 ||
		   Double.compare(minScore, 1D) > 0)
			minScore = .5;
		
		Collection<MatchingDetails<ReferenceSpeciesData, ReferenceSpeciesData>> matchings = this.match(minScore, maxCandidates, term).getResults().getMatchingDetails();
			
		elapsed = System.currentTimeMillis() - start;
		
		LOG.info("Search request for species with scientific name ~= {} [ parameters: requestId={}, minScore={}, maxCandidates={} ] yield {} results in {} mSec.", term, requestId, minScore, maxCandidates, matchings.size(), elapsed);
		
		return this.updateResponse(new ScientificNameSearchResponse(requestId, "ScientificName", minScore, maxCandidates, elapsed, matchings));
	}
}
