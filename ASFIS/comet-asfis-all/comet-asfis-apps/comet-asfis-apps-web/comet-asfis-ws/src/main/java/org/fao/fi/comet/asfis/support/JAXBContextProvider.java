package org.fao.fi.comet.asfis.support;

import javax.ws.rs.ext.ContextResolver;
import javax.ws.rs.ext.Provider;
import javax.xml.bind.JAXBContext;

import org.fao.fi.comet.asfis.support.model.CommonSearchResponse;
import org.fao.fi.comet.asfis.support.model.MatchingResults;
import org.fao.fi.comet.asfis.support.model.ScientificNameMatchingResults;
import org.fao.fi.comet.asfis.support.model.ScientificNameSearchResponse;
import org.fao.fi.comet.asfis.support.model.VernacularNameMatchingResults;
import org.fao.fi.comet.asfis.support.model.VernacularNameSearchResponse;
import org.fao.fi.comet.core.model.common.TypedComplexName;
import org.fao.fi.comet.core.model.engine.MatchingEngineProcessResult;
import org.fao.fi.comet.domain.species.model.InputSpeciesData;
import org.fao.fi.comet.domain.species.model.ReferenceSpeciesData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 22/nov/2013   Fabio     Creation.
 *
 * @version 1.0
 * @since 22/nov/2013
 */
@Provider
public class JAXBContextProvider implements ContextResolver<JAXBContext> {
	static final private Logger LOG = LoggerFactory.getLogger(JAXBContextProvider.class);
	
	private JAXBContext _context;

	public JAXBContextProvider() {
		super();
		
		LOG.info("Initializing {}", this.getClass().getName());
	}
	
	/* (non-Javadoc)
	 * @see javax.ws.rs.ext.ContextResolver#getContext(java.lang.Class)
	 */
	@Override
	public JAXBContext getContext(Class<?> type) {
		if(this._context == null) {
			try {
				this._context = JAXBContext.newInstance(new Class[] {
					InputSpeciesData.class,
					ReferenceSpeciesData.class,
					TypedComplexName.class,
					MatchingEngineProcessResult.class,
					CommonSearchResponse.class,
					MatchingResults.class,
					VernacularNameMatchingResults.class,
					ScientificNameMatchingResults.class,
					VernacularNameSearchResponse.class,
					ScientificNameSearchResponse.class
				});
			} catch (Throwable t) {
				LOG.error("Unable to get JAXB context for {}: {} [ {} ]", new Object[] { type, t.getClass().getSimpleName(), t.getMessage() }, t);
			}
		}
		
		return this._context;
	}
}