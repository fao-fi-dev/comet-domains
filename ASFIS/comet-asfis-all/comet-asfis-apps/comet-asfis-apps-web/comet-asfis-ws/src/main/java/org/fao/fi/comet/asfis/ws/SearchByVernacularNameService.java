/**
 * (c) 2014 FAO / UN (project: comet-asfis-ws)
 */
package org.fao.fi.comet.asfis.ws;

import java.util.Collection;
import java.util.List;

import javax.inject.Named;
import javax.inject.Singleton;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.fao.fi.comet.asfis.support.AbstractASFISMatchingEngine;
import org.fao.fi.comet.asfis.support.impl.VernacularNameASFISMatchingEngine;
import org.fao.fi.comet.asfis.support.model.VernacularNameMatchingResults;
import org.fao.fi.comet.asfis.support.model.VernacularNameSearchResponse;
import org.fao.fi.comet.core.model.engine.MatchingDetails;
import org.fao.fi.comet.core.model.engine.MatchingEngineProcessConfiguration;
import org.fao.fi.comet.core.model.engine.MatchingEngineProcessResult;
import org.fao.fi.comet.core.patterns.data.providers.DataProvider;
import org.fao.fi.comet.domain.species.model.ReferenceSpeciesData;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 18 Sep 2014   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 18 Sep 2014
 */
@Singleton @Named("asfis.ws.search.vernacular.name")
@Path("/vn")
public class SearchByVernacularNameService extends AbstractSearchService<VernacularNameMatchingResults> {
	protected MatchingEngineProcessResult<ReferenceSpeciesData, ReferenceSpeciesData, MatchingEngineProcessConfiguration> match(Double minScore, Integer maxCandidates, String language, List<String> input) throws Exception {
		DataProvider<ReferenceSpeciesData> sources = this._sourceData.convert(input.toArray(new String[input.size()]));
		DataProvider<ReferenceSpeciesData> targets = this._referenceData.getReferenceData();
		
		AbstractASFISMatchingEngine engine = new VernacularNameASFISMatchingEngine(minScore, maxCandidates, language);
		
		return engine.match(sources, targets);
	}
	@POST
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public VernacularNameSearchResponse searchPOST(@FormParam("requestId") String requestId, @FormParam("minScore") Double minScore, @FormParam("maxCandidates") Integer maxCandidates, @FormParam("language") String language, @FormParam("term") List<String> term) throws Exception {
		return this.doSearch(requestId, minScore, maxCandidates, language, term);
	}
	
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("json")
	public VernacularNameSearchResponse searchJSON(@QueryParam("requestId") String requestId, @QueryParam("minScore") Double minScore, @QueryParam("maxCandidates") Integer maxCandidates, @QueryParam("language") String language, @QueryParam("term") List<String> term) throws Exception {
		return this.doSearch(requestId, minScore, maxCandidates, language, term);
	}
	
	@GET
	@Produces({ MediaType.APPLICATION_XML })
	@Path("xml")
	public VernacularNameSearchResponse searchXML(@QueryParam("requestId") String requestId, @QueryParam("minScore") Double minScore, @QueryParam("maxCandidates") Integer maxCandidates, @QueryParam("language") String language, @QueryParam("term") List<String> term) throws Exception {
		return this.doSearch(requestId, minScore, maxCandidates, language, term);
	}
	
	protected VernacularNameSearchResponse doSearch(String requestId, Double minScore, Integer maxCandidates, String language, List<String> term) throws Exception {
		long elapsed, start = System.currentTimeMillis();
		
		if(requestId == null || "".equals(requestId.trim()))
			requestId = String.valueOf(start);

		if(maxCandidates == null || maxCandidates <= 0)
			maxCandidates = 10;
		
		if(minScore == null || 
		   Double.compare(minScore, 0D) <= 0 ||
		   Double.compare(minScore, 1D) > 0)
			minScore = .5;
		
		LOG.info("Searching for species with vernacular name ~= {} [ parameters: requestId={}, minScore={}, maxCandidates={}, language={} ]", term, requestId, minScore, maxCandidates, language);

		Collection<MatchingDetails<ReferenceSpeciesData, ReferenceSpeciesData>> matchings = this.match(minScore, maxCandidates, language, term).getResults().getMatchingDetails();
			
		elapsed = System.currentTimeMillis() - start;
		
		LOG.info("Search request for species with vernacular name ~= {} [ parameters: requestId={}, minScore={}, maxCandidates={}, language={} ] yield {} results in {} mSec.", term, requestId, minScore, maxCandidates, language, matchings.size(), elapsed);
			
		return this.updateResponse(new VernacularNameSearchResponse(requestId, "VernacularName", minScore, maxCandidates, language, elapsed, matchings));
	}
}
