/**
 * (c) 2014 FAO / UN (project: comet-asfis-ws)
 */
package org.fao.fi.comet.asfis.support.model;

import java.util.ArrayList;
import java.util.Collection;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlType;

import org.fao.fi.comet.domain.species.model.ReferenceSpeciesData;
import org.fao.fi.comet.domain.species.model.VernacularNameData;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 18 Sep 2014   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 18 Sep 2014
 */
@XmlType(name="MatchingReferenceSpeciesData")
@XmlAccessorType(XmlAccessType.FIELD)
public class MatchingReferenceSpeciesData {
	@XmlAttribute(name="figisId")
	private Integer _figisID;
	
	@XmlAttribute(name="a3c")
	private String _3ac;
	
	@XmlElement(name="ScientificName")
	private String _scientificName;
	
	@XmlElement(name="Kingdom")
	private String _kingdom;
	
	@XmlElement(name="Phylum")
	private String _phylum;
	
	@XmlElement(name="Class")
	private String _class;
	
	@XmlElement(name="Order")
	private String _order;
	
	@XmlElement(name="Family")
	private String _family;	
	
	@XmlElement(name="Genus")
	private String _genus;
	
	@XmlElement(name="Species")
	private String _species;
		
	@XmlElement(name="Authority")
	private String _authority;
	
	@XmlElementWrapper(name="VernacularNames")
	@XmlElement(name="VernacularName")
	private Collection<VernacularNameData> _vernacularNames;
		
	@XmlAttribute(name="score")
	private Double _score;
	
	/**
	 * Class constructor
	 *
	 */
	public MatchingReferenceSpeciesData() {
		super();
	}

	/**
	 * Class constructor
	 *
	 * @param data
	 * @param figisID
	 * @param score
	 */
	public MatchingReferenceSpeciesData(ReferenceSpeciesData data, Integer figisID, Double score) {
		super();
		
		this._figisID = figisID;
		this._score = score;
		
		this._3ac = data.getId().substring(4);
		this._kingdom = data.getKingdom();
		this._phylum = data.getPhylum();
		this._class = data.getKlass();
		this._order = data.getOrder();
		this._family = data.getFamily();
		this._genus = data.getGenus();
		this._species = data.getSpecies();
		this._scientificName = data.getScientificName();
		this._authority = data.getAuthor();
		
		this._vernacularNames = new ArrayList<VernacularNameData>();
		
		if(data.getVernacularNames() != null) {
			for(VernacularNameData in : data.getVernacularNames())
				this._vernacularNames.add(
					new VernacularNameData(
						null,
						in.getLanguage().substring(11),
						null,
						in.getName()
					)
				);
		}
	}

	/**
	 * @return the '3ac' value
	 */
	public final String get3ac() {
		return this._3ac;
	}

	/**
	 * @param name the '3ac' value to set
	 */
	public final void set3ac(String name) {
		this._3ac = name;
	}

	/**
	 * @return the 'figisID' value
	 */
	public final Integer getFigisID() {
		return this._figisID;
	}

	/**
	 * @param figisID the 'figisID' value to set
	 */
	public final void setFigisID(Integer figisID) {
		this._figisID = figisID;
	}

	/**
	 * @return the 'scientificName' value
	 */
	public final String getScientificName() {
		return this._scientificName;
	}

	/**
	 * @param scientificName the 'scientificName' value to set
	 */
	public final void setScientificName(String scientificName) {
		this._scientificName = scientificName;
	}

	/**
	 * @return the 'score' value
	 */
	public final Double getScore() {
		return this._score;
	}

	/**
	 * @param score the 'score' value to set
	 */
	public final void setScore(Double score) {
		this._score = score;
	}

	/**
	 * @return the 'authority' value
	 */
	public final String getAuthority() {
		return this._authority;
	}

	/**
	 * @param authority the 'authority' value to set
	 */
	public final void setAuthority(String authority) {
		this._authority = authority;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((this._3ac == null) ? 0 : this._3ac.hashCode());
		result = prime * result + ((this._authority == null) ? 0 : this._authority.hashCode());
		result = prime * result + ((this._class == null) ? 0 : this._class.hashCode());
		result = prime * result + ((this._family == null) ? 0 : this._family.hashCode());
		result = prime * result + ((this._figisID == null) ? 0 : this._figisID.hashCode());
		result = prime * result + ((this._genus == null) ? 0 : this._genus.hashCode());
		result = prime * result + ((this._kingdom == null) ? 0 : this._kingdom.hashCode());
		result = prime * result + ((this._order == null) ? 0 : this._order.hashCode());
		result = prime * result + ((this._phylum == null) ? 0 : this._phylum.hashCode());
		result = prime * result + ((this._scientificName == null) ? 0 : this._scientificName.hashCode());
		result = prime * result + ((this._score == null) ? 0 : this._score.hashCode());
		result = prime * result + ((this._species == null) ? 0 : this._species.hashCode());
		result = prime * result + ((this._vernacularNames == null) ? 0 : this._vernacularNames.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MatchingReferenceSpeciesData other = (MatchingReferenceSpeciesData) obj;
		if (this._3ac == null) {
			if (other._3ac != null)
				return false;
		} else if (!this._3ac.equals(other._3ac))
			return false;
		if (this._authority == null) {
			if (other._authority != null)
				return false;
		} else if (!this._authority.equals(other._authority))
			return false;
		if (this._class == null) {
			if (other._class != null)
				return false;
		} else if (!this._class.equals(other._class))
			return false;
		if (this._family == null) {
			if (other._family != null)
				return false;
		} else if (!this._family.equals(other._family))
			return false;
		if (this._figisID == null) {
			if (other._figisID != null)
				return false;
		} else if (!this._figisID.equals(other._figisID))
			return false;
		if (this._genus == null) {
			if (other._genus != null)
				return false;
		} else if (!this._genus.equals(other._genus))
			return false;
		if (this._kingdom == null) {
			if (other._kingdom != null)
				return false;
		} else if (!this._kingdom.equals(other._kingdom))
			return false;
		if (this._order == null) {
			if (other._order != null)
				return false;
		} else if (!this._order.equals(other._order))
			return false;
		if (this._phylum == null) {
			if (other._phylum != null)
				return false;
		} else if (!this._phylum.equals(other._phylum))
			return false;
		if (this._scientificName == null) {
			if (other._scientificName != null)
				return false;
		} else if (!this._scientificName.equals(other._scientificName))
			return false;
		if (this._score == null) {
			if (other._score != null)
				return false;
		} else if (!this._score.equals(other._score))
			return false;
		if (this._species == null) {
			if (other._species != null)
				return false;
		} else if (!this._species.equals(other._species))
			return false;
		if (this._vernacularNames == null) {
			if (other._vernacularNames != null)
				return false;
		} else if (!this._vernacularNames.equals(other._vernacularNames))
			return false;
		return true;
	}
}
