/**
 * (c) 2014 FAO / UN (project: comet-asfis-ws)
 */
package org.fao.fi.comet.asfis.support;

import java.util.ArrayList;
import java.util.Collection;

import javax.inject.Singleton;

import org.fao.fi.comet.core.patterns.data.providers.DataProvider;
import org.fao.fi.comet.core.patterns.data.providers.impl.basic.CollectionBackedDataProvider;
import org.fao.fi.comet.domain.species.ReferenceSpeciesFactory;
import org.fao.fi.comet.domain.species.model.ReferenceSpeciesData;
import org.fao.fi.comet.domain.species.model.VernacularNameData;
import org.fao.fi.comet.domain.species.tools.lexical.processors.icu.queue.impl.AuthoritiesSimplifierProcessorQueue;
import org.fao.fi.comet.domain.species.tools.lexical.processors.icu.queue.impl.SpeciesNormalizerProcessorQueue;
import org.fao.fi.comet.domain.species.tools.lexical.processors.icu.queue.impl.SpeciesSimplifierProcessorQueue;
import org.fao.fi.comet.domain.species.tools.lexical.processors.impl.AuthoritiesNormalizerProcessor;
import org.fao.fi.comet.domain.species.tools.parsers.SpeciesNameParser;
import org.fao.fi.comet.domain.species.tools.parsers.impl.SimpleSpeciesNameParser;
import org.fao.vrmf.core.tools.lexical.processors.LexicalProcessor;
import org.fao.vrmf.core.tools.lexical.soundex.PhraseSoundexGenerator;
import org.fao.vrmf.core.tools.lexical.soundex.SoundexGenerator;
import org.fao.vrmf.core.tools.lexical.soundex.impl.BasicPhraseSoundexGenerator;
import org.fao.vrmf.core.tools.lexical.soundex.impl.BasicSoundexGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 17 Sep 2014   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 17 Sep 2014
 */
@Singleton
public class SourceDataInitializer {
	static final private Logger LOG = LoggerFactory.getLogger(SourceDataInitializer.class);
	static final private SpeciesNameParser PARSER = new SimpleSpeciesNameParser();
	
	static final private LexicalProcessor SPECIES_SIMPLIFIER = new SpeciesSimplifierProcessorQueue();
	static final private LexicalProcessor SPECIES_NORMALIZER = new SpeciesNormalizerProcessorQueue();
	static final private LexicalProcessor AUTHORITIES_SIMPLIFIER = new AuthoritiesSimplifierProcessorQueue();
	static final private LexicalProcessor AUTHORITIES_NORMALIZER = new AuthoritiesNormalizerProcessor();
	
	static final private PhraseSoundexGenerator PHRASE_SOUNDEXER = new BasicPhraseSoundexGenerator();
	static final private SoundexGenerator SOUNDEXER = new BasicSoundexGenerator();

	public DataProvider<ReferenceSpeciesData> convert(String[] inputs) {
		Collection<ReferenceSpeciesData> data = new ArrayList<ReferenceSpeciesData>();

		VernacularNameData vernacular;
		for(String input : inputs) {
			for(ReferenceSpeciesData in : PARSER.parse(new String[] { input })) {
				LOG.info("Input '{}' has been parsed as: {} ({})", input, in.getScientificName(), in.getAuthor());
				
				in.setDataSource("USER_INPUT");
				in.setId(input);
				
				vernacular = new VernacularNameData();
				vernacular.setName(input);
				vernacular.setLanguage("UNKNOWN");
				vernacular.setParentId(in.getId());
				
				in.setVernacularNames(new VernacularNameData[] { vernacular });
				
				in = ReferenceSpeciesFactory.updateComplexNames(in, 
																SPECIES_SIMPLIFIER,
																SPECIES_NORMALIZER,
																AUTHORITIES_SIMPLIFIER,
																AUTHORITIES_NORMALIZER,
																PHRASE_SOUNDEXER,
																SOUNDEXER);
				
				in = ReferenceSpeciesFactory.updateComplexVernacularNames(in,
																		  SPECIES_SIMPLIFIER,
																		  SPECIES_NORMALIZER,
																		  AUTHORITIES_SIMPLIFIER,
																		  AUTHORITIES_NORMALIZER,
																		  PHRASE_SOUNDEXER,
																		  SOUNDEXER);
				
				data.add(in);
			}
		}
		
		return new CollectionBackedDataProvider<ReferenceSpeciesData>("USER_INPUT", data);
	}
}