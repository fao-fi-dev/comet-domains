/**
 * (c) 2014 FAO / UN (project: comet-asfis-ws)
 */
package org.fao.fi.comet.asfis.support.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import org.fao.fi.comet.core.model.engine.Matching;
import org.fao.fi.comet.core.model.engine.MatchingDetails;
import org.fao.fi.comet.domain.species.model.ReferenceSpeciesData;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 18 Sep 2014   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 18 Sep 2014
 */
@XmlRootElement(name="SearchResponse")
@XmlAccessorType(XmlAccessType.FIELD)
public class VernacularNameSearchResponse extends CommonSearchResponse<VernacularNameMatchingResults> {
	@XmlAttribute(name="language")
	private String _language;

	/**
	 * Class constructor
	 */
	public VernacularNameSearchResponse() {
		super();
	}

	/**
	 * Class constructor
	 *
	 * @param requestId
	 * @param searchType
	 * @param minScore
	 * @param maxCandidates
	 * @param language
	 * @param searchTerm
	 * @param elapsed
	 * @param data
	 */
	public VernacularNameSearchResponse(String requestId, String searchType, double minScore, int maxCandidates, String language, long elapsed, Collection<MatchingDetails<ReferenceSpeciesData, ReferenceSpeciesData>> data) {
		super(requestId, searchType, minScore, maxCandidates, elapsed, data);
		
		this._language = language;
	}

	/**
	 * @return the 'language' value
	 */
	public final String getLanguage() {
		return this._language;
	}

	/**
	 * @param language the 'language' value to set
	 */
	public final void setLanguage(String language) {
		this._language = language;
	}
	
	/* (non-Javadoc)
	 * @see org.fao.fi.comet.asfis.support.model.CommonSearchResponse#convert(org.fao.fi.comet.core.model.engine.MatchingDetails, java.util.Collection)
	 */
	@Override
	protected VernacularNameMatchingResults convert(MatchingDetails<ReferenceSpeciesData, ReferenceSpeciesData> source, Collection<Matching<ReferenceSpeciesData, ReferenceSpeciesData>> targets) {
		VernacularNameMatchingResults results = new VernacularNameMatchingResults();

		ReferenceSpeciesData sourceData = (ReferenceSpeciesData)source.getSource();
		
		results.setSearchTerm(sourceData.getGenericCName().getName());
		
		List<MatchingReferenceSpeciesData> matchings = new ArrayList<MatchingReferenceSpeciesData>();
		
		for(Matching<ReferenceSpeciesData, ReferenceSpeciesData> in : targets)
			matchings.add(
				new MatchingReferenceSpeciesData(
					(ReferenceSpeciesData)in.getTarget(),
					null, //The FIGIS ID must be set later...
				    in.getScoreValue()
				)
			);
		
		results.setMatchings(matchings);
		
		return results;
	}
}
