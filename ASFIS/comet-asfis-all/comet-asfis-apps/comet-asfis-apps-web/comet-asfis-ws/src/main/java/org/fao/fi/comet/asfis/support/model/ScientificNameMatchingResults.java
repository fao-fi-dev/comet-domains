/**
 * (c) 2014 FAO / UN (project: comet-asfis-ws)
 */
package org.fao.fi.comet.asfis.support.model;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 22 Sep 2014   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 22 Sep 2014
 */
@XmlAccessorType(XmlAccessType.FIELD)
public class ScientificNameMatchingResults extends MatchingResults {
	@XmlAttribute(name="parsedTerm")
	private String _parsedTerm;

	/**
	 * Class constructor
	 *
	 */
	public ScientificNameMatchingResults() {
		super();
	}

	/**
	 * Class constructor
	 *
	 * @param searchTerm
	 * @param matchings
	 * @param parsedTerm
	 */
	public ScientificNameMatchingResults(String searchTerm, List<MatchingReferenceSpeciesData> matchings, String parsedTerm) {
		super(searchTerm, matchings);
		
		this._parsedTerm = parsedTerm;
	}

	/**
	 * @return the 'parsedTerm' value
	 */
	public final String getParsedTerm() {
		return this._parsedTerm;
	}

	/**
	 * @param parsedTerm the 'parsedTerm' value to set
	 */
	public final void setParsedTerm(String parsedTerm) {
		this._parsedTerm = parsedTerm;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((this._parsedTerm == null) ? 0 : this._parsedTerm.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		ScientificNameMatchingResults other = (ScientificNameMatchingResults) obj;
		if (this._parsedTerm == null) {
			if (other._parsedTerm != null)
				return false;
		} else if (!this._parsedTerm.equals(other._parsedTerm))
			return false;
		return true;
	}
}