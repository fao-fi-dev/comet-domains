/**
 * (c) 2014 FAO / UN (project: reports-store-gateway-web-base)
 */
package org.fao.fi.comet.asfis.ws.config;

import org.fao.fi.comet.asfis.support.JAXBContextProvider;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.server.ResourceConfig;

/**
 * Place your class / interface description here.
 *
 * History:
 *
 * ------------- --------------- -----------------------
 * Date			 Author			 Comment
 * ------------- --------------- -----------------------
 * 30 Apr 2014   Fiorellato     Creation.
 *
 * @version 1.0
 * @since 30 Apr 2014
 */
public class ASFISSearchWebServicesConfiguration extends ResourceConfig {
	/**
	 * Class constructor
	 */
	public ASFISSearchWebServicesConfiguration() {
		register(JacksonFeature.class);
		register(JAXBContextProvider.class);
	}
}
